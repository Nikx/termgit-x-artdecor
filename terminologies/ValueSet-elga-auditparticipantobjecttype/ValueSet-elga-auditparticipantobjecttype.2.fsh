Instance: elga-auditparticipantobjecttype 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-auditparticipantobjecttype" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-auditparticipantobjecttype" 
* name = "elga-auditparticipantobjecttype" 
* title = "ELGA_AuditParticipantObjectType" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "Codeliste der ELGA spezifischen Audit Participant Object Type Codes. Der Audit Participant Object Type beschreibt die Art eines Objekts, welches im Audit Event referenziert ist." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.154" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.153"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype"
* compose.include[0].version = "1.0.1+20240129"
* compose.include[0].concept[0].code = "100"
* compose.include[0].concept[0].display = "Policy"
* compose.include[0].concept[1].code = "110"
* compose.include[0].concept[1].display = "Kontaktbestätigung"
* compose.include[0].concept[2].code = "120"
* compose.include[0].concept[2].display = "Security Token"
* compose.include[0].concept[3].code = "130"
* compose.include[0].concept[3].display = "Vollmacht"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype"
* expansion.contains[0].version = "1.0.1+20240129"
* expansion.contains[0].code = #100
* expansion.contains[0].display = "Policy"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.153"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype"
* expansion.contains[1].version = "1.0.1+20240129"
* expansion.contains[1].code = #110
* expansion.contains[1].display = "Kontaktbestätigung"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.153"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype"
* expansion.contains[2].version = "1.0.1+20240129"
* expansion.contains[2].code = #120
* expansion.contains[2].display = "Security Token"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.153"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype"
* expansion.contains[3].version = "1.0.1+20240129"
* expansion.contains[3].code = #130
* expansion.contains[3].display = "Vollmacht"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.153"
