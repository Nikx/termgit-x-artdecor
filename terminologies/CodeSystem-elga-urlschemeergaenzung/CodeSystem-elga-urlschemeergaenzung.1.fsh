Instance: elga-urlschemeergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-urlschemeergaenzung" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-urlschemeergaenzung" 
* name = "elga-urlschemeergaenzung" 
* title = "ELGA_URLSchemeErgaenzung" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "**Description:** Set of codes describing the Universal Resource Locator which are not included in the corresponding HL7 codelist

**Beschreibung:** Enthält Codes zur Beschreibung von Universal Resource Locators (System zur Beschreibung der Lage der Ressourcen), die nicht in der korrespondierenden HL7 Codeliste enthalten waren" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.55" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 2 
* concept[0].code = #https
* concept[0].display = "HTTPS"
* concept[1].code = #me
* concept[1].display = "ME-Nummer"
