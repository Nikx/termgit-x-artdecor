Instance: lkf-diagnose-typ 
InstanceOf: ValueSet 
Usage: #definition 
* id = "lkf-diagnose-typ" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/lkf-diagnose-typ" 
* name = "lkf-diagnose-typ" 
* title = "LKF_Diagnose-Typ" 
* status = #active 
* version = "1.1.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.69" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.206"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-typ"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #H
* compose.include[0].concept[0].display = "Hauptdiagnose"
* compose.include[0].concept[1].code = #Z
* compose.include[0].concept[1].display = "Zusatzdiagnose"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-typ"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #H
* expansion.contains[0].display = "Hauptdiagnose"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.206"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-typ"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #Z
* expansion.contains[1].display = "Zusatzdiagnose"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.206"
