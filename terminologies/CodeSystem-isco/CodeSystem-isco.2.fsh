Instance: isco 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "isco" 
* url = "https://termgit.elga.gv.at/CodeSystem/isco" 
* name = "isco" 
* title = "ISCO" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230505" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.2.9.6.2.7" 
* date = "2023-05-05" 
* count = 39 
* #22 "Health professionals"
* #221 "Medical doctors"
* #2211 "Generalist medical practitioners"
* #2212 "Specialist medical practitioners"
* #222 "Nursing and midwifery professionals"
* #2221 "Nursing professionals"
* #2222 "Midwifery professionals"
* #223 "Traditional and complementary medicine professionals"
* #224 "Paramedical practitioners"
* #225 "Veterinarians"
* #226 "Other health professionals"
* #2261 "Dentists"
* #2262 "Pharmacists"
* #2263 "Environmental and occupational health and hygiene professionals"
* #2264 "Physiotherapists"
* #2265 "Dieticians and nutritionists"
* #2266 "Audiologists and speech therapists"
* #2267 "Optometrists and ophthalmic opticians"
* #2269 "Health professionals not elsewhere classified"
* #32 "Health associate professionals"
* #321 "Medical and pharmaceutical technicians"
* #3211 "Medical imaging and therapeutic equipment technicians"
* #3212 "Medical and pathology laboratory technicians"
* #3213 "Pharmaceutical technicians and assistants"
* #3214 "Medical and dental prosthetic technicians"
* #322 "Nursing and midwifery associate professionals"
* #3221 "Nursing associate professionals"
* #3222 "Midwifery associate professionals"
* #323 "Traditional and complementary medicine associate professionals"
* #325 "Other health associate professionals"
* #3251 "Dental assistants and therapists"
* #3252 "Medical records and health information technicians"
* #3253 "Community health workers"
* #3254 "Dispensing opticians"
* #3255 "Physiotherapy technicians and assistants"
* #3256 "Medical assistants"
* #3257 "Environmental and occupational health inspectors and associates"
* #3258 "Ambulance workers"
* #3259 "Health associate professionals not elsewhere classified"
