Instance: lkf-diagnose-statauf 
InstanceOf: ValueSet 
Usage: #definition 
* id = "lkf-diagnose-statauf" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/lkf-diagnose-statauf" 
* name = "lkf-diagnose-statauf" 
* title = "LKF_Diagnose-statAuf" 
* status = #active 
* version = "1.1.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.68" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.208"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-statauf"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = "J"
* compose.include[0].concept[0].display = "Ja"
* compose.include[0].concept[1].code = "N"
* compose.include[0].concept[1].display = "Nein"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-statauf"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #J
* expansion.contains[0].display = "Ja"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.208"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/lkf-diagnose-statauf"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #N
* expansion.contains[1].display = "Nein"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.208"
