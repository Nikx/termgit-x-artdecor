Instance: elga-participationfunctioncode 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-participationfunctioncode" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-participationfunctioncode" 
* name = "elga-participationfunctioncode" 
* title = "ELGA_ParticipationFunctionCode" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "**Description:** Set of valid type codes for participants (admitting physician, primary care physician...)

**Beschreibung:** Erlaubte TypeCodes für Participants (Einweiser, Hausarzt). " 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.15" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "http://www.hl7.org" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.88"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-ParticipationFunction"
* compose.include[0].version = "2.1.0"
* compose.include[0].concept[0].code = #PCP
* compose.include[0].concept[0].display = "primary care physician"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Hausärztin/Hausarzt" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-ParticipationFunction"
* expansion.contains[0].version = "2.1.0"
* expansion.contains[0].code = #PCP
* expansion.contains[0].display = "primary care physician"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Hausärztin/Hausarzt" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.88"
