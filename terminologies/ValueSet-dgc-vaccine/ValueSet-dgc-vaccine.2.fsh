Instance: dgc-vaccine 
InstanceOf: ValueSet 
Usage: #definition 
* id = "dgc-vaccine" 
* url = "https://termgit.elga.gv.at/ValueSet/dgc-vaccine" 
* name = "dgc-vaccine" 
* title = "DGC_Vaccine" 
* status = #retired 
* version = "1.2.0+20230417" 
* description = "To be used in certificate 2" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.56" 
* date = "2023-04-17" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/atc-deutsch-wido"
* compose.include[0].version = "ATC deutsch (WIdO)_202205"
* compose.include[0].concept[0].code = "J07BX03"
* compose.include[0].concept[0].display = "Covid-19-Impfstoffe"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Covid-19 Vakzine" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "covid-19 vaccines" 
* compose.include[0].concept[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#relationships "relationships" 
* compose.include[0].concept[0].designation[2].value = "2.16.840.1.113883.6.73:J07BX03" 
* compose.include[0].concept[0].designation[3].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[3].value = "DEPRECATED" 
* compose.include[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* compose.include[1].version = "1.2.0+20230308"
* compose.include[1].concept[0].code = "28531000087107"
* compose.include[1].concept[0].display = "COVID-19 vaccine"
* compose.include[1].concept[0].designation[0].language = #de-AT 
* compose.include[1].concept[0].designation[0].value = "SARS-CoV-2-Impfstoff" 

* expansion.timestamp = "2023-11-08T16:49:11.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/atc-deutsch-wido"
* expansion.contains[0].version = "ATC deutsch (WIdO)_202205"
* expansion.contains[0].code = #J07BX03
* expansion.contains[0].display = "Covid-19-Impfstoffe"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Covid-19 Vakzine" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[1].value = "covid-19 vaccines" 
* expansion.contains[0].designation[2].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#relationships "relationships" 
* expansion.contains[0].designation[2].value = "2.16.840.1.113883.6.73:J07BX03" 
* expansion.contains[0].designation[3].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[3].value = "DEPRECATED" 
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].version = "1.2.0+20230308"
* expansion.contains[1].code = #28531000087107
* expansion.contains[1].display = "COVID-19 vaccine"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "SARS-CoV-2-Impfstoff" 
