Instance: eimpf-zusatzklassifikation 
InstanceOf: ValueSet 
Usage: #definition 
* id = "eimpf-zusatzklassifikation" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/eimpf-zusatzklassifikation" 
* name = "eimpf-zusatzklassifikation" 
* title = "eImpf_Zusatzklassifikation" 
* status = #active 
* version = "3.2.0+20240325" 
* description = "**Description:** Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.62" 
* date = "2024-03-25" 
* compose.inactive = "True" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* compose.include[0].version = "1.3.0+20230803"
* compose.include[0].concept[0].code = "46224007"
* compose.include[0].concept[0].display = "Impfstelle (Impfsetting)"
* compose.include[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* compose.include[1].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* compose.include[1].version = "2.14.0+20240201"
* compose.include[1].concept[0].code = "IS001"
* compose.include[1].concept[0].display = "Bildungseinrichtung"
* compose.include[1].concept[1].code = "IS002"
* compose.include[1].concept[1].display = "Arbeitsplatz/Betriebe"
* compose.include[1].concept[2].code = "IS003"
* compose.include[1].concept[2].display = "Krankenhaus inkl. Kur- und Rehaeinrichtungen"
* compose.include[1].concept[3].code = "IS004"
* compose.include[1].concept[3].display = "Ordination"
* compose.include[1].concept[4].code = "IS005"
* compose.include[1].concept[4].display = "Öffentliche Impfstelle"
* compose.include[1].concept[5].code = "IS006"
* compose.include[1].concept[5].display = "Wohnbereich"
* compose.include[1].concept[6].code = "IS007"
* compose.include[1].concept[6].display = "Öffentliche Impfstraße / Impfbus"
* compose.include[1].concept[7].code = "IS008"
* compose.include[1].concept[7].display = "Betreute Wohneinrichtung"
* compose.include[1].concept[8].code = "IS009"
* compose.include[1].concept[8].display = "Impfprogramm"
* compose.include[1].concept[9].code = "IS010"
* compose.include[1].concept[9].display = "Kostenfreies Impfprogramm"
* compose.include[1].concept[10].code = "IS011"
* compose.include[1].concept[10].display = "Öffentliches Impfprogramm (ÖIP) Influenza"
* compose.include[1].concept[10].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[1].concept[10].designation[0].value = "DEPRECATED" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[0].version = "1.3.0+20230803"
* expansion.contains[0].code = #46224007
* expansion.contains[0].display = "Impfstelle (Impfsetting)"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[0].contains[0].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[0].version = "2.14.0+20240201"
* expansion.contains[0].contains[0].code = #IS001
* expansion.contains[0].contains[0].display = "Bildungseinrichtung"
* expansion.contains[0].contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[0].contains[1].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[1].version = "2.14.0+20240201"
* expansion.contains[0].contains[1].code = #IS002
* expansion.contains[0].contains[1].display = "Arbeitsplatz/Betriebe"
* expansion.contains[0].contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[0].contains[2].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[2].version = "2.14.0+20240201"
* expansion.contains[0].contains[2].code = #IS003
* expansion.contains[0].contains[2].display = "Krankenhaus inkl. Kur- und Rehaeinrichtungen"
* expansion.contains[0].contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[0].contains[3].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[3].version = "2.14.0+20240201"
* expansion.contains[0].contains[3].code = #IS004
* expansion.contains[0].contains[3].display = "Ordination"
* expansion.contains[0].contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[3].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[0].contains[4].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[4].version = "2.14.0+20240201"
* expansion.contains[0].contains[4].code = #IS005
* expansion.contains[0].contains[4].display = "Öffentliche Impfstelle"
* expansion.contains[0].contains[4].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[4].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[0].contains[5].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[5].version = "2.14.0+20240201"
* expansion.contains[0].contains[5].code = #IS006
* expansion.contains[0].contains[5].display = "Wohnbereich"
* expansion.contains[0].contains[5].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[5].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[0].contains[6].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[6].version = "2.14.0+20240201"
* expansion.contains[0].contains[6].code = #IS007
* expansion.contains[0].contains[6].display = "Öffentliche Impfstraße / Impfbus"
* expansion.contains[0].contains[6].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[6].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[0].contains[7].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].contains[7].version = "2.14.0+20240201"
* expansion.contains[0].contains[7].code = #IS008
* expansion.contains[0].contains[7].display = "Betreute Wohneinrichtung"
* expansion.contains[0].contains[7].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[7].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[1].version = "2.14.0+20240201"
* expansion.contains[1].code = #IS009
* expansion.contains[1].display = "Impfprogramm"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[1].contains[0].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[1].contains[0].version = "2.14.0+20240201"
* expansion.contains[1].contains[0].code = #IS010
* expansion.contains[1].contains[0].display = "Kostenfreies Impfprogramm"
* expansion.contains[1].contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[1].contains[1].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[1].contains[1].version = "2.14.0+20240201"
* expansion.contains[1].contains[1].inactive = true
* expansion.contains[1].contains[1].code = #IS011
* expansion.contains[1].contains[1].display = "Öffentliches Impfprogramm (ÖIP) Influenza"
* expansion.contains[1].contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].contains[1].designation[0].value = "DEPRECATED" 
* expansion.contains[1].contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
