Instance: elga-rollen 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-rollen" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-rollen" 
* name = "elga-rollen" 
* title = "ELGA_Rollen" 
* status = #active 
* version = "2.1.0+20240325" 
* description = "**Description:** Definition of the roles for the ELGA Access Control system using nomenclature defined by the 'Healthcare-Provider Rollen' in Austria

**Beschreibung:** Definition der Rollen für das ELGA-Berechtigungssystem. Verweist auf die Nomenklatur der Healthcare-Provider Rollen in Österreich." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.26" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "https://www.bmg.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.bmg.gv.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.158"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen"
* compose.include[0].version = "1.0.1+20240129"
* compose.include[0].concept[0].code = #607
* compose.include[0].concept[0].display = "ELGA-Widerspruchstelle"
* compose.include[0].concept[1].code = #608
* compose.include[0].concept[1].display = "ELGA-Regelwerkadministration"
* compose.include[0].concept[2].code = #609
* compose.include[0].concept[2].display = "ELGA-Sicherheitsadministration"
* compose.include[0].concept[3].code = #610
* compose.include[0].concept[3].display = "ELGA-Teilnehmerin/Teilnehmer"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[3].designation[0].value = "Bürgerin/Bürger" 
* compose.include[0].concept[4].code = #611
* compose.include[0].concept[4].display = "ELGA-Vollmachtnehmende Person"
* compose.include[0].concept[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[4].designation[0].value = "Vertretende Person einer ELGA-Teilnehmerin/ Teilnehmer (natürliche Person)" 
* compose.include[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* compose.include[1].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* compose.include[1].version = "2.0.1+20240129"
* compose.include[1].concept[0].code = #700
* compose.include[1].concept[0].display = "Ärztin/Arzt"
* compose.include[1].concept[1].code = #701
* compose.include[1].concept[1].display = "Zahnärztin/Zahnarzt"
* compose.include[1].concept[2].code = #702
* compose.include[1].concept[2].display = "Krankenanstalt"
* compose.include[1].concept[3].code = #703
* compose.include[1].concept[3].display = "Einrichtung der Pflege"
* compose.include[1].concept[4].code = #704
* compose.include[1].concept[4].display = "Apotheke"
* compose.include[1].concept[5].code = #705
* compose.include[1].concept[5].display = "ELGA-Beratung"
* compose.include[1].concept[6].code = #706
* compose.include[1].concept[6].display = "ELGA-Ombudsstelle"
* compose.include[1].concept[7].code = #716
* compose.include[1].concept[7].display = "Amtsärztin/Amtsarzt"
* compose.include[1].concept[8].code = #717
* compose.include[1].concept[8].display = "Korrekturberechtigte Person"
* compose.include[1].concept[9].code = #718
* compose.include[1].concept[9].display = "Krisenmanagerin/Krisenmanager"
* compose.include[1].concept[10].code = #719
* compose.include[1].concept[10].display = "Auswertungsberechtigte Person"
* compose.include[1].concept[11].code = #720
* compose.include[1].concept[11].display = "Verrechnungsberechtigte Person"
* compose.include[1].concept[12].code = #721
* compose.include[1].concept[12].display = "Arbeitsmedizin"
* compose.include[1].concept[13].code = #722
* compose.include[1].concept[13].display = "Hebamme"
* compose.include[1].concept[14].code = #723
* compose.include[1].concept[14].display = "Straf- und Maßnahmenvollzug"
* compose.include[1].concept[15].code = #724
* compose.include[1].concept[15].display = "Labor und Pathologie"
* compose.include[1].concept[16].code = #725
* compose.include[1].concept[16].display = "EMS/EPI-Service"
* compose.include[1].concept[17].code = #726
* compose.include[1].concept[17].display = "Diplomierte Gesundheits- und Krankenpflegerin/Diplomierter Gesundheits- und Krankenpfleger"
* compose.include[1].concept[18].code = #727
* compose.include[1].concept[18].display = "NCPeH cross-border services"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen"
* expansion.contains[0].version = "1.0.1+20240129"
* expansion.contains[0].code = #607
* expansion.contains[0].display = "ELGA-Widerspruchstelle"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.158"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen"
* expansion.contains[1].version = "1.0.1+20240129"
* expansion.contains[1].code = #608
* expansion.contains[1].display = "ELGA-Regelwerkadministration"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.158"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen"
* expansion.contains[2].version = "1.0.1+20240129"
* expansion.contains[2].code = #609
* expansion.contains[2].display = "ELGA-Sicherheitsadministration"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.158"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen"
* expansion.contains[3].version = "1.0.1+20240129"
* expansion.contains[3].code = #610
* expansion.contains[3].display = "ELGA-Teilnehmerin/Teilnehmer"
* expansion.contains[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[3].designation[0].value = "Bürgerin/Bürger" 
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.158"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem/elga-funktionsrollen"
* expansion.contains[4].version = "1.0.1+20240129"
* expansion.contains[4].code = #611
* expansion.contains[4].display = "ELGA-Vollmachtnehmende Person"
* expansion.contains[4].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[4].designation[0].value = "Vertretende Person einer ELGA-Teilnehmerin/ Teilnehmer (natürliche Person)" 
* expansion.contains[4].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[4].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.158"
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[5].version = "2.0.1+20240129"
* expansion.contains[5].code = #700
* expansion.contains[5].display = "Ärztin/Arzt"
* expansion.contains[5].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[5].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[6].version = "2.0.1+20240129"
* expansion.contains[6].code = #701
* expansion.contains[6].display = "Zahnärztin/Zahnarzt"
* expansion.contains[6].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[6].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[7].version = "2.0.1+20240129"
* expansion.contains[7].code = #702
* expansion.contains[7].display = "Krankenanstalt"
* expansion.contains[7].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[7].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[8].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[8].version = "2.0.1+20240129"
* expansion.contains[8].code = #703
* expansion.contains[8].display = "Einrichtung der Pflege"
* expansion.contains[8].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[8].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[9].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[9].version = "2.0.1+20240129"
* expansion.contains[9].code = #704
* expansion.contains[9].display = "Apotheke"
* expansion.contains[9].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[9].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[10].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[10].version = "2.0.1+20240129"
* expansion.contains[10].code = #705
* expansion.contains[10].display = "ELGA-Beratung"
* expansion.contains[10].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[10].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[11].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[11].version = "2.0.1+20240129"
* expansion.contains[11].code = #706
* expansion.contains[11].display = "ELGA-Ombudsstelle"
* expansion.contains[11].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[11].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[12].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[12].version = "2.0.1+20240129"
* expansion.contains[12].code = #716
* expansion.contains[12].display = "Amtsärztin/Amtsarzt"
* expansion.contains[12].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[12].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[13].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[13].version = "2.0.1+20240129"
* expansion.contains[13].code = #717
* expansion.contains[13].display = "Korrekturberechtigte Person"
* expansion.contains[13].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[13].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[14].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[14].version = "2.0.1+20240129"
* expansion.contains[14].code = #718
* expansion.contains[14].display = "Krisenmanagerin/Krisenmanager"
* expansion.contains[14].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[14].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[15].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[15].version = "2.0.1+20240129"
* expansion.contains[15].code = #719
* expansion.contains[15].display = "Auswertungsberechtigte Person"
* expansion.contains[15].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[15].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[16].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[16].version = "2.0.1+20240129"
* expansion.contains[16].code = #720
* expansion.contains[16].display = "Verrechnungsberechtigte Person"
* expansion.contains[16].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[16].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[17].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[17].version = "2.0.1+20240129"
* expansion.contains[17].code = #721
* expansion.contains[17].display = "Arbeitsmedizin"
* expansion.contains[17].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[17].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[18].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[18].version = "2.0.1+20240129"
* expansion.contains[18].code = #722
* expansion.contains[18].display = "Hebamme"
* expansion.contains[18].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[18].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[19].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[19].version = "2.0.1+20240129"
* expansion.contains[19].code = #723
* expansion.contains[19].display = "Straf- und Maßnahmenvollzug"
* expansion.contains[19].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[19].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[20].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[20].version = "2.0.1+20240129"
* expansion.contains[20].code = #724
* expansion.contains[20].display = "Labor und Pathologie"
* expansion.contains[20].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[20].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[21].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[21].version = "2.0.1+20240129"
* expansion.contains[21].code = #725
* expansion.contains[21].display = "EMS/EPI-Service"
* expansion.contains[21].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[21].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[22].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[22].version = "2.0.1+20240129"
* expansion.contains[22].code = #726
* expansion.contains[22].display = "Diplomierte Gesundheits- und Krankenpflegerin/Diplomierter Gesundheits- und Krankenpfleger"
* expansion.contains[22].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[22].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
* expansion.contains[23].system = "https://termgit.elga.gv.at/CodeSystem/elga-gda-aggregatrollen"
* expansion.contains[23].version = "2.0.1+20240129"
* expansion.contains[23].code = #727
* expansion.contains[23].display = "NCPeH cross-border services"
* expansion.contains[23].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[23].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.3"
