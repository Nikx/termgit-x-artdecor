Instance: hl7-at-administrativegender-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "hl7-at-administrativegender-ergaenzung" 
* url = "https://termgit.elga.gv.at/CodeSystem/hl7-at-administrativegender-ergaenzung" 
* name = "hl7-at-administrativegender-ergaenzung" 
* title = "HL7-at_AdministrativeGender-Ergaenzung" 
* status = #active 
* content = #complete 
* version = "1.0.0+20231113" 
* description = "Österreich-spezifische Codes, die für die weitere Untergliederung des dritten Geschlechts dokumentiert werden können." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.224" 
* date = "2023-11-13" 
* publisher = "see" 
* contact[0].name = "HL7 Austria" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.hl7.at" 
* count = 3 
* #D "Divers"
* #I "Inter"
* #O "Offen"
