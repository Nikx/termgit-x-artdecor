Instance: elga-expecteddeliverydatemethod 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-expecteddeliverydatemethod" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-expecteddeliverydatemethod" 
* name = "elga-expecteddeliverydatemethod" 
* title = "ELGA_ExpectedDeliveryDateMethod" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "Berechnungsmethode Geburtstermin" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.202" 
* date = "2024-03-25" 
* publisher = "ELGA GmbH" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
* compose.include[0].system = "http://loinc.org"
* compose.include[0].version = "2.73"
* compose.include[0].concept[0].code = #11778-8
* compose.include[0].concept[0].display = "Delivery date Estimated"
* compose.include[0].concept[1].code = #11779-6
* compose.include[0].concept[1].display = "Delivery date Estimated from last menstrual period"
* compose.include[0].concept[2].code = #11780-4
* compose.include[0].concept[2].display = "Delivery date Estimated from ovulation date"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://loinc.org"
* expansion.contains[0].version = "2.73"
* expansion.contains[0].code = #11778-8
* expansion.contains[0].display = "Delivery date Estimated"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
* expansion.contains[1].system = "http://loinc.org"
* expansion.contains[1].version = "2.73"
* expansion.contains[1].code = #11779-6
* expansion.contains[1].display = "Delivery date Estimated from last menstrual period"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
* expansion.contains[2].system = "http://loinc.org"
* expansion.contains[2].version = "2.73"
* expansion.contains[2].code = #11780-4
* expansion.contains[2].display = "Delivery date Estimated from ovulation date"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
