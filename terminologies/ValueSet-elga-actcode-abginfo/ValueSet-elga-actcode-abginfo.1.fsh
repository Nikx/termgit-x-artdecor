Instance: elga-actcode-abginfo 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-actcode-abginfo" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-actcode-abginfo" 
* name = "elga-actcode-abginfo" 
* title = "ELGA_ActCode_AbgInfo" 
* status = #active 
* version = "1.1.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.160" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.103"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-actcode"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #ERGINFO
* compose.include[0].concept[0].display = "Ergänzende Informationen zur Abgabe"
* compose.include[0].concept[1].code = #MAGZUB
* compose.include[0].concept[1].display = "Ergänzende Informationen zur magistralen Zubereitung"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-actcode"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #ERGINFO
* expansion.contains[0].display = "Ergänzende Informationen zur Abgabe"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.103"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/elga-actcode"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #MAGZUB
* expansion.contains[1].display = "Ergänzende Informationen zur magistralen Zubereitung"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.103"
