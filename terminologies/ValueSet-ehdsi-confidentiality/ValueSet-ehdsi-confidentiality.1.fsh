Instance: ehdsi-confidentiality 
InstanceOf: ValueSet 
Usage: #definition 
* id = "ehdsi-confidentiality" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/ehdsi-confidentiality" 
* name = "ehdsi-confidentiality" 
* title = "EHDSI_Confidentiality" 
* status = #active 
* version = "1.1.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.3.6.1.4.1.12559.11.10.1.3.1.42.31" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* compose.include[0].version = "2.2.0"
* compose.include[0].concept[0].code = #N
* compose.include[0].concept[0].display = "normal"
* compose.include[0].concept[1].code = #R
* compose.include[0].concept[1].display = "restricted"
* compose.include[0].concept[2].code = #V
* compose.include[0].concept[2].display = "very restricted"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[0].version = "2.2.0"
* expansion.contains[0].code = #N
* expansion.contains[0].display = "normal"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[1].version = "2.2.0"
* expansion.contains[1].code = #R
* expansion.contains[1].display = "restricted"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/v3-Confidentiality"
* expansion.contains[2].version = "2.2.0"
* expansion.contains[2].code = #V
* expansion.contains[2].display = "very restricted"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.25"
