Instance: elga-languageabilitymode 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-languageabilitymode" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-languageabilitymode" 
* name = "elga-languageabilitymode" 
* title = "ELGA_LanguageAbilityMode" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "**Description:** List of codes representing the method of expression of the language

**Beschreibung:** Codes zur Angabe der Sprachfähigkeit von Patienten (Methode zum Ausdruck von Spache)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.175" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "http://www.hl7.org/documentcenter/public_temp_ED0BB625-1C23-BA17-0CE4EA7EB5B49D1F/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityMode.html" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org/documentcenter/public_temp_ED0BB625-1C23-BA17-0CE4EA7EB5B49D1F/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityMode.html" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.60"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityMode"
* compose.include[0].version = "2.1.0"
* compose.include[0].concept[0].code = #ESP
* compose.include[0].concept[0].display = "Expressed spoken"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "spricht" 
* compose.include[0].concept[1].code = #EWR
* compose.include[0].concept[1].display = "Expressed written"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "schreibt" 
* compose.include[0].concept[2].code = #RSP
* compose.include[0].concept[2].display = "Received spoken"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "versteht gesprochen" 
* compose.include[0].concept[3].code = #RWR
* compose.include[0].concept[3].display = "Received written"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "versteht geschrieben" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityMode"
* expansion.contains[0].version = "2.1.0"
* expansion.contains[0].code = #ESP
* expansion.contains[0].display = "Expressed spoken"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "spricht" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.60"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityMode"
* expansion.contains[1].version = "2.1.0"
* expansion.contains[1].code = #EWR
* expansion.contains[1].display = "Expressed written"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "schreibt" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.60"
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityMode"
* expansion.contains[2].version = "2.1.0"
* expansion.contains[2].code = #RSP
* expansion.contains[2].display = "Received spoken"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "versteht gesprochen" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.60"
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityMode"
* expansion.contains[3].version = "2.1.0"
* expansion.contains[3].code = #RWR
* expansion.contains[3].display = "Received written"
* expansion.contains[3].designation[0].language = #de-AT 
* expansion.contains[3].designation[0].value = "versteht geschrieben" 
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.60"
