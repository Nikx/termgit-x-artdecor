Instance: austrian-designation-use 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "austrian-designation-use" 
* url = "https://termgit.elga.gv.at/CodeSystem/austrian-designation-use" 
* name = "austrian-designation-use" 
* title = "Austrian Designation Use" 
* status = #active 
* content = #complete 
* version = "2.1.0+20240402" 
* description = "Mögliche Designations für den österreichischen eHealth-Bereich" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.217" 
* date = "2024-04-02" 
* publisher = "ELGA GmbH" 
* count = 8 
* #alt "alt"
* #alt ^definition = Alternativer oder vergangener Displayname
* #alterslimit_max "alterslimit_max"
* #alterslimit_max ^definition = Maximalalter in Monaten für kostenfreies Impfprogramm
* #alterslimit_min "alterslimit_min"
* #alterslimit_min ^definition = Mindestalter in Monaten für kostenfreies Impfprogramm
* #concept_beschreibung "concept_beschreibung"
* #concept_beschreibung ^definition = Weiterführende Beschreibung
* #einheit_codiert "einheit_codiert"
* #einheit_codiert ^definition = Wird nur im Laborbereich genutzt
* #einheit_print "einheit_print"
* #einheit_print ^definition = Wird nur im Laborbereich genutzt
* #hinweise "hinweise"
* #hinweise ^definition = Spezielle Hinweise
* #relationships "relationships"
* #relationships ^definition = Mapping zu anderen Konzepten
