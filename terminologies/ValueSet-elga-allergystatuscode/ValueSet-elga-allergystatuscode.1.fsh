Instance: elga-allergystatuscode 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-allergystatuscode" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-allergystatuscode" 
* name = "elga-allergystatuscode" 
* title = "ELGA_AllergyStatusCode" 
* status = #active 
* version = "2.1.0+20240325" 
* description = "**Description:** Clinical Status of Allergies Or Intolerances. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Klinischer Status von Allergien und Intoleranzen. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.183" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #55561003
* compose.include[0].concept[0].display = "bestehend"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "The subject is currently experiencing the symptoms of the condition or there is evidence of the condition" 
* compose.include[0].concept[1].code = #73425007
* compose.include[0].concept[1].display = "nicht mehr bestehend"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "The subject is no longer experiencing the symptoms of the condition or there is no longer evidence of the condition" 
* compose.include[0].concept[2].code = #723506003
* compose.include[0].concept[2].display = "abgeklungen"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[0].value = "The subject is no longer experiencing the symptoms of the condition and there is a negligible perceived risk of the symptoms returning" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #55561003
* expansion.contains[0].display = "bestehend"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "The subject is currently experiencing the symptoms of the condition or there is evidence of the condition" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #73425007
* expansion.contains[1].display = "nicht mehr bestehend"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[0].value = "The subject is no longer experiencing the symptoms of the condition or there is no longer evidence of the condition" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].contains[0].version = "1.0.0+20230131"
* expansion.contains[1].contains[0].code = #723506003
* expansion.contains[1].contains[0].display = "abgeklungen"
* expansion.contains[1].contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].contains[0].designation[0].value = "The subject is no longer experiencing the symptoms of the condition and there is a negligible perceived risk of the symptoms returning" 
* expansion.contains[1].contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
