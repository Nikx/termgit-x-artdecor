Instance: elga-medikationfrequenz 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-medikationfrequenz" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-medikationfrequenz" 
* name = "elga-medikationfrequenz" 
* title = "ELGA_MedikationFrequenz" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** ELGA ValueSet for frequency

**Beschreibung:** ELGA ValueSet für Frequenz" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.69" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.8"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/ucum"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #d
* compose.include[0].concept[0].display = "Day"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Tag" 
* compose.include[0].concept[1].code = #mo
* compose.include[0].concept[1].display = "Month"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Monat" 
* compose.include[0].concept[2].code = #wk
* compose.include[0].concept[2].display = "Week"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Woche" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ucum"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #d
* expansion.contains[0].display = "Day"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Tag" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.8"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ucum"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #mo
* expansion.contains[1].display = "Month"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Monat" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.8"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/ucum"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #wk
* expansion.contains[2].display = "Week"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "Woche" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.8"
