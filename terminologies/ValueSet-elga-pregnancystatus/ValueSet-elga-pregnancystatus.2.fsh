Instance: elga-pregnancystatus 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-pregnancystatus" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-pregnancystatus" 
* name = "elga-pregnancystatus" 
* title = "ELGA_PregnancyStatus" 
* status = #active 
* version = "1.2.0+20240325" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.52" 
* date = "2024-03-25" 
* publisher = "ELGA GmbH" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
* compose.include[0].system = "http://loinc.org"
* compose.include[0].version = "2.73"
* compose.include[0].concept[0].code = "LA15173-0"
* compose.include[0].concept[0].display = "Pregnant"
* compose.include[0].concept[1].code = "LA26683-5"
* compose.include[0].concept[1].display = "Not pregnant"
* compose.include[0].concept[2].code = "LA4489-6"
* compose.include[0].concept[2].display = "Unknown"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://loinc.org"
* expansion.contains[0].version = "2.73"
* expansion.contains[0].code = #LA15173-0
* expansion.contains[0].display = "Pregnant"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
* expansion.contains[1].system = "http://loinc.org"
* expansion.contains[1].version = "2.73"
* expansion.contains[1].code = #LA26683-5
* expansion.contains[1].display = "Not pregnant"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
* expansion.contains[2].system = "http://loinc.org"
* expansion.contains[2].version = "2.73"
* expansion.contains[2].code = #LA4489-6
* expansion.contains[2].display = "Unknown"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.1"
