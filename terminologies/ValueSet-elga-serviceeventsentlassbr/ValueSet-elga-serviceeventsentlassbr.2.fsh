Instance: elga-serviceeventsentlassbr 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-serviceeventsentlassbr" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-serviceeventsentlassbr" 
* name = "elga-serviceeventsentlassbr" 
* title = "ELGA_ServiceEventsEntlassbr" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** Codes to distinguish discharge service events

**Beschreibung:** Value Set zur Unterscheidung der Service Events im Bereich Entlassung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.57" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.21"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-serviceeventsentlassbrief"
* compose.include[0].version = "1.0.1+20240129"
* compose.include[0].concept[0].code = "GDLPUB"
* compose.include[0].concept[0].display = "Gesundheitsdienstleistung Pflege und Betreuung"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[0].value = "Verwendung im Pflegesituationsbericht" 
* compose.include[0].concept[1].code = "GDLSTATAUF"
* compose.include[0].concept[1].display = "Gesundheitsdienstleistung im Rahmen eines stationären Aufenthalts"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-serviceeventsentlassbrief"
* expansion.contains[0].version = "1.0.1+20240129"
* expansion.contains[0].code = #GDLPUB
* expansion.contains[0].display = "Gesundheitsdienstleistung Pflege und Betreuung"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[0].value = "Verwendung im Pflegesituationsbericht" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.21"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/elga-serviceeventsentlassbrief"
* expansion.contains[1].version = "1.0.1+20240129"
* expansion.contains[1].code = #GDLSTATAUF
* expansion.contains[1].display = "Gesundheitsdienstleistung im Rahmen eines stationären Aufenthalts"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.21"
