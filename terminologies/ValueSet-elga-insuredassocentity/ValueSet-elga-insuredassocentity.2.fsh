Instance: elga-insuredassocentity 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-insuredassocentity" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-insuredassocentity" 
* name = "elga-insuredassocentity" 
* title = "ELGA_InsuredAssocEntity" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "**Description:** Codes for a further detailed description of the insurance relationship. Excerpt of the 'CoverageRoleType' of the Rolecode-Codelist. Currently the ELGA_InsuredAssocEntity value set only uses two codes from the parent code system.

**Beschreibung:** Codes zur näheren Beschreibung des Versicherungsverhältnisses. Auszug aus dem Bereich 'CoverageRoleType' der Codeliste RoleCode. Derzeit werden nur zwei Codes aus der Liste verwendet." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.9" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "http://www.hl7.org" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.111"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-RoleCode"
* compose.include[0].version = "2.2.0"
* compose.include[0].concept[0].code = "FAMDEP"
* compose.include[0].concept[0].display = "family dependent"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Mitversichert" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[1].value = "Patient ist bei einem Familienmitglied mitversichert" 
* compose.include[0].concept[1].code = "SELF"
* compose.include[0].concept[1].display = "self"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Selbstversichert" 
* compose.include[0].concept[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[1].value = "Patient ist selbst der Versicherte" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-RoleCode"
* expansion.contains[0].version = "2.2.0"
* expansion.contains[0].code = #FAMDEP
* expansion.contains[0].display = "family dependent"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Mitversichert" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[1].value = "Patient ist bei einem Familienmitglied mitversichert" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.111"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-RoleCode"
* expansion.contains[1].version = "2.2.0"
* expansion.contains[1].code = #SELF
* expansion.contains[1].display = "self"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Selbstversichert" 
* expansion.contains[1].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[1].value = "Patient ist selbst der Versicherte" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.111"
