Instance: elga-artderdiagnose 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-artderdiagnose" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-artderdiagnose" 
* name = "elga-artderdiagnose" 
* title = "ELGA_ArtderDiagnose" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** This list shows the different types of diagnoses. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** In dieser Liste sind die unterschiedlichen Arten der Diagnosen abgebildet. Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.23" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = "406523004"
* compose.include[0].concept[0].display = "Überweisungsdiagnose"
* compose.include[0].concept[1].code = "52870002"
* compose.include[0].concept[1].display = "Aufnahmediagnose"
* compose.include[0].concept[2].code = "6934004"
* compose.include[0].concept[2].display = "Dauerdiagnose"
* compose.include[0].concept[3].code = "8319008"
* compose.include[0].concept[3].display = "Hauptdiagnose"
* compose.include[0].concept[4].code = "85097005"
* compose.include[0].concept[4].display = "Nebendiagnose"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #406523004
* expansion.contains[0].display = "Überweisungsdiagnose"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #52870002
* expansion.contains[1].display = "Aufnahmediagnose"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #6934004
* expansion.contains[2].display = "Dauerdiagnose"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[3].version = "1.0.0+20230131"
* expansion.contains[3].code = #8319008
* expansion.contains[3].display = "Hauptdiagnose"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[4].version = "1.0.0+20230131"
* expansion.contains[4].code = #85097005
* expansion.contains[4].display = "Nebendiagnose"
* expansion.contains[4].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[4].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
