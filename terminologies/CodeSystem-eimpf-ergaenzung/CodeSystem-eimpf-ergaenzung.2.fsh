Instance: eimpf-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "eimpf-ergaenzung" 
* url = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung" 
* name = "eimpf-ergaenzung" 
* title = "eImpf_Ergaenzung" 
* status = #active 
* content = #complete 
* version = "2.16.0+20240404" 
* description = "**Description:** Supplementary codes for the e-vaccination  **Beschreibung:** Ergänzende Codes für den e-Impfpass" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.183" 
* date = "2024-04-04" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at/" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at/" 
* count = 157 
* property[0].code = #hints 
* property[0].type = #string 
* property[1].code = #status 
* property[1].type = #code 
* #B "Auffrischung"
* #B.I "Auffrischungsserie Teil 1"
* #B.II "Auffrischungsserie Teil 2"
* #D1 "Dosis 1"
* #D2 "Dosis 2"
* #D3 "Dosis 3"
* #D4 "Dosis 4"
* #D5 "Dosis 5"
* #D6 "Dosis 6"
* #D7 "Dosis 7"
* #D8 "Dosis 8"
* #D9 "Dosis 9"
* #DGC-R "Genesungs-Zertifikat"
* #DGC-R ^property[0].code = #hints 
* #DGC-R ^property[0].valueString = "DEPRECATED" 
* #DGC-R ^property[1].code = #status 
* #DGC-R ^property[1].valueCode = #retired 
* #DGC-T "Test-Zertifikat"
* #DGC-T ^property[0].code = #hints 
* #DGC-T ^property[0].valueString = "DEPRECATED" 
* #DGC-T ^property[1].code = #status 
* #DGC-T ^property[1].valueCode = #retired 
* #DGC-V "Impf-Zertifikat"
* #DGC-V ^property[0].code = #hints 
* #DGC-V ^property[0].valueString = "DEPRECATED" 
* #DGC-V ^property[1].code = #status 
* #DGC-V ^property[1].valueCode = #retired 
* #IG1 "Indikationsimpfung für Risikogruppe"
* #IG2 "Wiederholungsimpfung aufgrund medizinischer Indikation"
* #IS001 "Bildungseinrichtung"
* #IS002 "Arbeitsplatz/Betriebe"
* #IS003 "Krankenhaus inkl. Kur- und Rehaeinrichtungen"
* #IS004 "Ordination"
* #IS005 "Öffentliche Impfstelle"
* #IS006 "Wohnbereich"
* #IS007 "Öffentliche Impfstraße / Impfbus"
* #IS008 "Betreute Wohneinrichtung"
* #IS009 "Impfprogramm"
* #IS010 "Kostenfreies Impfprogramm"
* #IS010 ^definition = Kostenfreies Impfprogramm des Bundes, der Bundesländer und der Sozialversicherung
* #IS011 "Öffentliches Impfprogramm (ÖIP) Influenza"
* #IS011 ^definition = Öffentliches Impfprogramm (ÖIP) Influenza des Bundes, der Bundesländer und der Sozialversicherung
* #SCHEMA001 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Erstimpfung bis 1 Jahr"
* #SCHEMA002 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Erstimpfung ab 1 Jahr"
* #SCHEMA003 "Kombinationsschema Di-Te-Pert-IPV-HepB, Erstimpfung ab 6 Jahren"
* #SCHEMA003 ^property[0].code = #hints 
* #SCHEMA003 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA003 ^property[1].code = #status 
* #SCHEMA003 ^property[1].valueCode = #retired 
* #SCHEMA004 "Kombinationsschema Di-Te-Pert-HiB-IPV-HepB, Indikation"
* #SCHEMA005 "FSME Grundschema, FSME-Immun"
* #SCHEMA006 "FSME Grundschema, Encepur"
* #SCHEMA007 "FSME Grundschema, FSME-Immun, Erstimpfung bis 1 Jahr"
* #SCHEMA008 "FSME Grundschema, Encepur, Erstimpfung bis 1 Jahr"
* #SCHEMA009 "FSME Schnellschema, FSME-Immun"
* #SCHEMA010 "FSME Schnellschema, Encepur"
* #SCHEMA011 "Haemophilus influenzae Typ B Indikationsschema"
* #SCHEMA012 "Hepatitis A Monokomponente Indikationsschema"
* #SCHEMA013 "Hepatitis AB Grundschema"
* #SCHEMA014 "Hepatitis A Monokomponente Grundschema"
* #SCHEMA015 "Hepatitis AB Schnellschema"
* #SCHEMA015 ^property[0].code = #hints 
* #SCHEMA015 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA015 ^property[1].code = #status 
* #SCHEMA015 ^property[1].valueCode = #retired 
* #SCHEMA016 "Hepatitis A & Typhus Grundschema"
* #SCHEMA017 "Hepatitis B Monokomponente Grundschema"
* #SCHEMA019 "Hepatitis B Non-/Low-Responder Grundschema"
* #SCHEMA020 "HPV Grundschema, bis 21 Jahre"
* #SCHEMA021 "HPV Grundschema, ab 21 Jahren"
* #SCHEMA022 "HPV Indikationsschema"
* #SCHEMA023 "Influenza Grundschema, Erstimpfung Kinder"
* #SCHEMA024 "Influenza Grundschema, Einmalimpfung"
* #SCHEMA025 "MMR Grundschema ab 9 Monaten"
* #SCHEMA027 "MMR Indikationsschema, Erstimpfung 6-8 Monate"
* #SCHEMA028 "Meningokokken B Grundschema, Erstimpfung 2-5 Monate, 4 Dosen"
* #SCHEMA028 ^property[0].code = #hints 
* #SCHEMA028 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA028 ^property[1].code = #status 
* #SCHEMA028 ^property[1].valueCode = #retired 
* #SCHEMA029 "Meningokokken B Grundschema, Erstimpfung 2-5 Monate"
* #SCHEMA030 "Meningokokken B Grundschema, Erstimpfung 6-11 Monate"
* #SCHEMA031 "Meningokokken B Grundschema, Erstimpfung 12-23 Monate"
* #SCHEMA032 "Meningokokken B Grundschema, Erstimpfung 2-25 Jahre, Bexsero"
* #SCHEMA033 "Meningokokken B Indikationsschema, ab 25 Jahren, Bexsero"
* #SCHEMA034 "Meningokokken B Indikationsschema, ab 25 Jahren, Trumenba"
* #SCHEMA035 "Meningokokken C Indikationsschema, 2-4 Monate, Neisvac C"
* #SCHEMA035 ^property[0].code = #hints 
* #SCHEMA035 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA035 ^property[1].code = #status 
* #SCHEMA035 ^property[1].valueCode = #retired 
* #SCHEMA036 "Meningokokken C Indikationsschema, 2-12 Monate, Menjugate"
* #SCHEMA036 ^property[0].code = #hints 
* #SCHEMA036 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA036 ^property[1].code = #status 
* #SCHEMA036 ^property[1].valueCode = #retired 
* #SCHEMA037 "Meningokokken C Indikationsschema, 4-12 Monate, Neisvac C"
* #SCHEMA037 ^property[0].code = #hints 
* #SCHEMA037 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA037 ^property[1].code = #status 
* #SCHEMA037 ^property[1].valueCode = #retired 
* #SCHEMA038 "Meningokokken C Grundschema, ab 1 Jahr"
* #SCHEMA039 "Meningokokken ACWY Grundschema, ab 10 Jahren"
* #SCHEMA040 "Meningokokken ACWY Indikationsschema, ab 1 Jahr"
* #SCHEMA040 ^property[0].code = #hints 
* #SCHEMA040 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA040 ^property[1].code = #status 
* #SCHEMA040 ^property[1].valueCode = #retired 
* #SCHEMA041 "Meningokokken ACWY Indikationsschema, Erstimpfung bis 1 Jahr"
* #SCHEMA041 ^property[0].code = #hints 
* #SCHEMA041 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA041 ^property[1].code = #status 
* #SCHEMA041 ^property[1].valueCode = #retired 
* #SCHEMA042 "Pneumokokken Grundschema, Erstimpfung bis 1 Jahr"
* #SCHEMA043 "Pneumokokken Grundschema, Erstimpfung 1-2 Jahre"
* #SCHEMA044 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung bis 1 Jahr"
* #SCHEMA045 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung im 2. Lebensjahr"
* #SCHEMA046 "Pneumokokken Indikationsschema (hohes Risiko), Erstimpfung ab 3 Jahren"
* #SCHEMA050 "Rotavirus Grundschema, 2 Dosen, Erstimpfung ab der vollendeten 6.-20. Lebenswoche, Rotarix"
* #SCHEMA051 "Rotavirus Grundschema, 2 Dosen, Erstimpfung ab der vollendeten 24.-28. Lebenswoche, Rotateq"
* #SCHEMA052 "Rotavirus Grundschema, 1 Dosis, Erstimpfung ab der vollendeten 20.-24. Lebenswoche, Rotarix"
* #SCHEMA053 "Rotavirus Grundschema, 1 Dosis, Erstimpfung ab der vollendeten 28.-32. Lebenswoche, Rotateq"
* #SCHEMA054 "Rotavirus Grundschema, 3 Dosen, Erstimpfung ab der vollendeten 6.- 24. Lebenswoche, Rotateq"
* #SCHEMA060 "Varizellen Grundschema, ab 1 Jahr"
* #SCHEMA061 "Kombinationsschema MMRV, ab 9 Monaten, MMR - MMRV - V"
* #SCHEMA062 "Kombinationsschema MMRV, ab 12 Monaten, MMR - MMRV - V"
* #SCHEMA063 "Varizellen Indikationsschema, Erstimpfung bis 1 Jahr"
* #SCHEMA064 "Herpes Zoster Grundschema, ab 50 Jahren"
* #SCHEMA065 "Gelbfieber Grundschema, ab dem vollendeten 9. Lebensmonat"
* #SCHEMA066 "Japanische Enzephalitis Grundschema, ab 2 Monaten"
* #SCHEMA067 "Japanische Enzephalitis Schnellschema, ab 18 Jahren"
* #SCHEMA068 "Typhus Grundschema, Injektion"
* #SCHEMA069 "Typhus Grundschema, Oral"
* #SCHEMA070 "Tollwut Grundschema"
* #SCHEMA071 "Tollwut Schnellschema"
* #SCHEMA072 "Tollwut postexpositionelles Schema, Essen 4 Dosen"
* #SCHEMA073 "Tollwut postexpositionelles Schema, Essen 5 Dosen"
* #SCHEMA074 "Tollwut postexpositionelles Schema, Zagreb"
* #SCHEMA075 "Tollwut postexpositionelles Schema, mit Tollwut-Vorimpfung"
* #SCHEMA076 "Cholera Grundschema, Erstimpfung 2-6 Jahre"
* #SCHEMA078 "Cholera Grundschema, Erstimpfung ab 6 Jahren"
* #SCHEMA079 "Meningokokken B Grundschema, Erstimpfung 10-25 Jahre, Trumenba"
* #SCHEMA080 "Kombinationsschema Di-Te-Pert-IPV"
* #SCHEMA081 "Kombinationsschema Di-Te-Pert"
* #SCHEMA082 "Kombinationsschema Di-Te"
* #SCHEMA082 ^property[0].code = #hints 
* #SCHEMA082 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA082 ^property[1].code = #status 
* #SCHEMA082 ^property[1].valueCode = #retired 
* #SCHEMA083 "Tetanus Monokomponente"
* #SCHEMA083 ^property[0].code = #hints 
* #SCHEMA083 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA083 ^property[1].code = #status 
* #SCHEMA083 ^property[1].valueCode = #retired 
* #SCHEMA084 "Poliomyelitis Indikationsschema ab dem vollendeten 2. Lebensmonat"
* #SCHEMA085 "Hepatitis B Monokomponente Indikationsschema"
* #SCHEMA086 "SARS-CoV-2 Grundschema, Comirnaty"
* #SCHEMA086 ^property[0].code = #hints 
* #SCHEMA086 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA086 ^property[1].code = #status 
* #SCHEMA086 ^property[1].valueCode = #retired 
* #SCHEMA087 "SARS-CoV-2 Grundschema, Spikevax"
* #SCHEMA087 ^property[0].code = #hints 
* #SCHEMA087 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA087 ^property[1].code = #status 
* #SCHEMA087 ^property[1].valueCode = #retired 
* #SCHEMA088 "SARS-CoV-2 Grundschema, Vaxzevria"
* #SCHEMA088 ^property[0].code = #hints 
* #SCHEMA088 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA088 ^property[1].code = #status 
* #SCHEMA088 ^property[1].valueCode = #retired 
* #SCHEMA089 "SARS-CoV-2 Grundschema, Jcovden"
* #SCHEMA089 ^property[0].code = #hints 
* #SCHEMA089 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA089 ^property[1].code = #status 
* #SCHEMA089 ^property[1].valueCode = #retired 
* #SCHEMA090 "SARS-CoV-2 Grundschema, Auslandsimpfung"
* #SCHEMA090 ^property[0].code = #hints 
* #SCHEMA090 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA090 ^property[1].code = #status 
* #SCHEMA090 ^property[1].valueCode = #retired 
* #SCHEMA091 "Herpes Zoster Grundschema, nach Zostavax"
* #SCHEMA092 "Herpes Zoster Indikationsschema, ab 18 Jahren"
* #SCHEMA093 "MMR Grundschema, ab 1 Jahr"
* #SCHEMA094 "SARS-CoV-2 Indikationsschema, Comirnaty"
* #SCHEMA094 ^property[0].code = #hints 
* #SCHEMA094 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA094 ^property[1].code = #status 
* #SCHEMA094 ^property[1].valueCode = #retired 
* #SCHEMA095 "SARS-CoV-2 Indikationsschema, Spikevax"
* #SCHEMA095 ^property[0].code = #hints 
* #SCHEMA095 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA095 ^property[1].code = #status 
* #SCHEMA095 ^property[1].valueCode = #retired 
* #SCHEMA096 "SARS-CoV-2 heterologes Schema (Impfstoffwechsel)"
* #SCHEMA096 ^property[0].code = #hints 
* #SCHEMA096 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA096 ^property[1].code = #status 
* #SCHEMA096 ^property[1].valueCode = #retired 
* #SCHEMA097 "Pneumokokken Grundschema, Erstimpfung 2-5 Jahre"
* #SCHEMA098 "Pneumokokken Grundschema, ab 60 Jahre"
* #SCHEMA099 "SARS-CoV-2 Grundschema, Nuvaxovid"
* #SCHEMA099 ^property[0].code = #hints 
* #SCHEMA099 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA099 ^property[1].code = #status 
* #SCHEMA099 ^property[1].valueCode = #retired 
* #SCHEMA100 "Kombinationsschema MMRV, ab 9 Monaten, MMRV - MMRV"
* #SCHEMA101 "Kombinationsschema MMRV, ab 12 Monaten, MMRV - MMRV"
* #SCHEMA102 "Hepatitis B Monokomponente Schnellschema durch Indikation"
* #SCHEMA103 "Hepatitis B Prophylaxe Neugeborener Indikationsschema"
* #SCHEMA104 "Influenza Indikationsschema"
* #SCHEMA105 "Kombinationsschema Di-Te-IPV"
* #SCHEMA105 ^property[0].code = #hints 
* #SCHEMA105 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA105 ^property[1].code = #status 
* #SCHEMA105 ^property[1].valueCode = #retired 
* #SCHEMA106 "Meningokokken ACWY Indikationsschema, Nimenrix bis 6 Monate"
* #SCHEMA107 "Meningokokken ACWY Indikationsschema, Nimenrix ab 6 Monate"
* #SCHEMA108 "Meningokokken ACWY Indikationsschema, Nimenrix ab 1 Jahr"
* #SCHEMA108 ^property[0].code = #hints 
* #SCHEMA108 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA108 ^property[1].code = #status 
* #SCHEMA108 ^property[1].valueCode = #retired 
* #SCHEMA109 "Meningokokken ACWY Indikationsschema, Menveo ab 2 Jahre"
* #SCHEMA110 "COVID-19 Indikationsschema"
* #SCHEMA111 "(Affen-)Pocken Indikationsschema"
* #SCHEMA112 "SARS-CoV-2 Grundschema, Valneva"
* #SCHEMA112 ^property[0].code = #hints 
* #SCHEMA112 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA112 ^property[1].code = #status 
* #SCHEMA112 ^property[1].valueCode = #retired 
* #SCHEMA113 "Kombinationsschema MMRV, ab 9 Monaten, MMRV - MMR - V"
* #SCHEMA113 ^property[0].code = #hints 
* #SCHEMA113 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA113 ^property[1].code = #status 
* #SCHEMA113 ^property[1].valueCode = #retired 
* #SCHEMA114 "Kombinationsschema MMRV, ab 12 Monaten, MMRV - MMR - V"
* #SCHEMA114 ^property[0].code = #hints 
* #SCHEMA114 ^property[0].valueString = "DEPRECATED" 
* #SCHEMA114 ^property[1].code = #status 
* #SCHEMA114 ^property[1].valueCode = #retired 
* #SCHEMA115 "(Affen-)Pocken Indikationsschema, einmalige Impfung"
* #SCHEMA116 "Pneumokokken Indikationsschema (erhöhtes Risiko), ab 50 Jahren"
* #SCHEMA117 "COVID-19 Grundschema, Kleinkind"
* #SCHEMA118 "Pneumokokken Indikationsschema, Frühgeborene"
* #SCHEMA119 "Dengue-Fieber Grundschema, 2 Dosen"
* #SCHEMA120 "Poliomyelitis Indikationsschema ab dem vollendeten 1. Lebensjahr"
* #SCHEMA121 "RSV Grundschema"
* #SCHEMA122 "COVID-19 Grundschema"
* #SCHEMA123 "Tollwut Schnellschema (WHO)"
* #SCHEMA124 "Tollwut Schnellschema (WHO), intradermal"
* #SCHEMA125 "Meningokokken ACWY, ab 1 Jahr"
* #SCHEMA126 "FSME beschleunigtes konventionelles Schema, Encepur"
* #_AllgemeineBevölkerung "Allgemeine Bevölkerung"
* #_AllgemeineBevölkerung ^property[0].code = #hints 
* #_AllgemeineBevölkerung ^property[0].valueString = "DEPRECATED" 
* #_AllgemeineBevölkerung ^property[1].code = #status 
* #_AllgemeineBevölkerung ^property[1].valueCode = #retired 
* #_Ausnahme "Ausnahme"
* #_Blau "Blau"
* #_Einzel "Impfungen"
* #_Empfohlene "Allgemein empfohlene Impfungen"
* #_Gelb "Gelb"
* #_Grau "Grau"
* #_Grün "Grün"
* #_Kombinationen "Kombinationsimpfungen"
* #_Reise "Reise-/Indikationsimpfungen"
* #_Rot "Rot"
* #_SpezielleRisikogruppefür "Spezielle Risikogruppe für"
* #_Weitere "Weitere Impfungen"
* #off-label-use "off-label-use"
