Instance: elga-medikationabgabeart 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-medikationabgabeart" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-medikationabgabeart" 
* name = "elga-medikationabgabeart" 
* title = "ELGA_MedikationAbgabeArt" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "ELGA Value Set für die Medikations-Abgabeart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.159" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.4"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-ActCode"
* compose.include[0].version = "8.0.0"
* compose.include[0].concept[0].code = "FFC"
* compose.include[0].concept[0].display = "First Fill - Complete"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Vollständige Abgabe" 
* compose.include[0].concept[1].code = "FFP"
* compose.include[0].concept[1].display = "First Fill - Part Fill"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Partielle Abgabe (erste Abgabe)" 
* compose.include[0].concept[2].code = "RFC"
* compose.include[0].concept[2].display = "Refill - Complete"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Partielle Abgabe (letzte Abgabe)" 
* compose.include[0].concept[3].code = "RFP"
* compose.include[0].concept[3].display = "Refill - Part Fill"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "Partielle Abgabe (weitere Teilabgabe)" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-ActCode"
* expansion.contains[0].version = "8.0.0"
* expansion.contains[0].code = #FFC
* expansion.contains[0].display = "First Fill - Complete"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Vollständige Abgabe" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.4"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-ActCode"
* expansion.contains[1].version = "8.0.0"
* expansion.contains[1].code = #FFP
* expansion.contains[1].display = "First Fill - Part Fill"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Partielle Abgabe (erste Abgabe)" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.4"
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/v3-ActCode"
* expansion.contains[2].version = "8.0.0"
* expansion.contains[2].code = #RFC
* expansion.contains[2].display = "Refill - Complete"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "Partielle Abgabe (letzte Abgabe)" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.4"
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/v3-ActCode"
* expansion.contains[3].version = "8.0.0"
* expansion.contains[3].code = #RFP
* expansion.contains[3].display = "Refill - Part Fill"
* expansion.contains[3].designation[0].language = #de-AT 
* expansion.contains[3].designation[0].value = "Partielle Abgabe (weitere Teilabgabe)" 
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.4"
