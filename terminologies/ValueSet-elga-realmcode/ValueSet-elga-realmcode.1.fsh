Instance: elga-realmcode 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-realmcode" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-realmcode" 
* name = "elga-realmcode" 
* title = "ELGA_RealmCode" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** Coding of the documents realm. In ELGA documents must use the code 'AT'

**Beschreibung:** Beschreibt den Hoheitsbereich des Dokuments. In ELGA fixiert mit AT." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.3" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.0.3166.1.2.2"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/iso-3166-alpha-2-code"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #AT
* compose.include[0].concept[0].display = "Austria"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Österreich" 
* compose.include[0].concept[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[1].value = "Einziger erlaubter Wert" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/iso-3166-alpha-2-code"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #AT
* expansion.contains[0].display = "Austria"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Österreich" 
* expansion.contains[0].designation[1].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[1].value = "Einziger erlaubter Wert" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.0.3166.1.2.2"
