Instance: hl7-at-maritalstatus-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "hl7-at-maritalstatus-ergaenzung" 
* url = "https://termgit.elga.gv.at/CodeSystem/hl7-at-maritalstatus-ergaenzung" 
* name = "hl7-at-maritalstatus-ergaenzung" 
* title = "HL7-at_MaritalStatus-Ergaenzung" 
* status = #active 
* content = #complete 
* version = "1.0.0+20240402" 
* description = "Österreich-spezifische Codes für den Personenstand, in Ergänzung der internationalen HL7-Codes für MaritalStatus" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.227 " 
* date = "2024-04-02" 
* publisher = "see" 
* contact[0].name = "HL7 Austria" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.hl7.at" 
* count = 5 
* #B "Ehe aufgehoben"
* #E "In eingetragener Partnerschaft lebend"
* #G "Aufgelöste eingetragene Partnerschaft"
* #H "Hinterbliebene:r eingetragene:r Partner:in"
* #N "Eingetragene Partnerschaft für nichtig erklärt"
