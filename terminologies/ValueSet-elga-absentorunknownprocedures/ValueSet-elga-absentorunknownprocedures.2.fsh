Instance: elga-absentorunknownprocedures 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-absentorunknownprocedures" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-absentorunknownprocedures" 
* name = "elga-absentorunknownprocedures" 
* title = "ELGA_AbsentOrUnknownProcedures" 
* status = #active 
* version = "1.1.0+20231117" 
* description = "Keine Eingriffe oder Therapien" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.193" 
* date = "2023-11-17" 
* compose.include[0].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* compose.include[0].version = "1.1.0"
* compose.include[0].concept[0].code = "no-procedure-info"
* compose.include[0].concept[0].display = "No information about past history of procedures"
* compose.include[0].concept[1].code = "no-known-procedures"
* compose.include[0].concept[1].display = "No known procedures"

* expansion.timestamp = "2023-11-23T13:49:32.0000Z"

* expansion.contains[0].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[0].version = "1.1.0"
* expansion.contains[0].code = #no-procedure-info
* expansion.contains[0].display = "No information about past history of procedures"
* expansion.contains[1].system = "http://hl7.org/fhir/uv/ips/CodeSystem/absent-unknown-uv-ips"
* expansion.contains[1].version = "1.1.0"
* expansion.contains[1].code = #no-known-procedures
* expansion.contains[1].display = "No known procedures"
