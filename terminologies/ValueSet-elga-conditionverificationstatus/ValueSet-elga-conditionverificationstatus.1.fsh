Instance: elga-conditionverificationstatus 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-conditionverificationstatus" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-conditionverificationstatus" 
* name = "elga-conditionverificationstatus" 
* title = "ELGA_ConditionVerificationStatus" 
* status = #active 
* version = "2.1.0+20240325" 
* description = "**Description:** Clinical Status of Allergies Or Intolerances 

**Beschreibung:** Klinischer Status von Allergien und Intoleranzen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.184" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.4.642.3.115"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #unconfirmed
* compose.include[0].concept[0].display = "unbestätigt"
* compose.include[0].concept[1].code = #provisional
* compose.include[0].concept[1].display = "vorläufig"
* compose.include[0].concept[2].code = #differential
* compose.include[0].concept[2].display = "alternativ möglich (Differentialdiagnose)"
* compose.include[0].concept[3].code = #confirmed
* compose.include[0].concept[3].display = "bestätigt"
* compose.include[0].concept[4].code = #refuted
* compose.include[0].concept[4].display = "ausgeschlossen"
* compose.include[0].concept[5].code = #unknown
* compose.include[0].concept[5].display = "unbekannt"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #unconfirmed
* expansion.contains[0].display = "unbestätigt"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.4.642.3.115"
* expansion.contains[0].contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus"
* expansion.contains[0].contains[0].version = "1.0.0+20230131"
* expansion.contains[0].contains[0].code = #provisional
* expansion.contains[0].contains[0].display = "vorläufig"
* expansion.contains[0].contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.4.642.3.115"
* expansion.contains[0].contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus"
* expansion.contains[0].contains[1].version = "1.0.0+20230131"
* expansion.contains[0].contains[1].code = #differential
* expansion.contains[0].contains[1].display = "alternativ möglich (Differentialdiagnose)"
* expansion.contains[0].contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.4.642.3.115"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #confirmed
* expansion.contains[1].display = "bestätigt"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.4.642.3.115"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #refuted
* expansion.contains[2].display = "ausgeschlossen"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.4.642.3.115"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/ips-conditionverificationstatus"
* expansion.contains[3].version = "1.0.0+20230131"
* expansion.contains[3].code = #unknown
* expansion.contains[3].display = "unbekannt"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.4.642.3.115"
