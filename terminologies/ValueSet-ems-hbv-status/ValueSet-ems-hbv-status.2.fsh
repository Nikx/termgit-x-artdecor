Instance: ems-hbv-status 
InstanceOf: ValueSet 
Usage: #definition 
* id = "ems-hbv-status" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/ems-hbv-status" 
* name = "ems-hbv-status" 
* title = "EMS_HBV_Status" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "Dieses Valueset wird verwendet um den HBV Status zu codieren. Verwendung im Zuge vom EMS" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.93" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.88"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/ems-posneg"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = "NEG"
* compose.include[0].concept[0].display = "negativ"
* compose.include[0].concept[1].code = "NOTEST"
* compose.include[0].concept[1].display = "nicht durchgeführt"
* compose.include[0].concept[2].code = "POS"
* compose.include[0].concept[2].display = "positiv"
* compose.include[0].concept[3].code = "UNK"
* compose.include[0].concept[3].display = "unbekannt"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ems-posneg"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #NEG
* expansion.contains[0].display = "negativ"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.88"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ems-posneg"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #NOTEST
* expansion.contains[1].display = "nicht durchgeführt"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.88"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/ems-posneg"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #POS
* expansion.contains[2].display = "positiv"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.88"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/ems-posneg"
* expansion.contains[3].version = "1.0.0+20230131"
* expansion.contains[3].code = #UNK
* expansion.contains[3].display = "unbekannt"
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.88"
