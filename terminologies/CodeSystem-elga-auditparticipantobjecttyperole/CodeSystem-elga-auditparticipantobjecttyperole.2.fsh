Instance: elga-auditparticipantobjecttyperole 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-auditparticipantobjecttyperole" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttyperole" 
* name = "elga-auditparticipantobjecttyperole" 
* title = "ELGA_AuditParticipantObjectTypeRole" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "Liste der ELGA spezifischen Audit Participant Object Type Code Roles. Die Audit Participant Object Type Role definiert die Verwendung eines Objekts, welches im Audit Event referenziert ist." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.154" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 15 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #100 "Policy"
* #100 ^property[0].code = #child 
* #100 ^property[0].valueCode = #101 
* #100 ^property[1].code = #child 
* #100 ^property[1].valueCode = #102 
* #101 "Generelle Policy"
* #101 ^property[0].code = #parent 
* #101 ^property[0].valueCode = #100 
* #102 "Patientenzustimmung"
* #102 ^property[0].code = #parent 
* #102 ^property[0].valueCode = #100 
* #110 "Kontaktbestätigung"
* #110 ^property[0].code = #child 
* #110 ^property[0].valueCode = #111 
* #110 ^property[1].code = #child 
* #110 ^property[1].valueCode = #112 
* #110 ^property[2].code = #child 
* #110 ^property[2].valueCode = #113 
* #111 "Bürgerkarte"
* #111 ^property[0].code = #parent 
* #111 ^property[0].valueCode = #110 
* #112 "e-card"
* #112 ^property[0].code = #parent 
* #112 ^property[0].valueCode = #110 
* #113 "Delegierter Kontakt"
* #113 ^property[0].code = #parent 
* #113 ^property[0].valueCode = #110 
* #120 "Security Token"
* #120 ^property[0].code = #child 
* #120 ^property[0].valueCode = #121 
* #120 ^property[1].code = #child 
* #120 ^property[1].valueCode = #122 
* #120 ^property[2].code = #child 
* #120 ^property[2].valueCode = #123 
* #120 ^property[3].code = #child 
* #120 ^property[3].valueCode = #124 
* #120 ^property[4].code = #child 
* #120 ^property[4].valueCode = #125 
* #120 ^property[5].code = #child 
* #120 ^property[5].valueCode = #126 
* #121 "HCP Assertion"
* #121 ^property[0].code = #parent 
* #121 ^property[0].valueCode = #120 
* #122 "User 1 Assertion"
* #122 ^property[0].code = #parent 
* #122 ^property[0].valueCode = #120 
* #123 "Mandate 1 Assertion"
* #123 ^property[0].code = #parent 
* #123 ^property[0].valueCode = #120 
* #124 "Treatment Assertion"
* #124 ^property[0].code = #parent 
* #124 ^property[0].valueCode = #120 
* #125 "User 2 Assertion"
* #125 ^property[0].code = #parent 
* #125 ^property[0].valueCode = #120 
* #126 "Mandate 2 Assertion"
* #126 ^property[0].code = #parent 
* #126 ^property[0].valueCode = #120 
* #130 "Vollmacht"
