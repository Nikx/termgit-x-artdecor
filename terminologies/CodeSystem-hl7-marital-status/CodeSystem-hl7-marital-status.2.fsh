Instance: hl7-marital-status 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "hl7-marital-status" 
* url = "https://termgit.elga.gv.at/CodeSystem/hl7-marital-status" 
* name = "hl7-marital-status" 
* title = "HL7 Marital Status" 
* status = #retired 
* content = #complete 
* version = "1.0.0+20230131" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.840.1.113883.5.2" 
* date = "2023-06-30" 
* count = 9 
* #A "Annulled"
* #D "Divorced"
* #I "Interlocutory"
* #L "Legally Separated"
* #M "Married"
* #P "Polygamous"
* #S "Never Married"
* #T "Domestic partner"
* #W "Widowed"
