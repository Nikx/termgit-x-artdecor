Instance: medikationrezeptpflichtstatus 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "medikationrezeptpflichtstatus" 
* url = "https://termgit.elga.gv.at/CodeSystem/medikationrezeptpflichtstatus" 
* name = "medikationrezeptpflichtstatus" 
* title = "Medikation_Rezeptpflichtstatus" 
* status = #active 
* content = #complete 
* version = "1.1.0+20230724" 
* description = "**Description:** Medikation Rezeptpflichtstatus. Liability and terms of use you can find on this website https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit

**Beschreibung:** Medikation Rezeptpflichtstatus. Haftung und Nutzungsbestimmungen finden Sie unter folgender Webseite https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.7" 
* date = "2023-07-24" 
* count = 8 
* #100000072076 "Arzneimittel zur Abgabe ohne aerztliche Verschreibung"
* #100000072077 "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung"
* #100000072078 "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist"
* #100000072079 "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung"
* #100000072084 "Arzneimittel zur Abgabe gegen aerztliche Verschreibung"
* #100000072086 "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung"
* #100000157313 "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte"
* #900000000002 "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution"
