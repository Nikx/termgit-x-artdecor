Instance: medikationrezeptpflichtstatus 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "medikationrezeptpflichtstatus" 
* url = "https://termgit.elga.gv.at/CodeSystem/medikationrezeptpflichtstatus" 
* name = "medikationrezeptpflichtstatus" 
* title = "Medikation_Rezeptpflichtstatus" 
* status = #active 
* content = #complete 
* version = "1.1.0+20230724" 
* description = "**Description:** Medikation Rezeptpflichtstatus. Liability and terms of use you can find on this website https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit

**Beschreibung:** Medikation Rezeptpflichtstatus. Haftung und Nutzungsbestimmungen finden Sie unter folgender Webseite https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.4.3.4.3.7" 
* date = "2023-07-24" 
* count = 8 
* concept[0].code = #100000072076
* concept[0].display = "Arzneimittel zur Abgabe ohne aerztliche Verschreibung"
* concept[1].code = #100000072077
* concept[1].display = "Arzneimittel zur einmaligen oder wiederholten Abgabe gegen aerztliche Verschreibung"
* concept[2].code = #100000072078
* concept[2].display = "Arzneimittel zur eingeschraenkten Abgabe gegen aerztliche Verschreibung, deren Anwendung definierten Spezialisten vorbehalten ist"
* concept[3].code = #100000072079
* concept[3].display = "Arzneimittel zur einmaligen Abgabe auf aerztliche Verschreibung"
* concept[4].code = #100000072084
* concept[4].display = "Arzneimittel zur Abgabe gegen aerztliche Verschreibung"
* concept[5].code = #100000072086
* concept[5].display = "Arzneimittel zur wiederholten Abgabe gegen aerztliche Verschreibung"
* concept[6].code = #100000157313
* concept[6].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Suchtgifte"
* concept[7].code = #900000000002
* concept[7].display = "Arzneimittel zur Abgabe gegen besondere aerztliche Verschreibung, Substitution"
