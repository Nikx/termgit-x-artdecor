Instance: elga-einnahmezeitpunkte 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-einnahmezeitpunkte" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-einnahmezeitpunkte" 
* name = "elga-einnahmezeitpunkte" 
* title = "ELGA_Einnahmezeitpunkte" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "**Description:** Timing Events for drug administration

**Beschreibung:** Einnahmezeitpunkte für Medikamente" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.59" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "https://www.hl7.org" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.hl7.org" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.139"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-TimingEvent"
* compose.include[0].version = "2.1.0"
* compose.include[0].concept[0].code = "ACD"
* compose.include[0].concept[0].display = "ACD"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Mittags" 
* compose.include[0].concept[1].code = "ACM"
* compose.include[0].concept[1].display = "ACM"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "Morgens" 
* compose.include[0].concept[2].code = "ACV"
* compose.include[0].concept[2].display = "ACV"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "Abends" 
* compose.include[0].concept[3].code = "HS"
* compose.include[0].concept[3].display = "HS"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "Nachts" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-TimingEvent"
* expansion.contains[0].version = "2.1.0"
* expansion.contains[0].code = #ACD
* expansion.contains[0].display = "ACD"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "Mittags" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.139"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-TimingEvent"
* expansion.contains[1].version = "2.1.0"
* expansion.contains[1].code = #ACM
* expansion.contains[1].display = "ACM"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "Morgens" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.139"
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/v3-TimingEvent"
* expansion.contains[2].version = "2.1.0"
* expansion.contains[2].code = #ACV
* expansion.contains[2].display = "ACV"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "Abends" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.139"
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/v3-TimingEvent"
* expansion.contains[3].version = "2.1.0"
* expansion.contains[3].code = #HS
* expansion.contains[3].display = "HS"
* expansion.contains[3].designation[0].language = #de-AT 
* expansion.contains[3].designation[0].value = "Nachts" 
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.139"
