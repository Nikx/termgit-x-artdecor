Instance: rads-thyroidimaging 
InstanceOf: ValueSet 
Usage: #definition 
* id = "rads-thyroidimaging" 
* url = "https://termgit.elga.gv.at/ValueSet/rads-thyroidimaging" 
* name = "rads-thyroidimaging" 
* title = "RADS_ThyroidImaging" 
* status = #draft 
* version = "0.1.0+20240410" 
* description = "**Description:** Draft value set for the normative ballot for the diagnostic imaging report (version 3.0.0). The codes will be replaced by SNOMED CT codes as soon as possible. Note: The Reporting and Data Systems (RADS) assessment scales are a registered trademark of the American College of Radiology.

**Beschreibung:** Value-Set-Entwurf für den normativen Ballot zum Befund bildgebende Diagnostik (Version 3.0.0). Die Codes werden sobald möglich durch SNOMED-CT-Codes ersetzt. Achtung: Die "Reporting and Data Systems (RADS)"-Skalen sind eine eingetragene Marke des American College of Radiology." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.94" 
* date = "2024-04-10" 
* publisher = "see" 
* contact[0].name = "American College of Radiology" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.acr.org/" 
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/ACR"
* compose.include[0].version = "2.0.1"
* compose.include[0].concept[0].code = "TR1"
* compose.include[0].concept[0].display = "TR1"
* compose.include[0].concept[0].designation[0].language = #en-US 
* compose.include[0].concept[0].designation[0].value = "Thyroid assessment (Category 1) - Benign" 
* compose.include[0].concept[1].code = "TR2"
* compose.include[0].concept[1].display = "TR2"
* compose.include[0].concept[1].designation[0].language = #en-US 
* compose.include[0].concept[1].designation[0].value = "Thyroid assessment (Category 2) - Not Suspicious" 
* compose.include[0].concept[2].code = "TR3"
* compose.include[0].concept[2].display = "TR3"
* compose.include[0].concept[2].designation[0].language = #en-US 
* compose.include[0].concept[2].designation[0].value = "Thyroid assessment (Category 3) - Mildly Suspicious" 
* compose.include[0].concept[3].code = "TR4"
* compose.include[0].concept[3].display = "TR4"
* compose.include[0].concept[3].designation[0].language = #en-US 
* compose.include[0].concept[3].designation[0].value = "Thyroid assessment (Category 4) - Moderately Suspicious" 
* compose.include[0].concept[4].code = "TR5"
* compose.include[0].concept[4].display = "TR5"
* compose.include[0].concept[4].designation[0].language = #en-US 
* compose.include[0].concept[4].designation[0].value = "Thyroid assessment (Category 5) - Highly Suspicious" 

* expansion.timestamp = "2024-04-10T08:47:55.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[0].version = "2.0.1"
* expansion.contains[0].code = #TR1
* expansion.contains[0].display = "TR1"
* expansion.contains[0].designation[0].language = #en-US 
* expansion.contains[0].designation[0].value = "Thyroid assessment (Category 1) - Benign" 
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[1].version = "2.0.1"
* expansion.contains[1].code = #TR2
* expansion.contains[1].display = "TR2"
* expansion.contains[1].designation[0].language = #en-US 
* expansion.contains[1].designation[0].value = "Thyroid assessment (Category 2) - Not Suspicious" 
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[2].version = "2.0.1"
* expansion.contains[2].code = #TR3
* expansion.contains[2].display = "TR3"
* expansion.contains[2].designation[0].language = #en-US 
* expansion.contains[2].designation[0].value = "Thyroid assessment (Category 3) - Mildly Suspicious" 
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[3].version = "2.0.1"
* expansion.contains[3].code = #TR4
* expansion.contains[3].display = "TR4"
* expansion.contains[3].designation[0].language = #en-US 
* expansion.contains[3].designation[0].value = "Thyroid assessment (Category 4) - Moderately Suspicious" 
* expansion.contains[4].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[4].version = "2.0.1"
* expansion.contains[4].code = #TR5
* expansion.contains[4].display = "TR5"
* expansion.contains[4].designation[0].language = #en-US 
* expansion.contains[4].designation[0].value = "Thyroid assessment (Category 5) - Highly Suspicious" 
