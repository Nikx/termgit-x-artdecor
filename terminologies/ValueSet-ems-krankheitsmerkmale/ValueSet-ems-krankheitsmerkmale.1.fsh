Instance: ems-krankheitsmerkmale 
InstanceOf: ValueSet 
Usage: #definition 
* id = "ems-krankheitsmerkmale" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/ems-krankheitsmerkmale" 
* name = "ems-krankheitsmerkmale" 
* title = "EMS_Krankheitsmerkmale" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** Concepts for symptoms based on the EMS Requirements

**Beschreibung:** Konzepte Krankheitsmerkmale laut EMS Anforderungen" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.18" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.105"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/ems-krankheitsmerkmale"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #Verstorben
* compose.include[0].concept[0].display = "Verstorben"
* compose.include[0].concept[1].code = #TOD
* compose.include[0].concept[1].display = "Tod"
* compose.include[0].concept[2].code = #TOD_KRANKHEIT
* compose.include[0].concept[2].display = "Krankheitsbezogener Tod"
* compose.include[0].concept[3].code = #AUSSCHEIDER
* compose.include[0].concept[3].display = "Ausscheider"
* compose.include[0].concept[4].code = #ASYMPTOMATISCH
* compose.include[0].concept[4].display = "Asymptomatisch"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ems-krankheitsmerkmale"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].abstract = true
* expansion.contains[0].code = #Verstorben
* expansion.contains[0].display = "Verstorben"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.105"
* expansion.contains[0].contains[0].system = "https://termgit.elga.gv.at/CodeSystem/ems-krankheitsmerkmale"
* expansion.contains[0].contains[0].version = "1.0.0+20230131"
* expansion.contains[0].contains[0].code = #TOD
* expansion.contains[0].contains[0].display = "Tod"
* expansion.contains[0].contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.105"
* expansion.contains[0].contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ems-krankheitsmerkmale"
* expansion.contains[0].contains[1].version = "1.0.0+20230131"
* expansion.contains[0].contains[1].code = #TOD_KRANKHEIT
* expansion.contains[0].contains[1].display = "Krankheitsbezogener Tod"
* expansion.contains[0].contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.105"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/ems-krankheitsmerkmale"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #AUSSCHEIDER
* expansion.contains[1].display = "Ausscheider"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.105"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/ems-krankheitsmerkmale"
* expansion.contains[2].version = "1.0.0+20230131"
* expansion.contains[2].code = #ASYMPTOMATISCH
* expansion.contains[2].display = "Asymptomatisch"
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.105"
