Instance: oeaek-spezialdiplom 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "oeaek-spezialdiplom" 
* url = "https://termgit.elga.gv.at/CodeSystem/oeaek-spezialdiplom" 
* name = "oeaek-spezialdiplom" 
* title = "OEAEK_Spezialdiplom" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "**Description:** Code List of all special diplomas valid in Austria issued by the Medical Chamber of Austria

**Beschreibung:** Code Liste aller in Österreich gültigen Spezialdiplome" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.3.1.10.40" 
* date = "2018-07-01" 
* count = 30 
* #39 "Sexualmedizin"
* #44 "Sportmedizin"
* #45 "Umweltmedizin"
* #46 "Kur-, Präventivmedizin und Wellness"
* #49 "Psychosoziale Medizin"
* #50 "Psychosomatische Medizin"
* #51 "Psychotherapeutische Medizin"
* #52 "Geriatrie"
* #53 "Akupunktur"
* #56 "Arbeitsmedizin"
* #61 "Manuelle Medizin"
* #62 "Homöopathie"
* #63 "Neuraltherapie"
* #64 "Ernährungsmedizin"
* #65 "Krankenhaushygiene"
* #66 "Klinischer Prüfarzt"
* #67 "Diagnostik und Therapie nach Dr. F.X. Mayr"
* #68 "Schularzt"
* #69 "Anthroposophische Medizin"
* #72 "Genetik"
* #73 "Applied Kinesiology"
* #74 "Chinesische Diagnostik und Arzneitherapie"
* #75 "Kneippmedizin"
* #84 "Forensisch-psychiatrische Gutachten"
* #85 "Spezielle Schmerztherapie"
* #86 "Begleitende Krebsbehandlungen"
* #87 "Palliativmedizin"
* #91 "Orthomolekulare Medizin"
* #92 "Phytotherapie"
* #93 "Substitutionsbehandlung"
