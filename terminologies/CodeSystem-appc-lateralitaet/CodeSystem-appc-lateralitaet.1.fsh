Instance: appc-lateralitaet 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "appc-lateralitaet" 
* url = "https://termgit.elga.gv.at/CodeSystem/appc-lateralitaet" 
* name = "appc-lateralitaet" 
* title = "APPC_Lateralitaet" 
* status = #active 
* content = #complete 
* version = "1.0.1+20230925" 
* description = "**Description:** Code List for all APPC-Codes for 2nd Axis: Laterality  **Beschreibung:** Code Liste aller Codes der 2. APPC-Achse: Lateralität" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.38.2" 
* date = "2023-09-25" 
* publisher = "see" 
* contact[0].name = "http://www.bura.at/" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/" 
* count = 6 
* concept[0].code = #0
* concept[0].display = "Lateralitaet unbestimmt"
* concept[1].code = #1
* concept[1].display = "rechts"
* concept[2].code = #2
* concept[2].display = "links"
* concept[3].code = #3
* concept[3].display = "beidseits"
* concept[4].code = #4
* concept[4].display = "unpaariges Organ"
* concept[5].code = #5
* concept[5].display = "Atypische Situation ( - Transplantat etc.)"
