Instance: appc-lateralitaet 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "appc-lateralitaet" 
* url = "https://termgit.elga.gv.at/CodeSystem/appc-lateralitaet" 
* name = "appc-lateralitaet" 
* title = "APPC_Lateralitaet" 
* status = #active 
* content = #complete 
* version = "1.0.1+20230925" 
* description = "**Description:** Code List for all APPC-Codes for 2nd Axis: Laterality  **Beschreibung:** Code Liste aller Codes der 2. APPC-Achse: Lateralität" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.38.2" 
* date = "2023-09-25" 
* publisher = "see" 
* contact[0].name = "http://www.bura.at/" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/" 
* count = 6 
* #0 "Lateralitaet unbestimmt"
* #1 "rechts"
* #2 "links"
* #3 "beidseits"
* #4 "unpaariges Organ"
* #5 "Atypische Situation ( - Transplantat etc.)"
