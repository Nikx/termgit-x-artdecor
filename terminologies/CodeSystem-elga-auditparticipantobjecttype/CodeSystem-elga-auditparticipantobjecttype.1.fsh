Instance: elga-auditparticipantobjecttype 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-auditparticipantobjecttype" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjecttype" 
* name = "elga-auditparticipantobjecttype" 
* title = "ELGA_AuditParticipantObjectType" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "Codeliste der ELGA spezifischen Audit Participant Object Type Codes. Der Audit Participant Object Type beschreibt die Art eines Objekts, welches im Audit Event referenziert ist." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.153" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 5 
* concept[0].code = #0
* concept[0].display = "ELGA Objekt"
* concept[1].code = #100
* concept[1].display = "Policy"
* concept[2].code = #110
* concept[2].display = "Kontaktbestätigung"
* concept[3].code = #120
* concept[3].display = "Security Token"
* concept[4].code = #130
* concept[4].display = "Vollmacht"
