Instance: elga-sections 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-sections" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-sections" 
* name = "elga-sections" 
* title = "ELGA_Sections" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "**Description:** Set of codes defining the sections and their labeling (displayed as the title of the section) permitted by ELGA

**Beschreibung:** Auflistung der Codes für erlaubte Sektionen in ELGA und ihre Bezeichnungen (Titelzeile)." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.40" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 34 
* property[0].code = #hints 
* property[0].type = #string 
* #ABBEM "Abschließende Bemerkungen"
* #ABBEM ^property[0].code = #hints 
* #ABBEM ^property[0].valueString = "Titel der Sektion wird vom ELGA-Referenzstylesheet nicht dargestellt" 
* #ANGEFUNTERS "Angeforderte Untersuchungen"
* #ANM "Anmerkungen"
* #BEFAUS "Ausstehende Befunde"
* #BEFBEI "Beigelegte erhobene Befunde"
* #BEFERH "Auszüge aus erhobenen Befunden"
* #BEIL "Beilagen"
* #BRIEFT "Brieftext"
* #BRIEFT ^property[0].code = #hints 
* #BRIEFT ^property[0].valueString = "Titel der Sektion wird vom ELGA-Referenzstylesheet nicht dargestellt" 
* #FachAnamnse "Fachspezifische Anamnese"
* #FachDiagnostik "Fachspezifische Diagnostik"
* #INFEKTSER "Infektionsserologie"
* #LISTGDA "Liste der behandelnden GDA"
* #MELDEPERR "Meldepflichtige Erreger"
* #MOLEKERN "Molekularer Erregernachweis"
* #OPBER "Operationsbericht"
* #OUTCOMEMEAS "Outcome Measurement"
* #PFATM "Atmung"
* #PFAUS "Ausscheidung"
* #PFDIAG "Pflege- und Betreuungsdiagnosen"
* #PFERN "Ernährung"
* #PFHAUT "Hautzustand"
* #PFKLEI "Körperpflege und Kleiden"
* #PFKOMM "Kommunikation"
* #PFMED "Medikamentenverabreichung"
* #PFMEDBEH "Pflegerelevante Informationen zur medizinischen Behandlung"
* #PFMOB "Mobilität"
* #PFORIE "Orientierung und Bewusstseinslage"
* #PFROLL "Rollenwahrnehmung und Sinnfindung"
* #PFSCHL "Schlaf"
* #PFSOZV "Soziale Umstände und Verhalten"
* #PUBUMF "Pflege- und Betreuungsumfang"
* #PUBUMF ^definition = Alle Informationen zur Beschreibung des Pflege- und Betreuungsumfangs
* #REHAZIELE "Rehabilitationsziele"
* #RES "Hilfsmittel und Ressourcen"
* #TERMIN "Termine, Kontrollen, Wiederbestellung"
