Instance: elga-actcode 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-actcode" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-actcode" 
* name = "elga-actcode" 
* title = "ELGA_ActCode" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "ELGA_Codeliste für Act Codes zur Verwendung in der e-Medikation" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.103" 
* date = "2015-03-31" 
* count = 5 
* concept[0].code = #ALTEIN
* concept[0].display = "Informationen zur alternativen Einnahme"
* concept[1].code = #ARZNEIINFO
* concept[1].display = "Informationen zur Arznei"
* concept[2].code = #ERGINFO
* concept[2].display = "Ergänzende Informationen zur Abgabe"
* concept[3].code = #MAGZUB
* concept[3].display = "Ergänzende Informationen zur magistralen Zubereitung"
* concept[4].code = #ZINFO
* concept[4].display = "Zusatzinformationen für den Patienten"
