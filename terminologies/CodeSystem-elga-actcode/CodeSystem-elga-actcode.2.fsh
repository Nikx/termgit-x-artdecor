Instance: elga-actcode 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-actcode" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-actcode" 
* name = "elga-actcode" 
* title = "ELGA_ActCode" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "ELGA_Codeliste für Act Codes zur Verwendung in der e-Medikation" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.103" 
* date = "2015-03-31" 
* count = 5 
* #ALTEIN "Informationen zur alternativen Einnahme"
* #ARZNEIINFO "Informationen zur Arznei"
* #ERGINFO "Ergänzende Informationen zur Abgabe"
* #MAGZUB "Ergänzende Informationen zur magistralen Zubereitung"
* #ZINFO "Zusatzinformationen für den Patienten"
