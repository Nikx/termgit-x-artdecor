Instance: elga-practicesetting 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-practicesetting" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-practicesetting" 
* name = "elga-practicesetting" 
* title = "ELGA_PracticeSetting" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "**Description:** Describes the assignment of a document to a medical area. These codes are used within XDS Metadata attribute ''practiceSettingCode''.      Terminologies are inherited from ''Österreichische Strukturplan Gesundheit'' as far as possilbe. Opposite to this piece of work, some concepts are newly introduced. Where differentiation was reasonable, concepts were conducted in more detail. More concepts will be added if necessary, especially for physiotherapy, ergotherapy a.s.f.. A CDA implementation guide may specify the use of this codes.

**Beschreibung:** Beschreibt die fachlliche Zuordnung eines Dokumentes in ELGA. Verwendung in XDS Metadaten im Attribut ''practiceSettingCode''.      Die Benennungen sind soweit möglich aus der Nomenklatur des Österreichischen Strukturplan Gesundheit (ÖSG) übernommen. Gegenüber diesem Codewerk sind einige Konzepte neu hinzugefügt worden. Wo Unterscheidungen sinnvoll waren, wurden Konzepte auch genauer ausgeführt. Weitere Konzepte werden hinzugefügt, wenn Bedarf durch weitere CDA-Dokumentenklassen gegeben sein wird, zB für nicht-ärztliche Gesundheitsberufe wie Physiotherapie, Ergotherapie etc.     Ein CDA-Implementierungsleitfaden kann die Verwendung der möglichen PracticeSettingCodes vorschreiben.     Ein Mapping zu den LKF Funktionscodes ist angegeben, wenn möglich." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.12" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 43 
* property[0].code = #Relationships 
* property[0].type = #string 
* #F001 "Allgemeinmedizin"
* #F001 ^definition = Dokumente aus Allgemeinmedizinischen Organisationen
* #F001 ^property[0].code = #Relationships 
* #F001 ^property[0].valueString = "LKF Funktionscode: 1- 91 11" 
* #F002 "Anästhesiologie und Intensivmedizin"
* #F002 ^property[0].code = #Relationships 
* #F002 ^property[0].valueString = "LKF Funktionscode: 1- 71 --" 
* #F005 "Augenheilkunde"
* #F005 ^property[0].code = #Relationships 
* #F005 ^property[0].valueString = "LKF Funktionscode: 1- 41 --" 
* #F006 "Blutgruppenserologie und Transfusionsmedizin"
* #F006 ^property[0].code = #Relationships 
* #F006 ^property[0].valueString = "LKF Funktionscode: 1- 71 12" 
* #F007 "Chirurgie"
* #F007 ^definition = Chirurgie (inklusive Gefäß- und Transplantationschirugie)
* #F007 ^property[0].code = #Relationships 
* #F007 ^property[0].valueString = "LKF Funktionscode: 1- 21 -- (nicht 1- 21 14, 1- 21 16)" 
* #F010 "Gynäkologie und Geburtshilfe"
* #F010 ^definition = Frauenheilkunde (Gynäkologie), Geburtshilfe, inklusive Hebammentätigkeit, Wochenbettbetreuung, Schwangerenvorsorge
* #F010 ^property[0].code = #Relationships 
* #F010 ^property[0].valueString = "LKF Funktionscode: 1- 31 --             LKF Funktionscode: 1- 32 --             LKF Funktionscode: 1- 33 --" 
* #F014 "Hals-, Nasen- und Ohrenheilkunde"
* #F014 ^definition = Hals-, Nasen- und Ohrenkrankheiten
* #F014 ^property[0].code = #Relationships 
* #F014 ^property[0].valueString = "LKF Funktionscode: 1- 42 --" 
* #F015 "Dermatologie"
* #F015 ^definition = Haut- und Geschlechtskrankheiten
* #F015 ^property[0].code = #Relationships 
* #F015 ^property[0].valueString = "LKF Funktionscode: 1- 45 --" 
* #F016 "Mikrobiologie"
* #F016 ^definition = Dokumente aus dem Bereich mikrobiologisch-serologische Labordiagnostik
* #F016 ^property[0].code = #Relationships 
* #F016 ^property[0].valueString = "LKF Funktionscode: 1- 83 --             LKF Funktionscode: 1- -- 79" 
* #F019 "Innere Medizin"
* #F019 ^property[0].code = #Relationships 
* #F019 ^property[0].valueString = "LKF Funktionscode: 1- 11 --" 
* #F023 "Interdisziplinärer Bereich"
* #F023 ^definition = Interdisziplinärer Bereich  (Eingeschränkter Geltungsbereich, nur wenn in einem CDA-Leitfaden vorgesehen)
* #F023 ^property[0].code = #Relationships 
* #F023 ^property[0].valueString = "LKF Funktionscode: 1- 91 --" 
* #F025 "Kinder- und Jugendpsychiatrie"
* #F025 ^property[0].code = #Relationships 
* #F025 ^property[0].valueString = "LKF Funktionscode: 1- 64 --" 
* #F026 "Kinder- und Jugendchirurgie"
* #F026 ^definition = Kinder-Chirurgie
* #F026 ^property[0].code = #Relationships 
* #F026 ^property[0].valueString = "LKF Funktionscode: 1- 27 --" 
* #F027 "Kinder- und Jugendheilkunde"
* #F027 ^definition = Kinderheilkunde
* #F027 ^property[0].code = #Relationships 
* #F027 ^property[0].valueString = "LKF Funktionscode: 1- 51 --" 
* #F028 "Labordiagnostik"
* #F028 ^definition = Dokumente aus dem Bereich medizinisch-chemische Labordiagnostik
* #F028 ^property[0].code = #Relationships 
* #F028 ^property[0].valueString = "LKF Funktionscode: 1- 82 --" 
* #F029 "Mund-, Kiefer- und Gesichtschirurgie"
* #F029 ^property[0].code = #Relationships 
* #F029 ^property[0].valueString = "LKF Funktionscode: 1- 24 --" 
* #F031 "Neurochirurgie"
* #F031 ^property[0].code = #Relationships 
* #F031 ^property[0].valueString = "LKF Funktionscode: 1- 25 --" 
* #F032 "Neurologie"
* #F032 ^definition = Neurologie, Inkl. Stroke Unit sowie Neurologie Phase B, C
* #F032 ^property[0].code = #Relationships 
* #F032 ^property[0].valueString = "LKF Funktionscode: 1- 63 --" 
* #F033 "Nuklearmedizin"
* #F033 ^property[0].code = #Relationships 
* #F033 ^property[0].valueString = "LKF Funktionscode: 1- 75 --" 
* #F035 "Orthopädie und orthopädische Chirurgie"
* #F035 ^property[0].code = #Relationships 
* #F035 ^property[0].valueString = "LKF Funktionscode: 1- 23 --" 
* #F036 "Palliativmedizin"
* #F036 ^definition = Dokumente der palliativ-medizinischen Versorgung
* #F036 ^property[0].code = #Relationships 
* #F036 ^property[0].valueString = "LKF Funktionscode: 1- 91 37" 
* #F037 "Pathologie"
* #F037 ^property[0].code = #Relationships 
* #F037 ^property[0].valueString = "LKF Funktionscode: 1- 81 --" 
* #F040 "Physikalische Medizin und Rehabilitation"
* #F040 ^definition = Physikalische Medizin
* #F040 ^property[0].code = #Relationships 
* #F040 ^property[0].valueString = "LKF Funktionscode: 1- 78 --" 
* #F041 "Plastische Chirurgie"
* #F041 ^property[0].code = #Relationships 
* #F041 ^property[0].valueString = "LKF Funktionscode: 1- 26 --" 
* #F042 "Psychiatrie"
* #F042 ^property[0].code = #Relationships 
* #F042 ^property[0].valueString = "LKF Funktionscode: 1- 62 --" 
* #F043 "Pulmologie"
* #F043 ^definition = Pulmologie (Lungenheilkunde)
* #F043 ^property[0].code = #Relationships 
* #F043 ^property[0].valueString = "LKF Funktionscode: 1- 12 --" 
* #F044 "Radiologie"
* #F044 ^property[0].code = #Relationships 
* #F044 ^property[0].valueString = "LKF Funktionscode: 1- 72 --" 
* #F048 "Remobilisation/Nachsorge"
* #F048 ^property[0].code = #Relationships 
* #F048 ^property[0].valueString = "LKF Funktionscode: 1- 91 35" 
* #F052 "Unfallchirurgie"
* #F052 ^property[0].code = #Relationships 
* #F052 ^property[0].valueString = "LKF Funktionscode: 1- 22 --" 
* #F053 "Urologie"
* #F053 ^property[0].code = #Relationships 
* #F053 ^property[0].valueString = "LKF Funktionscode: 1- 43 --" 
* #F055 "Zahn-, Mund- und Kieferheilkunde"
* #F055 ^property[0].code = #Relationships 
* #F055 ^property[0].valueString = "LKF Funktionscode: 1- 48 --" 
* #F056 "Akutgeriatrie/Remobilisation"
* #F056 ^definition = Dokumente aus dem Bereich der Akutgeriatrie und Remobilisation
* #F056 ^property[0].code = #Relationships 
* #F056 ^property[0].valueString = "LKF Funktionscode: 1- 11 36             LKF Funktionscode: 1- 63 36" 
* #F057 "Gesundheits- und Krankenpflege"
* #F057 ^definition = Gesundheits- und Krankenpflegerische Dokumente (nicht zu verwenden für Dokumente aus dem Krankenhausbereich)
* #F058 "Herzchirurgie"
* #F058 ^property[0].code = #Relationships 
* #F058 ^property[0].valueString = "LKF Funktionscode: 1- 21 14" 
* #F059 "Klinische Psychologie"
* #F059 ^property[0].code = #Relationships 
* #F059 ^property[0].valueString = "LKF Funktionscode: 1- 62 17" 
* #F060 "Kur- und Prävention"
* #F061 "Psychosomatik"
* #F061 ^property[0].code = #Relationships 
* #F061 ^property[0].valueString = "LKF Funktionscode: 1- -- 68" 
* #F062 "Radioonkologie"
* #F062 ^definition = Dokumente aus Strahlentherapie und Radioonkologie
* #F062 ^property[0].code = #Relationships 
* #F062 ^property[0].valueString = "LKF Funktionscode: 1- 72 12             LKF Funktionscode: 1- 72 58" 
* #F063 "Rechtliche Dokumente"
* #F063 ^definition = Rechtliche Dokumente wie Patientenverfügungen
* #F064 "Thoraxchirurgie"
* #F064 ^property[0].code = #Relationships 
* #F064 ^property[0].valueString = "LKF Funktionscode: 1- 21 16" 
* #F065 "Patienten Verwaltung"
* #F065 ^definition = Dokumente aus dem Bereich der Patienten Verwaltung (Administration)
* #F066 "Krankenhauspflege"
* #F066 ^definition = Dokumente aus dem pflegerischen Bereich im Krankenhaus (nur zu verwenden, wenn nicht eine spezifischere Zuordnung zu einem anderen Fachbereich möglich ist)
* #F067 "Orthopädie und Traumatologie"
* #F067 ^property[0].code = #Relationships 
* #F067 ^property[0].valueString = "LKF Funktionscode: 1- 30 --" 
