Instance: eimpf-zusatzklassifikation-impfprogramm 
InstanceOf: ValueSet 
Usage: #definition 
* id = "eimpf-zusatzklassifikation-impfprogramm" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/eimpf-zusatzklassifikation-impfprogramm" 
* name = "eimpf-zusatzklassifikation-impfprogramm" 
* title = "eImpf_Zusatzklassifikation_Impfprogramm" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "**Description:** Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).  **Beschreibung:** Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.78" 
* date = "2024-03-25" 
* compose.inactive = "True" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* compose.include[0].version = "2.14.0+20240201"
* compose.include[0].concept[0].code = "IS010"
* compose.include[0].concept[0].display = "Kostenfreies Impfprogramm"
* compose.include[0].concept[1].code = "IS011"
* compose.include[0].concept[1].display = "Öffentliches Impfprogramm (ÖIP) Influenza"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[0].value = "DEPRECATED" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[0].version = "2.14.0+20240201"
* expansion.contains[0].code = #IS010
* expansion.contains[0].display = "Kostenfreies Impfprogramm"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/eimpf-ergaenzung"
* expansion.contains[1].version = "2.14.0+20240201"
* expansion.contains[1].inactive = true
* expansion.contains[1].code = #IS011
* expansion.contains[1].display = "Öffentliches Impfprogramm (ÖIP) Influenza"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].designation[0].value = "DEPRECATED" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.183"
