Instance: elga-lebensmittelallergene 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-lebensmittelallergene" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-lebensmittelallergene" 
* name = "elga-lebensmittelallergene" 
* title = "ELGA-Lebensmittelallergene" 
* status = #active 
* content = #complete 
* version = "2.0.0+20230131" 
* description = "Liste von Stoffen oder Erzeugnissen, die Allergien oder Unverträglichkeiten auslösen (Quelle: https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:02011R1169-20180101&from=DE#E0017)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.215" 
* date = "2022-11-02" 
* publisher = "ELGA GmbH" 
* count = 14 
* #EU0001 "Glutenhaltiges Getreide"
* #EU0001 ^definition = Glutenhaltiges Getreide, namentlich Weizen (wie Dinkel und Khorasan-Weizen), Roggen, Gerste, Hafer oder Hybridstämme davon, sowie daraus hergestellte Erzeugnisse, ausgenommen
a) Glukosesirupe auf Weizenbasis einschließlich Dextrose und daraus gewonnene Erzeugnisse, soweit das Verfahren, das sie durchlaufen haben, die Allergenität, die von der EFSA für das entsprechende Erzeugnis ermittelt wurde, aus dem sie gewonnen wurden, wahrscheinlich nicht erhöht,
b) Maltodextrine auf Weizenbasis und daraus gewonnene Erzeugnisse, soweit das Verfahren, das sie durchlaufen haben, die Allergenität, die von der EFSA für das entsprechende Erzeugnis ermittelt wurde, aus dem sie gewonnen wurden, wahrscheinlich nicht erhöht,
c) Glukosesirupe auf Gerstenbasis,
d) Getreide zur Herstellung von alkoholischen Destillaten einschließlich Ethylalkohol landwirtschaftlichen Ursprungs
* #EU0002 "Krebstiere"
* #EU0002 ^definition = Krebstiere und daraus gewonnene Erzeugnisse
* #EU0003 "Eier"
* #EU0003 ^definition = Eier und daraus gewonnene Erzeugnisse
* #EU0004 "Fische"
* #EU0004 ^definition = Fische und daraus gewonnene Erzeugnisse, außer
a) Fischgelatine, die als Trägerstoff für Vitamin- oder Karotinoidzubereitungen verwendet wird,
b) Fischgelatine oder Hausenblase, die als Klärhilfsmittel in Bier und Wein verwendet wird
* #EU0005 "Erdnüsse"
* #EU0005 ^definition = Erdnüsse und daraus gewonnene Erzeugnisse
* #EU0006 "Sojabohnen"
* #EU0006 ^definition = Sojabohnen und daraus gewonnene Erzeugnisse, außer
a) vollständig raffiniertes Sojabohnenöl und -fett und daraus gewonnene Erzeugnisse, soweit das Verfahren, das sie durchlaufen haben, die Allergenität, die von der EFSA für das entsprechende Erzeugnis ermittelt wurde, aus dem sie gewonnen wurden, wahrscheinlich nicht erhöht,
b) natürliche gemischte Tocopherole (E306), natürliches D-alpha-Tocopherol, natürliches D-alpha-Tocopherolacetat, natürliches D-alpha-Tocopherolsukzinat aus Sojabohnenquellen,
c) aus pflanzlichen Ölen gewonnene Phytosterine und Phytosterinester aus Sojabohnenquellen,
d) aus Pflanzenölsterinen gewonnene Phytostanolester aus Sojabohnenquellen
* #EU0007 "Milch"
* #EU0007 ^definition = Milch und daraus gewonnene Erzeugnisse (einschließlich Laktose), außer
a) Molke zur Herstellung von alkoholischen Destillaten einschließlich Ethylalkohol landwirtschaftlichen Ursprungs; 
b) Lactit
* #EU0008 "Schalenfrüchte"
* #EU0008 ^definition = Schalenfrüchte, namentlich Mandeln (Amygdalus communis L.), Haselnüsse (Corylus avellana), Walnüsse (Juglans regia), Kaschunüsse (Anacardium occidentale), Pecannüsse (Carya illinoiesis (Wangenh.) K. Koch), Paranüsse (Bertholletia excelsa), Pistazien (Pistacia vera), Macadamia- oder Queenslandnüsse (Macadamia ternifolia) sowie daraus gewonnene Erzeugnisse, außer Nüssen zur Herstellung von alkoholischen Destillaten einschließlich Ethylalkohol landwirtschaftlichen Ursprungs
* #EU0009 "Sellerie"
* #EU0009 ^definition = Sellerie und daraus gewonnene Erzeugnisse
* #EU0010 "Senf"
* #EU0010 ^definition = Senf und daraus gewonnene Erzeugnisse
* #EU0011 "Sesamsamen"
* #EU0011 ^definition = Sesamsamen und daraus gewonnene Erzeugnisse
* #EU0012 "Schwefeldioxid und Sulphite"
* #EU0012 ^definition = Schwefeldioxid und Sulphite in Konzentrationen von mehr als 10 mg/kg oder 10 mg/l als insgesamt vorhandenes SO 2 , die für verzehrfertige oder gemäß den Anweisungen des Herstellers in den ursprünglichen Zustand zurückgeführte Erzeugnisse zu berechnen sind
* #EU0013 "Lupinen"
* #EU0013 ^definition = Lupinen und daraus gewonnene Erzeugnisse
* #EU0014 "Weichtiere"
* #EU0014 ^definition = Weichtiere und daraus gewonnene Erzeugnisse
