<ClaML version="2.0.0">
    <Meta name="titleLong" value="ELGA_Dosisparameter"/>
    <Meta name="resource" value="ValueSet"/>
    <Meta name="id" value="elga-dosisparameter"/>
    <Meta name="profile" value="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset"/>
    <Meta name="url" value="https://termgit.elga.gv.at/ValueSet/elga-dosisparameter"/>
    <Meta name="preliminary" value="false"/>
    <Meta name="status" value="active"/>
    <Meta name="description" value="Codes für Dosisparameter zur Ermittlung der Patientendosis"/>
    <Meta name="description_eng" value="Codes für Dosisparameter zur Ermittlung der Patientendosis"/>
    <Meta name="count" value="6"/>
    <Meta name="extension" value="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid|oid"/>
    <Identifier uid="1.2.40.0.34.10.166"/>
    <Title name="elga-dosisparameter" version="1.2.0+20240411" date="2024-04-11">ELGA_Dosisparameter</Title>
    <ClassKinds>
        <ClassKind name="code"/>
    </ClassKinds>
    <RubricKinds>
        <RubricKind name="preferred"/>
    </RubricKinds>
    <Class code="111636">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
        <Meta name="codeSystemVersion" value="1.0.0+20230131"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Eingangsdosis"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.840.10008.2.16.4"/>
        <Rubric kind="preferred">
            <Label>Entrance Exposure at RP</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Eingangsdosis</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_codiert^einheit_codiert^|mGy</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_print^einheit_print^|mGy</Label>
        </Rubric>
    </Class>
    <Class code="111637">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
        <Meta name="codeSystemVersion" value="1.0.0+20230131"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Mittlere Parenchymdosis (AGD)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.840.10008.2.16.4"/>
        <Rubric kind="preferred">
            <Label>Accumulated Average Glandular Dose</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Mittlere Parenchymdosis (AGD)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_codiert^einheit_codiert^|mGy</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_print^einheit_print^|mGy</Label>
        </Rubric>
    </Class>
    <Class code="113507">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
        <Meta name="codeSystemVersion" value="1.0.0+20230131"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Applizierte Aktivität"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.840.10008.2.16.4"/>
        <Rubric kind="preferred">
            <Label>Administered activity</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Applizierte Aktivität</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_codiert^einheit_codiert^|MBq</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_print^einheit_print^|MBq</Label>
        </Rubric>
    </Class>
    <Class code="113722">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
        <Meta name="codeSystemVersion" value="1.0.0+20230131"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Dosisflächenprodukt (DAP)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.840.10008.2.16.4"/>
        <Rubric kind="preferred">
            <Label>Dose Area Product Total</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Dosisflächenprodukt (DAP)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_codiert^einheit_codiert^|cGy.cm2</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_print^einheit_print^|cGycm²</Label>
        </Rubric>
    </Class>
    <Class code="113813">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
        <Meta name="codeSystemVersion" value="1.0.0+20230131"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Dosislängenprodukt (DLP)"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.840.10008.2.16.4"/>
        <Rubric kind="preferred">
            <Label>CT Dose Length Product Total</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Dosislängenprodukt (DLP)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_codiert^einheit_codiert^|mGy.cm</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_print^einheit_print^|mGycm</Label>
        </Rubric>
    </Class>
    <Class code="113839">
        <Meta name="codeSystem" value="https://termgit.elga.gv.at/CodeSystem/dcm"/>
        <Meta name="codeSystemVersion" value="1.0.0+20230131"/>
        <Meta name="TS_ATTRIBUTE_MEANING" value="Effektive Dosis"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Meta name="http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid" value="urn:oid:1.2.840.10008.2.16.4"/>
        <Rubric kind="preferred">
            <Label>Effective Dose</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|Effektive Dosis</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_codiert^einheit_codiert^|mSv</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>|https://termgit.elga.gv.at/CodeSystem/austrian-designation-use^^einheit_print^einheit_print^|mSv</Label>
        </Rubric>
    </Class>
</ClaML>
