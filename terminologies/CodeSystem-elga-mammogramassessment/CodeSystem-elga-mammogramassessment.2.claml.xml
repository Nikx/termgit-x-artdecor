<ClaML version="2.0.0">
    <Meta name="titleLong" value="ELGA_MammogramAssessment"/>
    <Meta name="resource" value="CodeSystem"/>
    <Meta name="id" value="elga-mammogramassessment"/>
    <Meta name="url" value="https://termgit.elga.gv.at/CodeSystem/elga-mammogramassessment"/>
    <Meta name="preliminary" value="false"/>
    <Meta name="status" value="active"/>
    <Meta name="description_eng" value="Definition of the values for a mammogram according DICOM Coding Scheme 'BI' from 'Digital Imaging and Communications in Medicine (DICOM) Part 16: Content Mapping Resource'"/>
    <Meta name="description" value="Definition der Werte für ein Mammogramm entsprechend DICOM Coding Scheme 'BI' aus 'Digital Imaging and Communications in Medicine (DICOM) Part 16: Content Mapping Resource'"/>
    <Meta name="content" value="complete"/>
    <Meta name="website" value="https://www.elga.gv.at"/>
    <Meta name="count" value="10"/>
    <Meta name="property" value="child|||code"/>
    <Meta name="property" value="parent|||code"/>
    <Identifier uid="1.2.40.0.34.5.49"/>
    <Title name="elga-mammogramassessment" version="1.0.1+20240129" date="2024-01-29">ELGA_MammogramAssessment</Title>
    <Authors>
        <Author name="">url^https://www.elga.gv.at^^^^</Author>
    </Authors>
    <ClassKinds>
        <ClassKind name="code"/>
    </ClassKinds>
    <RubricKinds>
        <RubricKind name="preferred"/>
    </RubricKinds>
    <Class code="II.AC.a">
        <Meta name="TS_ATTRIBUTE_MEANING" value="0  braucht zusätzliche Bildgebung"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>0 - Need additional imaging evaluation</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>0   braucht zusätzliche Bildgebung</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|0  braucht zusätzliche Bildgebung</Label>
        </Rubric>
    </Class>
    <Class code="II.AC.b.1">
        <Meta name="TS_ATTRIBUTE_MEANING" value="1  Negativ"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>1  Negative</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>1  negativ (d.h. keine Masse, keine Architekturstörung, kein Kalk, keine Hautverdickung etc.)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|1  Negativ</Label>
        </Rubric>
    </Class>
    <Class code="II.AC.b.2">
        <Meta name="TS_ATTRIBUTE_MEANING" value="2  Benigner Befund"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>2  Benign Finding</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>2  benigne Befunde (z.B. Zysten, intramammäre Lymphknoten, Brustimplantate, stationäre postoperative Befunde, stationäre Fibroadenome)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|2  Benigner Befund</Label>
        </Rubric>
    </Class>
    <Class code="II.AC.b.3">
        <Meta name="TS_ATTRIBUTE_MEANING" value="3  Wahrscheinlich benigner Befund, kurzfristige Verlaufskontrolle empfohlen"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>3 - Probably Benign Finding  short interval followup</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>3  Wahrscheinlich benigne Befunde, kurzfristige Verlaufskontrolle empfohlen (Läsionen mit scharfen Rändern, ovalärer Form, horizontaler Orientierung, Malignitätsrisiko unter 2 %.)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|3  Wahrscheinlich benigner Befund, kurzfristige Verlaufskontrolle empfohlen</Label>
        </Rubric>
    </Class>
    <Class code="II.AC.b.4">
        <Meta name="TS_ATTRIBUTE_MEANING" value="4  Suspekter Befund, Biopsie sollte erwogen werden"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="false"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="S"/>
        <SubClass code="MA.II.A.5.4A"/>
        <SubClass code="MA.II.A.5.4B"/>
        <SubClass code="MA.II.A.5.4C"/>
        <Rubric kind="preferred">
            <Label>4 - Suspicious abnormality, biopsy should be considered</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>4  Verdächtig, Biopsie sollte erwogen werden (Läsionen mit intermediärer Wahrscheinlichkeit für Malignität)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|4  Suspekter Befund, Biopsie sollte erwogen werden</Label>
        </Rubric>
    </Class>
    <Class code="MA.II.A.5.4A">
        <Meta name="TS_ATTRIBUTE_MEANING" value="4A  Geringgradiger Verdacht"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="1"/>
        <Meta name="Type" value="L"/>
        <SuperClass code="II.AC.b.4"/>
        <Rubric kind="preferred">
            <Label>4A  Low suspicion</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>4A  Geringgradig verdächtiger Befund, Biopsie sollte erwogen werden</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|4A  Geringgradiger Verdacht</Label>
        </Rubric>
    </Class>
    <Class code="MA.II.A.5.4B">
        <Meta name="TS_ATTRIBUTE_MEANING" value="4B  Zwischenstuflicher Verdacht"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="1"/>
        <Meta name="Type" value="L"/>
        <SuperClass code="II.AC.b.4"/>
        <Rubric kind="preferred">
            <Label>4B  Intermediate suspicion</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>4B  Zwischenstuflich verdächtiger Befund, Biopsie sollte erwogen werden</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|4B  Zwischenstuflicher Verdacht</Label>
        </Rubric>
    </Class>
    <Class code="MA.II.A.5.4C">
        <Meta name="TS_ATTRIBUTE_MEANING" value="4C  Mäßiger Verdacht"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="1"/>
        <Meta name="Type" value="L"/>
        <SuperClass code="II.AC.b.4"/>
        <Rubric kind="preferred">
            <Label>4C  Moderate suspicion</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>4C  Mäßig verdächtiger Befund, Biopsie sollte erwogen werden</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|4C  Mäßiger Verdacht</Label>
        </Rubric>
    </Class>
    <Class code="II.AC.b.5">
        <Meta name="TS_ATTRIBUTE_MEANING" value="5  Hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>5 - Highly suggestive of malignancy, take appropriate action</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>5  hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen (Risiko auf Malignität von 95 % oder größer)</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|5  Hochsuspekt, fast sicher maligne, eine adäquate Aktion sollte folgen</Label>
        </Rubric>
    </Class>
    <Class code="MA.II.A.5.6">
        <Meta name="TS_ATTRIBUTE_MEANING" value="6  Gesichertes Malignom"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>6 - Known biopsy proven malignancy</Label>
        </Rubric>
        <Rubric kind="note">
            <Label>6  Bekannter maligner Befund, mit Biopsie bestätigt</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>de-AT|^^^^|6  Gesichertes Malignom</Label>
        </Rubric>
    </Class>
</ClaML>
