Instance: elga-e-health-anwendungen 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-e-health-anwendungen" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-e-health-anwendungen" 
* name = "elga-e-health-anwendungen" 
* title = "ELGA_e-Health_Anwendungen" 
* status = #active 
* content = #complete 
* version = "2.0.1+20240129" 
* description = "ELGA Codeliste für autonome ELGA-Anwendungen und Services (relevant für das Berechtigungssystem)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.159" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 12 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* #100 "ELGA Anwendungen"
* #100 ^property[0].code = #child 
* #100 ^property[0].valueCode = #101 
* #100 ^property[1].code = #child 
* #100 ^property[1].valueCode = #102 
* #101 "E-Befunde"
* #101 ^property[0].code = #parent 
* #101 ^property[0].valueCode = #100 
* #102 "E-Medikation"
* #102 ^property[0].code = #parent 
* #102 ^property[0].valueCode = #100 
* #103 "e-Impfpass Anwendung"
* #104 "Patientenverfügung Anwendung"
* #105 "EMS Epidemiologisches Meldesystem/EPI-Service"
* #106 "e-Prescription"
* #107 "National Contact Point for eHealth"
* #150 "Virtuelle Organisation für Testzwecke"
* #151 "PVN Gesunder Tennengau Hallein"
* #152 "PVN Gesunder Tennengau Lammertal"
* #153 "PVN Gesunder Tennengau-Süd"
