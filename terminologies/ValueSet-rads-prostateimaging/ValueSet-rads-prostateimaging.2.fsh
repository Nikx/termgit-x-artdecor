Instance: rads-prostateimaging 
InstanceOf: ValueSet 
Usage: #definition 
* id = "rads-prostateimaging" 
* url = "https://termgit.elga.gv.at/ValueSet/rads-prostateimaging" 
* name = "rads-prostateimaging" 
* title = "RADS_ProstateImaging" 
* status = #draft 
* version = "0.1.0+20240410" 
* description = "**Description:** Draft value set for the normative ballot for the diagnostic imaging report (version 3.0.0). The codes will be replaced by SNOMED CT codes as soon as possible. Note: The Reporting and Data Systems (RADS) assessment scales are a registered trademark of the American College of Radiology.

**Beschreibung:** Value-Set-Entwurf für den normativen Ballot zum Befund bildgebende Diagnostik (Version 3.0.0). Die Codes werden sobald möglich durch SNOMED-CT-Codes ersetzt. Achtung: Die "Reporting and Data Systems (RADS)"-Skalen sind eine eingetragene Marke des American College of Radiology." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.6.0.10.93" 
* date = "2024-04-10" 
* publisher = "see" 
* contact[0].name = "American College of Radiology" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.acr.org/" 
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/ACR"
* compose.include[0].version = "2.0.1"
* compose.include[0].concept[0].code = "PI-RADS1"
* compose.include[0].concept[0].display = "PI-RADS 1"
* compose.include[0].concept[0].designation[0].language = #en-US 
* compose.include[0].concept[0].designation[0].value = "Prostate assessment (Category 1) - Very low (clinically significant cancer is highly unlikely to be present)" 
* compose.include[0].concept[1].code = "PI-RADS2"
* compose.include[0].concept[1].display = "PI-RADS 2"
* compose.include[0].concept[1].designation[0].language = #en-US 
* compose.include[0].concept[1].designation[0].value = "Prostate assessment (Category 2) - Low (clinically significant cancer is unlikely to bepresent)" 
* compose.include[0].concept[2].code = "PI-RADS3"
* compose.include[0].concept[2].display = "PI-RADS 3"
* compose.include[0].concept[2].designation[0].language = #en-US 
* compose.include[0].concept[2].designation[0].value = "Prostate assessment (Category 3) - Intermediate (the presence of clinically significant cancer is equivocal)" 
* compose.include[0].concept[3].code = "PI-RADS4"
* compose.include[0].concept[3].display = "PI-RADS 4"
* compose.include[0].concept[3].designation[0].language = #en-US 
* compose.include[0].concept[3].designation[0].value = "Prostate assessment (Category 4) - High (clinically significant cancer is likely to be present)" 
* compose.include[0].concept[4].code = "PI-RADS5"
* compose.include[0].concept[4].display = "PI-RADS 5"
* compose.include[0].concept[4].designation[0].language = #en-US 
* compose.include[0].concept[4].designation[0].value = "Prostate assessment (Category 5) - Very high (clinically significant cancer is highly likely to be present)" 

* expansion.timestamp = "2024-04-10T08:47:55.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[0].version = "2.0.1"
* expansion.contains[0].code = #PI-RADS1
* expansion.contains[0].display = "PI-RADS 1"
* expansion.contains[0].designation[0].language = #en-US 
* expansion.contains[0].designation[0].value = "Prostate assessment (Category 1) - Very low (clinically significant cancer is highly unlikely to be present)" 
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[1].version = "2.0.1"
* expansion.contains[1].code = #PI-RADS2
* expansion.contains[1].display = "PI-RADS 2"
* expansion.contains[1].designation[0].language = #en-US 
* expansion.contains[1].designation[0].value = "Prostate assessment (Category 2) - Low (clinically significant cancer is unlikely to bepresent)" 
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[2].version = "2.0.1"
* expansion.contains[2].code = #PI-RADS3
* expansion.contains[2].display = "PI-RADS 3"
* expansion.contains[2].designation[0].language = #en-US 
* expansion.contains[2].designation[0].value = "Prostate assessment (Category 3) - Intermediate (the presence of clinically significant cancer is equivocal)" 
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[3].version = "2.0.1"
* expansion.contains[3].code = #PI-RADS4
* expansion.contains[3].display = "PI-RADS 4"
* expansion.contains[3].designation[0].language = #en-US 
* expansion.contains[3].designation[0].value = "Prostate assessment (Category 4) - High (clinically significant cancer is likely to be present)" 
* expansion.contains[4].system = "http://terminology.hl7.org/CodeSystem/ACR"
* expansion.contains[4].version = "2.0.1"
* expansion.contains[4].code = #PI-RADS5
* expansion.contains[4].display = "PI-RADS 5"
* expansion.contains[4].designation[0].language = #en-US 
* expansion.contains[4].designation[0].value = "Prostate assessment (Category 5) - Very high (clinically significant cancer is highly likely to be present)" 
