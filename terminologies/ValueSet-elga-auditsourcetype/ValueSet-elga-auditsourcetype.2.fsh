Instance: elga-auditsourcetype 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-auditsourcetype" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-auditsourcetype" 
* name = "elga-auditsourcetype" 
* title = "ELGA_AuditSourceType" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "Liste der ELGA spezifischen Audit Source Type Codes. Der Audit Source Type Code beschreibt die Art des Systems, welches den Audit Event erstellt hat." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.152" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* compose.include[0].version = "1.0.1+20240129"
* compose.include[0].concept[0].code = "0"
* compose.include[0].concept[0].display = "PAP"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[0].designation[0].value = "ELGA PAP" 
* compose.include[0].concept[1].code = "1"
* compose.include[0].concept[1].display = "KBS"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[1].designation[0].value = "ELGA Kontaktbestätigungsservice" 
* compose.include[0].concept[2].code = "10"
* compose.include[0].concept[2].display = "CDM"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[2].designation[0].value = "Zentrales Content Delete Management Service" 
* compose.include[0].concept[3].code = "11"
* compose.include[0].concept[3].display = "CDD"
* compose.include[0].concept[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[3].designation[0].value = "Dezentraler Content Delete Management Daemon" 
* compose.include[0].concept[4].code = "12"
* compose.include[0].concept[4].display = "e-Medikation"
* compose.include[0].concept[5].code = "13"
* compose.include[0].concept[5].display = "EMS Epidemiologisches Meldesystem/EPI-Service"
* compose.include[0].concept[6].code = "14"
* compose.include[0].concept[6].display = "DSUB Broker"
* compose.include[0].concept[7].code = "2"
* compose.include[0].concept[7].display = "ETS"
* compose.include[0].concept[7].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[7].designation[0].value = "ELGA Token Service" 
* compose.include[0].concept[8].code = "3"
* compose.include[0].concept[8].display = "Policy Repository"
* compose.include[0].concept[9].code = "4"
* compose.include[0].concept[9].display = "Bürgerportal"
* compose.include[0].concept[10].code = "5"
* compose.include[0].concept[10].display = "Policy Administraion"
* compose.include[0].concept[10].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[10].designation[0].value = "Tool für die Verwaltung allgemeiner Policies" 
* compose.include[0].concept[11].code = "6"
* compose.include[0].concept[11].display = "GDA"
* compose.include[0].concept[11].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[11].designation[0].value = "GDA System" 
* compose.include[0].concept[12].code = "7"
* compose.include[0].concept[12].display = "Gateway"
* compose.include[0].concept[12].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[12].designation[0].value = "Zwischengeschaltetes System, Proxy,..." 
* compose.include[0].concept[13].code = "8"
* compose.include[0].concept[13].display = "ZGF"
* compose.include[0].concept[13].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[13].designation[0].value = "Zugriffssteuerungsfassade eines ELGA Bereichs" 
* compose.include[0].concept[14].code = "9"
* compose.include[0].concept[14].display = "A-ARR"
* compose.include[0].concept[14].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* compose.include[0].concept[14].designation[0].value = "Aggregiertes Audit Record Repository" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[0].version = "1.0.1+20240129"
* expansion.contains[0].code = #0
* expansion.contains[0].display = "PAP"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[0].designation[0].value = "ELGA PAP" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[1].version = "1.0.1+20240129"
* expansion.contains[1].code = #1
* expansion.contains[1].display = "KBS"
* expansion.contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[1].designation[0].value = "ELGA Kontaktbestätigungsservice" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[2].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[2].version = "1.0.1+20240129"
* expansion.contains[2].code = #10
* expansion.contains[2].display = "CDM"
* expansion.contains[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[2].designation[0].value = "Zentrales Content Delete Management Service" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[3].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[3].version = "1.0.1+20240129"
* expansion.contains[3].code = #11
* expansion.contains[3].display = "CDD"
* expansion.contains[3].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[3].designation[0].value = "Dezentraler Content Delete Management Daemon" 
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[4].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[4].version = "1.0.1+20240129"
* expansion.contains[4].code = #12
* expansion.contains[4].display = "e-Medikation"
* expansion.contains[4].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[4].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[5].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[5].version = "1.0.1+20240129"
* expansion.contains[5].code = #13
* expansion.contains[5].display = "EMS Epidemiologisches Meldesystem/EPI-Service"
* expansion.contains[5].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[5].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[6].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[6].version = "1.0.1+20240129"
* expansion.contains[6].code = #14
* expansion.contains[6].display = "DSUB Broker"
* expansion.contains[6].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[6].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[7].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[7].version = "1.0.1+20240129"
* expansion.contains[7].code = #2
* expansion.contains[7].display = "ETS"
* expansion.contains[7].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[7].designation[0].value = "ELGA Token Service" 
* expansion.contains[7].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[7].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[8].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[8].version = "1.0.1+20240129"
* expansion.contains[8].code = #3
* expansion.contains[8].display = "Policy Repository"
* expansion.contains[8].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[8].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[9].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[9].version = "1.0.1+20240129"
* expansion.contains[9].code = #4
* expansion.contains[9].display = "Bürgerportal"
* expansion.contains[9].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[9].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[10].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[10].version = "1.0.1+20240129"
* expansion.contains[10].code = #5
* expansion.contains[10].display = "Policy Administraion"
* expansion.contains[10].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[10].designation[0].value = "Tool für die Verwaltung allgemeiner Policies" 
* expansion.contains[10].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[10].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[11].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[11].version = "1.0.1+20240129"
* expansion.contains[11].code = #6
* expansion.contains[11].display = "GDA"
* expansion.contains[11].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[11].designation[0].value = "GDA System" 
* expansion.contains[11].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[11].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[12].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[12].version = "1.0.1+20240129"
* expansion.contains[12].code = #7
* expansion.contains[12].display = "Gateway"
* expansion.contains[12].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[12].designation[0].value = "Zwischengeschaltetes System, Proxy,..." 
* expansion.contains[12].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[12].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[13].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[13].version = "1.0.1+20240129"
* expansion.contains[13].code = #8
* expansion.contains[13].display = "ZGF"
* expansion.contains[13].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[13].designation[0].value = "Zugriffssteuerungsfassade eines ELGA Bereichs" 
* expansion.contains[13].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[13].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
* expansion.contains[14].system = "https://termgit.elga.gv.at/CodeSystem/elga-auditsourcetype"
* expansion.contains[14].version = "1.0.1+20240129"
* expansion.contains[14].code = #9
* expansion.contains[14].display = "A-ARR"
* expansion.contains[14].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#concept_beschreibung "concept_beschreibung" 
* expansion.contains[14].designation[0].value = "Aggregiertes Audit Record Repository" 
* expansion.contains[14].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[14].extension[0].valueOid = "urn:oid:1.2.40.0.34.5.152"
