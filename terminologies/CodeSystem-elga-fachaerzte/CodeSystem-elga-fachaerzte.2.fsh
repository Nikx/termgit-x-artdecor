Instance: elga-fachaerzte 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-fachaerzte" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-fachaerzte" 
* name = "elga-fachaerzte" 
* title = "ELGA_Fachaerzte" 
* status = #active 
* content = #complete 
* version = "1.1.0+20230622" 
* description = "Berufsbezeichnungen für Fachärzte lt. Ärztinnen-/Ärzte-Ausbildungsordnung" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.160" 
* date = "2023-06-22" 
* publisher = "see" 
* contact[0].name = "http://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.elga.gv.at" 
* count = 68 
* property[0].code = #status 
* property[0].type = #code 
* property[1].code = #hints 
* property[1].type = #string 
* #102 "Fachärztin/Facharzt für Anästhesiologie und Intensivmedizin"
* #103 "Fachärztin/Facharzt für Anatomie"
* #104 "Fachärztin/Facharzt für Arbeitsmedizin und angewandte Physiologie"
* #105 "Fachärztin/Facharzt für Augenheilkunde und Optometrie"
* #106 "Fachärztin/Facharzt für Transfusionsmedizin"
* #107 "Fachärztin/Facharzt für Chirurgie"
* #107 ^property[0].code = #status 
* #107 ^property[0].valueCode = #retired 
* #107 ^property[1].code = #hints 
* #107 ^property[1].valueString = "DEPRECATED" 
* #108 "Fachärztin/Facharzt für Frauenheilkunde und Geburtshilfe"
* #109 "Fachärztin/Facharzt für Gerichtsmedizin"
* #110 "Fachärztin/Facharzt für Hals-, Nasen- und Ohrenheilkunde"
* #111 "Fachärztin/Facharzt für Haut- und Geschlechtskrankheiten"
* #112 "Fachärztin/Facharzt für Herzchirurgie"
* #113 "Fachärztin/Facharzt für Histologie, Embryologie und Zellbiologie"
* #114 "Fachärztin/Facharzt für Klinische Mikrobiologie und Hygiene"
* #115 "Fachärztin/Facharzt für Klinische Immunologie"
* #116 "Fachärztin/Facharzt für Innere Medizin"
* #117 "Fachärztin/Facharzt für Kinder- und Jugendchirurgie"
* #118 "Fachärztin/Facharzt für Kinder- und Jugendheilkunde"
* #119 "Fachärztin/Facharzt für Kinder- und Jugendpsychiatrie und Psychotherapeutische Medizin"
* #120 "Fachärztin/Facharzt für Lungenkrankheiten"
* #120 ^property[0].code = #status 
* #120 ^property[0].valueCode = #retired 
* #120 ^property[1].code = #hints 
* #120 ^property[1].valueString = "DEPRECATED" 
* #121 "Fachärztin/Facharzt für Medizinische Biologie"
* #121 ^property[0].code = #status 
* #121 ^property[0].valueCode = #retired 
* #121 ^property[1].code = #hints 
* #121 ^property[1].valueString = "DEPRECATED" 
* #122 "Fachärztin/Facharzt für Medizinische Biophysik"
* #122 ^property[0].code = #status 
* #122 ^property[0].valueCode = #retired 
* #122 ^property[1].code = #hints 
* #122 ^property[1].valueString = "DEPRECATED" 
* #123 "Fachärztin/Facharzt für Medizinische Genetik"
* #124 "Fachärztin/Facharzt für Medizinische und Chemische Labordiagnostik"
* #125 "Fachärztin/Facharzt für Medizinische Leistungsphysiologie"
* #125 ^property[0].code = #status 
* #125 ^property[0].valueCode = #retired 
* #125 ^property[1].code = #hints 
* #125 ^property[1].valueString = "DEPRECATED" 
* #126 "Fachärztin/Facharzt für Mikrobiologisch-Serologische Labordiagnostik"
* #126 ^property[0].code = #status 
* #126 ^property[0].valueCode = #retired 
* #126 ^property[1].code = #hints 
* #126 ^property[1].valueString = "DEPRECATED" 
* #127 "Fachärztin/Facharzt für Mund-, Kiefer- und Gesichtschirurgie"
* #128 "Fachärztin/Facharzt für Neurobiologie"
* #128 ^property[0].code = #status 
* #128 ^property[0].valueCode = #retired 
* #128 ^property[1].code = #hints 
* #128 ^property[1].valueString = "DEPRECATED" 
* #129 "Fachärztin/Facharzt für Neurochirurgie"
* #130 "Fachärztin/Facharzt für Neurologie"
* #131 "Fachärztin/Facharzt für Neurologie und Psychiatrie"
* #131 ^property[0].code = #status 
* #131 ^property[0].valueCode = #retired 
* #131 ^property[1].code = #hints 
* #131 ^property[1].valueString = "DEPRECATED" 
* #132 "Fachärztin/Facharzt für Klinische Pathologie und Neuropathologie"
* #133 "Fachärztin/Facharzt für Nuklearmedizin"
* #134 "Fachärztin/Facharzt für Orthopädie und Traumatologie"
* #135 "Fachärztin/Facharzt für Pathologie"
* #135 ^property[0].code = #status 
* #135 ^property[0].valueCode = #retired 
* #135 ^property[1].code = #hints 
* #135 ^property[1].valueString = "DEPRECATED" 
* #136 "Fachärztin/Facharzt für Pathophysiologie"
* #136 ^property[0].code = #status 
* #136 ^property[0].valueCode = #retired 
* #136 ^property[1].code = #hints 
* #136 ^property[1].valueString = "DEPRECATED" 
* #137 "Fachärztin/Facharzt für Pharmakologie und Toxikologie"
* #138 "Fachärztin/Facharzt für Physikalische Medizin und Allgemeine Rehabilitation"
* #139 "Fachärztin/Facharzt für Physiologie"
* #139 ^property[0].code = #status 
* #139 ^property[0].valueCode = #retired 
* #139 ^property[1].code = #hints 
* #139 ^property[1].valueString = "DEPRECATED" 
* #140 "Fachärztin/Facharzt für Plastische, Rekonstruktive und Ästhetische Chirurgie"
* #141 "Fachärztin/Facharzt für Psychiatrie"
* #141 ^property[0].code = #status 
* #141 ^property[0].valueCode = #retired 
* #141 ^property[1].code = #hints 
* #141 ^property[1].valueString = "DEPRECATED" 
* #142 "Fachärztin/Facharzt für Psychiatrie und Neurologie"
* #142 ^property[0].code = #status 
* #142 ^property[0].valueCode = #retired 
* #142 ^property[1].code = #hints 
* #142 ^property[1].valueString = "DEPRECATED" 
* #143 "Fachärztin/Facharzt für Psychiatrie und Psychotherapeutische Medizin"
* #144 "Fachärztin/Facharzt für Radiologie"
* #145 "Fachärztin/Facharzt für Sozialmedizin"
* #145 ^property[0].code = #status 
* #145 ^property[0].valueCode = #retired 
* #145 ^property[1].code = #hints 
* #145 ^property[1].valueString = "DEPRECATED" 
* #146 "Fachärztin/Facharzt für Klinische Immunologie und Spezifische Prophylaxe und Tropenmedizin"
* #147 "Fachärztin/Facharzt für Strahlentherapie-Radioonkologie"
* #148 "Fachärztin/Facharzt für Theoretische Sonderfächer"
* #148 ^property[0].code = #status 
* #148 ^property[0].valueCode = #retired 
* #148 ^property[1].code = #hints 
* #148 ^property[1].valueString = "DEPRECATED" 
* #149 "Fachärztin/Facharzt für Thoraxchirurgie"
* #150 "Fachärztin/Facharzt für Tumorbiologie"
* #150 ^property[0].code = #status 
* #150 ^property[0].valueCode = #retired 
* #150 ^property[1].code = #hints 
* #150 ^property[1].valueString = "DEPRECATED" 
* #151 "Fachärztin/Facharzt für Unfallchirurgie"
* #151 ^property[0].code = #status 
* #151 ^property[0].valueCode = #retired 
* #151 ^property[1].code = #hints 
* #151 ^property[1].valueString = "DEPRECATED" 
* #152 "Fachärztin/Facharzt für Urologie"
* #153 "Fachärztin/Facharzt für Klinische Mikrobiologie und Virologie"
* #154 "Fachärztin/Facharzt für Zahn-, Mund- und Kieferheilkunde"
* #154 ^property[0].code = #status 
* #154 ^property[0].valueCode = #retired 
* #154 ^property[1].code = #hints 
* #154 ^property[1].valueString = "DEPRECATED" 
* #155 "Fachärztin/Facharzt für Allgemeinchirurgie und Viszeralchirurgie"
* #156 "Fachärztin/Facharzt für Allgemeinchirurgie und Gefäßchirurgie"
* #157 "Fachärztin/Facharzt für Innere Medizin und Angiologie"
* #158 "Fachärztin/Facharzt für Innere Medizin und Endokrinologie und Diabetologie"
* #159 "Fachärztin/Facharzt für Innere Medizin und Gastroenterologie und Hepatologie"
* #160 "Fachärztin/Facharzt für Innere Medizin und Hämatologie und internistische Onkologie"
* #161 "Fachärztin/Facharzt für Innere Medizin und Infektiologie"
* #162 "Fachärztin/Facharzt für Innere Medizin und Intensivmedizin"
* #163 "Fachärztin/Facharzt für Innere Medizin und Kardiologie"
* #164 "Fachärztin/Facharzt für Innere Medizin und Nephrologie"
* #165 "Fachärztin/Facharzt für Innere Medizin und Pneumologie"
* #166 "Fachärztin/Facharzt für Innere Medizin und Rheumatologie"
* #167 "Fachärztin/Facharzt für Klinische Pathologie und Molekularpathologie"
* #168 "Fachärztin/Facharzt für Public Health"
* #169 "Fachärztin/Facharzt für Physiologie und Pathophysiologie"
