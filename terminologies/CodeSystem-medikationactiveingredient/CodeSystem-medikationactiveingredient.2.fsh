Instance: medikationactiveingredient 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "medikationactiveingredient" 
* url = "https://termgit.elga.gv.at/CodeSystem/medikationactiveingredient" 
* name = "medikationactiveingredient" 
* title = "Medikation_ActiveIngredient" 
* status = #active 
* content = #complete 
* version = "1.11.0+20240425" 
* description = "**Description:** Medikation Active Ingredient. Liability and terms of use you can find on this website https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit

**Beschreibung:** Medikation Active Ingredient. Haftung und Nutzungsbestimmungen finden Sie unter folgender Webseite https://www.basg.gv.at/fuer-unternehmen/online-service/nutzungshinweise#termgit" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.221" 
* date = "2024-04-25" 
* count = 2286 
* #100000076084 "D-CAMPHOR"
* #100000076084 ^designation[0].language = #de-AT 
* #100000076084 ^designation[0].value = "D-CAMPHER" 
* #100000076107 "DOXYLAMINE HYDROGEN SUCCINATE"
* #100000076107 ^designation[0].language = #de-AT 
* #100000076107 ^designation[0].value = "DOXYLAMIN HYDROGENSUCCINAT" 
* #100000076110 "SAMBUCI FLOS"
* #100000076118 "EQUISETUM STEM"
* #100000076118 ^designation[0].language = #de-AT 
* #100000076118 ^designation[0].value = "EQUISETI HERBA" 
* #100000076146 "GELATINE"
* #100000076239 "MORPHINE SULFATE"
* #100000076239 ^designation[0].language = #de-AT 
* #100000076239 ^designation[0].value = "MORPHINSULFAT" 
* #100000076242 "MUCOPOLYSACCHARIDE POLYSULFURIC ACID ESTER"
* #100000076242 ^designation[0].language = #de-AT 
* #100000076242 ^designation[0].value = "MUCOPOLYSACCHARIDPOLYSCHWEFELSÄUREESTER" 
* #100000076249 "MYRRH"
* #100000076249 ^designation[0].language = #de-AT 
* #100000076249 ^designation[0].value = "MYRRHA" 
* #100000076266 "MYRISTICA OIL"
* #100000076266 ^designation[0].language = #de-AT 
* #100000076266 ^designation[0].value = "MYRISTICAE FRAGRANTIS AETHEROLEUM" 
* #100000076280 "CARBON DIOXIDE"
* #100000076280 ^designation[0].language = #de-AT 
* #100000076280 ^designation[0].value = "KOHLENDIOXID" 
* #100000076318 "CENTAURY EXTRACT"
* #100000076318 ^designation[0].language = #de-AT 
* #100000076318 ^designation[0].value = "CENTAURII HERBA (AUSZUG)" 
* #100000076348 "CHAMOMILE OIL"
* #100000076348 ^designation[0].language = #de-AT 
* #100000076348 ^designation[0].value = "MATRICARIAE AETHEROLEUM" 
* #100000076365 "BUTCHER’S BROOM"
* #100000076365 ^designation[0].language = #de-AT 
* #100000076365 ^designation[0].value = "RUSCI RHIZOMA" 
* #100000076405 "CINNAMON BARK OIL, CEYLON"
* #100000076405 ^designation[0].language = #de-AT 
* #100000076405 ^designation[0].value = "CINNAMOMI ZEYLANICI CORTICIS AETHEROLEUM" 
* #100000076417 "CENTAURY"
* #100000076417 ^designation[0].language = #de-AT 
* #100000076417 ^designation[0].value = "CENTAURII HERBA" 
* #100000076482 "NEDOCROMIL SODIUM"
* #100000076482 ^designation[0].language = #de-AT 
* #100000076482 ^designation[0].value = "NEDOCROMIL DINATRIUM" 
* #100000076483 "NEISSERIA MENINGITIDIS"
* #100000076483 ^designation[0].language = #de-AT 
* #100000076483 ^designation[0].value = "NEISSERIA MENINGITIDIS (AUSZUG, PRODUKTE)" 
* #100000076490 "MANGANESE SULFATE"
* #100000076490 ^designation[0].language = #de-AT 
* #100000076490 ^designation[0].value = "MANGANSULFAT" 
* #100000076491 "MANNA"
* #100000076504 "MANGANESE CHLORIDE"
* #100000076504 ^designation[0].language = #de-AT 
* #100000076504 ^designation[0].value = "MANGANCHLORID" 
* #100000076533 "MEFLOQUINE HYDROCHLORIDE"
* #100000076533 ^designation[0].language = #de-AT 
* #100000076533 ^designation[0].value = "MEFLOQUIN HYDROCHLORID" 
* #100000076540 "MENTHONE"
* #100000076540 ^designation[0].language = #de-AT 
* #100000076540 ^designation[0].value = "P-MENTHAN-3-ON" 
* #100000076567 "CALCIUM GLYCEROPHOSPHATE"
* #100000076567 ^designation[0].language = #de-AT 
* #100000076567 ^designation[0].value = "CALCIUM GLYCEROL PHOSPHAT" 
* #100000076582 "CALCIUM CITRATE"
* #100000076582 ^designation[0].language = #de-AT 
* #100000076582 ^designation[0].value = "CALCIUMCITRAT" 
* #100000076592 "CALCIUM POLYSTYRENE SULFONATE"
* #100000076592 ^designation[0].language = #de-AT 
* #100000076592 ^designation[0].value = "CALCIUM POLYSTYROLSULFONAT" 
* #100000076601 "CAMPHENE"
* #100000076601 ^designation[0].language = #de-AT 
* #100000076601 ^designation[0].value = "CAMPHEN" 
* #100000076649 "AGRIMONY"
* #100000076649 ^designation[0].language = #de-AT 
* #100000076649 ^designation[0].value = "AGRIMONIAE HERBA" 
* #100000076662 "URAPIDIL"
* #100000076674 "AMIDOTRIZOIC ACID DIHYDRATE"
* #100000076674 ^designation[0].language = #de-AT 
* #100000076674 ^designation[0].value = "AMIDOTRIZOESÄURE DIHYDRAT" 
* #100000076681 "ARNICA FLOWER"
* #100000076681 ^designation[0].language = #de-AT 
* #100000076681 ^designation[0].value = "ARNICAE FLOS" 
* #100000076693 "AMIKACIN SULPHATE"
* #100000076693 ^designation[0].language = #de-AT 
* #100000076693 ^designation[0].value = "AMIKACINSULFAT" 
* #100000076696 "ANISI FRUCTUS"
* #100000076710 "VERBENA HERB"
* #100000076710 ^designation[0].language = #de-AT 
* #100000076710 ^designation[0].value = "VERBENAE HERBA" 
* #100000076740 "WALNUT LEAF EXTRACT"
* #100000076740 ^designation[0].language = #de-AT 
* #100000076740 ^designation[0].value = "JUGLANDIS FOLIUM (AUSZUG)" 
* #100000076769 "LOTEPREDNOL ETABONATE"
* #100000076769 ^designation[0].language = #de-AT 
* #100000076769 ^designation[0].value = "LOTEPREDNOL ETABONAT" 
* #100000076799 "MAGNESIUM ASPARTATE HYDROCHLORIDE"
* #100000076799 ^designation[0].language = #de-AT 
* #100000076799 ^designation[0].value = "MAGNESIUM ASPARTAT HYDROCHLORID TRIHYDRAT" 
* #100000076800 "MAGNESIUM CARBONATE HYDROXIDE"
* #100000076800 ^designation[0].language = #de-AT 
* #100000076800 ^designation[0].value = "BASISCHES MAGNESIUMCARBONAT" 
* #100000076808 "MAGNESIUM PHOSPHATE MONOBASIC"
* #100000076808 ^designation[0].language = #de-AT 
* #100000076808 ^designation[0].value = "MAGNESIUMHYDROGENPHOSPHAT" 
* #100000076814 "MAGNESIUM ACETATE"
* #100000076814 ^designation[0].language = #de-AT 
* #100000076814 ^designation[0].value = "MAGNESIUMACETAT" 
* #100000076821 "MAGNESIUM GLUCONATE"
* #100000076821 ^designation[0].language = #de-AT 
* #100000076821 ^designation[0].value = "MAGNESIUM DIGLUCONAT" 
* #100000076822 "MAGNESIUM HYDROGEN ASPARTATE"
* #100000076822 ^designation[0].language = #de-AT 
* #100000076822 ^designation[0].value = "MAGNESIUM HYDROGENASPARTAT" 
* #100000076831 "CAPSAICIN"
* #100000076849 "BISMUTH SUBCITRATE"
* #100000076849 ^designation[0].language = #de-AT 
* #100000076849 ^designation[0].value = "WISMUTSUBCITRAT" 
* #100000076855 "BETHANECHOL CHLORIDE"
* #100000076855 ^designation[0].language = #de-AT 
* #100000076855 ^designation[0].value = "BETHANECHOLCHLORID" 
* #100000076859 "BIOFLAVONOIDS"
* #100000076859 ^designation[0].language = #de-AT 
* #100000076859 ^designation[0].value = "FLAVONOIDE" 
* #100000076878 "BORNEOL"
* #100000076913 "BENZALKONIUM CHLORIDE SOLUTION"
* #100000076913 ^designation[0].language = #de-AT 
* #100000076913 ^designation[0].value = "BENZALKONIUMCHLORID-LÖSUNG" 
* #100000076915 "BETULAE FOLIUM"
* #100000076935 "TRIFLURIDINE"
* #100000076935 ^designation[0].language = #de-AT 
* #100000076935 ^designation[0].value = "TRIFLURIDIN" 
* #100000077037 "TOLTERODINE L-TARTRATE"
* #100000077037 ^designation[0].language = #de-AT 
* #100000077037 ^designation[0].value = "TOLTERODIN TARTRAT" 
* #100000077058 "TOLYLMETHYLCARBINOL NICOTINATE"
* #100000077058 ^designation[0].language = #de-AT 
* #100000077058 ^designation[0].value = "1-(P-TOLYL)ÄTHYL NICOTINAT" 
* #100000077067 "KLEBSIELLA PNEUMONIAE"
* #100000077067 ^designation[0].language = #de-AT 
* #100000077067 ^designation[0].value = "KLEBSIELLA" 
* #100000077069 "LACTOBACILLUS ACIDOPHILUS"
* #100000077073 "LACTOBACILLUS HELVETICUS"
* #100000077073 ^designation[0].language = #de-AT 
* #100000077073 ^designation[0].value = "LACTOBACILLUS HELVETICUS (AUSZUG, PRODUKTE)" 
* #100000077093 "LACTOPROTEIN"
* #100000077093 ^designation[0].language = #de-AT 
* #100000077093 ^designation[0].value = "PROTEINE" 
* #100000077103 "LECITHIN"
* #100000077109 "LEVOBUNOLOL HYDROCHLORIDE"
* #100000077109 ^designation[0].language = #de-AT 
* #100000077109 ^designation[0].value = "LEVOBUNOLOL HYDROCHLORID" 
* #100000077111 "LICHEN ISLANDICUS"
* #100000077210 "TIOCONAZOL"
* #100000077212 "TIOGUANINE"
* #100000077212 ^designation[0].language = #de-AT 
* #100000077212 ^designation[0].value = "TIOGUANIN" 
* #100000077237 "TIOTROPIUM BROMIDE"
* #100000077237 ^designation[0].language = #de-AT 
* #100000077237 ^designation[0].value = "TIOTROPIUMBROMID" 
* #100000077295 "TRIGLYCERIDES"
* #100000077295 ^designation[0].language = #de-AT 
* #100000077295 ^designation[0].value = "TRIGLYCERIDE" 
* #100000077370 "ISOMALTOSE, FERRIC COMPLEX"
* #100000077370 ^designation[0].language = #de-AT 
* #100000077370 ^designation[0].value = "EISEN(III)-ISOMALTOSEKOMPLEX" 
* #100000077400 "ANETHOLE"
* #100000077400 ^designation[0].language = #de-AT 
* #100000077400 ^designation[0].value = "ANETHOL" 
* #100000077408 "APRACLONIDINE HYDROCHLORIDE"
* #100000077408 ^designation[0].language = #de-AT 
* #100000077408 ^designation[0].value = "APRACLONIDIN HYDROCHLORID" 
* #100000077433 "ARNICA EXTRACT"
* #100000077433 ^designation[0].language = #de-AT 
* #100000077433 ^designation[0].value = "ARNICAE FLOS (AUSZUG)" 
* #100000077491 "TRIAMCINOLONE HEXACETONIDE"
* #100000077491 ^designation[0].language = #de-AT 
* #100000077491 ^designation[0].value = "TRIAMCINOLON HEXACETONID" 
* #100000077502 "TREOSULFAN"
* #100000077527 "SUMATRIPTAN SUCCINATE"
* #100000077527 ^designation[0].language = #de-AT 
* #100000077527 ^designation[0].value = "SUMATRIPTAN SUCCINAT" 
* #100000077547 "SUNFLOWER OIL"
* #100000077547 ^designation[0].language = #de-AT 
* #100000077547 ^designation[0].value = "HELIANTHI ANNUI OLEUM RAFFINATUM" 
* #100000077586 "ISOPROPANOL"
* #100000077586 ^designation[0].language = #de-AT 
* #100000077586 ^designation[0].value = "2-PROPANOL" 
* #100000077636 "IBUTILIDE FUMARATE"
* #100000077636 ^designation[0].language = #de-AT 
* #100000077636 ^designation[0].value = "IBUTILID FUMARAT" 
* #100000077657 "ICHTHAMMOL SODIUM"
* #100000077657 ^designation[0].language = #de-AT 
* #100000077657 ^designation[0].value = "NATRIUM SULFOBITUMINOSUM" 
* #100000077660 "IMMUNGLOBULIN"
* #100000077680 "ALUMINIUM POTASSIUM SULFATE"
* #100000077680 ^designation[0].language = #de-AT 
* #100000077680 ^designation[0].value = "ALUMINIUMKALIUMSULFAT" 
* #100000077696 "ALUMINIUM HYDROXIDE-MAGNESIUM CARBONATE"
* #100000077696 ^designation[0].language = #de-AT 
* #100000077696 ^designation[0].value = "ALUMINIUMHYDROXID X MAGNESIUMCARBONAT" 
* #100000077712 "AMMONIA"
* #100000077712 ^designation[0].language = #de-AT 
* #100000077712 ^designation[0].value = "AMMONIAK" 
* #100000077798 "SOYA OIL"
* #100000077798 ^designation[0].language = #de-AT 
* #100000077798 ^designation[0].value = "SOJAÖL" 
* #100000077816 "SOMATOSTATIN ACETATE"
* #100000077816 ^designation[0].language = #de-AT 
* #100000077816 ^designation[0].value = "SOMATOSTATIN ACETAT" 
* #100000077913 "HEMIN"
* #100000077965 "AJMALINE"
* #100000077965 ^designation[0].language = #de-AT 
* #100000077965 ^designation[0].value = "AJMALIN" 
* #100000077998 "ALLERGENS, POLLEN & PLANT EXTRACT"
* #100000077998 ^designation[0].language = #de-AT 
* #100000077998 ^designation[0].value = "ALLERGENE (POLLEN)" 
* #100000078009 "DIHYDROCODEINE TARTRATE"
* #100000078009 ^designation[0].language = #de-AT 
* #100000078009 ^designation[0].value = "DIHYDROCODEIN BITARTRAT" 
* #100000078010 "POTASSIUM SODIUM HYDROGEN CITRATE"
* #100000078010 ^designation[0].language = #de-AT 
* #100000078010 ^designation[0].value = "HEXAKALIUMHEXANATRIUMTRIHYDROGENPENTACITRAT" 
* #100000078014 "ALLANTOIN"
* #100000078023 "WATER FOR INJECTION"
* #100000078023 ^designation[0].language = #de-AT 
* #100000078023 ^designation[0].value = "WASSER FÜR INJEKTIONSZWECKE" 
* #100000078065 "HYDROCHLORIC ACID SOLUTION"
* #100000078065 ^designation[0].language = #de-AT 
* #100000078065 ^designation[0].value = "SALZSÄURE-LÖSUNG" 
* #100000078090 "SODIUM GLYCEROPHOSPHATE"
* #100000078090 ^designation[0].language = #de-AT 
* #100000078090 ^designation[0].value = "NATRIUMGLYCEROPHOSPHAT" 
* #100000078116 "SODIUM LAURYL SULFOACETATE"
* #100000078116 ^designation[0].language = #de-AT 
* #100000078116 ^designation[0].value = "NATRIUM DODECYLOXYCARBONYLMETHANSULFONAT" 
* #100000078119 "SODIUM PENTOSAN POLYSULFATE"
* #100000078119 ^designation[0].language = #de-AT 
* #100000078119 ^designation[0].value = "NATRIUM PENTOSAN POLYSULFAT" 
* #100000078123 "SODIUM SULFATE"
* #100000078123 ^designation[0].language = #de-AT 
* #100000078123 ^designation[0].value = "NATRIUMSULFAT" 
* #100000078140 "SODIUM PERCHLORATE"
* #100000078140 ^designation[0].language = #de-AT 
* #100000078140 ^designation[0].value = "NATRIUMPERCHLORAT" 
* #100000078141 "SODIUM PHOSPHATE DIBASIC"
* #100000078141 ^designation[0].language = #de-AT 
* #100000078141 ^designation[0].value = "NATRIUMMONOHYDROGENPHOSPHAT" 
* #100000078144 "SODIUM SULFATE ANHYDROUS"
* #100000078144 ^designation[0].language = #de-AT 
* #100000078144 ^designation[0].value = "NATRIUMSULFAT, WASSERFREIES" 
* #100000078149 "HISTIDINE HYDROCHLORIDE"
* #100000078149 ^designation[0].language = #de-AT 
* #100000078149 ^designation[0].value = "HISTIDINHYDROCHLORID" 
* #100000078163 "GINGER"
* #100000078163 ^designation[0].language = #de-AT 
* #100000078163 ^designation[0].value = "ZINGIBERIS RHIZOMA" 
* #100000078164 "GINKGO BILOBA EXTRACT"
* #100000078164 ^designation[0].language = #de-AT 
* #100000078164 ^designation[0].value = "GINKGONIS FOLIUM (AUSZUG)" 
* #100000078171 "GLUCOSE"
* #100000078231 "MICROCRYSTALLINE CELLULOSE"
* #100000078231 ^designation[0].language = #de-AT 
* #100000078231 ^designation[0].value = "CELLULOSE, MIKROKRISTALLIN" 
* #100000078271 "1-NAPHTHALENEACETIC ACID"
* #100000078271 ^designation[0].language = #de-AT 
* #100000078271 ^designation[0].value = "1-NAPTHYLESSIGSÄURE" 
* #100000078410 "SENNOSIDE A+B CALCIUM"
* #100000078410 ^designation[0].language = #de-AT 
* #100000078410 ^designation[0].value = "CALCIUMSALZE DER SENNOSIDE A+B" 
* #100000078471 "FERROUS LACTATE"
* #100000078471 ^designation[0].language = #de-AT 
* #100000078471 ^designation[0].value = "EISEN(II)-LACTAT" 
* #100000078474 "FERROUS SULFATE"
* #100000078474 ^designation[0].language = #de-AT 
* #100000078474 ^designation[0].value = "EISEN(II)-SULFAT" 
* #100000078481 "FLOS CARYOPHYLLI"
* #100000078481 ^designation[0].language = #de-AT 
* #100000078481 ^designation[0].value = "CARYOPHYLLI FLOS" 
* #100000078497 "FISH LIVER OIL"
* #100000078497 ^designation[0].language = #de-AT 
* #100000078497 ^designation[0].value = "FISCHLEBERÖL" 
* #100000078498 "FISH OIL"
* #100000078498 ^designation[0].language = #de-AT 
* #100000078498 ^designation[0].value = "FISCHÖL" 
* #100000078524 "SODIUM DIHYDROGEN PHOSPHATE"
* #100000078524 ^designation[0].language = #de-AT 
* #100000078524 ^designation[0].value = "NATRIUMDIHYDROGENPHOSPHAT" 
* #100000078529 "COLLAGEN"
* #100000078529 ^designation[0].language = #de-AT 
* #100000078529 ^designation[0].value = "KOLLAGEN" 
* #100000078537 "POLYSORBATE 20"
* #100000078537 ^designation[0].language = #de-AT 
* #100000078537 ^designation[0].value = "POLYSORBAT 20" 
* #100000078543 "SODIUM CARBONATE"
* #100000078543 ^designation[0].language = #de-AT 
* #100000078543 ^designation[0].value = "NATRIUMCARBONAT" 
* #100000078567 "PERTUZUMAB"
* #100000078568 "PREDNISOLONE HYDROGEN SUCCINATE"
* #100000078568 ^designation[0].language = #de-AT 
* #100000078568 ^designation[0].value = "PREDNISOLON SUCCINAT" 
* #100000078581 "ETORICOXIB"
* #100000078614 "SILVER NITRATE"
* #100000078614 ^designation[0].language = #de-AT 
* #100000078614 ^designation[0].value = "SILBERNITRAT" 
* #100000078648 "ROSEMARY OIL"
* #100000078648 ^designation[0].language = #de-AT 
* #100000078648 ^designation[0].value = "ROSMARINI AETHEROLEUM" 
* #100000078688 "EXTRACTUM GINSENG"
* #100000078688 ^designation[0].language = #de-AT 
* #100000078688 ^designation[0].value = "GINSENG RADIX (AUSZUG)" 
* #100000078720 "FERRIC CHLORIDE"
* #100000078720 ^designation[0].language = #de-AT 
* #100000078720 ^designation[0].value = "EISEN(III)-CHLORID" 
* #100000078736 "FENCHONE"
* #100000078736 ^designation[0].language = #de-AT 
* #100000078736 ^designation[0].value = "1,3,3-TRIMETHYLNORBORNAN-2-ON" 
* #100000078740 "FERRIC CHLORIDE HEXAHYDRATE"
* #100000078740 ^designation[0].language = #de-AT 
* #100000078740 ^designation[0].value = "EISEN(III)-CHLORID HEXAHYDRAT" 
* #100000078782 "TARTARIC ACID"
* #100000078782 ^designation[0].language = #de-AT 
* #100000078782 ^designation[0].value = "WEINSÄURE" 
* #100000078784 "TRYPSIN"
* #100000078804 "ACAMPROSATE CALCIUM"
* #100000078804 ^designation[0].language = #de-AT 
* #100000078804 ^designation[0].value = "CALCIUM ACAMPROSAT" 
* #100000078816 "ZOLMITRIPTAN"
* #100000078823 "ABCIXIMAB"
* #100000078834 "ADENOSINE"
* #100000078834 ^designation[0].language = #de-AT 
* #100000078834 ^designation[0].value = "ADENOSIN" 
* #100000078872 "PRIMULA FLOWERS"
* #100000078872 ^designation[0].language = #de-AT 
* #100000078872 ^designation[0].value = "PRIMULAE FLOS" 
* #100000078877 "PROPANOL"
* #100000078877 ^designation[0].language = #de-AT 
* #100000078877 ^designation[0].value = "1-PROPANOL" 
* #100000078889 "PUMPKIN OIL"
* #100000078889 ^designation[0].language = #de-AT 
* #100000078889 ^designation[0].value = "CUCURBITAE SEMEN OLEUM" 
* #100000078927 "EPHEDRINE SULFATE"
* #100000078927 ^designation[0].language = #de-AT 
* #100000078927 ^designation[0].value = "EPHEDRINSULFAT" 
* #100000078956 "ESCHERICHIA COLI"
* #100000078985 "ETHANOL"
* #100000079033 "TRIGLYCERIDES, MEDIUM-CHAIN"
* #100000079033 ^designation[0].language = #de-AT 
* #100000079033 ^designation[0].value = "MITTELKETTIGE TRIGLYCERIDE" 
* #100000079084 "VIGABATRIN"
* #100000079091 "VINFLUNINE"
* #100000079091 ^designation[0].language = #de-AT 
* #100000079091 ^designation[0].value = "VINFLUNIN" 
* #100000079169 "POTASSIUM AMINOBENZOATE"
* #100000079169 ^designation[0].language = #de-AT 
* #100000079169 ^designation[0].value = "KALIUM 4-AMINOBENZOAT" 
* #100000079201 "DIHYDROERGOCRYPTINE MESILATE"
* #100000079201 ^designation[0].language = #de-AT 
* #100000079201 ^designation[0].value = "DIHYDROERGOCRYPTIN METHANSULFONAT" 
* #100000079228 "DIMETHYLFUMARAT"
* #100000079273 "WORMWOOD"
* #100000079273 ^designation[0].language = #de-AT 
* #100000079273 ^designation[0].value = "ABSINTHII HERBA" 
* #100000079288 "ROSEMARY LEAF"
* #100000079288 ^designation[0].language = #de-AT 
* #100000079288 ^designation[0].value = "ROSMARINI FOLIUM" 
* #100000079291 "SELENIUM DISULPHIDE"
* #100000079291 ^designation[0].language = #de-AT 
* #100000079291 ^designation[0].value = "SELENDISULFID" 
* #100000079305 "RESTHARROW ROOT"
* #100000079305 ^designation[0].language = #de-AT 
* #100000079305 ^designation[0].value = "ONONIDIS RADIX" 
* #100000079317 "SODIUM FUSIDATE"
* #100000079317 ^designation[0].language = #de-AT 
* #100000079317 ^designation[0].value = "NATRIUM FUSIDAT" 
* #100000079337 "SODIUM HYDROGEN CARBONATE"
* #100000079337 ^designation[0].language = #de-AT 
* #100000079337 ^designation[0].value = "NATRIUMHYDROGENCARBONAT" 
* #100000079360 "XIPAMIDE"
* #100000079360 ^designation[0].language = #de-AT 
* #100000079360 ^designation[0].value = "XIPAMID" 
* #100000079418 "POTASSIUM HYDROGEN ASPARTATE"
* #100000079418 ^designation[0].language = #de-AT 
* #100000079418 ^designation[0].value = "KALIUM HYDROGENASPARTAT" 
* #100000079433 "PHENYL SALICYLATE"
* #100000079433 ^designation[0].language = #de-AT 
* #100000079433 ^designation[0].value = "SALIZYLSÄUREPHENYLESTER" 
* #100000079464 "PHYSOSTIGMINE SALICYLATE"
* #100000079464 ^designation[0].language = #de-AT 
* #100000079464 ^designation[0].value = "PHYSOSTIGMINSALICYLAT" 
* #100000079470 "PINUS SYLVESTRIS OIL"
* #100000079470 ^designation[0].language = #de-AT 
* #100000079470 ^designation[0].value = "PINI SILVESTRIS AETHEROLEUM" 
* #100000079516 "DANAPAROID"
* #100000079520 "DEOXYCHOLIC ACID"
* #100000079520 ^designation[0].language = #de-AT 
* #100000079520 ^designation[0].value = "DESOXYCHOLSÄURE" 
* #100000079574 "DIETHYLAMINE SALICYLATE"
* #100000079574 ^designation[0].language = #de-AT 
* #100000079574 ^designation[0].value = "DIETHYLAMINSALICYLAT" 
* #100000079598 "OMEGA-3-ACID ETHYL ESTERS"
* #100000079598 ^designation[0].language = #de-AT 
* #100000079598 ^designation[0].value = "OMEGA-3-SÄURENETHYLESTER" 
* #100000079618 "OMEGA-3-ACID TRIGLYCERIDES"
* #100000079618 ^designation[0].language = #de-AT 
* #100000079618 ^designation[0].value = "OMEGA-3-SÄUREN-TRIGLYCERIDE" 
* #100000079696 "CALCIUM HYDROGEN PHOSPHATE"
* #100000079696 ^designation[0].language = #de-AT 
* #100000079696 ^designation[0].value = "CALCIUMHYDROGENPHOSPHAT" 
* #100000079732 "PECTIN"
* #100000079732 ^designation[0].language = #de-AT 
* #100000079732 ^designation[0].value = "PECTINUM" 
* #100000079736 "PANCREAS EXTRACT"
* #100000079736 ^designation[0].language = #de-AT 
* #100000079736 ^designation[0].value = "PANKREAS(EXTRAKT)" 
* #100000079740 "PAPAIN"
* #100000079740 ^designation[0].language = #de-AT 
* #100000079740 ^designation[0].value = "PAPAYOTIN" 
* #100000079764 "DIHYDROERGOCORNINE MESILATE"
* #100000079764 ^designation[0].language = #de-AT 
* #100000079764 ^designation[0].value = "DIHYDROERGOCORNIN METHANSULFONAT" 
* #100000079798 "COLLAGENASE"
* #100000079798 ^designation[0].language = #de-AT 
* #100000079798 ^designation[0].value = "KOLLAGENASE" 
* #100000079804 "COPPER CHLORIDE"
* #100000079804 ^designation[0].language = #de-AT 
* #100000079804 ^designation[0].value = "KUPFERDICHLORID" 
* #100000079827 "CORTEX FRANGULAE"
* #100000079827 ^designation[0].language = #de-AT 
* #100000079827 ^designation[0].value = "FRANGULAE CORTEX" 
* #100000079843 "POTASSIUM DIHYDROGEN PHOSPHATE"
* #100000079843 ^designation[0].language = #de-AT 
* #100000079843 ^designation[0].value = "KALIUMDIHYDROGENPHOSPHAT" 
* #100000079851 "ISPAGHULA SEED"
* #100000079851 ^designation[0].language = #de-AT 
* #100000079851 ^designation[0].value = "PLANTAGINIS OVATAE SEMEN" 
* #100000079852 "JOSAMYCIN"
* #100000079863 "POTASSIUM HYDROGEN CARBONATE"
* #100000079863 ^designation[0].language = #de-AT 
* #100000079863 ^designation[0].value = "KALIUMHYDROGENCARBONAT" 
* #100000079870 "ISOSORBIDE MONONITRATE, DILUTED"
* #100000079870 ^designation[0].language = #de-AT 
* #100000079870 ^designation[0].value = "ISOSORBIDMONONITRAT, VERDÜNNTES" 
* #100000079896 "LIQUIRITIAE RADIX"
* #100000079897 "LOVAGE ROOT"
* #100000079897 ^designation[0].language = #de-AT 
* #100000079897 ^designation[0].value = "LEVISTICI RADIX" 
* #100000079921 "ALPHATOCOPHEROL ACETATE"
* #100000079921 ^designation[0].language = #de-AT 
* #100000079921 ^designation[0].value = "ALPHA-TOCOPHEROLACETAT" 
* #100000079971 "NICOTINALDEHYDE"
* #100000079971 ^designation[0].language = #de-AT 
* #100000079971 ^designation[0].value = "NICOTINALDEHYD" 
* #100000079991 "NICOTINE"
* #100000079991 ^designation[0].language = #de-AT 
* #100000079991 ^designation[0].value = "NICOTIN" 
* #100000080054 "OLIVE OIL"
* #100000080054 ^designation[0].language = #de-AT 
* #100000080054 ^designation[0].value = "OLIVAE OLEUM" 
* #100000080065 "ORNITHINE ASPARTATE"
* #100000080065 ^designation[0].language = #de-AT 
* #100000080065 ^designation[0].value = "ORNITHIN ASPARTAT" 
* #100000080089 "CIMICIFUGA RACEMOSA ROOT"
* #100000080089 ^designation[0].language = #de-AT 
* #100000080089 ^designation[0].value = "CIMICIFUGAE RHIZOMA (AUSZUG)" 
* #100000080114 "CITRONELLA OIL"
* #100000080114 ^designation[0].language = #de-AT 
* #100000080114 ^designation[0].value = "CITRONELLAE AETHEROLEUM" 
* #100000080119 "CLOVE OIL"
* #100000080119 ^designation[0].language = #de-AT 
* #100000080119 ^designation[0].value = "CARYOPHYLLI FLORIS AETHEROLEUM" 
* #100000080143 "MANGANESE SULPHATE MONOHYDRATE"
* #100000080143 ^designation[0].language = #de-AT 
* #100000080143 ^designation[0].value = "MANGANSULFAT MONOHYDRAT" 
* #100000080155 "GENTIAN ROOT"
* #100000080155 ^designation[0].language = #de-AT 
* #100000080155 ^designation[0].value = "GENTIANAE RADIX" 
* #100000080156 "GLUCOSE, ANHYDROUS"
* #100000080156 ^designation[0].language = #de-AT 
* #100000080156 ^designation[0].value = "GLUCOSE, WASSERFREI" 
* #100000080215 "CLEVIDIPINE"
* #100000080215 ^designation[0].language = #de-AT 
* #100000080215 ^designation[0].value = "CLEVIDIPIN BUTYRAT" 
* #100000080252 "ROFLUMILAST"
* #100000080269 "ROCURONIUM BROMIDE"
* #100000080269 ^designation[0].language = #de-AT 
* #100000080269 ^designation[0].value = "ROCURONIUM BROMID" 
* #100000080291 "RALTITREXED"
* #100000080329 "MODAFINIL"
* #100000080403 "GLIBENCLAMIDE"
* #100000080403 ^designation[0].language = #de-AT 
* #100000080403 ^designation[0].value = "GLIBENCLAMID" 
* #100000080406 "FLUTRIMAZOL"
* #100000080464 "FOTEMUSTINE"
* #100000080464 ^designation[0].language = #de-AT 
* #100000080464 ^designation[0].value = "FOTEMUSTIN" 
* #100000080500 "DYDROGESTERONE"
* #100000080500 ^designation[0].language = #de-AT 
* #100000080500 ^designation[0].value = "DYDROGESTERON" 
* #100000080561 "RIFAMPICIN"
* #100000080573 "RETIGABINE"
* #100000080573 ^designation[0].language = #de-AT 
* #100000080573 ^designation[0].value = "RETIGABIN" 
* #100000080618 "MOXIFLOXACIN"
* #100000080626 "MIDAZOLAM"
* #100000080629 "MIFEPRISTONE"
* #100000080629 ^designation[0].language = #de-AT 
* #100000080629 ^designation[0].value = "MIFEPRISTON" 
* #100000080630 "MIGLITOL"
* #100000080655 "MILRINONE"
* #100000080655 ^designation[0].language = #de-AT 
* #100000080655 ^designation[0].value = "MILRINON" 
* #100000080681 "FLUFENAMIC ACID"
* #100000080681 ^designation[0].language = #de-AT 
* #100000080681 ^designation[0].value = "FLUFENAMINSÄURE" 
* #100000080700 "FLUDROCORTISONE"
* #100000080700 ^designation[0].language = #de-AT 
* #100000080700 ^designation[0].value = "FLUDROCORTISON" 
* #100000080704 "FLUMAZENIL"
* #100000080734 "FLUOCORTOLONE"
* #100000080734 ^designation[0].language = #de-AT 
* #100000080734 ^designation[0].value = "FLUOCORTOLON" 
* #100000080786 "DORNASE ALFA"
* #100000080843 "PRULIFLOXACIN"
* #100000080846 "PYRAZINAMIDE"
* #100000080846 ^designation[0].language = #de-AT 
* #100000080846 ^designation[0].value = "PYRAZINAMID" 
* #100000080862 "PRUCALOPRIDE"
* #100000080862 ^designation[0].language = #de-AT 
* #100000080862 ^designation[0].value = "PRUCALOPRID" 
* #100000080890 "METHYLTHIONINIUM CHLORIDE"
* #100000080890 ^designation[0].language = #de-AT 
* #100000080890 ^designation[0].value = "METHYLTHIONINIUM CHLORID" 
* #100000080898 "MITOMYCIN"
* #100000080901 "MIVACURIUM CHLORIDE"
* #100000080901 ^designation[0].language = #de-AT 
* #100000080901 ^designation[0].value = "MIVACURIUM CHLORID" 
* #100000081006 "DRONEDARONE"
* #100000081006 ^designation[0].language = #de-AT 
* #100000081006 ^designation[0].value = "DRONEDARON" 
* #100000081007 "DROPERIDOL"
* #100000081124 "PROLINE"
* #100000081124 ^designation[0].language = #de-AT 
* #100000081124 ^designation[0].value = "PROLIN" 
* #100000081237 "FELBAMATE"
* #100000081237 ^designation[0].language = #de-AT 
* #100000081237 ^designation[0].value = "FELBAMAT" 
* #100000081316 "CARBASALATE CALCIUM"
* #100000081316 ^designation[0].language = #de-AT 
* #100000081316 ^designation[0].value = "CARBASALAT CALCIUM" 
* #100000081320 "CARBETOCIN"
* #100000081323 "CAROVERINE"
* #100000081323 ^designation[0].language = #de-AT 
* #100000081323 ^designation[0].value = "CAROVERIN" 
* #100000081364 "PROPYLTHIOURACIL"
* #100000081436 "METHOXYFLURANE"
* #100000081436 ^designation[0].language = #de-AT 
* #100000081436 ^designation[0].value = "METHOXYFLURAN" 
* #100000081536 "CETRIMONIUM BROMIDE"
* #100000081536 ^designation[0].language = #de-AT 
* #100000081536 ^designation[0].value = "CETRIMONIUM BROMID" 
* #100000081574 "FAMPRIDINE"
* #100000081574 ^designation[0].language = #de-AT 
* #100000081574 ^designation[0].value = "FAMPRIDIN" 
* #100000081577 "CHLORAMBUCIL"
* #100000081603 "CANRENOIC ACID"
* #100000081603 ^designation[0].language = #de-AT 
* #100000081603 ^designation[0].value = "CANRENOINSÄURE" 
* #100000081613 "CALCIUM SACCHARATE"
* #100000081613 ^designation[0].language = #de-AT 
* #100000081613 ^designation[0].value = "CALCIUM SACCHARAT" 
* #100000081645 "PIRFENIDONE"
* #100000081645 ^designation[0].language = #de-AT 
* #100000081645 ^designation[0].value = "PIRFENIDON" 
* #100000081656 "PRIMIDONE"
* #100000081656 ^designation[0].language = #de-AT 
* #100000081656 ^designation[0].value = "PRIMIDON" 
* #100000081734 "LYMECYCLINE"
* #100000081734 ^designation[0].language = #de-AT 
* #100000081734 ^designation[0].value = "LYMECYCLIN" 
* #100000081855 "DISULFIRAM"
* #100000081889 "CICLOPIROX"
* #100000081904 "CICLESONIDE"
* #100000081904 ^designation[0].language = #de-AT 
* #100000081904 ^designation[0].value = "CICLESONID" 
* #100000082024 "LOMUSTINE"
* #100000082024 ^designation[0].language = #de-AT 
* #100000082024 ^designation[0].value = "LOMUSTIN" 
* #100000082090 "ETONOGESTREL"
* #100000082091 "ETOPOSIDE"
* #100000082091 ^designation[0].language = #de-AT 
* #100000082091 ^designation[0].value = "ETOPOSID" 
* #100000082107 "ETOMIDATE"
* #100000082107 ^designation[0].language = #de-AT 
* #100000082107 ^designation[0].value = "ETOMIDAT" 
* #100000082128 "THIOTEPA"
* #100000082261 "PHENPROCOUMON"
* #100000082277 "LEVOMENOL"
* #100000082407 "TAURINE"
* #100000082407 ^designation[0].language = #de-AT 
* #100000082407 ^designation[0].value = "TAURIN" 
* #100000082416 "THIAMAZOLE"
* #100000082416 ^designation[0].language = #de-AT 
* #100000082416 ^designation[0].value = "THIAMAZOL" 
* #100000082427 "TAUROLIDINE"
* #100000082427 ^designation[0].language = #de-AT 
* #100000082427 ^designation[0].value = "TAUROLIDIN" 
* #100000082441 "TEGAFUR"
* #100000082481 "PENICILLAMINE"
* #100000082481 ^designation[0].language = #de-AT 
* #100000082481 ^designation[0].value = "PENICILLAMIN" 
* #100000082511 "PENTETIC ACID"
* #100000082511 ^designation[0].language = #de-AT 
* #100000082511 ^designation[0].value = "PENTETSÄURE" 
* #100000082512 "PENTETREOTIDE"
* #100000082512 ^designation[0].language = #de-AT 
* #100000082512 ^designation[0].value = "PENTETREOTID" 
* #100000082736 "TETRACYCLINE"
* #100000082736 ^designation[0].language = #de-AT 
* #100000082736 ^designation[0].value = "TETRACYCLIN" 
* #100000082866 "KEBUZONE"
* #100000082866 ^designation[0].language = #de-AT 
* #100000082866 ^designation[0].value = "KEBUZON" 
* #100000082993 "TACALCITOL"
* #100000083083 "IOMEPROL"
* #100000083168 "DESFLURANE"
* #100000083168 ^designation[0].language = #de-AT 
* #100000083168 ^designation[0].value = "DESFLURAN" 
* #100000083191 "DESOGESTREL"
* #100000083204 "DEXIBUPROFEN"
* #100000083272 "SULFAMETROL"
* #100000083292 "OLAFLUR"
* #100000083402 "IOBITRIDOL"
* #100000083471 "DECITABINE"
* #100000083471 ^designation[0].language = #de-AT 
* #100000083471 ^designation[0].value = "DECITABIN" 
* #100000083475 "DECTAFLUR"
* #100000083487 "SUCCIMER"
* #100000083590 "NONIVAMIDE"
* #100000083590 ^designation[0].language = #de-AT 
* #100000083590 ^designation[0].value = "NONIVAMID" 
* #100000083637 "IOHEXOL"
* #100000083651 "ICATIBANT"
* #100000083681 "IFOSFAMIDE"
* #100000083681 ^designation[0].language = #de-AT 
* #100000083681 ^designation[0].value = "IFOSFAMID" 
* #100000083903 "NILVADIPINE"
* #100000083903 ^designation[0].language = #de-AT 
* #100000083903 ^designation[0].value = "NILVADIPIN" 
* #100000084137 "SERINE"
* #100000084137 ^designation[0].language = #de-AT 
* #100000084137 ^designation[0].value = "SERIN" 
* #100000084188 "NICOBOXIL"
* #100000084232 "GLYCOPYRRONIUM BROMIDE"
* #100000084232 ^designation[0].language = #de-AT 
* #100000084232 ^designation[0].value = "GLYCOPYRRONIUMBROMID" 
* #100000084258 "GRANISETRON"
* #100000084299 "CLOBAZAM"
* #100000084340 "SERTINDOL"
* #100000084424 "MYRTECAINE"
* #100000084424 ^designation[0].language = #de-AT 
* #100000084424 ^designation[0].value = "MYRTECAIN" 
* #100000084425 "NABILONE"
* #100000084425 ^designation[0].language = #de-AT 
* #100000084425 ^designation[0].value = "NABILON" 
* #100000084509 "GADOTERIDOL"
* #100000084528 "CLONAZEPAM"
* #100000084604 "ACITRETIN"
* #100000084612 "TIROFIBAN HYDROCHLORIDE"
* #100000084612 ^designation[0].language = #de-AT 
* #100000084612 ^designation[0].value = "TIROFIBAN HYDROCHLORID" 
* #100000084640 "TRANYLCYPROMINE SULFATE"
* #100000084640 ^designation[0].language = #de-AT 
* #100000084640 ^designation[0].value = "TRANYLCYPROMIN SULFAT" 
* #100000084643 "TRIENTINE DIHYDROCHLORIDE"
* #100000084643 ^designation[0].language = #de-AT 
* #100000084643 ^designation[0].value = "TRIENTIN DIHYDROCHLORID" 
* #100000084649 "TRIPTORELIN EMBONATE"
* #100000084649 ^designation[0].language = #de-AT 
* #100000084649 ^designation[0].value = "TRIPTORELIN EMBONAT" 
* #100000084659 "TRAMAZOLINE HYDROCHLORIDE"
* #100000084659 ^designation[0].language = #de-AT 
* #100000084659 ^designation[0].value = "TRAMAZOLIN HYDROCHLORID" 
* #100000084660 "TRIAMCINOLONE ACETONIDE DIPOTASSIUM PHOSPHATE"
* #100000084660 ^designation[0].language = #de-AT 
* #100000084660 ^designation[0].value = "TRIAMCINOLON ACETONID DIKALIUMPHOSPHAT" 
* #100000084683 "CEFTAZIDIME PENTAHYDRATE"
* #100000084683 ^designation[0].language = #de-AT 
* #100000084683 ^designation[0].value = "CEFTAZIDIM-PENTAHYDRAT" 
* #100000084733 "CHLOROPROCAINE HYDROCHLORIDE"
* #100000084733 ^designation[0].language = #de-AT 
* #100000084733 ^designation[0].value = "CHLOROPROCAIN HYDROCHLORID" 
* #100000084742 "PEANUT OIL"
* #100000084742 ^designation[0].language = #de-AT 
* #100000084742 ^designation[0].value = "ARACHIDIS OLEUM RAFFINATUM" 
* #100000084758 "PIXANTRONE"
* #100000084758 ^designation[0].language = #de-AT 
* #100000084758 ^designation[0].value = "PIXANTRON" 
* #100000084794 "TERBUTALINE SULFATE"
* #100000084794 ^designation[0].language = #de-AT 
* #100000084794 ^designation[0].value = "TERBUTALINSULFAT" 
* #100000084795 "TERIPARATIDE ACETATE"
* #100000084795 ^designation[0].language = #de-AT 
* #100000084795 ^designation[0].value = "TERIPARATIDACETAT" 
* #100000084806 "CHLORTETRACYCLINE HYDROCHLORIDE"
* #100000084806 ^designation[0].language = #de-AT 
* #100000084806 ^designation[0].value = "CHLORTETRACYCLIN HYDROCHLORID" 
* #100000084827 "CICLOPIROX OLAMINE"
* #100000084827 ^designation[0].language = #de-AT 
* #100000084827 ^designation[0].value = "CICLOPIROX OLAMIN" 
* #100000084837 "BUSERELIN ACETATE"
* #100000084837 ^designation[0].language = #de-AT 
* #100000084837 ^designation[0].value = "BUSERELIN ACETAT" 
* #100000084845 "CALCITONIN"
* #100000084873 "CARBOMER"
* #100000084879 "CHYMOTRYPSIN"
* #100000084883 "IRON SUCROSE"
* #100000084883 ^designation[0].language = #de-AT 
* #100000084883 ^designation[0].value = "EISENSACCHAROSE" 
* #100000084885 "METHYL SALICYLATE"
* #100000084885 ^designation[0].language = #de-AT 
* #100000084885 ^designation[0].value = "METHYLSALICYLAT" 
* #100000084887 "RILMENIDINE PHOSPHATE"
* #100000084887 ^designation[0].language = #de-AT 
* #100000084887 ^designation[0].value = "RILMENIDIN DIHYDROGENPHOSPHAT" 
* #100000084903 "NICORANDIL"
* #100000084907 "BETAMETHASONE ACETATE"
* #100000084907 ^designation[0].language = #de-AT 
* #100000084907 ^designation[0].value = "BETAMETHASON ACETAT" 
* #100000084908 "JOSAMYCIN PROPIONATE"
* #100000084908 ^designation[0].language = #de-AT 
* #100000084908 ^designation[0].value = "JOSAMYCIN PROPIONAT" 
* #100000084942 "ROPIVACAINE HYDROCHLORIDE"
* #100000084942 ^designation[0].language = #de-AT 
* #100000084942 ^designation[0].value = "ROPIVACAIN HYDROCHLORID" 
* #100000084971 "BAMIPINE LACTATE"
* #100000084971 ^designation[0].language = #de-AT 
* #100000084971 ^designation[0].value = "BAMIPIN LACTAT" 
* #100000085013 "BENDAMUSTINE HYDROCHLORIDE"
* #100000085013 ^designation[0].language = #de-AT 
* #100000085013 ^designation[0].value = "BENDAMUSTIN HYDROCHLORID" 
* #100000085034 "SODIUM PICOSULFATE"
* #100000085034 ^designation[0].language = #de-AT 
* #100000085034 ^designation[0].value = "NATRIUM PICOSULFAT" 
* #100000085059 "ESOMEPRAZOLE MAGNESIUM"
* #100000085059 ^designation[0].language = #de-AT 
* #100000085059 ^designation[0].value = "ESOMEPRAZOL MAGNESIUM" 
* #100000085061 "BENZOYL PEROXIDE"
* #100000085061 ^designation[0].language = #de-AT 
* #100000085061 ^designation[0].value = "BENZOYLPEROXID" 
* #100000085063 "SODIUM ACETATE"
* #100000085063 ^designation[0].language = #de-AT 
* #100000085063 ^designation[0].value = "NATRIUMACETAT" 
* #100000085079 "PRIDINOL MESILATE"
* #100000085079 ^designation[0].language = #de-AT 
* #100000085079 ^designation[0].value = "PRIDINOL MESILAT" 
* #100000085080 "PRILOCAINE HYDROCHLORIDE"
* #100000085080 ^designation[0].language = #de-AT 
* #100000085080 ^designation[0].value = "PRILOCAIN HYDROCHLORID" 
* #100000085092 "PROPIVERINE HYDROCHLORIDE"
* #100000085092 ^designation[0].language = #de-AT 
* #100000085092 ^designation[0].value = "PROPIVERIN HYDROCHLORID" 
* #100000085102 "PROCYCLIDINE HYDROCHLORIDE"
* #100000085102 ^designation[0].language = #de-AT 
* #100000085102 ^designation[0].value = "PROCYCLIDIN HYDROCHLORID" 
* #100000085133 "PROTAMINE HYDROCHLORIDE"
* #100000085133 ^designation[0].language = #de-AT 
* #100000085133 ^designation[0].value = "PROTAMIN HYDROCHLORID" 
* #100000085134 "PROTHIPENDYL HYDROCHLORIDE"
* #100000085134 ^designation[0].language = #de-AT 
* #100000085134 ^designation[0].value = "PROTHIPENDYLHYDROCHLORID" 
* #100000085140 "BLEOMYCIN SULFATE"
* #100000085140 ^designation[0].language = #de-AT 
* #100000085140 ^designation[0].value = "BLEOMYCIN SULFAT" 
* #100000085170 "ARGININE HYDROCHLORIDE"
* #100000085170 ^designation[0].language = #de-AT 
* #100000085170 ^designation[0].value = "ARGININ HYDROCHLORID" 
* #100000085171 "OXALIPLATIN"
* #100000085190 "GADODIAMIDE"
* #100000085190 ^designation[0].language = #de-AT 
* #100000085190 ^designation[0].value = "GADODIAMID" 
* #100000085195 "LANTHANUM CARBONATE"
* #100000085195 ^designation[0].language = #de-AT 
* #100000085195 ^designation[0].value = "LANTHANCARBONAT" 
* #100000085198 "FUROSEMIDE SODIUM"
* #100000085198 ^designation[0].language = #de-AT 
* #100000085198 ^designation[0].value = "NATRIUM FUROSEMID" 
* #100000085201 "TETRACOSACTIDE"
* #100000085201 ^designation[0].language = #de-AT 
* #100000085201 ^designation[0].value = "TETRACOSACTID" 
* #100000085206 "ATRACURIUM BESILATE"
* #100000085206 ^designation[0].language = #de-AT 
* #100000085206 ^designation[0].value = "ATRACURIUM BESILAT" 
* #100000085208 "PHENAZONE"
* #100000085208 ^designation[0].language = #de-AT 
* #100000085208 ^designation[0].value = "PHENAZON" 
* #100000085224 "ATROPINE SULPHATE"
* #100000085224 ^designation[0].language = #de-AT 
* #100000085224 ^designation[0].value = "ATROPINSULFAT" 
* #100000085233 "TENECTEPLASE"
* #100000085237 "TIPRANAVIR"
* #100000085243 "BIVALIRUDIN"
* #100000085244 "BRINZOLAMIDE"
* #100000085244 ^designation[0].language = #de-AT 
* #100000085244 ^designation[0].value = "BRINZOLAMID" 
* #100000085245 "BUSULFAN"
* #100000085257 "AGOMELATINE"
* #100000085257 ^designation[0].language = #de-AT 
* #100000085257 ^designation[0].value = "AGOMELATIN" 
* #100000085259 "AMLODIPINE"
* #100000085259 ^designation[0].language = #de-AT 
* #100000085259 ^designation[0].value = "AMLODIPIN" 
* #100000085260 "ANAKINRA"
* #100000085262 "BOSENTAN"
* #100000085263 "BUPRENORPHINE"
* #100000085263 ^designation[0].language = #de-AT 
* #100000085263 ^designation[0].value = "BUPRENORPHIN" 
* #100000085266 "EFAVIRENZ"
* #100000085275 "PHENOXYBENZAMINE HYDROCHLORIDE"
* #100000085275 ^designation[0].language = #de-AT 
* #100000085275 ^designation[0].value = "PHENOXYBENZAMIN HYDROCHLORID" 
* #100000085294 "PHENOXYMETHYLPENICILLIN BENZATHINE"
* #100000085294 ^designation[0].language = #de-AT 
* #100000085294 ^designation[0].value = "PHENOXYMETHYLPENICILLIN BENZATHIN" 
* #100000085319 "POLYGELINE"
* #100000085319 ^designation[0].language = #de-AT 
* #100000085319 ^designation[0].value = "POLYGELIN" 
* #100000085336 "POLIDOCANOL"
* #100000085389 "MAPROTILINE MESYLATE"
* #100000085389 ^designation[0].language = #de-AT 
* #100000085389 ^designation[0].value = "MAPROTILIN MESILAT" 
* #100000085395 "CHROMIUM TRICHLORIDE HEXAHYDRATE"
* #100000085395 ^designation[0].language = #de-AT 
* #100000085395 ^designation[0].value = "CHROMCHLORID HEXAHYDRAT" 
* #100000085399 "RUPATADINE FUMARATE"
* #100000085399 ^designation[0].language = #de-AT 
* #100000085399 ^designation[0].value = "RUPATADIN FUMARAT" 
* #100000085403 "TEICOPLANIN"
* #100000085405 "ORTHOPHENYLPHENOL"
* #100000085405 ^designation[0].language = #de-AT 
* #100000085405 ^designation[0].value = "2-PHENYLPHENOL" 
* #100000085406 "CYSTEINE HYDROCHLORIDE"
* #100000085406 ^designation[0].language = #de-AT 
* #100000085406 ^designation[0].value = "CYSTEINHYDROCHLORID" 
* #100000085408 "ENTACAPONE"
* #100000085408 ^designation[0].language = #de-AT 
* #100000085408 ^designation[0].value = "ENTACAPON" 
* #100000085410 "CLADRIBINE"
* #100000085410 ^designation[0].language = #de-AT 
* #100000085410 ^designation[0].value = "CLADRIBIN" 
* #100000085411 "DACLIZUMAB"
* #100000085427 "EPOETIN ALFA"
* #100000085428 "EPTACOG ALFA (ACTIVATED)"
* #100000085428 ^designation[0].language = #de-AT 
* #100000085428 ^designation[0].value = "GERINNUNGSFAKTOR VIIa, REKOMBINANT - EPTACOG ALFA (AKTIVIERT)" 
* #100000085430 "DAPTOMYCIN"
* #100000085431 "DEFERIPRONE"
* #100000085431 ^designation[0].language = #de-AT 
* #100000085431 ^designation[0].value = "DEFERIPRON" 
* #100000085432 "DEXRAZOXANE"
* #100000085432 ^designation[0].language = #de-AT 
* #100000085432 ^designation[0].value = "DEXRAZOXAN" 
* #100000085435 "ABACAVIR"
* #100000085436 "FILGRASTIM"
* #100000085437 "GANIRELIX"
* #100000085440 "IMIGLUCERASE"
* #100000085441 "INSULIN ASPART"
* #100000085443 "IRBESARTAN"
* #100000085444 "LAMIVUDINE"
* #100000085444 ^designation[0].language = #de-AT 
* #100000085444 ^designation[0].value = "LAMIVUDIN" 
* #100000085446 "LEVODOPA"
* #100000085449 "MITOTANE"
* #100000085449 ^designation[0].language = #de-AT 
* #100000085449 ^designation[0].value = "MITOTAN" 
* #100000085451 "NEPAFENAC"
* #100000085452 "NITISINONE"
* #100000085452 ^designation[0].language = #de-AT 
* #100000085452 ^designation[0].value = "NITISINON" 
* #100000085456 "GLIMEPIRIDE"
* #100000085456 ^designation[0].language = #de-AT 
* #100000085456 ^designation[0].value = "GLIMEPIRID" 
* #100000085457 "HYDROXYCARBAMIDE"
* #100000085457 ^designation[0].language = #de-AT 
* #100000085457 ^designation[0].value = "HYDROXYCARBAMID" 
* #100000085458 "ILOPROST"
* #100000085460 "INSULINE GLARGINE"
* #100000085460 ^designation[0].language = #de-AT 
* #100000085460 ^designation[0].value = "INSULIN GLARGIN" 
* #100000085463 "LEFLUNOMIDE"
* #100000085463 ^designation[0].language = #de-AT 
* #100000085463 ^designation[0].value = "LEFLUNOMID" 
* #100000085465 "MECASERMIN"
* #100000085469 "NELARABINE"
* #100000085469 ^designation[0].language = #de-AT 
* #100000085469 ^designation[0].value = "NELARABIN" 
* #100000085470 "NEVIRAPINE"
* #100000085470 ^designation[0].language = #de-AT 
* #100000085470 ^designation[0].value = "NEVIRAPIN" 
* #100000085471 "OLANZAPINE"
* #100000085471 ^designation[0].language = #de-AT 
* #100000085471 ^designation[0].value = "OLANZAPIN" 
* #100000085472 "OXYBUTYNIN"
* #100000085474 "PACLITAXEL"
* #100000085475 "PEMETREXED"
* #100000085476 "POLYESTRADIOL PHOSPHATE"
* #100000085476 ^designation[0].language = #de-AT 
* #100000085476 ^designation[0].value = "POLYESTRADIOL PHOSPHAT" 
* #100000085481 "PRASTERONE ENANTATE"
* #100000085481 ^designation[0].language = #de-AT 
* #100000085481 ^designation[0].value = "PRASTERON ENANTAT" 
* #100000085483 "ORNITHINE HYDROCHLORIDE"
* #100000085483 ^designation[0].language = #de-AT 
* #100000085483 ^designation[0].value = "ORNITHIN HYDROCHLORID" 
* #100000085496 "PYRITHIONE ZINC"
* #100000085496 ^designation[0].language = #de-AT 
* #100000085496 ^designation[0].value = "PYRITHION ZINK" 
* #100000085509 "OXYBUPROCAINE HYDROCHLORIDE"
* #100000085509 ^designation[0].language = #de-AT 
* #100000085509 ^designation[0].value = "OXYBUPROCAIN HYDROCHLORID" 
* #100000085517 "PANTOTHENATE SODIUM"
* #100000085517 ^designation[0].language = #de-AT 
* #100000085517 ^designation[0].value = "NATRIUM PANTOTHENAT" 
* #100000085520 "PAROMOMYCIN SULFATE"
* #100000085520 ^designation[0].language = #de-AT 
* #100000085520 ^designation[0].value = "PAROMOMYCIN SULFAT" 
* #100000085529 "6-MERCAPTOPURINE MONOHYDRATE"
* #100000085529 ^designation[0].language = #de-AT 
* #100000085529 ^designation[0].value = "MERCAPTOPURIN MONOHYDRAT" 
* #100000085540 "PEGASPARGASE"
* #100000085608 "URAPIDIL HYDROCHLORIDE"
* #100000085608 ^designation[0].language = #de-AT 
* #100000085608 ^designation[0].value = "URAPIDIL HYDROCHLORID" 
* #100000085609 "SODIUM GADOXETATE"
* #100000085609 ^designation[0].language = #de-AT 
* #100000085609 ^designation[0].value = "DINATRIUM GADOXETAT" 
* #100000085626 "MEROPENEM TRIHYDRATE"
* #100000085626 ^designation[0].language = #de-AT 
* #100000085626 ^designation[0].value = "MEROPENEM TRIHYDRAT" 
* #100000085634 "FOLIC ACID"
* #100000085634 ^designation[0].language = #de-AT 
* #100000085634 ^designation[0].value = "FOLSÄURE" 
* #100000085642 "CALCIPOTRIOL"
* #100000085708 "NAFTIFINE HYDROCHLORIDE"
* #100000085708 ^designation[0].language = #de-AT 
* #100000085708 ^designation[0].value = "NAFTIFIN HYDROCHLORID" 
* #100000085740 "NONOXINOL"
* #100000085740 ^designation[0].language = #de-AT 
* #100000085740 ^designation[0].value = "NONOXYNOL" 
* #100000085744 "NOSCAPINE HYDROCHLORIDE"
* #100000085744 ^designation[0].language = #de-AT 
* #100000085744 ^designation[0].value = "NOSCAPINHYDROCHLORID" 
* #100000085789 "DICLOFENAC EPOLAMINE"
* #100000085789 ^designation[0].language = #de-AT 
* #100000085789 ^designation[0].value = "EPOLAMIN DICLOFENACAT" 
* #100000085821 "ZUCLOPENTHIXOL DECANOATE"
* #100000085821 ^designation[0].language = #de-AT 
* #100000085821 ^designation[0].value = "ZUCLOPENTHIXOL DECANOAT" 
* #100000085825 "WATER"
* #100000085825 ^designation[0].language = #de-AT 
* #100000085825 ^designation[0].value = "WASSER" 
* #100000085826 "HALOPERIDOL DECANOATE"
* #100000085826 ^designation[0].language = #de-AT 
* #100000085826 ^designation[0].value = "HALOPERIDOL DECANOAT" 
* #100000085834 "PROTEIN S"
* #100000085858 "BROTIZOLAM"
* #100000085872 "BETIATIDE"
* #100000085872 ^designation[0].language = #de-AT 
* #100000085872 ^designation[0].value = "BETIATID" 
* #100000085878 "BICALUTAMIDE"
* #100000085878 ^designation[0].language = #de-AT 
* #100000085878 ^designation[0].value = "BICALUTAMID" 
* #100000085894 "BEZAFIBRATE"
* #100000085894 ^designation[0].language = #de-AT 
* #100000085894 ^designation[0].value = "BEZAFIBRAT" 
* #100000085897 "BIBROCATHOL"
* #100000085918 "OMEPRAZOLE MAGNESIUM"
* #100000085918 ^designation[0].language = #de-AT 
* #100000085918 ^designation[0].value = "OMEPRAZOL MAGNESIUM" 
* #100000085933 "OCTENIDINE DIHYDROCHLORIDE"
* #100000085933 ^designation[0].language = #de-AT 
* #100000085933 ^designation[0].value = "OCTENIDIN DIHYDROCHLORID" 
* #100000085944 "METAMIZOLE SODIUM MONOHYDRATE"
* #100000085944 ^designation[0].language = #de-AT 
* #100000085944 ^designation[0].value = "METAMIZOL NATRIUM MONOHYDRAT" 
* #100000085993 "SODIUM SELENATE"
* #100000085993 ^designation[0].language = #de-AT 
* #100000085993 ^designation[0].value = "NATRIUMSELENAT" 
* #100000086014 "THIOCTIC ACID"
* #100000086014 ^designation[0].language = #de-AT 
* #100000086014 ^designation[0].value = "THIOCTSÄURE" 
* #100000086021 "TERAZOSIN HYDROCHLORIDE DIHYDRATE"
* #100000086021 ^designation[0].language = #de-AT 
* #100000086021 ^designation[0].value = "TERAZOSINHYDROCHLORID DIHYDRAT" 
* #100000086036 "ASCORBIC ACID"
* #100000086036 ^designation[0].language = #de-AT 
* #100000086036 ^designation[0].value = "ASCORBINSÄURE" 
* #100000086038 "NICOMORPHINE HYDROCHLORIDE"
* #100000086038 ^designation[0].language = #de-AT 
* #100000086038 ^designation[0].value = "NICOMORPHIN HYDROCHLORID" 
* #100000086058 "NICOTINE TARTRATE"
* #100000086058 ^designation[0].language = #de-AT 
* #100000086058 ^designation[0].value = "NICOTINDITARTRAT" 
* #100000086180 "MARIBAVIR"
* #100000086181 "MEBEVERINE HYDROCHLORIDE"
* #100000086181 ^designation[0].language = #de-AT 
* #100000086181 ^designation[0].value = "MEBEVERIN HYDROCHLORID" 
* #100000086275 "ALPHA TOCOPHEROL"
* #100000086275 ^designation[0].language = #de-AT 
* #100000086275 ^designation[0].value = "ALPHA-TOCOPHEROL" 
* #100000086292 "HUMAN PAPILLOMAVIRUS TYPE 11 L1 PROTEIN"
* #100000086292 ^designation[0].language = #de-AT 
* #100000086292 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 11 L1-PROTEIN" 
* #100000086311 "HUMAN PAPILLOMAVIRUS TYPE 6 L1 PROTEIN"
* #100000086311 ^designation[0].language = #de-AT 
* #100000086311 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 6 L1-PROTEIN" 
* #100000086312 "HUMAN PAPILLOMAVIRUS TYPE 16 L1 PROTEIN"
* #100000086312 ^designation[0].language = #de-AT 
* #100000086312 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 16 L1-PROTEIN" 
* #100000086313 "HUMAN PAPILLOMAVIRUS TYPE 18 L1 PROTEIN"
* #100000086313 ^designation[0].language = #de-AT 
* #100000086313 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 18 L1-PROTEIN" 
* #100000086395 "BENZYL ALCOHOL"
* #100000086395 ^designation[0].language = #de-AT 
* #100000086395 ^designation[0].value = "BENZYLALKOHOL" 
* #100000086397 "MELITRACEN HYDROCHLORIDE"
* #100000086397 ^designation[0].language = #de-AT 
* #100000086397 ^designation[0].value = "MELITRACEN HYDROCHLORID" 
* #100000086473 "NEOSTIGMINE METHYLSULFATE"
* #100000086473 ^designation[0].language = #de-AT 
* #100000086473 ^designation[0].value = "NEOSTIGMIN METILSULFAT" 
* #100000086553 "DIPOTASSIUM PHOSPHATE"
* #100000086553 ^designation[0].language = #de-AT 
* #100000086553 ^designation[0].value = "KALIUMMONOHYDROGENPHOSPHAT" 
* #100000086577 "PIRITRAMIDE"
* #100000086577 ^designation[0].language = #de-AT 
* #100000086577 ^designation[0].value = "PIRITRAMID" 
* #100000086598 "ARGININE"
* #100000086598 ^designation[0].language = #de-AT 
* #100000086598 ^designation[0].value = "ARGININ" 
* #100000086618 "ARGIPRESSIN"
* #100000086625 "ARTICAINE"
* #100000086625 ^designation[0].language = #de-AT 
* #100000086625 ^designation[0].value = "ARTICAIN" 
* #100000086636 "ATOVAQUONE"
* #100000086636 ^designation[0].language = #de-AT 
* #100000086636 ^designation[0].value = "ATOVAQUON" 
* #100000086645 "LENOGRASTIM"
* #100000086649 "D-GLUCOSE 1-PHOSPHATE DISODIUM"
* #100000086649 ^designation[0].language = #de-AT 
* #100000086649 ^designation[0].value = "D-GLUCOSE 1-PHOSPHAT DINATRIUMSALZ" 
* #100000086659 "AURANOFIN"
* #100000086663 "LAUROMACROGOL 400"
* #100000086673 "GOSERELIN ACETATE"
* #100000086673 ^designation[0].language = #de-AT 
* #100000086673 ^designation[0].value = "GOSERELIN ACETAT" 
* #100000086685 "HISTRELIN ACETATE"
* #100000086685 ^designation[0].language = #de-AT 
* #100000086685 ^designation[0].value = "HISTRELIN ACETAT" 
* #100000086689 "HYALURONIDASE"
* #100000086691 "HYDROCORTISONE SODIUM PHOSPHATE"
* #100000086691 ^designation[0].language = #de-AT 
* #100000086691 ^designation[0].value = "HYDROCORTISON-21- PHOSPHAT DINATRIUM" 
* #100000086703 "HEXOPRENALINE SULFATE"
* #100000086703 ^designation[0].language = #de-AT 
* #100000086703 ^designation[0].value = "HEXOPRENALIN SULFAT" 
* #100000086724 "SACCHAROSE"
* #100000086898 "TREPROSTINIL SODIUM"
* #100000086898 ^designation[0].language = #de-AT 
* #100000086898 ^designation[0].value = "TREPROSTINIL NATRIUM" 
* #100000086905 "AZACITIDINE"
* #100000086905 ^designation[0].language = #de-AT 
* #100000086905 ^designation[0].value = "AZACITIDIN" 
* #100000086938 "AMYLMETACRESOL"
* #100000086971 "FLUCLOXACILLIN SODIUM"
* #100000086971 ^designation[0].language = #de-AT 
* #100000086971 ^designation[0].value = "NATRIUM FLUCLOXACILLINAT" 
* #100000086983 "INDINAVIR SULFATE"
* #100000086983 ^designation[0].language = #de-AT 
* #100000086983 ^designation[0].value = "INDINAVIR SULFAT" 
* #100000087002 "FUSAFUNGINE"
* #100000087002 ^designation[0].language = #de-AT 
* #100000087002 ^designation[0].value = "FUSAFUNGIN" 
* #100000087080 "CARMELLOSE SODIUM"
* #100000087080 ^designation[0].language = #de-AT 
* #100000087080 ^designation[0].value = "CARMELLOSE NATRIUM" 
* #100000087110 "LACTIC ACID"
* #100000087110 ^designation[0].language = #de-AT 
* #100000087110 ^designation[0].value = "MILCHSÄURE" 
* #100000087131 "HYPROMELLOSE"
* #100000087132 "ISOPRENALINE HYDROCHLORIDE"
* #100000087132 ^designation[0].language = #de-AT 
* #100000087132 ^designation[0].value = "ISOPRENALIN HYDROCHLORID" 
* #100000087146 "DIDANOSINE"
* #100000087146 ^designation[0].language = #de-AT 
* #100000087146 ^designation[0].value = "DIDANOSIN" 
* #100000087202 "AMIDEFRINE MESILATE"
* #100000087202 ^designation[0].language = #de-AT 
* #100000087202 ^designation[0].value = "AMIDEFRIN MESILAT" 
* #100000087234 "AMISULPRIDE"
* #100000087234 ^designation[0].language = #de-AT 
* #100000087234 ^designation[0].value = "AMISULPRID" 
* #100000087239 "GLUCAGON HYDROCHLORIDE"
* #100000087239 ^designation[0].language = #de-AT 
* #100000087239 ^designation[0].value = "GLUCAGON HYDROCHLORID" 
* #100000087245 "EPINASTINE HYDROCHLORIDE"
* #100000087245 ^designation[0].language = #de-AT 
* #100000087245 ^designation[0].value = "EPINASTIN HYDROCHLORID" 
* #100000087265 "EPINEPHRINE BITARTRATE"
* #100000087265 ^designation[0].language = #de-AT 
* #100000087265 ^designation[0].value = "EPINEPHRIN HYDROGENTARTRAT" 
* #100000087271 "ESMOLOL HYDROCHLORIDE"
* #100000087271 ^designation[0].language = #de-AT 
* #100000087271 ^designation[0].value = "ESMOLOL HYDROCHLORID" 
* #100000087279 "ETHAMBUTOL DIHYDROCHLORIDE"
* #100000087279 ^designation[0].language = #de-AT 
* #100000087279 ^designation[0].value = "ETHAMBUTOL DIHYDROCHLORID" 
* #100000087294 "ESTRAMUSTINE PHOSPHATE SODIUM"
* #100000087294 ^designation[0].language = #de-AT 
* #100000087294 ^designation[0].value = "DINATRIUM ESTRAMUSTIN PHOSPHAT" 
* #100000087353 "CALCIUM HYDROGEN PHOSPHATE, ANHYDROUS"
* #100000087353 ^designation[0].language = #de-AT 
* #100000087353 ^designation[0].value = "CALCIUMHYDROGENPHOSPHAT, WASSERFREIES" 
* #100000087358 "TIN CHLORIDE DIHYDRATE"
* #100000087358 ^designation[0].language = #de-AT 
* #100000087358 ^designation[0].value = "ZINN(II)CHLORID DIHYDRAT" 
* #100000087360 "GENTAMICIN SULFATE"
* #100000087360 ^designation[0].language = #de-AT 
* #100000087360 ^designation[0].value = "GENTAMICINSULFAT" 
* #100000087496 "ETILEFRINE HYDROCHLORIDE"
* #100000087496 ^designation[0].language = #de-AT 
* #100000087496 ^designation[0].value = "ETILEFRIN HYDROCHLORID" 
* #100000087506 "DIHYDROCODEINE THIOCYANATE"
* #100000087506 ^designation[0].language = #de-AT 
* #100000087506 ^designation[0].value = "DIHYDROCODEIN THIOCYANAT" 
* #100000087512 "DIMETINDENE MALEATE"
* #100000087512 ^designation[0].language = #de-AT 
* #100000087512 ^designation[0].value = "DIMETINDEN MALEAT" 
* #100000087521 "DOBUTAMINE HYDROCHLORIDE"
* #100000087521 ^designation[0].language = #de-AT 
* #100000087521 ^designation[0].value = "DOBUTAMIN HYDROCHLORID" 
* #100000087530 "GIMERACIL"
* #100000087536 "DIPHENYLPYRALINE HYDROCHLORIDE"
* #100000087536 ^designation[0].language = #de-AT 
* #100000087536 ^designation[0].value = "DIPHENYLPYRALIN HYDROCHLORID" 
* #100000087548 "OTERACIL POTASSIUM"
* #100000087548 ^designation[0].language = #de-AT 
* #100000087548 ^designation[0].value = "OTERACIL KALIUM" 
* #100000087570 "L-CYSTEINE"
* #100000087570 ^designation[0].language = #de-AT 
* #100000087570 ^designation[0].value = "L-CYSTEIN" 
* #100000087571 "ESTROGENS, CONJUGATED"
* #100000087571 ^designation[0].language = #de-AT 
* #100000087571 ^designation[0].value = "KONJUGIERTE ESTROGENE" 
* #100000087594 "MAGNESIUM OXIDE"
* #100000087594 ^designation[0].language = #de-AT 
* #100000087594 ^designation[0].value = "MAGNESIUMOXID" 
* #100000087614 "GLYCERYL TRINITRATE"
* #100000087614 ^designation[0].language = #de-AT 
* #100000087614 ^designation[0].value = "GLYCEROLTRINITRAT" 
* #100000087657 "LIQUIRITIAE EXTRACTUM FLUIDUM"
* #100000087657 ^designation[0].language = #de-AT 
* #100000087657 ^designation[0].value = "LIQUIRITIAE RADIX (AUSZUG)" 
* #100000087680 "ADAPALENE"
* #100000087680 ^designation[0].language = #de-AT 
* #100000087680 ^designation[0].value = "ADAPALEN" 
* #100000087712 "ALBENDAZOL"
* #100000087773 "DESMOPRESSIN ACETATE"
* #100000087773 ^designation[0].language = #de-AT 
* #100000087773 ^designation[0].value = "DESMOPRESSIN ACETAT" 
* #100000087825 "LACTOBACILLUS ACIDOPHILUS DSM 4149; AQUAEOUS SUBSTRATE OF METABOLIC PRODUCTS"
* #100000087825 ^designation[0].language = #de-AT 
* #100000087825 ^designation[0].value = "LACTOBACILLUS ACIDOPHILUS (AUSZUG, PRODUKTE)" 
* #100000087827 "PLANTAGINIS OVATAE SEMINIS TEGUMENTUM"
* #100000087883 "VINDESINE SULFATE"
* #100000087883 ^designation[0].language = #de-AT 
* #100000087883 ^designation[0].value = "VINDESIN SULFAT" 
* #100000087899 "VINBLASTINE SULFATE"
* #100000087899 ^designation[0].language = #de-AT 
* #100000087899 ^designation[0].value = "VINBLASTIN SULFAT" 
* #100000087912 "ACEMETACIN"
* #100000087968 "CYCLIZINE HYDROCHLORIDE"
* #100000087968 ^designation[0].language = #de-AT 
* #100000087968 ^designation[0].value = "CYCLIZIN HYDROCHLORID" 
* #100000087970 "SUNITINIB"
* #100000087979 "COLESTYRAMINE"
* #100000087979 ^designation[0].language = #de-AT 
* #100000087979 ^designation[0].value = "COLESTYRAMIN" 
* #100000087994 "ZINC SULPHATE MONOHYDRATE"
* #100000087994 ^designation[0].language = #de-AT 
* #100000087994 ^designation[0].value = "ZINKSULFAT-MONOHYDRAT" 
* #100000087995 "IDURSULFASE"
* #100000087996 "TACROLIMUS MONOHYDRATE"
* #100000087996 ^designation[0].language = #de-AT 
* #100000087996 ^designation[0].value = "TACROLIMUS MONOHYDRAT" 
* #100000088000 "VALSARTAN"
* #100000088002 "ZIDOVUDINE"
* #100000088002 ^designation[0].language = #de-AT 
* #100000088002 ^designation[0].value = "ZIDOVUDIN" 
* #100000088014 "VARDENAFIL HYDROCHLORIDE TRIHYDRATE"
* #100000088014 ^designation[0].language = #de-AT 
* #100000088014 ^designation[0].value = "VARDENAFIL HYDROCHLORID TRIHYDRAT" 
* #100000088015 "DASATINIB"
* #100000088017 "TENOFOVIR DISOPROXIL"
* #100000088018 "VORICONAZOL"
* #100000088020 "ZONISAMIDE"
* #100000088020 ^designation[0].language = #de-AT 
* #100000088020 ^designation[0].value = "ZONISAMID" 
* #100000088028 "VIBRIO CHOLERAE"
* #100000088029 "ROTAVIRUS"
* #100000088053 "RANIBIZUMAB"
* #100000088058 "DISODIUM CLODRONATE"
* #100000088058 ^designation[0].language = #de-AT 
* #100000088058 ^designation[0].value = "DINATRIUM CLODRONAT" 
* #100000088075 "PINUS PUMILIO OIL"
* #100000088075 ^designation[0].language = #de-AT 
* #100000088075 ^designation[0].value = "PINI PUMILIONIS AETHEROLEUM" 
* #100000088079 "MIDOSTAURIN"
* #100000088090 "YEAST"
* #100000088090 ^designation[0].language = #de-AT 
* #100000088090 ^designation[0].value = "HEFE" 
* #100000088109 "SALAMIDACETIC ACID"
* #100000088109 ^designation[0].language = #de-AT 
* #100000088109 ^designation[0].value = "2-(2-CARBAMOYLPHENOXY)ESSIGSÄURE" 
* #100000088153 "IMIPENEM"
* #100000088206 "LANDIOLOL HYDROCHLORIDE"
* #100000088206 ^designation[0].language = #de-AT 
* #100000088206 ^designation[0].value = "LANDIOLOL HYDROCHLORID" 
* #100000088212 "BUPIVACAINE HYDROCHLORIDE MONOHYDRATE"
* #100000088212 ^designation[0].language = #de-AT 
* #100000088212 ^designation[0].value = "BUPIVACAINHYDROCHLORID MONOHYDRAT" 
* #100000088217 "FOSFOMYCIN SODIUM"
* #100000088217 ^designation[0].language = #de-AT 
* #100000088217 ^designation[0].value = "DINATRIUM FOSFOMYCINAT" 
* #100000088223 "CISPLATIN"
* #100000088235 "VINORELBINE TARTRATE"
* #100000088235 ^designation[0].language = #de-AT 
* #100000088235 ^designation[0].value = "VINORELBINTARTRAT" 
* #100000088238 "SODIUM SELENITE"
* #100000088238 ^designation[0].language = #de-AT 
* #100000088238 ^designation[0].value = "DINATRIUMSELENIT" 
* #100000088244 "DOCOSANOL"
* #100000088253 "NALBUPHINE HYDROCHLORIDE"
* #100000088253 ^designation[0].language = #de-AT 
* #100000088253 ^designation[0].value = "NALBUPHIN HYDROCHLORID" 
* #100000088254 "METHOHEXITAL SODIUM"
* #100000088254 ^designation[0].language = #de-AT 
* #100000088254 ^designation[0].value = "METHOHEXITAL NATRIUM" 
* #100000088260 "PHENYLEPHRINE"
* #100000088260 ^designation[0].language = #de-AT 
* #100000088260 ^designation[0].value = "PHENYLEPHRIN" 
* #100000088272 "BROMELAIN"
* #100000088297 "TIANEPTINE SODIUM"
* #100000088297 ^designation[0].language = #de-AT 
* #100000088297 ^designation[0].value = "TIANEPTIN NATRIUM" 
* #100000088298 "ZUCLOPENTHIXOL ACETATE"
* #100000088298 ^designation[0].language = #de-AT 
* #100000088298 ^designation[0].value = "ZUCLOPENTHIXOL ACETAT" 
* #100000088304 "N-ACETYLTYROSINE"
* #100000088304 ^designation[0].language = #de-AT 
* #100000088304 ^designation[0].value = "N-ACETYLTYROSIN" 
* #100000088309 "COMPLEMENT C1 ESTERASE INHIBITOR"
* #100000088309 ^designation[0].language = #de-AT 
* #100000088309 ^designation[0].value = "C1-ESTERASE-INHIBITOR" 
* #100000088337 "CALCIUM PHOSPHATE"
* #100000088337 ^designation[0].language = #de-AT 
* #100000088337 ^designation[0].value = "CALCIUMPHOSPHAT" 
* #100000088371 "FENOTEROL HYDROBROMIDE"
* #100000088371 ^designation[0].language = #de-AT 
* #100000088371 ^designation[0].value = "FENOTEROL HYDROBROMID" 
* #100000088372 "NOREPINEPHRINE BITARTRATE"
* #100000088372 ^designation[0].language = #de-AT 
* #100000088372 ^designation[0].value = "NOREPINEPHRINTARTRAT MONOHYDRAT" 
* #100000088393 "MUPIROCIN CALCIUM DIHYDRATE"
* #100000088393 ^designation[0].language = #de-AT 
* #100000088393 ^designation[0].value = "CALCIUM MUPIROCINAT DIHYDRAT" 
* #100000088394 "DEMECLOCYCLINE HYDROCHLORIDE"
* #100000088394 ^designation[0].language = #de-AT 
* #100000088394 ^designation[0].value = "DEMECLOCYCLIN HYDROCHLORID" 
* #100000088395 "DACARBAZINE"
* #100000088395 ^designation[0].language = #de-AT 
* #100000088395 ^designation[0].value = "DACARBAZIN" 
* #100000088399 "MOXONIDINE"
* #100000088399 ^designation[0].language = #de-AT 
* #100000088399 ^designation[0].value = "MOXONIDIN" 
* #100000088414 "FROVATRIPTAN SUCCINATE"
* #100000088414 ^designation[0].language = #de-AT 
* #100000088414 ^designation[0].value = "FROVATRIPTAN SUCCINAT" 
* #100000088415 "FLUPENTIXOL DECANOATE"
* #100000088415 ^designation[0].language = #de-AT 
* #100000088415 ^designation[0].value = "FLUPENTIXOL DECANOAT" 
* #100000088443 "BUMETANIDE"
* #100000088443 ^designation[0].language = #de-AT 
* #100000088443 ^designation[0].value = "BUMETANID" 
* #100000088449 "BUPIVACAINE"
* #100000088449 ^designation[0].language = #de-AT 
* #100000088449 ^designation[0].value = "BUPIVACAIN" 
* #100000088482 "AMOROLFINE HYDROCHLORIDE"
* #100000088482 ^designation[0].language = #de-AT 
* #100000088482 ^designation[0].value = "AMOROLFIN HYDROCHLORID" 
* #100000088484 "ARGININE ASPARTATE"
* #100000088484 ^designation[0].language = #de-AT 
* #100000088484 ^designation[0].value = "ARGININ ASPARTAT" 
* #100000088486 "ANISE OIL"
* #100000088486 ^designation[0].language = #de-AT 
* #100000088486 ^designation[0].value = "ANISI AETHEROLEUM" 
* #100000088498 "HESPERIDIN"
* #100000088501 "TIOTROPIUM BROMIDE MONOHYDRATE"
* #100000088501 ^designation[0].language = #de-AT 
* #100000088501 ^designation[0].value = "TIOTROPIUMBROMID-MONOHYDRAT" 
* #100000088526 "FENFLURAMINE HYDROCHLORIDE"
* #100000088526 ^designation[0].language = #de-AT 
* #100000088526 ^designation[0].value = "FENFLURAMIN HYDROCHLORID" 
* #100000088544 "IODIXANOL"
* #100000088589 "THIAMINE DISULFIDE"
* #100000088589 ^designation[0].language = #de-AT 
* #100000088589 ^designation[0].value = "THIAMIN DISULFID" 
* #100000088590 "COPPER SULFATE"
* #100000088590 ^designation[0].language = #de-AT 
* #100000088590 ^designation[0].value = "KUPFERSULFAT" 
* #100000088634 "BUTIZIDE"
* #100000088634 ^designation[0].language = #de-AT 
* #100000088634 ^designation[0].value = "BUTIZID" 
* #100000088646 "BRIVUDINE"
* #100000088646 ^designation[0].language = #de-AT 
* #100000088646 ^designation[0].value = "BRIVUDIN" 
* #100000088685 "ALGELDRATE"
* #100000088685 ^designation[0].language = #de-AT 
* #100000088685 ^designation[0].value = "ALUMINIUMHYDROXID" 
* #100000088693 "BROMFENAC"
* #100000088767 "FLUNARIZINE DIHYDROCHLORIDE"
* #100000088767 ^designation[0].language = #de-AT 
* #100000088767 ^designation[0].value = "FLUNARIZIN DIHYDROCHLORID" 
* #100000088809 "TETRACAINE"
* #100000088809 ^designation[0].language = #de-AT 
* #100000088809 ^designation[0].value = "TETRACAIN" 
* #100000088816 "PERINDOPRIL ARGININE"
* #100000088816 ^designation[0].language = #de-AT 
* #100000088816 ^designation[0].value = "PERINDOPRIL ARGININ" 
* #100000088819 "SULTIAME"
* #100000088819 ^designation[0].language = #de-AT 
* #100000088819 ^designation[0].value = "SULTIAM" 
* #100000088820 "ZIPRASIDONE HYDROCHLORIDE"
* #100000088820 ^designation[0].language = #de-AT 
* #100000088820 ^designation[0].value = "ZIPRASIDONHYDROCHLORID" 
* #100000088825 "SILVER PROTEIN"
* #100000088825 ^designation[0].language = #de-AT 
* #100000088825 ^designation[0].value = "PROTEIN-SILBER" 
* #100000088827 "CORTICORELIN TRIFLUTATE"
* #100000088827 ^designation[0].language = #de-AT 
* #100000088827 ^designation[0].value = "CORTICORELIN TRIFLUTAT" 
* #100000088868 "CORYNEBACTERIUM DIPHTHERIAE"
* #100000088868 ^designation[0].language = #de-AT 
* #100000088868 ^designation[0].value = "CORYNEBACTERIUM DIPHTHERIAE (AUSZUG, PRODUKTE)" 
* #100000088884 "ERYTHROMYCIN LACTOBIONATE"
* #100000088884 ^designation[0].language = #de-AT 
* #100000088884 ^designation[0].value = "ERYTHROMYCIN LACTOBIONAT" 
* #100000088912 "CALCIUM GLUCONATE"
* #100000088912 ^designation[0].language = #de-AT 
* #100000088912 ^designation[0].value = "CALCIUMGLUCONAT" 
* #100000088915 "CALCIPOTRIOL, ANHYDROUS"
* #100000088915 ^designation[0].language = #de-AT 
* #100000088915 ^designation[0].value = "CALCIPOTRIOL, WASSERFREI" 
* #100000088916 "SULFADIAZINE SILVER"
* #100000088916 ^designation[0].language = #de-AT 
* #100000088916 ^designation[0].value = "SULFADIAZIN SILBER" 
* #100000088995 "LACTOBIONIC ACID"
* #100000088995 ^designation[0].language = #de-AT 
* #100000088995 ^designation[0].value = "LACTOBIONSÄURE" 
* #100000088999 "ACETIC ACID"
* #100000088999 ^designation[0].language = #de-AT 
* #100000088999 ^designation[0].value = "ESSIGSÄURE" 
* #100000089009 "TESTOSTERONE UNDECYLATE"
* #100000089009 ^designation[0].language = #de-AT 
* #100000089009 ^designation[0].value = "TESTOSTERON UNDECENOAT" 
* #100000089010 "RIBOFLAVIN"
* #100000089013 "RIBOFLAVIN SODIUM PHOSPHATE DIHYDRATE"
* #100000089013 ^designation[0].language = #de-AT 
* #100000089013 ^designation[0].value = "NATRIUM RIBOFLAVINPHOSPHAT DIHYDRAT" 
* #100000089016 "NICOTINE RESINATE"
* #100000089016 ^designation[0].language = #de-AT 
* #100000089016 ^designation[0].value = "NICOTINRESINAT" 
* #100000089026 "RIBOFLAVIN SODIUM PHOSPHATE"
* #100000089026 ^designation[0].language = #de-AT 
* #100000089026 ^designation[0].value = "NATRIUM RIBOFLAVINPHOSPHAT" 
* #100000089042 "HYDROXOCOBALAMIN ACETATE"
* #100000089042 ^designation[0].language = #de-AT 
* #100000089042 ^designation[0].value = "HYDROXOCOBALAMIN ACETAT" 
* #100000089047 "AZITHROMYCIN DIHYDRATE"
* #100000089047 ^designation[0].language = #de-AT 
* #100000089047 ^designation[0].value = "AZITHROMYCIN DIHYDRAT" 
* #100000089059 "PYRANTEL EMBONATE"
* #100000089059 ^designation[0].language = #de-AT 
* #100000089059 ^designation[0].value = "PYRANTEL EMBONAT" 
* #100000089060 "TACALCITOL MONOHYDRATE"
* #100000089060 ^designation[0].language = #de-AT 
* #100000089060 ^designation[0].value = "TACALCITOL MONOHYDRAT" 
* #100000089067 "LEVOFLOXACIN"
* #100000089087 "HUMAN COAGULATION FACTOR IX"
* #100000089087 ^designation[0].language = #de-AT 
* #100000089087 ^designation[0].value = "GERINNUNGSFAKTOR IX" 
* #100000089090 "ALENDRONATE SODIUM"
* #100000089090 ^designation[0].language = #de-AT 
* #100000089090 ^designation[0].value = "NATRIUMALENDRONAT" 
* #100000089092 "HUMAN COAGULATION FACTOR VII"
* #100000089092 ^designation[0].language = #de-AT 
* #100000089092 ^designation[0].value = "GERINNUNGSFAKTOR VII" 
* #100000089108 "ANTITHROMBIN III"
* #100000089109 "HUMAN COAGULATION FACTOR II"
* #100000089109 ^designation[0].language = #de-AT 
* #100000089109 ^designation[0].value = "GERINNUNGSFAKTOR II" 
* #100000089110 "HUMAN COAGULATION FACTOR VIII"
* #100000089110 ^designation[0].language = #de-AT 
* #100000089110 ^designation[0].value = "GERINNUNGSFAKTOR VIII" 
* #100000089111 "HEPATITIS B SURFACE ANTIGEN"
* #100000089111 ^designation[0].language = #de-AT 
* #100000089111 ^designation[0].value = "HEPATITIS-B-VIRUS-ANTIGEN" 
* #100000089113 "FACTOR X"
* #100000089113 ^designation[0].language = #de-AT 
* #100000089113 ^designation[0].value = "GERINNUNGSFAKTOR X" 
* #100000089118 "HUMAN PLASMA (POOLED AND TREATED FOR VIRUS INACTIVATION)"
* #100000089118 ^designation[0].language = #de-AT 
* #100000089118 ^designation[0].value = "BLUTPLASMA" 
* #100000089121 "ZINC SULPHATE"
* #100000089121 ^designation[0].language = #de-AT 
* #100000089121 ^designation[0].value = "ZINKSULFAT" 
* #100000089131 "ECULIZUMAB"
* #100000089135 "SODIUM OXYBATE"
* #100000089135 ^designation[0].language = #de-AT 
* #100000089135 ^designation[0].value = "NATRIUM 4-HYDROXYBUTYRAT" 
* #100000089139 "EMEDASTINE DIFUMARATE"
* #100000089139 ^designation[0].language = #de-AT 
* #100000089139 ^designation[0].value = "EMEDASTIN DIFUMARAT" 
* #100000089148 "EPOETIN ZETA"
* #100000089156 "VILDAGLIPTIN"
* #100000089157 "ZICONOTIDE ACETATE"
* #100000089157 ^designation[0].language = #de-AT 
* #100000089157 ^designation[0].value = "ZICONOTID ACETAT" 
* #100000089158 "GLUCARPIDASE"
* #100000089164 "RANOLAZINE"
* #100000089164 ^designation[0].language = #de-AT 
* #100000089164 ^designation[0].value = "RANOLAZIN" 
* #100000089166 "RIBAVIRIN"
* #100000089167 "RITONAVIR"
* #100000089170 "SIROLIMUS"
* #100000089172 "TACROLIMUS"
* #100000089183 "RETEPLASE"
* #100000089184 "RILUZOL"
* #100000089185 "RIVASTIGMINE"
* #100000089185 ^designation[0].language = #de-AT 
* #100000089185 ^designation[0].value = "RIVASTIGMIN" 
* #100000089186 "RUFINAMIDE"
* #100000089186 ^designation[0].language = #de-AT 
* #100000089186 ^designation[0].value = "RUFINAMID" 
* #100000089187 "SILDENAFIL"
* #100000089188 "SOMATROPIN"
* #100000089189 "SUFENTANIL"
* #100000089190 "TELMISARTAN"
* #100000089191 "TESTOSTERONE"
* #100000089191 ^designation[0].language = #de-AT 
* #100000089191 ^designation[0].value = "TESTOSTERON" 
* #100000089192 "TIMOLOL"
* #100000089193 "TERIPARATIDE"
* #100000089193 ^designation[0].language = #de-AT 
* #100000089193 ^designation[0].value = "TERIPARATID" 
* #100000089194 "THALIDOMIDE"
* #100000089194 ^designation[0].language = #de-AT 
* #100000089194 ^designation[0].value = "THALIDOMID" 
* #100000089195 "TOLCAPONE"
* #100000089195 ^designation[0].language = #de-AT 
* #100000089195 ^designation[0].value = "TOLCAPON" 
* #100000089203 "HISTAMINE DIHYDROCHLORIDE"
* #100000089203 ^designation[0].language = #de-AT 
* #100000089203 ^designation[0].value = "HISTAMINDIHYDROCHLORID" 
* #100000089218 "ESTRADIOL HEMIHYDRATE"
* #100000089218 ^designation[0].language = #de-AT 
* #100000089218 ^designation[0].value = "ESTRADIOL HEMIHYDRAT" 
* #100000089225 "NALOXONE HYDROCHLORIDE DIHYDRATE"
* #100000089225 ^designation[0].language = #de-AT 
* #100000089225 ^designation[0].value = "NALOXON HYDROCHLORID DIHYDRAT" 
* #100000089234 "TERIFLUNOMIDE"
* #100000089234 ^designation[0].language = #de-AT 
* #100000089234 ^designation[0].value = "TERIFLUNOMID" 
* #100000089235 "SAXAGLIPTIN"
* #100000089237 "NILOTINIB"
* #100000089242 "LIRAGLUTIDE"
* #100000089242 ^designation[0].language = #de-AT 
* #100000089242 ^designation[0].value = "LIRAGLUTID" 
* #100000089243 "YELLOW FEVER VIRUS"
* #100000089243 ^designation[0].language = #de-AT 
* #100000089243 ^designation[0].value = "GELBFIEBERVIRUS" 
* #100000089244 "HEPATITIS A VIRUS"
* #100000089244 ^designation[0].language = #de-AT 
* #100000089244 ^designation[0].value = "HEPATITIS-A-VIRUS" 
* #100000089248 "BORDETELLA PERTUSSIS"
* #100000089253 "OFATUMUMAB"
* #100000089254 "MARAVIROC"
* #100000089256 "MERCAPTAMINE HYDROCHLORIDE"
* #100000089256 ^designation[0].language = #de-AT 
* #100000089256 ^designation[0].value = "MERCAPTAMIN HYDROCHLORID" 
* #100000089257 "TELBIVUDINE"
* #100000089257 ^designation[0].language = #de-AT 
* #100000089257 ^designation[0].value = "TELBIVUDIN" 
* #100000089278 "CLOSTRIDIUM TETANI"
* #100000089278 ^designation[0].language = #de-AT 
* #100000089278 ^designation[0].value = "CLOSTRIDIUM TETANI (AUSZUG, PRODUKTE)" 
* #100000089281 "MUMPS VIRUS"
* #100000089281 ^designation[0].language = #de-AT 
* #100000089281 ^designation[0].value = "MUMPSVIRUS" 
* #100000089283 "AGALSIDASE BETA"
* #100000089284 "ARSENIC TRIOXIDE"
* #100000089284 ^designation[0].language = #de-AT 
* #100000089284 ^designation[0].value = "DIARSENTRIOXID" 
* #100000089286 "CASPOFUNGIN ACETATE"
* #100000089286 ^designation[0].language = #de-AT 
* #100000089286 ^designation[0].value = "CASPOFUNGIN ACETAT" 
* #100000089288 "EPTIFIBATIDE"
* #100000089288 ^designation[0].language = #de-AT 
* #100000089288 ^designation[0].value = "EPTIFIBATID" 
* #100000089289 "FONDAPARINUX SODIUM"
* #100000089289 ^designation[0].language = #de-AT 
* #100000089289 ^designation[0].value = "FONDAPARINUX NATRIUM" 
* #100000089291 "OMALIZUMAB"
* #100000089292 "RITUXIMAB"
* #100000089301 "APOMORPHINE HYDROCHLORIDE HEMIHYDRATE"
* #100000089301 ^designation[0].language = #de-AT 
* #100000089301 ^designation[0].value = "APOMORPHINHYDROCHLORID HEMIHYDRAT" 
* #100000089302 "BIMATOPROST"
* #100000089303 "CAPECITABINE"
* #100000089303 ^designation[0].language = #de-AT 
* #100000089303 ^designation[0].value = "CAPECITABIN" 
* #100000089304 "CETRORELIX ACETATE"
* #100000089304 ^designation[0].language = #de-AT 
* #100000089304 ^designation[0].value = "CETRORELIX ACETAT" 
* #100000089306 "FOLLITROPIN BETA"
* #100000089309 "PARECOXIB SODIUM"
* #100000089309 ^designation[0].language = #de-AT 
* #100000089309 ^designation[0].value = "PARECOXIB NATRIUM" 
* #100000089310 "TADALAFIL"
* #100000089312 "TRAVOPROST"
* #100000089314 "TRASTUZUMAB"
* #100000089315 "ZINC ACETATE"
* #100000089315 ^designation[0].language = #de-AT 
* #100000089315 ^designation[0].value = "ZINKDIACETAT" 
* #100000089333 "ZINC OXIDE"
* #100000089333 ^designation[0].language = #de-AT 
* #100000089333 ^designation[0].value = "ZINKOXID" 
* #100000089334 "ALBUMIN TANNATE"
* #100000089334 ^designation[0].language = #de-AT 
* #100000089334 ^designation[0].value = "TANNINUM ALBUMINATUM" 
* #100000089335 "BETAIN"
* #100000089337 "CLOPIDOGREL SULFATE"
* #100000089337 ^designation[0].language = #de-AT 
* #100000089337 ^designation[0].value = "CLOPIDOGREL SULFAT" 
* #100000089348 "MEASLES VIRUS"
* #100000089348 ^designation[0].language = #de-AT 
* #100000089348 ^designation[0].value = "MASERNVIRUS" 
* #100000089367 "DARUNAVIR"
* #100000089370 "AMLODIPINE MALEATE"
* #100000089370 ^designation[0].language = #de-AT 
* #100000089370 ^designation[0].value = "AMLODIPIN MALEAT" 
* #100000089379 "LAPATINIB"
* #100000089380 "FEBUXOSTAT"
* #100000089381 "FOSAPREPITANT"
* #100000089383 "PANITUMUMAB"
* #100000089386 "FACTOR VIII INHIBITOR BYPASSING FRACTION"
* #100000089386 ^designation[0].language = #de-AT 
* #100000089386 ^designation[0].value = "FAKTOR VIII-INHIBITOR-BYPASSING-AKTIVITÄT" 
* #100000089387 "ALITRETINOIN"
* #100000089389 "ATOSIBAN ACETATE"
* #100000089389 ^designation[0].language = #de-AT 
* #100000089389 ^designation[0].value = "ATOSIBAN ACETAT" 
* #100000089391 "CELECOXIB"
* #100000089392 "CORIFOLLITROPIN ALFA"
* #100000089393 "BEXAROTENE"
* #100000089393 ^designation[0].language = #de-AT 
* #100000089393 ^designation[0].value = "BEXAROTEN" 
* #100000089395 "DESLORATADINE"
* #100000089395 ^designation[0].language = #de-AT 
* #100000089395 ^designation[0].value = "DESLORATADIN" 
* #100000089399 "GANCICLOVIR SODIUM"
* #100000089399 ^designation[0].language = #de-AT 
* #100000089399 ^designation[0].value = "GANCICLOVIR NATRIUM" 
* #100000089404 "INTERFERON ALFA"
* #100000089405 "LOPINAVIR"
* #100000089406 "NONACOG ALFA"
* #100000089406 ^designation[0].language = #de-AT 
* #100000089406 ^designation[0].value = "GERINNUNGSFAKTOR IX, REKOMBINANT (NONACOG ALFA)" 
* #100000089409 "PRAMIPEXOLE DIHYDROCHLORIDE"
* #100000089409 ^designation[0].language = #de-AT 
* #100000089409 ^designation[0].value = "PRAMIPEXOL DIHYDROCHLORID" 
* #100000089410 "BRIVARACETAM"
* #100000089413 "EFLORNITHINE HYDROCHLORIDE"
* #100000089413 ^designation[0].language = #de-AT 
* #100000089413 ^designation[0].value = "EFLORNITHIN HYDROCHLORID" 
* #100000089417 "INFLIXIMAB"
* #100000089418 "INSULIN DETEMIR"
* #100000089422 "INTERFERON BETA"
* #100000089423 "MERCAPTAMINE BITARTRATE"
* #100000089423 ^designation[0].language = #de-AT 
* #100000089423 ^designation[0].value = "MERCAPTAMIN BITARTRAT" 
* #100000089425 "PEGVISOMANT"
* #100000089427 "RASBURICASE"
* #100000089429 "CATUMAXOMAB"
* #100000089434 "LACOSAMIDE"
* #100000089434 ^designation[0].language = #de-AT 
* #100000089434 ^designation[0].value = "LACOSAMID" 
* #100000089435 "AMLODIPINE MESILATE MONOHYDRATE"
* #100000089435 ^designation[0].language = #de-AT 
* #100000089435 ^designation[0].value = "AMLODIPIN MESILAT MONOHYDRAT" 
* #100000089441 "AMBRISENTAN"
* #100000089442 "AXITINIB"
* #100000089458 "CERTOLIZUMAB PEGOL"
* #100000089485 "SERUM GONADOTROPHIN"
* #100000089485 ^designation[0].language = #de-AT 
* #100000089485 ^designation[0].value = "GONADOTROPES HORMON" 
* #100000089503 "SEVELAMER"
* #100000089504 "FULVESTRANT"
* #100000089508 "INTERFERON GAMMA"
* #100000089510 "THROMBIN"
* #100000089512 "ZINC CITRATE"
* #100000089512 ^designation[0].language = #de-AT 
* #100000089512 ^designation[0].value = "ZINKCITRAT" 
* #100000089515 "ZINC GLUCONATE"
* #100000089515 ^designation[0].language = #de-AT 
* #100000089515 ^designation[0].value = "ZINKDIGLUCONAT" 
* #100000089517 "ATAZANAVIR"
* #100000089520 "MYCOPHENOLATE SODIUM"
* #100000089520 ^designation[0].language = #de-AT 
* #100000089520 ^designation[0].value = "NATRIUM MYCOPHENOLAT" 
* #100000089521 "SODIUM IBANDRONATE"
* #100000089521 ^designation[0].language = #de-AT 
* #100000089521 ^designation[0].value = "NATRIUM IBANDRONAT" 
* #100000089523 "ADALIMUMAB"
* #100000089526 "MIGLUSTAT"
* #100000089527 "INSULINE GLULISINE"
* #100000089527 ^designation[0].language = #de-AT 
* #100000089527 ^designation[0].value = "INSULIN GLULISIN" 
* #100000089529 "POSACONAZOL"
* #100000089533 "ZINC OROTATE"
* #100000089533 ^designation[0].language = #de-AT 
* #100000089533 ^designation[0].value = "ZINK OROTAT" 
* #100000089535 "BEVACIZUMAB"
* #100000089537 "MOROCTOCOG ALFA"
* #100000089537 ^designation[0].language = #de-AT 
* #100000089537 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT (MOROCTOCOG ALFA)" 
* #100000089538 "OCTOCOG ALFA"
* #100000089538 ^designation[0].language = #de-AT 
* #100000089538 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT (OCTOCOG ALFA)" 
* #100000089539 "TIGECYCLINE"
* #100000089539 ^designation[0].language = #de-AT 
* #100000089539 ^designation[0].value = "TIGECYCLIN" 
* #100000089541 "APREPITANT"
* #100000089543 "LARONIDASE"
* #100000089544 "CARGLUMIC ACID"
* #100000089544 ^designation[0].language = #de-AT 
* #100000089544 ^designation[0].value = "CARGLUMSÄURE" 
* #100000089547 "PALONOSETRON HYDROCHLORIDE"
* #100000089547 ^designation[0].language = #de-AT 
* #100000089547 ^designation[0].value = "PALONOSETRON HYDROCHLORID" 
* #100000089557 "ABATACEPT"
* #100000089560 "GEMTUZUMAB OZOGAMICIN"
* #100000089562 "HEXAMINOLEVULINATE HYDROCHLORIDE"
* #100000089562 ^designation[0].language = #de-AT 
* #100000089562 ^designation[0].value = "HEXAMINOLÄVULINAT-HYDROCHLORID" 
* #100000089563 "GALSULFASE"
* #100000089564 "ALGLUCOSIDASE ALFA"
* #100000089565 "ALISKIREN"
* #100000089566 "ENTECAVIR"
* #100000089567 "METHYL AMINOLEVULINATE HYDROCHLORIDE"
* #100000089567 ^designation[0].language = #de-AT 
* #100000089567 ^designation[0].value = "METHYL 5-AMINOLEVULINAT HYDROCHLORID" 
* #100000089568 "PARATHYROID HORMONE"
* #100000089568 ^designation[0].language = #de-AT 
* #100000089568 ^designation[0].value = "PARATHYROIDHORMON" 
* #100000089569 "EXENATIDE"
* #100000089570 "DEFERASIROX"
* #100000089571 "AMLODIPINE MESILATE"
* #100000089571 ^designation[0].language = #de-AT 
* #100000089571 ^designation[0].value = "AMLODIPIN MESILAT" 
* #100000089572 "ANIDULAFUNGIN"
* #100000089575 "MICAFUNGIN SODIUM"
* #100000089575 ^designation[0].language = #de-AT 
* #100000089575 ^designation[0].value = "MICAFUNGIN NATRIUM" 
* #100000089577 "VARDENAFIL HYDROCHLORIDE"
* #100000089577 ^designation[0].language = #de-AT 
* #100000089577 ^designation[0].value = "VARDENAFIL HYDROCHLORID" 
* #100000089579 "PALIFERMIN"
* #100000089581 "ROTIGOTINE"
* #100000089581 ^designation[0].language = #de-AT 
* #100000089581 ^designation[0].value = "ROTIGOTIN" 
* #100000089582 "TEMSIROLIMUS"
* #100000089584 "AMINOLEVULINIC ACID HYDROCHLORIDE"
* #100000089584 ^designation[0].language = #de-AT 
* #100000089584 ^designation[0].value = "5-AMINOLEVULINSÄURE HYDROCHLORID" 
* #100000089585 "CARBIDOPA MONOHYDRATE"
* #100000089585 ^designation[0].language = #de-AT 
* #100000089585 ^designation[0].value = "CARBIDOPA MONOHYDRAT" 
* #100000089587 "CLOFARABINE"
* #100000089587 ^designation[0].language = #de-AT 
* #100000089587 ^designation[0].value = "CLOFARABIN" 
* #100000089591 "ENTECAVIR MONOHYDRATE"
* #100000089591 ^designation[0].language = #de-AT 
* #100000089591 ^designation[0].value = "ENTECAVIR MONOHYDRAT" 
* #100000089594 "NATALIZUMAB"
* #100000089598 "ATAZANAVIR SULFATE"
* #100000089598 ^designation[0].language = #de-AT 
* #100000089598 ^designation[0].value = "ATAZANAVIR SULFAT" 
* #100000089599 "ALENDRONATE SODIUM MONOHYDRATE"
* #100000089599 ^designation[0].language = #de-AT 
* #100000089599 ^designation[0].value = "NATRIUMALENDRONAT MONOHYDRAT" 
* #100000089604 "FLUPENTIXOL DIHYDROCHLORIDE"
* #100000089604 ^designation[0].language = #de-AT 
* #100000089604 ^designation[0].value = "FLUPENTIXOL DIHYDROCHLORID" 
* #100000089619 "ALENDRONATE SODIUM TRIHYDRATE"
* #100000089619 ^designation[0].language = #de-AT 
* #100000089619 ^designation[0].value = "NATRIUMALENDRONAT TRIHYDRAT" 
* #100000089624 "CETYLPYRIDINIUM CHLORIDE"
* #100000089624 ^designation[0].language = #de-AT 
* #100000089624 ^designation[0].value = "CETYLPYRIDINIUMCHLORID" 
* #100000089631 "GUANFACINE HYDROCHLORIDE"
* #100000089631 ^designation[0].language = #de-AT 
* #100000089631 ^designation[0].value = "GUANFACIN HYDROCHLORID" 
* #100000089633 "GLUCAGON"
* #100000089638 "TEMOZOLOMIDE"
* #100000089638 ^designation[0].language = #de-AT 
* #100000089638 ^designation[0].value = "TEMOZOLOMID" 
* #100000089639 "SUGAMMADEX"
* #100000089649 "GADOBENATE DIMEGLUMINE"
* #100000089649 ^designation[0].language = #de-AT 
* #100000089649 ^designation[0].value = "DIMEGLUMIN GADOBENAT" 
* #100000089650 "CAFFEINE, ANHYDROUS"
* #100000089650 ^designation[0].language = #de-AT 
* #100000089650 ^designation[0].value = "COFFEIN, WASSERFREI" 
* #100000089652 "MOXIFLOXACIN HYDROCHLORIDE"
* #100000089652 ^designation[0].language = #de-AT 
* #100000089652 ^designation[0].value = "MOXIFLOXACIN HYDROCHLORID" 
* #100000089657 "BAZEDOXIFENE"
* #100000089657 ^designation[0].language = #de-AT 
* #100000089657 ^designation[0].value = "BAZEDOXIFEN" 
* #100000089658 "DALBAVANCIN"
* #100000089659 "METHYLNALTREXONE BROMIDE"
* #100000089659 ^designation[0].language = #de-AT 
* #100000089659 ^designation[0].value = "METHYLNALTREXONIUMBROMID" 
* #100000089666 "FOSAMPRENAVIR CALCIUM"
* #100000089667 "ARTICAINE HYDROCHLORIDE"
* #100000089667 ^designation[0].language = #de-AT 
* #100000089667 ^designation[0].value = "ARTICAIN HYDROCHLORID" 
* #100000089670 "TAMSULOSIN HYDROCHLORIDE"
* #100000089670 ^designation[0].language = #de-AT 
* #100000089670 ^designation[0].value = "TAMSULOSIN HYDROCHLORID" 
* #100000089671 "POTASSIUM HYDROGEN TARTRATE"
* #100000089671 ^designation[0].language = #de-AT 
* #100000089671 ^designation[0].value = "KALIUMHYDROGENTARTRAT" 
* #100000089693 "CAFFEINE"
* #100000089693 ^designation[0].language = #de-AT 
* #100000089693 ^designation[0].value = "COFFEIN" 
* #100000089694 "STAVUDINE"
* #100000089694 ^designation[0].language = #de-AT 
* #100000089694 ^designation[0].value = "STAVUDIN" 
* #100000089697 "POTASSIUM ACETATE"
* #100000089697 ^designation[0].language = #de-AT 
* #100000089697 ^designation[0].value = "KALIUMACETAT" 
* #100000089784 "ESOMEPRAZOLE SODIUM"
* #100000089784 ^designation[0].language = #de-AT 
* #100000089784 ^designation[0].value = "ESOMEPRAZOL NATRIUM" 
* #100000089787 "GONADORELIN ACETATE"
* #100000089787 ^designation[0].language = #de-AT 
* #100000089787 ^designation[0].value = "GONADORELIN ACETAT" 
* #100000089788 "BARIUM SULFATE"
* #100000089788 ^designation[0].language = #de-AT 
* #100000089788 ^designation[0].value = "BARIUMSULFAT" 
* #100000089793 "METHYLDOPA SESQUIHYDRATE"
* #100000089793 ^designation[0].language = #de-AT 
* #100000089793 ^designation[0].value = "METHYLDOPA SESQUIHYDRAT" 
* #100000089809 "GEMCITABINE HYDROCHLORIDE"
* #100000089809 ^designation[0].language = #de-AT 
* #100000089809 ^designation[0].value = "GEMCITABIN HYDROCHLORID" 
* #100000089811 "ISOTRETINOIN"
* #100000089817 "ETRAVIRINE"
* #100000089817 ^designation[0].language = #de-AT 
* #100000089817 ^designation[0].value = "ETRAVIRIN" 
* #100000089823 "JAPANESE ENCEPHALITIS VIRUS (INACTIVATED)"
* #100000089823 ^designation[0].language = #de-AT 
* #100000089823 ^designation[0].value = "JAPANISCHE ENZEPHALITIS VIRUS" 
* #100000089827 "LABETALOL HYDROCHLORIDE"
* #100000089827 ^designation[0].language = #de-AT 
* #100000089827 ^designation[0].value = "LABETALOL HYDROCHLORID" 
* #100000089829 "BENZOCAINE"
* #100000089829 ^designation[0].language = #de-AT 
* #100000089829 ^designation[0].value = "BENZOCAIN" 
* #100000089833 "SIMETICONE"
* #100000089833 ^designation[0].language = #de-AT 
* #100000089833 ^designation[0].value = "SIMETICON" 
* #100000089834 "PEGFILGRASTIM"
* #100000089838 "SODIUM ACETATE TRIHYDRATE"
* #100000089838 ^designation[0].language = #de-AT 
* #100000089838 ^designation[0].value = "NATRIUMACETAT TRIHYDRAT" 
* #100000089850 "DILTIAZEM HYDROCHLORIDE"
* #100000089850 ^designation[0].language = #de-AT 
* #100000089850 ^designation[0].value = "DILTIAZEM HYDROCHLORID" 
* #100000089852 "CEFTRIAXONE DISODIUM"
* #100000089852 ^designation[0].language = #de-AT 
* #100000089852 ^designation[0].value = "CEFTRIAXON DINATRIUM" 
* #100000089855 "GADOTERIC ACID"
* #100000089855 ^designation[0].language = #de-AT 
* #100000089855 ^designation[0].value = "GADOTERSÄURE" 
* #100000089859 "MAGNESIUM ACETATE TETRAHYDRATE"
* #100000089859 ^designation[0].language = #de-AT 
* #100000089859 ^designation[0].value = "MAGNESIUMACETAT TETRAHYDRAT" 
* #100000089861 "MAGNESIUM SULFATE DIHYDRATE"
* #100000089861 ^designation[0].language = #de-AT 
* #100000089861 ^designation[0].value = "MAGNESIUMSULFAT DIHYDRAT" 
* #100000089867 "LANSOPRAZOL"
* #100000089869 "CEFACLOR MONOHYDRATE"
* #100000089869 ^designation[0].language = #de-AT 
* #100000089869 ^designation[0].value = "CEFACLOR MONOHYDRAT" 
* #100000089872 "FOSFOMYCIN TROMETAMOL"
* #100000089872 ^designation[0].language = #de-AT 
* #100000089872 ^designation[0].value = "FOSFOMYCIN-TROMETAMOL" 
* #100000089873 "FLUVASTATIN SODIUM"
* #100000089873 ^designation[0].language = #de-AT 
* #100000089873 ^designation[0].value = "FLUVASTATIN NATRIUM" 
* #100000089874 "CYTARABINE"
* #100000089874 ^designation[0].language = #de-AT 
* #100000089874 ^designation[0].value = "CYTARABIN" 
* #100000089877 "SUXAMETHONIUM CHLORIDE"
* #100000089877 ^designation[0].language = #de-AT 
* #100000089877 ^designation[0].value = "SUXAMETHONIUM CHLORID" 
* #100000089878 "INSULIN"
* #100000089882 "ALEMTUZUMAB"
* #100000089883 "RABIES VIRUS"
* #100000089883 ^designation[0].language = #de-AT 
* #100000089883 ^designation[0].value = "RABIESVIRUS" 
* #100000089890 "ANHYDROUS TIOTROPIUM BROMIDE"
* #100000089890 ^designation[0].language = #de-AT 
* #100000089890 ^designation[0].value = "TIOTROPIUMBROMID-WASSERFREI" 
* #100000089915 "TICK-BORNE ENCEPHALITIS VIRUS"
* #100000089915 ^designation[0].language = #de-AT 
* #100000089915 ^designation[0].value = "FRÜHSOMMER-MENINGOENCEPHALITIS-VIRUS" 
* #100000089920 "METOCLOPRAMIDE HYDROCHLORIDE"
* #100000089920 ^designation[0].language = #de-AT 
* #100000089920 ^designation[0].value = "METOCLOPRAMIDHYDROCHLORID" 
* #100000089921 "ALTEPLASE"
* #100000089922 "PYRIDOXINE HYDROCHLORIDE"
* #100000089922 ^designation[0].language = #de-AT 
* #100000089922 ^designation[0].value = "PYRIDOXIN HYDROCHLORID" 
* #100000089927 "BECLOMETASONE DIPROPIONATE ANHYDROUS"
* #100000089927 ^designation[0].language = #de-AT 
* #100000089927 ^designation[0].value = "BECLOMETASONDIPROPIONAT, WASSERFREIES" 
* #100000089937 "LYSINE HYDROCHLORIDE"
* #100000089937 ^designation[0].language = #de-AT 
* #100000089937 ^designation[0].value = "LYSIN HYDROCHLORID" 
* #100000089938 "NALTREXONE HYDROCHLORIDE"
* #100000089938 ^designation[0].language = #de-AT 
* #100000089938 ^designation[0].value = "NALTREXON HYDROCHLORID" 
* #100000089957 "ARIPIPRAZOL"
* #100000089964 "PIPERACILLIN SODIUM"
* #100000089964 ^designation[0].language = #de-AT 
* #100000089964 ^designation[0].value = "PIPERACILLIN-NATRIUM" 
* #100000089965 "PAROXETINE HYDROCHLORIDE"
* #100000089965 ^designation[0].language = #de-AT 
* #100000089965 ^designation[0].value = "PAROXETINHYDROCHLORID" 
* #100000089966 "OLMESARTAN MEDOXOMIL"
* #100000089968 "RACECADOTRIL"
* #100000089971 "DOXORUBICIN HYDROCHLORIDE"
* #100000089971 ^designation[0].language = #de-AT 
* #100000089971 ^designation[0].value = "DOXORUBICIN HYDROCHLORID" 
* #100000089972 "PHENYTOIN SODIUM"
* #100000089972 ^designation[0].language = #de-AT 
* #100000089972 ^designation[0].value = "PHENYTOIN NATRIUM" 
* #100000089973 "LIOTHYRONINE SODIUM"
* #100000089973 ^designation[0].language = #de-AT 
* #100000089973 ^designation[0].value = "NATRIUM LIOTHYRONINAT" 
* #100000089974 "AMPICILLIN SODIUM"
* #100000089974 ^designation[0].language = #de-AT 
* #100000089974 ^designation[0].value = "AMPICILLIN NATRIUM" 
* #100000089986 "NYSTATIN"
* #100000089988 "DOXAZOSIN MESILATE"
* #100000089988 ^designation[0].language = #de-AT 
* #100000089988 ^designation[0].value = "DOXAZOSIN MESILAT" 
* #100000089989 "FELODIPINE"
* #100000089989 ^designation[0].language = #de-AT 
* #100000089989 ^designation[0].value = "FELODIPIN" 
* #100000089998 "BETAHISTINE DIHYDROCHLORIDE"
* #100000089998 ^designation[0].language = #de-AT 
* #100000089998 ^designation[0].value = "BETAHISTIN DIHYDROCHLORID" 
* #100000090010 "CEFAZOLIN SODIUM"
* #100000090010 ^designation[0].language = #de-AT 
* #100000090010 ^designation[0].value = "CEFAZOLIN NATRIUM" 
* #100000090012 "QUINAGOLIDE HYDROCHLORIDE"
* #100000090012 ^designation[0].language = #de-AT 
* #100000090012 ^designation[0].value = "QUINAGOLID HYDROCHLORID" 
* #100000090025 "FLUDARABINE PHOSPHATE"
* #100000090025 ^designation[0].language = #de-AT 
* #100000090025 ^designation[0].value = "FLUDARABIN PHOSPHAT" 
* #100000090027 "SULBACTAM SODIUM"
* #100000090027 ^designation[0].language = #de-AT 
* #100000090027 ^designation[0].value = "SULBACTAM NATRIUM" 
* #100000090028 "NADROPARIN CALCIUM"
* #100000090029 "NEOMYCIN SULFATE"
* #100000090029 ^designation[0].language = #de-AT 
* #100000090029 ^designation[0].value = "NEOMYCIN SULFAT" 
* #100000090030 "RABEPRAZOLE SODIUM"
* #100000090030 ^designation[0].language = #de-AT 
* #100000090030 ^designation[0].value = "RABEPRAZOL NATRIUM" 
* #100000090033 "TIMOLOL MALEATE"
* #100000090033 ^designation[0].language = #de-AT 
* #100000090033 ^designation[0].value = "TIMOLOLMALEAT" 
* #100000090038 "SALICYLIC ACID"
* #100000090038 ^designation[0].language = #de-AT 
* #100000090038 ^designation[0].value = "SALICYLSÄURE" 
* #100000090056 "BELIMUMAB"
* #100000090069 "STRONTIUM RANELATE"
* #100000090069 ^designation[0].language = #de-AT 
* #100000090069 ^designation[0].value = "DISTRONTIUM RANELAT" 
* #100000090070 "PERFLUTREN"
* #100000090076 "SODIUM HYALURONATE"
* #100000090076 ^designation[0].language = #de-AT 
* #100000090076 ^designation[0].value = "NATRIUMHYALURONAT" 
* #100000090079 "AMLODIPINE BESILATE"
* #100000090079 ^designation[0].language = #de-AT 
* #100000090079 ^designation[0].value = "AMLODIPIN BESILAT" 
* #100000090082 "BETAMETHASONE SODIUM PHOSPHATE"
* #100000090082 ^designation[0].language = #de-AT 
* #100000090082 ^designation[0].value = "BETAMETHASON DINATRIUMPHOSPHAT" 
* #100000090084 "DEFEROXAMINE MESILATE"
* #100000090084 ^designation[0].language = #de-AT 
* #100000090084 ^designation[0].value = "DEFEROXAMIN MESILAT" 
* #100000090085 "MEMANTINE HYDROCHLORIDE"
* #100000090085 ^designation[0].language = #de-AT 
* #100000090085 ^designation[0].value = "MEMANTIN HYDROCHLORID" 
* #100000090101 "SODIUM LACTATE"
* #100000090101 ^designation[0].language = #de-AT 
* #100000090101 ^designation[0].value = "NATRIUMLACTAT" 
* #100000090104 "MAGNESIUM HYDROXIDE"
* #100000090104 ^designation[0].language = #de-AT 
* #100000090104 ^designation[0].value = "MAGNESIUMHYDROXID" 
* #100000090106 "CALCIUM CHLORIDE DIHYDRATE"
* #100000090106 ^designation[0].language = #de-AT 
* #100000090106 ^designation[0].value = "CALCIUMCHLORID DIHYDRAT" 
* #100000090107 "BIPERIDEN"
* #100000090108 "CLENBUTEROL HYDROCHLORIDE"
* #100000090108 ^designation[0].language = #de-AT 
* #100000090108 ^designation[0].value = "CLENBUTEROL HYDROCHLORID" 
* #100000090109 "MAGNESIUM CHLORIDE"
* #100000090109 ^designation[0].language = #de-AT 
* #100000090109 ^designation[0].value = "MAGNESIUMCHLORID" 
* #100000090111 "IBUPROFEN LYSINE"
* #100000090111 ^designation[0].language = #de-AT 
* #100000090111 ^designation[0].value = "IBUPROFEN LYSIN" 
* #100000090113 "AMOXICILLIN SODIUM"
* #100000090113 ^designation[0].language = #de-AT 
* #100000090113 ^designation[0].value = "NATRIUM AMOXICILLINAT" 
* #100000090114 "MIANSERIN HYDROCHLORIDE"
* #100000090114 ^designation[0].language = #de-AT 
* #100000090114 ^designation[0].value = "MIANSERIN HYDROCHLORID" 
* #100000090120 "MERCAPTOPURIN"
* #100000090123 "SODIUM ALGINATE"
* #100000090123 ^designation[0].language = #de-AT 
* #100000090123 ^designation[0].value = "NATRIUMALGINAT" 
* #100000090127 "FORMOTEROL FUMARATE DIHYDRATE"
* #100000090127 ^designation[0].language = #de-AT 
* #100000090127 ^designation[0].value = "FORMOTEROLFUMARAT DIHYDRAT" 
* #100000090133 "METHYLPHENIDATE HYDROCHLORIDE"
* #100000090133 ^designation[0].language = #de-AT 
* #100000090133 ^designation[0].value = "METHYLPHENIDAT HYDROCHLORID" 
* #100000090134 "MEPIVACAINE HYDROCHLORIDE"
* #100000090134 ^designation[0].language = #de-AT 
* #100000090134 ^designation[0].value = "MEPIVACAIN HYDROCHLORID" 
* #100000090144 "ERTAPENEM SODIUM"
* #100000090144 ^designation[0].language = #de-AT 
* #100000090144 ^designation[0].value = "ERTAPENEM NATRIUM" 
* #100000090150 "HEPARIN SODIUM"
* #100000090150 ^designation[0].language = #de-AT 
* #100000090150 ^designation[0].value = "HEPARIN NATRIUM" 
* #100000090151 "GLICLAZIDE"
* #100000090151 ^designation[0].language = #de-AT 
* #100000090151 ^designation[0].value = "GLICLAZID" 
* #100000090152 "ENOXAPARIN SODIUM"
* #100000090152 ^designation[0].language = #de-AT 
* #100000090152 ^designation[0].value = "ENOXAPARIN NATRIUM" 
* #100000090159 "FLUOCINOLONE ACETONIDE"
* #100000090159 ^designation[0].language = #de-AT 
* #100000090159 ^designation[0].value = "FLUOCINOLON ACETONID" 
* #100000090166 "LOPERAMIDE HYDROCHLORIDE"
* #100000090166 ^designation[0].language = #de-AT 
* #100000090166 ^designation[0].value = "LOPERAMID HYDROCHLORID" 
* #100000090168 "PREDNISOLONE ACETATE"
* #100000090168 ^designation[0].language = #de-AT 
* #100000090168 ^designation[0].value = "PREDNISOLON ACETAT" 
* #100000090171 "DIMETICONE"
* #100000090171 ^designation[0].language = #de-AT 
* #100000090171 ^designation[0].value = "DIMETICON" 
* #100000090176 "PHENYLEPHRINE HYDROCHLORIDE"
* #100000090176 ^designation[0].language = #de-AT 
* #100000090176 ^designation[0].value = "PHENYLEPHRINHYDROCHLORID" 
* #100000090185 "CILASTATIN SODIUM"
* #100000090185 ^designation[0].language = #de-AT 
* #100000090185 ^designation[0].value = "CILASTATIN NATRIUM" 
* #100000090186 "OMEPRAZOLE SODIUM"
* #100000090186 ^designation[0].language = #de-AT 
* #100000090186 ^designation[0].value = "OMEPRAZOL NATRIUM" 
* #100000090187 "ISOCONAZOLE NITRATE"
* #100000090187 ^designation[0].language = #de-AT 
* #100000090187 ^designation[0].value = "ISOCONAZOL NITRAT" 
* #100000090188 "CODEINE PHOSPHATE HEMIHYDRATE"
* #100000090188 ^designation[0].language = #de-AT 
* #100000090188 ^designation[0].value = "CODEINPHOSPHAT HEMIHYDRAT" 
* #100000090201 "TAMOXIFENCITRAT"
* #100000090202 "SODIUM HYDROXIDE"
* #100000090202 ^designation[0].language = #de-AT 
* #100000090202 ^designation[0].value = "NATRIUMHYDROXID" 
* #100000090209 "CARMUSTINE"
* #100000090209 ^designation[0].language = #de-AT 
* #100000090209 ^designation[0].value = "CARMUSTIN" 
* #100000090210 "MECETRONIUM ETILSULFATE"
* #100000090210 ^designation[0].language = #de-AT 
* #100000090210 ^designation[0].value = "MECETRONIUM ETILSULFAT" 
* #100000090211 "CHLORPHENAMINE MALEATE"
* #100000090211 ^designation[0].language = #de-AT 
* #100000090211 ^designation[0].value = "CHLORPHENAMIN HYDROGENMALEAT" 
* #100000090223 "NITRENDIPINE"
* #100000090223 ^designation[0].language = #de-AT 
* #100000090223 ^designation[0].value = "NITRENDIPIN" 
* #100000090229 "ERGOCALCIFEROL"
* #100000090232 "ZINC CHLORIDE"
* #100000090232 ^designation[0].language = #de-AT 
* #100000090232 ^designation[0].value = "ZINKCHLORID" 
* #100000090233 "MELATONIN"
* #100000090235 "ZINC CITRATE TRIHYDRATE"
* #100000090235 ^designation[0].language = #de-AT 
* #100000090235 ^designation[0].value = "ZINKCITRAT TRIHYDRAT" 
* #100000090248 "INSULIN LISPRO"
* #100000090250 "METHOTREXAT"
* #100000090252 "PALIVIZUMAB"
* #100000090253 "CALCIUM SULFATE"
* #100000090253 ^designation[0].language = #de-AT 
* #100000090253 ^designation[0].value = "CALCIUMSULFAT" 
* #100000090260 "MITOXANTRONE HYDROCHLORIDE"
* #100000090260 ^designation[0].language = #de-AT 
* #100000090260 ^designation[0].value = "MITOXANTRON DIHYDROCHLORID" 
* #100000090268 "RIZATRIPTAN BENZOATE"
* #100000090268 ^designation[0].language = #de-AT 
* #100000090268 ^designation[0].value = "RIZATRIPTAN BENZOAT" 
* #100000090269 "OXYMETAZOLINE HYDROCHLORIDE"
* #100000090269 ^designation[0].language = #de-AT 
* #100000090269 ^designation[0].value = "OXYMETAZOLIN HYDROCHLORID" 
* #100000090270 "PARACETAMOL"
* #100000090273 "PHENOXYMETHYLPENICILLIN POTASSIUM"
* #100000090273 ^designation[0].language = #de-AT 
* #100000090273 ^designation[0].value = "PHENOXYMETHYLPENICILLIN-KALIUM" 
* #100000090274 "BENZOYL PEROXIDE, HYDROUS"
* #100000090274 ^designation[0].language = #de-AT 
* #100000090274 ^designation[0].value = "BENZOYLPEROXID, WASSERHALTIGES" 
* #100000090277 "CYCLOPHOSPHAMIDE"
* #100000090277 ^designation[0].language = #de-AT 
* #100000090277 ^designation[0].value = "CYCLOPHOSPHAMID" 
* #100000090278 "MIDODRINE HYDROCHLORIDE"
* #100000090278 ^designation[0].language = #de-AT 
* #100000090278 ^designation[0].value = "MIDODRIN HYDROCHLORID" 
* #100000090279 "NOMEGESTROL ACETATE"
* #100000090279 ^designation[0].language = #de-AT 
* #100000090279 ^designation[0].value = "NOMEGESTROL ACETAT" 
* #100000090280 "CALCIUM FOLINATE"
* #100000090280 ^designation[0].language = #de-AT 
* #100000090280 ^designation[0].value = "CALCIUM FOLINAT" 
* #100000090284 "IBRITUMOMAB TIUXETAN"
* #100000090285 "METRONIDAZOLE"
* #100000090285 ^designation[0].language = #de-AT 
* #100000090285 ^designation[0].value = "METRONIDAZOL" 
* #100000090291 "TORASEMIDE"
* #100000090291 ^designation[0].language = #de-AT 
* #100000090291 ^designation[0].value = "TORASEMID" 
* #100000090296 "PEGINTERFERON ALFA-2B"
* #100000090297 "BIPERIDEN HYDROCHLORIDE"
* #100000090297 ^designation[0].language = #de-AT 
* #100000090297 ^designation[0].value = "BIPERIDEN HYDROCHLORID" 
* #100000090301 "DIMETHYLSULFOXIDE"
* #100000090301 ^designation[0].language = #de-AT 
* #100000090301 ^designation[0].value = "DIMETHYLSULFOXID" 
* #100000090303 "ATORVASTATIN CALCIUM"
* #100000090309 "TRAZODONE HYDROCHLORIDE"
* #100000090309 ^designation[0].language = #de-AT 
* #100000090309 ^designation[0].value = "TRAZODON HYDROCHLORID" 
* #100000090311 "AMITRIPTYLINE HYDROCHLORIDE"
* #100000090311 ^designation[0].language = #de-AT 
* #100000090311 ^designation[0].value = "AMITRIPTYLIN HYDROCHLORID" 
* #100000090313 "DORZOLAMIDE HYDROCHLORIDE"
* #100000090313 ^designation[0].language = #de-AT 
* #100000090313 ^designation[0].value = "DORZOLAMID HYDROCHLORID" 
* #100000090314 "CARBIDOPA"
* #100000090317 "FUROSEMIDE"
* #100000090317 ^designation[0].language = #de-AT 
* #100000090317 ^designation[0].value = "FUROSEMID" 
* #100000090318 "MAGALDRATE"
* #100000090318 ^designation[0].language = #de-AT 
* #100000090318 ^designation[0].value = "MAGALDRAT" 
* #100000090319 "SORAFENIB TOSILATE"
* #100000090319 ^designation[0].language = #de-AT 
* #100000090319 ^designation[0].value = "SORAFENIBTOSILAT" 
* #100000090321 "CYPROTERONE ACETATE"
* #100000090321 ^designation[0].language = #de-AT 
* #100000090321 ^designation[0].value = "CYPROTERON ACETAT" 
* #100000090324 "LEVOCARNITINE"
* #100000090324 ^designation[0].language = #de-AT 
* #100000090324 ^designation[0].value = "LEVOCARNITIN" 
* #100000090325 "CLONIDINE HYDROCHLORIDE"
* #100000090325 ^designation[0].language = #de-AT 
* #100000090325 ^designation[0].value = "CLONIDIN HYDROCHLORID" 
* #100000090335 "BACLOFEN"
* #100000090337 "GLIPIZIDE"
* #100000090337 ^designation[0].language = #de-AT 
* #100000090337 ^designation[0].value = "GLIPIZID" 
* #100000090341 "LENALIDOMIDE"
* #100000090341 ^designation[0].language = #de-AT 
* #100000090341 ^designation[0].value = "LENALIDOMID" 
* #100000090352 "OXETACAINE"
* #100000090352 ^designation[0].language = #de-AT 
* #100000090352 ^designation[0].value = "OXETACAIN" 
* #100000090354 "PROGESTERONE"
* #100000090354 ^designation[0].language = #de-AT 
* #100000090354 ^designation[0].value = "PROGESTERON" 
* #100000090359 "PANTOPRAZOLE SODIUM"
* #100000090359 ^designation[0].language = #de-AT 
* #100000090359 ^designation[0].value = "PANTOPRAZOL NATRIUM" 
* #100000090361 "PHENOXYMETHYLPENICILLIN"
* #100000090365 "IBUPROFEN"
* #100000090366 "MINOXIDIL"
* #100000090368 "OXYCODONE HYDROCHLORIDE"
* #100000090368 ^designation[0].language = #de-AT 
* #100000090368 ^designation[0].value = "OXYCODON HYDROCHLORID" 
* #100000090369 "MICONAZOL"
* #100000090370 "SOLIFENACIN SUCCINATE"
* #100000090370 ^designation[0].language = #de-AT 
* #100000090370 ^designation[0].value = "SOLIFENACIN SUCCINAT" 
* #100000090371 "NIFEDIPINE"
* #100000090371 ^designation[0].language = #de-AT 
* #100000090371 ^designation[0].value = "NIFEDIPIN" 
* #100000090377 "FENTANYL"
* #100000090383 "NAPROXEN"
* #100000090386 "TRIAMCINOLONE ACETONIDE"
* #100000090386 ^designation[0].language = #de-AT 
* #100000090386 ^designation[0].value = "TRIAMCINOLON ACETONID" 
* #100000090387 "HUMAN VON WILLEBRAND FACTOR"
* #100000090387 ^designation[0].language = #de-AT 
* #100000090387 ^designation[0].value = "VON WILLEBRAND FAKTOR AUS MENSCHLICHEM PLASMA" 
* #100000090389 "METOPROLOL TARTRATE"
* #100000090389 ^designation[0].language = #de-AT 
* #100000090389 ^designation[0].value = "METOPROLOL TARTRAT" 
* #100000090390 "VALACICLOVIR HYDROCHLORIDE"
* #100000090390 ^designation[0].language = #de-AT 
* #100000090390 ^designation[0].value = "VALACICLOVIRHYDROCHLORID" 
* #100000090391 "NAPHAZOLINE HYDROCHLORIDE"
* #100000090391 ^designation[0].language = #de-AT 
* #100000090391 ^designation[0].value = "NAPHAZOLIN HYDROCHLORID" 
* #100000090392 "ISOSORBIDE MONONITRATE"
* #100000090392 ^designation[0].language = #de-AT 
* #100000090392 ^designation[0].value = "ISOSORBIDMONONITRAT" 
* #100000090393 "NANDROLONE DECANOATE"
* #100000090393 ^designation[0].language = #de-AT 
* #100000090393 ^designation[0].value = "NANDROLON DECANOAT" 
* #100000090395 "SODIUM POLYSTYRENE SULPHONATE"
* #100000090395 ^designation[0].language = #de-AT 
* #100000090395 ^designation[0].value = "NATRIUMPOLYSTYROLSULFONAT" 
* #100000090399 "DABIGATRAN ETEXILATE MESILATE"
* #100000090399 ^designation[0].language = #de-AT 
* #100000090399 ^designation[0].value = "DABIGATRANETEXILATMESILAT" 
* #100000090407 "ANAGRELIDE HYDROCHLORIDE"
* #100000090407 ^designation[0].language = #de-AT 
* #100000090407 ^designation[0].value = "ANAGRELIDHYDROCHLORID" 
* #100000090410 "BROMOCRIPTINE MESILATE"
* #100000090410 ^designation[0].language = #de-AT 
* #100000090410 ^designation[0].value = "BROMOCRIPTIN MESILAT" 
* #100000090411 "CALCIUM LACTOGLUCONATE"
* #100000090411 ^designation[0].language = #de-AT 
* #100000090411 ^designation[0].value = "CALCIUM LACTAT GLUCONAT" 
* #100000090413 "GUAIFENESIN"
* #100000090415 "FLECAINIDE ACETATE"
* #100000090415 ^designation[0].language = #de-AT 
* #100000090415 ^designation[0].value = "FLECAINID ACETAT" 
* #100000090423 "VALPROIC ACID"
* #100000090423 ^designation[0].language = #de-AT 
* #100000090423 ^designation[0].value = "VALPROINSÄURE" 
* #100000090424 "ALFENTANIL HYDROCHLORIDE"
* #100000090424 ^designation[0].language = #de-AT 
* #100000090424 ^designation[0].value = "ALFENTANIL HYDROCHLORID" 
* #100000090425 "ARGININE GLUTAMATE"
* #100000090425 ^designation[0].language = #de-AT 
* #100000090425 ^designation[0].value = "ARGININ GLUTAMAT" 
* #100000090426 "AZELASTINE HYDROCHLORIDE"
* #100000090426 ^designation[0].language = #de-AT 
* #100000090426 ^designation[0].value = "AZELASTIN HYDROCHLORID" 
* #100000090427 "BACITRACIN ZINC"
* #100000090427 ^designation[0].language = #de-AT 
* #100000090427 ^designation[0].value = "ZINK BACITRACIN" 
* #100000090429 "CEFAMANDOLE NAFATE"
* #100000090429 ^designation[0].language = #de-AT 
* #100000090429 ^designation[0].value = "CEFAMANDOL NAFAT" 
* #100000090431 "BUPROPION HYDROCHLORIDE"
* #100000090431 ^designation[0].language = #de-AT 
* #100000090431 ^designation[0].value = "BUPROPION HYDROCHLORID" 
* #100000090433 "BENZOIC ACID"
* #100000090433 ^designation[0].language = #de-AT 
* #100000090433 ^designation[0].value = "BENZOESÄURE" 
* #100000090443 "GLUCOSE MONOHYDRATE"
* #100000090443 ^designation[0].language = #de-AT 
* #100000090443 ^designation[0].value = "GLUCOSE MONOHYDRAT" 
* #100000090444 "SULFUR"
* #100000090444 ^designation[0].language = #de-AT 
* #100000090444 ^designation[0].value = "SCHWEFEL" 
* #100000090445 "FERROUS FUMARATE"
* #100000090445 ^designation[0].language = #de-AT 
* #100000090445 ^designation[0].value = "EISEN(II)-FUMARAT" 
* #100000090449 "PENTOXYVERINE CITRATE"
* #100000090449 ^designation[0].language = #de-AT 
* #100000090449 ^designation[0].value = "PENTOXYVERIN CITRAT" 
* #100000090451 "RANITIDINE HYDROCHLORIDE"
* #100000090451 ^designation[0].language = #de-AT 
* #100000090451 ^designation[0].value = "RANITIDIN HYDROCHLORID" 
* #100000090452 "UROFOLLITROPIN"
* #100000090453 "DIPHENHYDRAMINE HYDROCHLORIDE"
* #100000090453 ^designation[0].language = #de-AT 
* #100000090453 ^designation[0].value = "DIPHENHYDRAMIN HYDROCHLORID" 
* #100000090454 "GRANISETRON HYDROCHLORIDE"
* #100000090454 ^designation[0].language = #de-AT 
* #100000090454 ^designation[0].value = "GRANISETRON HYDROCHLORID" 
* #100000090467 "PHENIRAMINE MALEATE"
* #100000090467 ^designation[0].language = #de-AT 
* #100000090467 ^designation[0].value = "PHENIRAMIN HYDROGENMALEAT" 
* #100000090468 "QUINAPRIL HYDROCHLORIDE"
* #100000090468 ^designation[0].language = #de-AT 
* #100000090468 ^designation[0].value = "QUINAPRIL HYDROCHLORID" 
* #100000090469 "REMIFENTANIL HYDROCHLORIDE"
* #100000090469 ^designation[0].language = #de-AT 
* #100000090469 ^designation[0].value = "REMIFENTANIL HYDROCHLORID" 
* #100000090471 "VANCOMYCIN HYDROCHLORIDE"
* #100000090471 ^designation[0].language = #de-AT 
* #100000090471 ^designation[0].value = "VANCOMYCIN HYDROCHLORID" 
* #100000090474 "VERAPAMIL HYDROCHLORIDE"
* #100000090474 ^designation[0].language = #de-AT 
* #100000090474 ^designation[0].value = "VERAPAMIL HYDROCHLORID" 
* #100000090475 "DIACEREIN"
* #100000090476 "CISATRACURIUM BESILATE"
* #100000090476 ^designation[0].language = #de-AT 
* #100000090476 ^designation[0].value = "CISATRACURIUM BESILAT" 
* #100000090479 "SODIUM MOLYBDATE DIHYDRATE"
* #100000090479 ^designation[0].language = #de-AT 
* #100000090479 ^designation[0].value = "DINATRIUMMOLYBDAT DIHYDRAT" 
* #100000090485 "CALCIUM PANTOTHENATE"
* #100000090485 ^designation[0].language = #de-AT 
* #100000090485 ^designation[0].value = "CALCIUM PANTOTHENAT" 
* #100000090489 "LEVOBUPIVACAINE HYDROCHLORIDE"
* #100000090489 ^designation[0].language = #de-AT 
* #100000090489 ^designation[0].value = "LEVOBUPIVACAIN HYDROCHLORID" 
* #100000090493 "ESTRADIOL VALERATE"
* #100000090493 ^designation[0].language = #de-AT 
* #100000090493 ^designation[0].value = "ESTRADIOL VALERAT" 
* #100000090494 "MORPHINE HYDROCHLORIDE"
* #100000090494 ^designation[0].language = #de-AT 
* #100000090494 ^designation[0].value = "MORPHINHYDROCHLORID" 
* #100000090495 "FORMOTEROL FUMARATE"
* #100000090495 ^designation[0].language = #de-AT 
* #100000090495 ^designation[0].value = "FORMOTEROLFUMARAT" 
* #100000090497 "MANGANESE CHLORIDE TETRAHYDRATE"
* #100000090497 ^designation[0].language = #de-AT 
* #100000090497 ^designation[0].value = "MANGANCHLORID TETRAHYDRAT" 
* #100000090500 "OXYGEN"
* #100000090500 ^designation[0].language = #de-AT 
* #100000090500 ^designation[0].value = "SAUERSTOFF" 
* #100000090503 "CLOMIPRAMIN HYDROCHLORID"
* #100000090504 "IDARUBICIN HYDROCHLORIDE"
* #100000090504 ^designation[0].language = #de-AT 
* #100000090504 ^designation[0].value = "IDARUBICIN HYDROCHLORID" 
* #100000090505 "TRIPTORELIN ACETATE"
* #100000090505 ^designation[0].language = #de-AT 
* #100000090505 ^designation[0].value = "TRIPTORELIN ACETAT" 
* #100000090506 "ESCITALOPRAM OXALATE"
* #100000090506 ^designation[0].language = #de-AT 
* #100000090506 ^designation[0].value = "ESCITALOPRAM OXALAT" 
* #100000090509 "BENZATHINE BENZYLPENICILLIN"
* #100000090509 ^designation[0].language = #de-AT 
* #100000090509 ^designation[0].value = "BENZATHIN BENZYLPENICILLIN" 
* #100000090511 "INDIUM (111IN) CHLORIDE"
* #100000090511 ^designation[0].language = #de-AT 
* #100000090511 ^designation[0].value = "INDIUM CHLORID [*111*IN]" 
* #100000090521 "CALCIPOTRIOL MONOHYDRATE"
* #100000090521 ^designation[0].language = #de-AT 
* #100000090521 ^designation[0].value = "CALCIPOTRIOL MONOHYDRAT" 
* #100000090523 "METHYLPREDNISOLONE SODIUM SUCCINATE"
* #100000090523 ^designation[0].language = #de-AT 
* #100000090523 ^designation[0].value = "NATRIUM METHYLPREDNISOLON SUCCINAT" 
* #100000090524 "ATOMOXETINE HYDROCHLORIDE"
* #100000090524 ^designation[0].language = #de-AT 
* #100000090524 ^designation[0].value = "ATOMOXETIN HYDROCHLORID" 
* #100000090542 "DEXAMETHASONE SODIUM PHOSPHATE"
* #100000090542 ^designation[0].language = #de-AT 
* #100000090542 ^designation[0].value = "DEXAMETHASON DINATRIUMPHOSPHAT" 
* #100000090545 "ERYTHROMYCIN ETHYLSUCCINATE"
* #100000090545 ^designation[0].language = #de-AT 
* #100000090545 ^designation[0].value = "ERYTHROMYCINETHYLSUCCINAT" 
* #100000090550 "CHLORMADINONE ACETATE"
* #100000090550 ^designation[0].language = #de-AT 
* #100000090550 ^designation[0].value = "CHLORMADINONACETAT" 
* #100000090552 "CLINDAMYCIN HYDROCHLORIDE"
* #100000090552 ^designation[0].language = #de-AT 
* #100000090552 ^designation[0].value = "CLINDAMYCIN HYDROCHLORID" 
* #100000090553 "CHLORMETHINE HYDROCHLORIDE"
* #100000090553 ^designation[0].language = #de-AT 
* #100000090553 ^designation[0].value = "CHLORMETHIN HYDROCHLORID" 
* #100000090554 "CITALOPRAM HYDROBROMIDE"
* #100000090554 ^designation[0].language = #de-AT 
* #100000090554 ^designation[0].value = "CITALOPRAM HYDROBROMID" 
* #100000090555 "CLINDAMYCIN PHOSPHATE"
* #100000090555 ^designation[0].language = #de-AT 
* #100000090555 ^designation[0].value = "CLINDAMYCIN PHOSPHAT" 
* #100000090556 "COLCHICINE"
* #100000090556 ^designation[0].language = #de-AT 
* #100000090556 ^designation[0].value = "COLCHICIN" 
* #100000090557 "DEXRAZOXANE HYDROCHLORIDE"
* #100000090557 ^designation[0].language = #de-AT 
* #100000090557 ^designation[0].value = "DEXRAZOXAN HYDROCHLORID" 
* #100000090558 "EPIRUBICIN HYDROCHLORIDE"
* #100000090558 ^designation[0].language = #de-AT 
* #100000090558 ^designation[0].value = "EPIRUBICIN HYDROCHLORID" 
* #100000090559 "GALANTAMINE HYDROBROMIDE"
* #100000090559 ^designation[0].language = #de-AT 
* #100000090559 ^designation[0].value = "GALANTAMIN HYDROBROMID" 
* #100000090560 "HYDROXYCHLOROQUINE SULFATE"
* #100000090560 ^designation[0].language = #de-AT 
* #100000090560 ^designation[0].value = "HYDROXYCHLOROQUIN SULFAT" 
* #100000090561 "IRINOTECAN HYDROCHLORIDE"
* #100000090561 ^designation[0].language = #de-AT 
* #100000090561 ^designation[0].value = "IRINOTECAN HYDROCHLORID" 
* #100000090562 "LEVOMEPROMAZINE HYDROCHLORIDE"
* #100000090562 ^designation[0].language = #de-AT 
* #100000090562 ^designation[0].value = "LEVOMEPROMAZIN HYDROCHLORID" 
* #100000090563 "LIDOCAINE HYDROCHLORIDE MONOHYDRATE"
* #100000090563 ^designation[0].language = #de-AT 
* #100000090563 ^designation[0].value = "LIDOCAIN HYDROCHLORID MONOHYDRAT" 
* #100000090564 "SALBUTAMOL SULFATE"
* #100000090564 ^designation[0].language = #de-AT 
* #100000090564 ^designation[0].value = "SALBUTAMOLSULFAT" 
* #100000090573 "CLOMIFENE CITRATE"
* #100000090573 ^designation[0].language = #de-AT 
* #100000090573 ^designation[0].value = "CLOMIFEN DIHYDROGENCITRAT" 
* #100000090574 "COLESEVELAM HYDROCHLORIDE"
* #100000090574 ^designation[0].language = #de-AT 
* #100000090574 ^designation[0].value = "COLESEVELAM HYDROCHLORID" 
* #100000090575 "DIHYDROERGOTAMINE MESILATE"
* #100000090575 ^designation[0].language = #de-AT 
* #100000090575 ^designation[0].value = "DIHYDROERGOTAMIN MESILAT" 
* #100000090576 "GADOPENTETATE DIMEGLUMINE"
* #100000090576 ^designation[0].language = #de-AT 
* #100000090576 ^designation[0].value = "DIMEGLUMIN GADOPENTETAT" 
* #100000090577 "HYDROMORPHONE HYDROCHLORIDE"
* #100000090577 ^designation[0].language = #de-AT 
* #100000090577 ^designation[0].value = "HYDROMORPHONHYDROCHLORID" 
* #100000090580 "LIDOCAINE HYDROCHLORIDE"
* #100000090580 ^designation[0].language = #de-AT 
* #100000090580 ^designation[0].value = "LIDOCAIN HYDROCHLORID" 
* #100000090582 "SERTRALINE HYDROCHLORIDE"
* #100000090582 ^designation[0].language = #de-AT 
* #100000090582 ^designation[0].value = "SERTRALIN HYDROCHLORID" 
* #100000090606 "ROPIVACAINE HYDROCHLORIDE MONOHYDRATE"
* #100000090606 ^designation[0].language = #de-AT 
* #100000090606 ^designation[0].value = "ROPIVACAINHYDROCHLORID MONOHYDRAT" 
* #100000090616 "DOCETAXEL"
* #100000090624 "TERBINAFINE HYDROCHLORIDE"
* #100000090624 ^designation[0].language = #de-AT 
* #100000090624 ^designation[0].value = "TERBINAFIN HYDROCHLORID" 
* #100000090630 "HUMAN FIBRINOGEN"
* #100000090630 ^designation[0].language = #de-AT 
* #100000090630 ^designation[0].value = "FIBRINOGEN" 
* #100000090631 "HUMAN CLOTTABLE PROTEIN CONTAINING MAINLY FIBRINOGEN AND FIBRONECTIN"
* #100000090631 ^designation[0].language = #de-AT 
* #100000090631 ^designation[0].value = "PROTEIN, GERINNBARES (HUMAN)" 
* #100000090650 "PLANTAGINIS LANCEOLATAE FOLIUM"
* #100000090678 "THYMI HERBA"
* #100000090680 "SENNAE FOLIUM"
* #100000090705 "ABACAVIR SULFATE"
* #100000090705 ^designation[0].language = #de-AT 
* #100000090705 ^designation[0].value = "ABACAVIR HEMISULFAT" 
* #100000090712 "DENOSUMAB"
* #100000090715 "VANDETANIB"
* #100000090733 "PAZOPANIB"
* #100000090751 "LORNOXICAM"
* #100000090754 "AMBROXOL HYDROCHLORIDE"
* #100000090754 ^designation[0].language = #de-AT 
* #100000090754 ^designation[0].value = "AMBROXOL HYDROCHLORID" 
* #100000090756 "POLYGONI HERBA"
* #100000090759 "PASSIFLORAE HERBA"
* #100000090763 "MEPOLIZUMAB"
* #100000090775 "POLYGONI AVICULARIS HERBA"
* #100000090781 "SILODOSIN"
* #100000090816 "MATRICARIAE FLOS"
* #100000090817 "MENTHAE PIPERITAE FOLIUM"
* #100000090819 "MELISSAE FOLIUM"
* #100000090843 "TILIAE FLOS"
* #100000090863 "HYPERICI HERBA"
* #100000090867 "HYPERICI HERBA EXTRACTUM"
* #100000090867 ^designation[0].language = #de-AT 
* #100000090867 ^designation[0].value = "HYPERICI HERBA (AUSZUG)" 
* #100000090958 "ESSENTIAL OIL - LAVANDULA ANGUSTIFOLIA"
* #100000090958 ^designation[0].language = #de-AT 
* #100000090958 ^designation[0].value = "LAVANDULAE AETHEROLEUM" 
* #100000090961 "CHARCOAL ACTIVATED"
* #100000090961 ^designation[0].language = #de-AT 
* #100000090961 ^designation[0].value = "AKTIVKOHLE" 
* #100000090996 "LUPULI FLOS"
* #100000091035 "CYANOCOBALAMIN"
* #100000091036 "CHLORHEXIDINE GLUCONATE"
* #100000091036 ^designation[0].language = #de-AT 
* #100000091036 ^designation[0].value = "CHLORHEXIDINDIGLUCONAT" 
* #100000091037 "ZOLPIDEM TARTRATE"
* #100000091037 ^designation[0].language = #de-AT 
* #100000091037 ^designation[0].value = "ZOLPIDEMTARTRAT" 
* #100000091038 "SELEGILINE HYDROCHLORIDE"
* #100000091038 ^designation[0].language = #de-AT 
* #100000091038 ^designation[0].value = "SELEGILIN HYDROCHLORID" 
* #100000091040 "TRANEXAMIC ACID"
* #100000091040 ^designation[0].language = #de-AT 
* #100000091040 ^designation[0].value = "TRANEXAMSÄURE" 
* #100000091045 "TICLOPIDINE HYDROCHLORIDE"
* #100000091045 ^designation[0].language = #de-AT 
* #100000091045 ^designation[0].value = "TICLOPIDIN HYDROCHLORID" 
* #100000091047 "ESCIN"
* #100000091047 ^designation[0].language = #de-AT 
* #100000091047 ^designation[0].value = "AESCIN" 
* #100000091052 "FERROUS CHLORIDE"
* #100000091052 ^designation[0].language = #de-AT 
* #100000091052 ^designation[0].value = "EISEN(II)CHLORID" 
* #100000091053 "CHLORHEXIDINE DIGLUCONATE SOLUTION"
* #100000091053 ^designation[0].language = #de-AT 
* #100000091053 ^designation[0].value = "CHLORHEXIDINDIGLUCONAT-LÖSUNG" 
* #100000091054 "RALOXIFENE HYDROCHLORIDE"
* #100000091054 ^designation[0].language = #de-AT 
* #100000091054 ^designation[0].value = "RALOXIFEN HYDROCHLORID" 
* #100000091056 "SULPIRIDE"
* #100000091056 ^designation[0].language = #de-AT 
* #100000091056 ^designation[0].value = "SULPIRID" 
* #100000091059 "LEVONORGESTREL"
* #100000091062 "PANTOPRAZOLE SODIUM SESQUIHYDRATE"
* #100000091062 ^designation[0].language = #de-AT 
* #100000091062 ^designation[0].value = "PANTOPRAZOL NATRIUM SESQUIHYDRAT" 
* #100000091063 "DIHYDROERGOCRISTINE MESILATE"
* #100000091063 ^designation[0].language = #de-AT 
* #100000091063 ^designation[0].value = "DIHYDROERGOCRISTIN METHANSULFONAT" 
* #100000091065 "TOBRAMYCIN"
* #100000091068 "PETHIDINE HYDROCHLORIDE"
* #100000091068 ^designation[0].language = #de-AT 
* #100000091068 ^designation[0].value = "PETHIDIN HYDROCHLORID" 
* #100000091069 "DEQUALINIUM CHLORIDE"
* #100000091069 ^designation[0].language = #de-AT 
* #100000091069 ^designation[0].value = "DEQUALINIUM CHLORID" 
* #100000091071 "CALCIUM ASCORBATE"
* #100000091071 ^designation[0].language = #de-AT 
* #100000091071 ^designation[0].value = "CALCIUMASCORBAT" 
* #100000091072 "TETRACYCLINE HYDROCHLORIDE"
* #100000091072 ^designation[0].language = #de-AT 
* #100000091072 ^designation[0].value = "TETRACYCLIN HYDROCHLORID" 
* #100000091073 "PENCICLOVIR"
* #100000091074 "DICLOFENAC DIETHYLAMINE"
* #100000091074 ^designation[0].language = #de-AT 
* #100000091074 ^designation[0].value = "DICLOFENAC DIETHYLAMIN" 
* #100000091076 "NORFLOXACIN"
* #100000091077 "LYSINE"
* #100000091077 ^designation[0].language = #de-AT 
* #100000091077 ^designation[0].value = "LYSIN" 
* #100000091078 "ERGOTAMINE TARTRATE"
* #100000091078 ^designation[0].language = #de-AT 
* #100000091078 ^designation[0].value = "ERGOTAMIN TARTRAT" 
* #100000091081 "PREDNISOLONE CAPROATE"
* #100000091081 ^designation[0].language = #de-AT 
* #100000091081 ^designation[0].value = "PREDNISOLON HEXANOAT" 
* #100000091082 "CYSTINE"
* #100000091082 ^designation[0].language = #de-AT 
* #100000091082 ^designation[0].value = "CYSTIN" 
* #100000091090 "SILDENAFIL CITRATE"
* #100000091090 ^designation[0].language = #de-AT 
* #100000091090 ^designation[0].value = "SILDENAFIL CITRAT" 
* #100000091092 "THIAMINE HYDROCHLORIDE"
* #100000091092 ^designation[0].language = #de-AT 
* #100000091092 ^designation[0].value = "THIAMIN HYDROCHLORID" 
* #100000091094 "PROCAINE HYDROCHLORIDE"
* #100000091094 ^designation[0].language = #de-AT 
* #100000091094 ^designation[0].value = "PROCAIN HYDROCHLORID" 
* #100000091095 "EPINEPHRINE HYDROCHLORIDE"
* #100000091095 ^designation[0].language = #de-AT 
* #100000091095 ^designation[0].value = "EPINEPHRIN HYDROCHLORID" 
* #100000091096 "FENTICONAZOLE NITRATE"
* #100000091096 ^designation[0].language = #de-AT 
* #100000091096 ^designation[0].value = "FENTICONAZOL NITRAT" 
* #100000091097 "PODOPHYLLOTOXIN"
* #100000091108 "SUFENTANIL CITRATE"
* #100000091108 ^designation[0].language = #de-AT 
* #100000091108 ^designation[0].value = "SUFENTANILCITRAT" 
* #100000091110 "TIAPRIDE HYDROCHLORIDE"
* #100000091110 ^designation[0].language = #de-AT 
* #100000091110 ^designation[0].value = "TIAPRID HYDROCHLORID" 
* #100000091113 "TOPOTECAN HYDROCHLORIDE"
* #100000091113 ^designation[0].language = #de-AT 
* #100000091113 ^designation[0].value = "TOPOTECAN HYDROCHLORID" 
* #100000091153 "ESSENTIAL OIL - THYMUS VULGARIS"
* #100000091153 ^designation[0].language = #de-AT 
* #100000091153 ^designation[0].value = "THYMI TYPO THYMOLO AETHEROLEUM" 
* #100000091166 "CLOPIDOGREL"
* #100000091179 "CANNABIDIOL"
* #100000091183 "ATENOLOL"
* #100000091186 "VALERIANAE RADIX"
* #100000091195 "TANACETI PARTHENII HERBA"
* #100000091200 "DONEPEZIL HYDROCHLORIDE"
* #100000091200 ^designation[0].language = #de-AT 
* #100000091200 ^designation[0].value = "DONEPEZIL HYDROCHLORID" 
* #100000091201 "MILNACIPRAN HYDROCHLORIDE"
* #100000091201 ^designation[0].language = #de-AT 
* #100000091201 ^designation[0].value = "MILNACIPRAN HYDROCHLORID" 
* #100000091240 "PREDNISOLONE METASULFOBENZOATE SODIUM"
* #100000091240 ^designation[0].language = #de-AT 
* #100000091240 ^designation[0].value = "NATRIUM PREDNISOLON M-SULFOBENZOAT" 
* #100000091241 "RIFAMYCIN SODIUM"
* #100000091241 ^designation[0].language = #de-AT 
* #100000091241 ^designation[0].value = "RIFAMYCIN NATRIUM" 
* #100000091242 "TRIMETAZIDINE DIHYDROCHLORIDE"
* #100000091242 ^designation[0].language = #de-AT 
* #100000091242 ^designation[0].value = "TRIMETAZIDIN DIHYDROCHLORID" 
* #100000091251 "CITALOPRAM HYDROCHLORIDE"
* #100000091251 ^designation[0].language = #de-AT 
* #100000091251 ^designation[0].value = "CITALOPRAM HYDROCHLORID" 
* #100000091252 "MAGNESIUM CITRATE"
* #100000091252 ^designation[0].language = #de-AT 
* #100000091252 ^designation[0].value = "MAGNESIUMCITRAT" 
* #100000091257 "ATORVASTATIN CALCIUM TRIHYDRATE"
* #100000091257 ^designation[0].language = #de-AT 
* #100000091257 ^designation[0].value = "ATORVASTATIN CALCIUM TRIHYDRAT" 
* #100000091269 "RETINOL PALMITATE"
* #100000091269 ^designation[0].language = #de-AT 
* #100000091269 ^designation[0].value = "RETINOLPALMITAT" 
* #100000091273 "REPAGLINIDE"
* #100000091273 ^designation[0].language = #de-AT 
* #100000091273 ^designation[0].value = "REPAGLINID" 
* #100000091275 "FLUVOXAMINE MALEATE"
* #100000091275 ^designation[0].language = #de-AT 
* #100000091275 ^designation[0].value = "FLUVOXAMINMALEAT" 
* #100000091287 "RASAGILINE MESILATE"
* #100000091287 ^designation[0].language = #de-AT 
* #100000091287 ^designation[0].value = "RASAGILIN MESILAT" 
* #100000091291 "BUPIVACAINE HYDROCHLORIDE"
* #100000091291 ^designation[0].language = #de-AT 
* #100000091291 ^designation[0].value = "BUPIVACAINHYDROCHLORID" 
* #100000091292 "PROPRANOLOL HYDROCHLORIDE"
* #100000091292 ^designation[0].language = #de-AT 
* #100000091292 ^designation[0].value = "PROPRANOLOL HYDROCHLORID" 
* #100000091294 "APIXABAN"
* #100000091301 "DALTEPARIN SODIUM"
* #100000091301 ^designation[0].language = #de-AT 
* #100000091301 ^designation[0].value = "DALTEPARIN NATRIUM" 
* #100000091302 "ZOFENOPRIL CALCIUM"
* #100000091306 "EPROSARTAN MESILATE"
* #100000091306 ^designation[0].language = #de-AT 
* #100000091306 ^designation[0].value = "EPROSARTAN MESILAT" 
* #100000091309 "BUDESONIDE"
* #100000091309 ^designation[0].language = #de-AT 
* #100000091309 ^designation[0].value = "BUDESONID" 
* #100000091312 "QUETIAPINE FUMARATE"
* #100000091312 ^designation[0].language = #de-AT 
* #100000091312 ^designation[0].value = "QUETIAPIN FUMARAT" 
* #100000091320 "POTASSIUM IODIDE"
* #100000091320 ^designation[0].language = #de-AT 
* #100000091320 ^designation[0].value = "KALIUMIODID" 
* #100000091323 "SULFUR HEXAFLUORIDE"
* #100000091323 ^designation[0].language = #de-AT 
* #100000091323 ^designation[0].value = "SCHWEFELHEXAFLUORID" 
* #100000091324 "VENLAFAXINE HYDROCHLORIDE"
* #100000091324 ^designation[0].language = #de-AT 
* #100000091324 ^designation[0].value = "VENLAFAXIN HYDROCHLORID" 
* #100000091340 "EPOPROSTENOL SODIUM"
* #100000091340 ^designation[0].language = #de-AT 
* #100000091340 ^designation[0].value = "NATRIUM EPOPROSTENOLAT" 
* #100000091342 "EPHEDRINE HYDROCHLORIDE"
* #100000091342 ^designation[0].language = #de-AT 
* #100000091342 ^designation[0].value = "EPHEDRINHYDROCHLORID" 
* #100000091343 "ENALAPRIL MALEATE"
* #100000091343 ^designation[0].language = #de-AT 
* #100000091343 ^designation[0].value = "ENALAPRIL MALEAT" 
* #100000091347 "EPOETIN BETA"
* #100000091353 "PAROXETINE MESILATE"
* #100000091353 ^designation[0].language = #de-AT 
* #100000091353 ^designation[0].value = "PAROXETIN MESILAT" 
* #100000091357 "NARATRIPTAN HYDROCHLORIDE"
* #100000091357 ^designation[0].language = #de-AT 
* #100000091357 ^designation[0].value = "NARATRIPTAN HYDROCHLORID" 
* #100000091359 "CEFALEXIN"
* #100000091360 "LYNESTRENOL"
* #100000091361 "VINCRISTINE SULFATE"
* #100000091361 ^designation[0].language = #de-AT 
* #100000091361 ^designation[0].value = "VINCRISTIN SULFAT" 
* #100000091362 "PIVMECILLINAM HYDROCHLORIDE"
* #100000091362 ^designation[0].language = #de-AT 
* #100000091362 ^designation[0].value = "PIVMECILLINAM HYDROCHLORID" 
* #100000091364 "MENTHOL"
* #100000091366 "METFORMIN HYDROCHLORIDE"
* #100000091366 ^designation[0].language = #de-AT 
* #100000091366 ^designation[0].value = "METFORMIN HYDROCHLORID" 
* #100000091368 "METOPROLOL SUCCINATE"
* #100000091368 ^designation[0].language = #de-AT 
* #100000091368 ^designation[0].value = "METOPROLOL SUCCINAT" 
* #100000091373 "PROGUANIL HYDROCHLORIDE"
* #100000091373 ^designation[0].language = #de-AT 
* #100000091373 ^designation[0].value = "PROGUANIL HYDROCHLORID" 
* #100000091374 "SOTALOL HYDROCHLORIDE"
* #100000091374 ^designation[0].language = #de-AT 
* #100000091374 ^designation[0].value = "SOTALOL HYDROCHLORID" 
* #100000091375 "VALGANCICLOVIR HYDROCHLORIDE"
* #100000091375 ^designation[0].language = #de-AT 
* #100000091375 ^designation[0].value = "VALGANCICLOVIR HYDROCHLORID" 
* #100000091381 "SALMETEROL XINAFOATE"
* #100000091381 ^designation[0].language = #de-AT 
* #100000091381 ^designation[0].value = "SALMETEROL XINAFOAT" 
* #100000091382 "MANNITOL"
* #100000091382 ^designation[0].language = #de-AT 
* #100000091382 ^designation[0].value = "MANNITOL, E-421" 
* #100000091386 "METHYLPREDNISOLONE HYDROGEN SUCCINATE"
* #100000091386 ^designation[0].language = #de-AT 
* #100000091386 ^designation[0].value = "METHYLPREDNISOLONHYDROGENSUCCINAT" 
* #100000091392 "IMMUNOSERA FOR HUMAN USE, ANIMAL"
* #100000091392 ^designation[0].language = #de-AT 
* #100000091392 ^designation[0].value = "INFLUENZAVIRUS (AUSZUG, PRODUKTE)" 
* #100000091403 "FERROUS GLUCONATE"
* #100000091403 ^designation[0].language = #de-AT 
* #100000091403 ^designation[0].value = "EISEN(II)-GLUCONAT" 
* #100000091409 "MAGNESIUM CHLORIDE HEXAHYDRATE"
* #100000091409 ^designation[0].language = #de-AT 
* #100000091409 ^designation[0].value = "MAGNESIUMCHLORID HEXAHYDRAT" 
* #100000091410 "MELPHALAN"
* #100000091427 "ACENOCUMAROL"
* #100000091430 "HYDROXOCOBALAMIN"
* #100000091432 "IOPAMIDOL"
* #100000091436 "CEFUROXIME SODIUM"
* #100000091436 ^designation[0].language = #de-AT 
* #100000091436 ^designation[0].value = "CEFUROXIM-NATRIUM" 
* #100000091438 "CETUXIMAB"
* #100000091439 "PIOGLITAZONE HYDROCHLORIDE"
* #100000091439 ^designation[0].language = #de-AT 
* #100000091439 ^designation[0].value = "PIOGLITAZON HYDROCHLORID" 
* #100000091440 "ORLISTAT"
* #100000091441 "MIRTAZAPINE"
* #100000091441 ^designation[0].language = #de-AT 
* #100000091441 ^designation[0].value = "MIRTAZAPIN" 
* #100000091442 "LOSARTAN POTASSIUM"
* #100000091442 ^designation[0].language = #de-AT 
* #100000091442 ^designation[0].value = "LOSARTAN KALIUM" 
* #100000091446 "METHADONE HYDROCHLORIDE"
* #100000091446 ^designation[0].language = #de-AT 
* #100000091446 ^designation[0].value = "METHADONHYDROCHLORID" 
* #100000091447 "SODIUM ASCORBATE"
* #100000091447 ^designation[0].language = #de-AT 
* #100000091447 ^designation[0].value = "NATRIUM ASCORBAT" 
* #100000091448 "SODIUM VALPROATE"
* #100000091448 ^designation[0].language = #de-AT 
* #100000091448 ^designation[0].value = "NATRIUM VALPROAT" 
* #100000091449 "ROSUVASTATIN CALCIUM"
* #100000091450 "BORTEZOMIB"
* #100000091454 "REBOXETINE MESILATE"
* #100000091454 ^designation[0].language = #de-AT 
* #100000091454 ^designation[0].value = "REBOXETIN MESILAT" 
* #100000091457 "ETANERCEPT"
* #100000091459 "MONTELUKAST SODIUM"
* #100000091459 ^designation[0].language = #de-AT 
* #100000091459 ^designation[0].value = "MONTELUKAST NATRIUM" 
* #100000091460 "METILDIGOXIN"
* #100000091466 "OSELTAMIVIR PHOSPHATE"
* #100000091466 ^designation[0].language = #de-AT 
* #100000091466 ^designation[0].value = "OSELTAMIVIR PHOSPHAT" 
* #100000091467 "ROPINIROLE HYDROCHLORIDE"
* #100000091467 ^designation[0].language = #de-AT 
* #100000091467 ^designation[0].value = "ROPINIROL HYDROCHLORID" 
* #100000091468 "CETIRIZINE DIHYDROCHLORIDE"
* #100000091468 ^designation[0].language = #de-AT 
* #100000091468 ^designation[0].value = "CETIRIZIN DIHYDROCHLORID" 
* #100000091470 "PRAMIPEXOLE DIHYDROCHLORIDE MONOHYDRATE"
* #100000091470 ^designation[0].language = #de-AT 
* #100000091470 ^designation[0].value = "PRAMIPEXOL DIHYDROCHLORID MONOHYDRAT" 
* #100000091473 "CHORIOGONADOTROPIN ALFA"
* #100000091475 "AZATHIOPRINE"
* #100000091475 ^designation[0].language = #de-AT 
* #100000091475 ^designation[0].value = "AZATHIOPRIN" 
* #100000091480 "PROPAFENONE HYDROCHLORIDE"
* #100000091480 ^designation[0].language = #de-AT 
* #100000091480 ^designation[0].value = "PROPAFENON HYDROCHLORID" 
* #100000091484 "THEOPHYLLINE"
* #100000091484 ^designation[0].language = #de-AT 
* #100000091484 ^designation[0].value = "THEOPHYLLIN" 
* #100000091493 "AFLIBERCEPT"
* #100000091496 "THYROTROPIN ALFA"
* #100000091501 "COD-LIVER OIL"
* #100000091501 ^designation[0].language = #de-AT 
* #100000091501 ^designation[0].value = "JECORIS ASELLI OLEUM" 
* #100000091512 "BOSENTAN MONOHYDRATE"
* #100000091512 ^designation[0].language = #de-AT 
* #100000091512 ^designation[0].value = "BOSENTAN MONOHYDRAT" 
* #100000091514 "BETAMETHASONE DIPROPIONATE"
* #100000091514 ^designation[0].language = #de-AT 
* #100000091514 ^designation[0].value = "BETAMETHASON DIPROPIONAT" 
* #100000091516 "CLOBETASOL PROPIONATE"
* #100000091516 ^designation[0].language = #de-AT 
* #100000091516 ^designation[0].value = "CLOBETASOL PROPIONAT" 
* #100000091518 "CALCIUM CARBONATE"
* #100000091518 ^designation[0].language = #de-AT 
* #100000091518 ^designation[0].value = "CALCIUMCARBONAT" 
* #100000091520 "PREDNISOLONE"
* #100000091520 ^designation[0].language = #de-AT 
* #100000091520 ^designation[0].value = "PREDNISOLON" 
* #100000091524 "SODIUM GLUCONATE"
* #100000091524 ^designation[0].language = #de-AT 
* #100000091524 ^designation[0].value = "NATRIUM GLUCONAT" 
* #100000091529 "LEVOFLOXACIN HEMIHYDRATE"
* #100000091529 ^designation[0].language = #de-AT 
* #100000091529 ^designation[0].value = "LEVOFLOXACIN HEMIHYDRAT" 
* #100000091533 "CANDESARTAN CILEXETIL"
* #100000091536 "CIPROFLOXACIN HYDROCHLORIDE"
* #100000091536 ^designation[0].language = #de-AT 
* #100000091536 ^designation[0].value = "CIPROFLOXACINHYDROCHLORID" 
* #100000091539 "PROPOFOL"
* #100000091540 "ALFACALCIDOL"
* #100000091545 "SUCRALFATE"
* #100000091545 ^designation[0].language = #de-AT 
* #100000091545 ^designation[0].value = "SUCRALFAT" 
* #100000091548 "ZOLEDRONIC ACID MONOHYDRATE"
* #100000091548 ^designation[0].language = #de-AT 
* #100000091548 ^designation[0].value = "ZOLEDRONSÄURE MONOHYDRAT" 
* #100000091552 "METOCLOPRAMIDE HYDROCHLORIDE MONOHYDRATE"
* #100000091552 ^designation[0].language = #de-AT 
* #100000091552 ^designation[0].value = "METOCLOPRAMIDHYDROCHLORID MONOHYDRAT" 
* #100000091553 "HISTIDINE HYDROCHLORIDE MONOHYDRATE"
* #100000091553 ^designation[0].language = #de-AT 
* #100000091553 ^designation[0].value = "HISTIDINHYDROCHLORID MONOHYDRAT" 
* #100000091554 "SODIUM FLUORIDE"
* #100000091554 ^designation[0].language = #de-AT 
* #100000091554 ^designation[0].value = "NATRIUMFLUORID" 
* #100000091555 "FLUOXETINE HYDROCHLORIDE"
* #100000091555 ^designation[0].language = #de-AT 
* #100000091555 ^designation[0].value = "FLUOXETIN HYDROCHLORID" 
* #100000091556 "BECLOMETASONE DIPROPIONATE"
* #100000091556 ^designation[0].language = #de-AT 
* #100000091556 ^designation[0].value = "BECLOMETASONDIPROPIONAT" 
* #100000091558 "BENSERAZIDE HYDROCHLORIDE"
* #100000091558 ^designation[0].language = #de-AT 
* #100000091558 ^designation[0].value = "BENSERAZID HYDROCHLORID" 
* #100000091567 "GLATIRAMER ACETATE"
* #100000091567 ^designation[0].language = #de-AT 
* #100000091567 ^designation[0].value = "GLATIRAMER ACETAT" 
* #100000091569 "LITHIUM CARBONATE"
* #100000091569 ^designation[0].language = #de-AT 
* #100000091569 ^designation[0].value = "LITHIUMCARBONAT" 
* #100000091571 "TRABECTEDIN"
* #100000091572 "SULFASALAZINE"
* #100000091572 ^designation[0].language = #de-AT 
* #100000091572 ^designation[0].value = "SULFASALAZIN" 
* #100000091573 "LEUPRORELIN ACETATE"
* #100000091573 ^designation[0].language = #de-AT 
* #100000091573 ^designation[0].value = "LEUPRORELIN ACETAT" 
* #100000091575 "COPPER CHLORIDE DIHYDRATE"
* #100000091575 ^designation[0].language = #de-AT 
* #100000091575 ^designation[0].value = "KUPFERDICHLORID DIHYDRAT" 
* #100000091576 "BEMIPARIN SODIUM"
* #100000091576 ^designation[0].language = #de-AT 
* #100000091576 ^designation[0].value = "BEMIPARIN NATRIUM" 
* #100000091579 "CEFPODOXIME PROXETIL"
* #100000091579 ^designation[0].language = #de-AT 
* #100000091579 ^designation[0].value = "CEFPODOXIM PROXETIL" 
* #100000091580 "CALCIUM DOBESILATE"
* #100000091580 ^designation[0].language = #de-AT 
* #100000091580 ^designation[0].value = "CALCIUM DOBESILAT" 
* #100000091581 "AMMONIUMCHLORID"
* #100000091586 "LANREOTIDE ACETATE"
* #100000091586 ^designation[0].language = #de-AT 
* #100000091586 ^designation[0].value = "LANREOTID ACETAT" 
* #100000091587 "OCTREOTIDE ACETATE"
* #100000091587 ^designation[0].language = #de-AT 
* #100000091587 ^designation[0].value = "OCTREOTID ACETAT" 
* #100000091592 "FLUOROURACIL"
* #100000091594 "FLUCYTOSINE"
* #100000091594 ^designation[0].language = #de-AT 
* #100000091594 ^designation[0].value = "FLUCYTOSIN" 
* #100000091595 "AZTREONAM"
* #100000091596 "AMOXICILLIN"
* #100000091598 "EZETIMIBE"
* #100000091598 ^designation[0].language = #de-AT 
* #100000091598 ^designation[0].value = "EZETIMIB" 
* #100000091603 "IOVERSOL"
* #100000091606 "VERTEPORFIN"
* #100000091607 "RIFABUTIN"
* #100000091609 "BENZYLPENICILLIN SODIUM"
* #100000091609 ^designation[0].language = #de-AT 
* #100000091609 ^designation[0].value = "BENZYLPENICILLIN NATRIUM" 
* #100000091610 "PAROXETINE HYDROCHLORIDE HEMIHYDRATE"
* #100000091610 ^designation[0].language = #de-AT 
* #100000091610 ^designation[0].value = "PAROXETINHYDROCHLORID HEMIHYDRAT" 
* #100000091612 "MEDROXYPROGESTERONE ACETATE"
* #100000091612 ^designation[0].language = #de-AT 
* #100000091612 ^designation[0].value = "MEDROXYPROGESTERON ACETAT" 
* #100000091613 "DARBEPOETIN ALFA"
* #100000091614 "MORPHINE HYDROCHLORIDE TRIHYDRATE"
* #100000091614 ^designation[0].language = #de-AT 
* #100000091614 ^designation[0].value = "MORPHINHYDROCHLORID TRIHYDRAT" 
* #100000091615 "MICAFUNGIN"
* #100000091618 "CEFEPIME DIHYDROCHLORIDE MONOHYDRATE"
* #100000091618 ^designation[0].language = #de-AT 
* #100000091618 ^designation[0].value = "CEFEPIMDIHYDROCHLORID MONOHYDRAT" 
* #100000091619 "AMILORIDE HYDROCHLORIDE"
* #100000091619 ^designation[0].language = #de-AT 
* #100000091619 ^designation[0].value = "AMILORID HYDROCHLORID" 
* #100000091622 "ARGATROBAN"
* #100000091628 "PENTOXIFYLLINE"
* #100000091628 ^designation[0].language = #de-AT 
* #100000091628 ^designation[0].value = "PENTOXIFYLLIN" 
* #100000091631 "LISINOPRIL DIHYDRATE"
* #100000091631 ^designation[0].language = #de-AT 
* #100000091631 ^designation[0].value = "LISINOPRIL-DIHYDRAT" 
* #100000091632 "LERCANIDIPINE HYDROCHLORIDE"
* #100000091632 ^designation[0].language = #de-AT 
* #100000091632 ^designation[0].value = "LERCANIDIPIN HYDROCHLORID" 
* #100000091634 "LACTULOSE"
* #100000091637 "IMIPENEM MONOHYDRATE"
* #100000091637 ^designation[0].language = #de-AT 
* #100000091637 ^designation[0].value = "IMIPENEM MONOHYDRAT" 
* #100000091647 "LEVOTHYROXINE SODIUM"
* #100000091647 ^designation[0].language = #de-AT 
* #100000091647 ^designation[0].value = "LEVOTHYROXIN NATRIUM" 
* #100000091649 "NALOXONE HYDROCHLORIDE"
* #100000091649 ^designation[0].language = #de-AT 
* #100000091649 ^designation[0].value = "NALOXON HYDROCHLORID" 
* #100000091651 "ISOFLURAN"
* #100000091653 "KETOROLAC TROMETAMOL"
* #100000091653 ^designation[0].language = #de-AT 
* #100000091653 ^designation[0].value = "KETOROLAC-TROMETAMOL" 
* #100000091655 "LACTITOL"
* #100000091655 ^designation[0].language = #de-AT 
* #100000091655 ^designation[0].value = "LACTITOL, E-966" 
* #100000091667 "GADOVERSETAMIDE"
* #100000091667 ^designation[0].language = #de-AT 
* #100000091667 ^designation[0].value = "GADOVERSETAMID" 
* #100000091669 "LEVETIRACETAM"
* #100000091670 "CILAZAPRIL"
* #100000091671 "IVERMECTIN"
* #100000091674 "IPRATROPIUM BROMIDE MONOHYDRATE"
* #100000091674 ^designation[0].language = #de-AT 
* #100000091674 ^designation[0].value = "IPRATROPIUMBROMID MONOHYDRAT" 
* #100000091677 "POVIDONE, IODINATED"
* #100000091677 ^designation[0].language = #de-AT 
* #100000091677 ^designation[0].value = "POVIDON-IOD" 
* #100000091679 "SUMATRIPTAN"
* #100000091681 "ROMIPLOSTIM"
* #100000091682 "SEVELAMER CARBONATE"
* #100000091682 ^designation[0].language = #de-AT 
* #100000091682 ^designation[0].value = "SEVELAMERCARBONAT" 
* #100000091683 "TOLVAPTAN"
* #100000091685 "GANIRELIX ACETATE"
* #100000091685 ^designation[0].language = #de-AT 
* #100000091685 ^designation[0].value = "GANIRELIXACETAT" 
* #100000091686 "RALTEGRAVIR"
* #100000091695 "BENZYDAMINE HYDROCHLORIDE"
* #100000091695 ^designation[0].language = #de-AT 
* #100000091695 ^designation[0].value = "BENZYDAMIN HYDROCHLORID" 
* #100000091697 "ITRACONAZOL"
* #100000091700 "SAPROPTERIN HYDROCHLORIDE"
* #100000091700 ^designation[0].language = #de-AT 
* #100000091700 ^designation[0].value = "SAPROPTERINDIHYDROCHLORID" 
* #100000091701 "TOCILIZUMAB"
* #100000091702 "USTEKINUMAB"
* #100000091703 "MEFENAMIC ACID"
* #100000091703 ^designation[0].language = #de-AT 
* #100000091703 ^designation[0].value = "MEFENAMINSÄURE" 
* #100000091704 "GOLIMUMAB"
* #100000091708 "ESKETAMINE HYDROCHLORIDE"
* #100000091708 ^designation[0].language = #de-AT 
* #100000091708 ^designation[0].value = "ESKETAMIN HYDROCHLORID" 
* #100000091712 "MEROPENEM"
* #100000091713 "LIDOCAINE"
* #100000091713 ^designation[0].language = #de-AT 
* #100000091713 ^designation[0].value = "LIDOCAIN" 
* #100000091714 "METAMIZOLE SODIUM"
* #100000091714 ^designation[0].language = #de-AT 
* #100000091714 ^designation[0].value = "METAMIZOL NATRIUM" 
* #100000091715 "MAGNESIUM SULFATE HEPTAHYDRATE"
* #100000091715 ^designation[0].language = #de-AT 
* #100000091715 ^designation[0].value = "MAGNESIUMSULFAT HEPTAHYDRAT" 
* #100000091719 "IMIQUIMOD"
* #100000091720 "EMTRICITABINE"
* #100000091720 ^designation[0].language = #de-AT 
* #100000091720 ^designation[0].value = "EMTRICITABIN" 
* #100000091721 "ETHINYLESTRADIOL"
* #100000091732 "PIMECROLIMUS"
* #100000091735 "DIPYRIDAMOLE"
* #100000091735 ^designation[0].language = #de-AT 
* #100000091735 ^designation[0].value = "DIPYRIDAMOL" 
* #100000091736 "PILOCARPINE HYDROCHLORIDE"
* #100000091736 ^designation[0].language = #de-AT 
* #100000091736 ^designation[0].value = "PILOCARPIN HYDROCHLORID" 
* #100000091737 "HYDROCHLOROTHIAZIDE"
* #100000091737 ^designation[0].language = #de-AT 
* #100000091737 ^designation[0].value = "HYDROCHLOROTHIAZID" 
* #100000091738 "GEFITINIB"
* #100000091739 "ESTRADIOL"
* #100000091744 "LYSINE ACETATE"
* #100000091744 ^designation[0].language = #de-AT 
* #100000091744 ^designation[0].value = "LYSIN ACETAT" 
* #100000091746 "VARENICLINE TARTRATE"
* #100000091746 ^designation[0].language = #de-AT 
* #100000091746 ^designation[0].value = "VARENICLINTARTRAT" 
* #100000091757 "SODIUM MOLYBDATE"
* #100000091757 ^designation[0].language = #de-AT 
* #100000091757 ^designation[0].value = "DINATRIUMMOLYBDAT" 
* #100000091760 "ITOPRIDE HYDROCHLORIDE"
* #100000091760 ^designation[0].language = #de-AT 
* #100000091760 ^designation[0].value = "ITOPRID HYDROCHLORID" 
* #100000091767 "NITRAZEPAM"
* #100000091768 "KETOPROFEN"
* #100000091775 "YOHIMBINE HYDROCHLORIDE"
* #100000091775 ^designation[0].language = #de-AT 
* #100000091775 ^designation[0].value = "YOHIMBIN HYDROCHLORID" 
* #100000091776 "THYMOL"
* #100000091777 "MALIC ACID"
* #100000091777 ^designation[0].language = #de-AT 
* #100000091777 ^designation[0].value = "ÄPFELSÄURE" 
* #100000091779 "POTASSIUM CITRATE"
* #100000091779 ^designation[0].language = #de-AT 
* #100000091779 ^designation[0].value = "KALIUMCITRAT" 
* #100000091781 "POLIOMYELITIS VACCINE"
* #100000091781 ^designation[0].language = #de-AT 
* #100000091781 ^designation[0].value = "POLIOMYELITISVIRUS" 
* #100000091783 "CHROMIC CHLORIDE"
* #100000091783 ^designation[0].language = #de-AT 
* #100000091783 ^designation[0].value = "CHROMCHLORID" 
* #100000091786 "SIMVASTATIN"
* #100000091789 "PRAVASTATIN SODIUM"
* #100000091789 ^designation[0].language = #de-AT 
* #100000091789 ^designation[0].value = "PRAVASTATIN NATRIUM" 
* #100000091796 "BRIMONIDINE TARTRATE"
* #100000091796 ^designation[0].language = #de-AT 
* #100000091796 ^designation[0].value = "BRIMONIDIN TARTRAT" 
* #100000091797 "FUSIDIC ACID"
* #100000091797 ^designation[0].language = #de-AT 
* #100000091797 ^designation[0].value = "FUSIDINSÄURE" 
* #100000091800 "TIZANIDINE HYDROCHLORIDE"
* #100000091800 ^designation[0].language = #de-AT 
* #100000091800 ^designation[0].value = "TIZANIDIN HYDROCHLORID" 
* #100000091802 "ALFUZOSIN HYDROCHLORIDE"
* #100000091802 ^designation[0].language = #de-AT 
* #100000091802 ^designation[0].value = "ALFUZOSIN HYDROCHLORID" 
* #100000091803 "METHYLPREDNISOLONE"
* #100000091803 ^designation[0].language = #de-AT 
* #100000091803 ^designation[0].value = "METHYLPREDNISOLON" 
* #100000091807 "POTASSIUM CHLORIDE"
* #100000091807 ^designation[0].language = #de-AT 
* #100000091807 ^designation[0].value = "KALIUMCHLORID" 
* #100000091808 "SAQUINAVIR MESILATE"
* #100000091808 ^designation[0].language = #de-AT 
* #100000091808 ^designation[0].value = "SAQUINAVIR MESILAT" 
* #100000091815 "ZOLEDRONIC ACID"
* #100000091815 ^designation[0].language = #de-AT 
* #100000091815 ^designation[0].value = "ZOLEDRONSÄURE" 
* #100000091816 "THIOPENTAL SODIUM"
* #100000091816 ^designation[0].language = #de-AT 
* #100000091816 ^designation[0].value = "THIOPENTAL NATRIUM" 
* #100000091819 "BETACAROTENE"
* #100000091819 ^designation[0].language = #de-AT 
* #100000091819 ^designation[0].value = "BETA-CAROTIN" 
* #100000091825 "IMATINIB MESILATE"
* #100000091825 ^designation[0].language = #de-AT 
* #100000091825 ^designation[0].value = "IMATINIB MESILAT" 
* #100000091826 "POTASSIUM HYDROXIDE"
* #100000091826 ^designation[0].language = #de-AT 
* #100000091826 ^designation[0].value = "KALIUMHYDROXID" 
* #100000091827 "SODIUM CITRATE"
* #100000091827 ^designation[0].language = #de-AT 
* #100000091827 ^designation[0].value = "NATRIUMCITRAT" 
* #100000091834 "SODIUM PHENYLBUTYRATE"
* #100000091834 ^designation[0].language = #de-AT 
* #100000091834 ^designation[0].value = "NATRIUMPHENYLBUTYRAT" 
* #100000091841 "MINOCYCLINE HYDROCHLORIDE"
* #100000091841 ^designation[0].language = #de-AT 
* #100000091841 ^designation[0].value = "MINOCYCLIN HYDROCHLORID" 
* #100000091843 "DEGARELIX"
* #100000091844 "PERMETHRIN"
* #100000091845 "PREGABALIN"
* #100000091847 "ASPARTIC ACID"
* #100000091847 ^designation[0].language = #de-AT 
* #100000091847 ^designation[0].value = "ASPARAGINSÄURE" 
* #100000091848 "CAMPHOR, RACEMIC"
* #100000091848 ^designation[0].language = #de-AT 
* #100000091848 ^designation[0].value = "RACEMISCHER CAMPHER" 
* #100000091852 "DARUNAVIR ETHANOLATE"
* #100000091852 ^designation[0].language = #de-AT 
* #100000091852 ^designation[0].value = "DARUNAVIR ETHANOLAT" 
* #100000091854 "PYRIDOSTIGMINE BROMIDE"
* #100000091854 ^designation[0].language = #de-AT 
* #100000091854 ^designation[0].value = "PYRIDOSTIGMIN BROMID" 
* #100000091859 "MICONAZOLE NITRATE"
* #100000091859 ^designation[0].language = #de-AT 
* #100000091859 ^designation[0].value = "MICONAZOL NITRAT" 
* #100000091860 "MOMETASONE FUROATE"
* #100000091860 ^designation[0].language = #de-AT 
* #100000091860 ^designation[0].value = "MOMETASON FUROAT" 
* #100000091871 "MIFAMURTIDE"
* #100000091871 ^designation[0].language = #de-AT 
* #100000091871 ^designation[0].value = "MIFAMURTID" 
* #100000091873 "NEVIRAPINE ANHYDROUS"
* #100000091873 ^designation[0].language = #de-AT 
* #100000091873 ^designation[0].value = "NEVIRAPIN WASSERFREI" 
* #100000091875 "SITAGLIPTIN"
* #100000091879 "RUBELLA VIRUS"
* #100000091879 ^designation[0].language = #de-AT 
* #100000091879 ^designation[0].value = "RÖTELNVIRUS" 
* #100000091889 "FLUOCORTOLONE PIVALATE"
* #100000091889 ^designation[0].language = #de-AT 
* #100000091889 ^designation[0].value = "FLUOCORTOLON PIVALAT" 
* #100000091890 "FLUPREDNIDENE ACETATE"
* #100000091890 ^designation[0].language = #de-AT 
* #100000091890 ^designation[0].value = "FLUPREDNIDEN ACETAT" 
* #100000091897 "INFLUENZA VIRUS"
* #100000091897 ^designation[0].language = #de-AT 
* #100000091897 ^designation[0].value = "INFLUENZAVIRUS" 
* #100000091907 "ALLOPURINOL"
* #100000091908 "ALPRAZOLAM"
* #100000091910 "PREDNISOLONE SODIUM SUCCINATE"
* #100000091910 ^designation[0].language = #de-AT 
* #100000091910 ^designation[0].value = "NATRIUM PREDNISOLON SUCCINAT" 
* #100000091913 "BORNAPRINE HYDROCHLORIDE"
* #100000091913 ^designation[0].language = #de-AT 
* #100000091913 ^designation[0].value = "BORNAPRIN HYDROCHLORID" 
* #100000091914 "AMSACRINE"
* #100000091914 ^designation[0].language = #de-AT 
* #100000091914 ^designation[0].value = "AMSACRIN" 
* #100000091915 "LISINOPRIL"
* #100000091917 "BROMHEXINE HYDROCHLORIDE"
* #100000091917 ^designation[0].language = #de-AT 
* #100000091917 ^designation[0].value = "BROMHEXIN HYDROCHLORID" 
* #100000091918 "DIMENHYDRINATE"
* #100000091918 ^designation[0].language = #de-AT 
* #100000091918 ^designation[0].value = "DIMENHYDRINAT" 
* #100000091921 "OXAZEPAM"
* #100000091922 "PHYTOMENADIONE"
* #100000091922 ^designation[0].language = #de-AT 
* #100000091922 ^designation[0].value = "PHYTOMENADION" 
* #100000091923 "MOLSIDOMINE"
* #100000091923 ^designation[0].language = #de-AT 
* #100000091923 ^designation[0].value = "MOLSIDOMIN" 
* #100000091926 "TRYPTOPHAN"
* #100000091933 "APROTININ"
* #100000091934 "DOSULEPIN HYDROCHLORIDE"
* #100000091934 ^designation[0].language = #de-AT 
* #100000091934 ^designation[0].value = "DOSULEPIN HYDROCHLORID" 
* #100000091937 "AZELAIC ACID"
* #100000091937 ^designation[0].language = #de-AT 
* #100000091937 ^designation[0].value = "AZELAINSÄURE" 
* #100000091943 "PREDNISON"
* #100000091946 "RIFAXIMIN"
* #100000091948 "BIFONAZOL"
* #100000091949 "BISACODYL"
* #100000091950 "SALICYLAMIDE"
* #100000091950 ^designation[0].language = #de-AT 
* #100000091950 ^designation[0].value = "SALICYLAMID" 
* #100000091953 "MESALAZINE"
* #100000091953 ^designation[0].language = #de-AT 
* #100000091953 ^designation[0].value = "MESALAZIN" 
* #100000091954 "TROPICAMIDE"
* #100000091954 ^designation[0].language = #de-AT 
* #100000091954 ^designation[0].value = "TROPICAMID" 
* #100000091958 "TURPENTINE OIL"
* #100000091958 ^designation[0].language = #de-AT 
* #100000091958 ^designation[0].value = "TEREBINTHINAE AETHEROLEUM" 
* #100000091960 "EPHEDRINE HEMIHYDRATE"
* #100000091960 ^designation[0].language = #de-AT 
* #100000091960 ^designation[0].value = "EPHEDRIN HEMIHYDRAT" 
* #100000091963 "LACTULOSE, LIQUID"
* #100000091963 ^designation[0].language = #de-AT 
* #100000091963 ^designation[0].value = "LACTULOSE-SIRUP" 
* #100000091968 "GLYCINE"
* #100000091968 ^designation[0].language = #de-AT 
* #100000091968 ^designation[0].value = "GLYCIN" 
* #100000091969 "ISOLEUCINE"
* #100000091969 ^designation[0].language = #de-AT 
* #100000091969 ^designation[0].value = "ISOLEUCIN" 
* #100000091977 "UREA"
* #100000091977 ^designation[0].language = #de-AT 
* #100000091977 ^designation[0].value = "HARNSTOFF" 
* #100000091979 "EUCALYPTUS OIL"
* #100000091979 ^designation[0].language = #de-AT 
* #100000091979 ^designation[0].value = "EUCALYPTI AETHEROLEUM" 
* #100000091983 "TAZOBACTAM"
* #100000091984 "IOPROMIDE"
* #100000091984 ^designation[0].language = #de-AT 
* #100000091984 ^designation[0].value = "IOPROMID" 
* #100000091985 "LORMETAZEPAM"
* #100000091986 "ELETRIPTAN HYDROBROMIDE"
* #100000091986 ^designation[0].language = #de-AT 
* #100000091986 ^designation[0].value = "ELETRIPTAN HYDROBROMID" 
* #100000091987 "PIRACETAM"
* #100000092005 "CINACALCET HYDROCHLORIDE"
* #100000092005 ^designation[0].language = #de-AT 
* #100000092005 ^designation[0].value = "CINACALCET HYDROCHLORID" 
* #100000092007 "PROTEIN C"
* #100000092009 "APOMORPHINE HYDROCHLORIDE"
* #100000092009 ^designation[0].language = #de-AT 
* #100000092009 ^designation[0].value = "APOMORPHINHYDROCHLORID" 
* #100000092010 "ASPARAGINASE"
* #100000092010 ^designation[0].language = #de-AT 
* #100000092010 ^designation[0].value = "L-ASPARAGIN-AMIDOHYDROLASE" 
* #100000092011 "DOPAMINE HYDROCHLORIDE"
* #100000092011 ^designation[0].language = #de-AT 
* #100000092011 ^designation[0].value = "DOPAMIN HYDROCHLORID" 
* #100000092012 "DOCUSATE SODIUM"
* #100000092012 ^designation[0].language = #de-AT 
* #100000092012 ^designation[0].value = "DOCUSAT NATRIUM" 
* #100000092013 "ALDESLEUKIN"
* #100000092015 "PRILOCAINE"
* #100000092015 ^designation[0].language = #de-AT 
* #100000092015 ^designation[0].value = "PRILOCAIN" 
* #100000092017 "NICERGOLINE"
* #100000092017 ^designation[0].language = #de-AT 
* #100000092017 ^designation[0].value = "NICERGOLIN" 
* #100000092018 "SODIUM LACTATE SOLUTION"
* #100000092018 ^designation[0].language = #de-AT 
* #100000092018 ^designation[0].value = "NATRIUMLACTAT-LÖSUNG" 
* #100000092030 "PSEUDOEPHEDRINE HYDROCHLORIDE"
* #100000092030 ^designation[0].language = #de-AT 
* #100000092030 ^designation[0].value = "PSEUDOEPHEDRINHYDROCHLORID" 
* #100000092032 "BUPRENORPHINE HYDROCHLORIDE"
* #100000092032 ^designation[0].language = #de-AT 
* #100000092032 ^designation[0].value = "BUPRENORPHIN HYDROCHLORID" 
* #100000092034 "CALCIUM ACETATE"
* #100000092034 ^designation[0].language = #de-AT 
* #100000092034 ^designation[0].value = "CALCIUMACETAT" 
* #100000092035 "DEXTROMETHORPHAN HYDROBROMIDE"
* #100000092035 ^designation[0].language = #de-AT 
* #100000092035 ^designation[0].value = "DEXTROMETHORPHAN HYDROBROMID" 
* #100000092037 "FOLLITROPIN ALFA"
* #100000092043 "SODIUM DIHYDROGEN PHOSPHATE DIHYDRATE"
* #100000092043 ^designation[0].language = #de-AT 
* #100000092043 ^designation[0].value = "NATRIUMDIHYDROGENPHOSPHAT DIHYDRAT" 
* #100000092046 "TREPROSTINIL"
* #100000092047 "OMEPRAZOL"
* #100000092048 "DIOSMIN"
* #100000092051 "RISEDRONATE SODIUM"
* #100000092051 ^designation[0].language = #de-AT 
* #100000092051 ^designation[0].value = "NATRIUM RISEDRONAT" 
* #100000092056 "AGALSIDASE ALFA"
* #100000092057 "LUTROPIN ALFA"
* #100000092058 "CHONDROITIN SULPHATE SODIUM"
* #100000092058 ^designation[0].language = #de-AT 
* #100000092058 ^designation[0].value = "CHONDROITINSULFAT-NATRIUM" 
* #100000092060 "DEXMEDETOMIDINE HYDROCHLORIDE"
* #100000092060 ^designation[0].language = #de-AT 
* #100000092060 ^designation[0].value = "DEXMEDETOMIDIN HYDROCHLORID" 
* #100000092064 "MEBENDAZOL"
* #100000092068 "MELPERONE HYDROCHLORIDE"
* #100000092068 ^designation[0].language = #de-AT 
* #100000092068 ^designation[0].value = "MELPERON HYDROCHLORID" 
* #100000092069 "GLUTAMIC ACID"
* #100000092069 ^designation[0].language = #de-AT 
* #100000092069 ^designation[0].value = "GLUTAMINSÄURE" 
* #100000092072 "ORPHENADRINE CITRATE"
* #100000092072 ^designation[0].language = #de-AT 
* #100000092072 ^designation[0].value = "ORPHENADRIN DIHYDROGENCITRAT" 
* #100000092074 "CLOTRIMAZOLE"
* #100000092074 ^designation[0].language = #de-AT 
* #100000092074 ^designation[0].value = "CLOTRIMAZOL" 
* #100000092075 "DEXPANTHENOL"
* #100000092078 "KETOCONAZOL"
* #100000092079 "BENZALKONIUM CHLORIDE"
* #100000092079 ^designation[0].language = #de-AT 
* #100000092079 ^designation[0].value = "BENZALKONIUMCHLORID" 
* #100000092081 "ROXITHROMYCIN"
* #100000092089 "NORELGESTROMIN"
* #100000092091 "RISPERIDONE"
* #100000092091 ^designation[0].language = #de-AT 
* #100000092091 ^designation[0].value = "RISPERIDON" 
* #100000092094 "FLUTAMIDE"
* #100000092094 ^designation[0].language = #de-AT 
* #100000092094 ^designation[0].value = "FLUTAMID" 
* #100000092096 "SORBITOL"
* #100000092098 "MELOXICAM"
* #100000092100 "AMPHOTERICIN B"
* #100000092104 "PHENYTOIN"
* #100000092105 "TAZOBACTAM SODIUM"
* #100000092105 ^designation[0].language = #de-AT 
* #100000092105 ^designation[0].value = "TAZOBACTAM NATRIUM" 
* #100000092107 "URSODEOXYCHOLIC ACID"
* #100000092107 ^designation[0].language = #de-AT 
* #100000092107 ^designation[0].value = "URSODEOXYCHOLSÄURE" 
* #100000092109 "PALIPERIDONE"
* #100000092109 ^designation[0].language = #de-AT 
* #100000092109 ^designation[0].value = "PALIPERIDON" 
* #100000092110 "CIPROFLOXACIN"
* #100000092112 "CLINDAMYCIN PALMITATE HYDROCHLORIDE"
* #100000092112 ^designation[0].language = #de-AT 
* #100000092112 ^designation[0].value = "CLINDAMYCIN PALMITAT HYDROCHLORID" 
* #100000092115 "SODIUM CHLORIDE"
* #100000092115 ^designation[0].language = #de-AT 
* #100000092115 ^designation[0].value = "NATRIUMCHLORID" 
* #100000092120 "PAMIDRONATE DISODIUM"
* #100000092120 ^designation[0].language = #de-AT 
* #100000092120 ^designation[0].value = "DINATRIUM PAMIDRONAT" 
* #100000092121 "CICLOSPORIN"
* #100000092126 "METHYLPREDNISOLONE ACEPONATE"
* #100000092126 ^designation[0].language = #de-AT 
* #100000092126 ^designation[0].value = "METHYLPREDNISOLON ACEPONAT" 
* #100000092127 "CARBAMAZEPINE"
* #100000092127 ^designation[0].language = #de-AT 
* #100000092127 ^designation[0].value = "CARBAMAZEPIN" 
* #100000092128 "AMANTADINE SULFATE"
* #100000092128 ^designation[0].language = #de-AT 
* #100000092128 ^designation[0].value = "AMANTADIN SULFAT" 
* #100000092130 "PROPYPHENAZONE"
* #100000092130 ^designation[0].language = #de-AT 
* #100000092130 ^designation[0].value = "PROPYPHENAZON" 
* #100000092133 "CINCHOCAINE HYDROCHLORIDE"
* #100000092133 ^designation[0].language = #de-AT 
* #100000092133 ^designation[0].value = "CINCHOCAIN HYDROCHLORID" 
* #100000092135 "BACITRACIN"
* #100000092138 "TYROTHRICIN"
* #100000092141 "TENOFOVIR DISOPROXIL FUMARATE"
* #100000092141 ^designation[0].language = #de-AT 
* #100000092141 ^designation[0].value = "TENOFOVIR DISOPROXIL FUMARAT" 
* #100000092142 "FENTANYL CITRATE"
* #100000092142 ^designation[0].language = #de-AT 
* #100000092142 ^designation[0].value = "FENTANYLCITRAT" 
* #100000092143 "CABERGOLINE"
* #100000092143 ^designation[0].language = #de-AT 
* #100000092143 ^designation[0].value = "CABERGOLIN" 
* #100000092148 "DAUNORUBICIN HYDROCHLORIDE"
* #100000092148 ^designation[0].language = #de-AT 
* #100000092148 ^designation[0].value = "DAUNORUBICIN HYDROCHLORID" 
* #100000092159 "TYROSINE"
* #100000092159 ^designation[0].language = #de-AT 
* #100000092159 ^designation[0].value = "TYROSIN" 
* #100000092162 "LORAZEPAM"
* #100000092165 "LOVASTATIN"
* #100000092166 "CINNARIZINE"
* #100000092166 ^designation[0].language = #de-AT 
* #100000092166 ^designation[0].value = "CINNARIZIN" 
* #100000092168 "NITROUS OXIDE"
* #100000092168 ^designation[0].language = #de-AT 
* #100000092168 ^designation[0].value = "DISTICKSTOFFMONOXID" 
* #100000092169 "OXYBUTYNIN HYDROCHLORIDE"
* #100000092169 ^designation[0].language = #de-AT 
* #100000092169 ^designation[0].value = "OXYBUTYNIN HYDROCHLORID" 
* #100000092173 "CHOLINE SALICYLATE"
* #100000092173 ^designation[0].language = #de-AT 
* #100000092173 ^designation[0].value = "CHOLIN SALICYLAT" 
* #100000092174 "CEFOTAXIME SODIUM"
* #100000092174 ^designation[0].language = #de-AT 
* #100000092174 ^designation[0].value = "CEFOTAXIM NATRIUM" 
* #100000092175 "INDOCYANINE GREEN"
* #100000092175 ^designation[0].language = #de-AT 
* #100000092175 ^designation[0].value = "INDOCYANINGRÜN" 
* #100000092178 "TRIAMCINOLONE"
* #100000092178 ^designation[0].language = #de-AT 
* #100000092178 ^designation[0].value = "TRIAMCINOLON" 
* #100000092179 "LORATADINE"
* #100000092179 ^designation[0].language = #de-AT 
* #100000092179 ^designation[0].value = "LORATADIN" 
* #100000092186 "KETOTIFEN HYDROGEN FUMARATE"
* #100000092186 ^designation[0].language = #de-AT 
* #100000092186 ^designation[0].value = "KETOTIFEN HYDROGENFUMARAT" 
* #100000092187 "OLOPATADINE HYDROCHLORIDE"
* #100000092187 ^designation[0].language = #de-AT 
* #100000092187 ^designation[0].value = "OLOPATADIN HYDROCHLORID" 
* #100000092188 "PEMETREXED DISODIUM"
* #100000092188 ^designation[0].language = #de-AT 
* #100000092188 ^designation[0].value = "PEMETREXED DINATRIUM" 
* #100000092190 "THIAMINE NITRATE"
* #100000092190 ^designation[0].language = #de-AT 
* #100000092190 ^designation[0].value = "THIAMIN NITRAT" 
* #100000092191 "PROTIRELIN"
* #100000092193 "NICOTINAMIDE"
* #100000092193 ^designation[0].language = #de-AT 
* #100000092193 ^designation[0].value = "NICOTINAMID" 
* #100000092209 "ALPROSTADIL ALFADEX"
* #100000092209 ^designation[0].language = #de-AT 
* #100000092209 ^designation[0].value = "ALPROSTADIL X ALFADEX" 
* #100000092212 "DOXYCYCLIN"
* #100000092217 "TITANIUM DIOXIDE"
* #100000092217 ^designation[0].language = #de-AT 
* #100000092217 ^designation[0].value = "TITANDIOXID" 
* #100000092220 "DINOPROSTONE"
* #100000092220 ^designation[0].language = #de-AT 
* #100000092220 ^designation[0].value = "DINOPROSTON" 
* #100000092221 "COPPER GLUCONATE"
* #100000092221 ^designation[0].language = #de-AT 
* #100000092221 ^designation[0].value = "KUPFERDIGLUCONAT" 
* #100000092224 "OXCARBAZEPIN"
* #100000092225 "ALUMINIUM OXIDE, HYDRATED"
* #100000092225 ^designation[0].language = #de-AT 
* #100000092225 ^designation[0].value = "ALUMINIUMOXID, WASSERHALTIGES" 
* #100000092226 "RIMEXOLONE"
* #100000092226 ^designation[0].language = #de-AT 
* #100000092226 ^designation[0].value = "RIMEXOLON" 
* #100000092237 "ONDANSETRON HYDROCHLORIDE"
* #100000092237 ^designation[0].language = #de-AT 
* #100000092237 ^designation[0].value = "ONDANSETRON HYDROCHLORID" 
* #100000092238 "ESZOPICLONE"
* #100000092238 ^designation[0].language = #de-AT 
* #100000092238 ^designation[0].value = "ESZOPICLON" 
* #100000092240 "RAMIPRIL"
* #100000092241 "PEPPERMINT OIL"
* #100000092241 ^designation[0].language = #de-AT 
* #100000092241 ^designation[0].value = "MENTHAE PIPERITAE AETHEROLEUM" 
* #100000092247 "MAGNESIUM OXIDE, LIGHT"
* #100000092247 ^designation[0].language = #de-AT 
* #100000092247 ^designation[0].value = "MAGNESIUMOXID, LEICHTES" 
* #100000092248 "NAFTIDROFURYL HYDROGEN OXALATE"
* #100000092248 ^designation[0].language = #de-AT 
* #100000092248 ^designation[0].value = "NAFTIDROFURYL HYDROGENOXALAT" 
* #100000092256 "NAPROXEN SODIUM"
* #100000092256 ^designation[0].language = #de-AT 
* #100000092256 ^designation[0].value = "NATRIUM NAPROXENAT" 
* #100000092257 "DOMPERIDONE"
* #100000092257 ^designation[0].language = #de-AT 
* #100000092257 ^designation[0].value = "DOMPERIDON" 
* #100000092258 "FENTANYL HYDROCHLORIDE"
* #100000092258 ^designation[0].language = #de-AT 
* #100000092258 ^designation[0].value = "FENTANYL HYDROCHLORID" 
* #100000092260 "HYDROCORTISONE ACETATE"
* #100000092260 ^designation[0].language = #de-AT 
* #100000092260 ^designation[0].value = "HYDROCORTISON ACETAT" 
* #100000092261 "ACETYLCYSTEIN"
* #100000092263 "HYDROXYETHYL SALICYLATE"
* #100000092263 ^designation[0].language = #de-AT 
* #100000092263 ^designation[0].value = "HYDROXYETHYLSALICYLAT" 
* #100000092265 "LEUCINE"
* #100000092265 ^designation[0].language = #de-AT 
* #100000092265 ^designation[0].value = "LEUCIN" 
* #100000092266 "LOXAPINE"
* #100000092266 ^designation[0].language = #de-AT 
* #100000092266 ^designation[0].value = "LOXAPIN" 
* #100000092267 "PARAFFIN, LIGHT LIQUID"
* #100000092267 ^designation[0].language = #de-AT 
* #100000092267 ^designation[0].value = "PARAFFIN, DÜNNFLÜSSIGES" 
* #100000092272 "DICLOFENAC SODIUM"
* #100000092272 ^designation[0].language = #de-AT 
* #100000092272 ^designation[0].value = "DICLOFENAC NATRIUM" 
* #100000092273 "NIMODIPIN"
* #100000092276 "ACARBOSE"
* #100000092280 "FAMCICLOVIR"
* #100000092281 "FEXOFENADINE HYDROCHLORIDE"
* #100000092281 ^designation[0].language = #de-AT 
* #100000092281 ^designation[0].value = "FEXOFENADIN HYDROCHLORID" 
* #100000092282 "LAMOTRIGINE"
* #100000092282 ^designation[0].language = #de-AT 
* #100000092282 ^designation[0].value = "LAMOTRIGIN" 
* #100000092288 "PAMIDRONIC ACID"
* #100000092288 ^designation[0].language = #de-AT 
* #100000092288 ^designation[0].value = "PAMIDRONSÄURE" 
* #100000092297 "EPLERENONE"
* #100000092297 ^designation[0].language = #de-AT 
* #100000092297 ^designation[0].value = "EPLERENON" 
* #100000092298 "EXEMESTANE"
* #100000092298 ^designation[0].language = #de-AT 
* #100000092298 ^designation[0].value = "EXEMESTAN" 
* #100000092300 "GADOBUTROL"
* #100000092301 "LATANOPROST"
* #100000092306 "ALPROSTADIL"
* #100000092309 "OFLOXACIN"
* #100000092310 "TERBINAFINE"
* #100000092310 ^designation[0].language = #de-AT 
* #100000092310 ^designation[0].value = "TERBINAFIN" 
* #100000092312 "CINEOLE"
* #100000092312 ^designation[0].language = #de-AT 
* #100000092312 ^designation[0].value = "CINEOL" 
* #100000092314 "TETRABENAZINE"
* #100000092314 ^designation[0].language = #de-AT 
* #100000092314 ^designation[0].value = "TETRABENAZIN" 
* #100000092317 "LEVOMENTHOL"
* #100000092318 "ACIPIMOX"
* #100000092320 "IMIDAPRIL HYDROCHLORIDE"
* #100000092320 ^designation[0].language = #de-AT 
* #100000092320 ^designation[0].value = "IMIDAPRIL HYDROCHLORID" 
* #100000092321 "DEXTROMETHORPHAN"
* #100000092326 "TROPISETRON HYDROCHLORIDE"
* #100000092326 ^designation[0].language = #de-AT 
* #100000092326 ^designation[0].value = "TROPISETRON HYDROCHLORID" 
* #100000092327 "EVEROLIMUS"
* #100000092328 "ALANINE"
* #100000092328 ^designation[0].language = #de-AT 
* #100000092328 ^designation[0].value = "ALANIN" 
* #100000092332 "ANASTROZOL"
* #100000092334 "ONDANSETRON"
* #100000092335 "TROSPIUM CHLORIDE"
* #100000092335 ^designation[0].language = #de-AT 
* #100000092335 ^designation[0].value = "TROSPIUM CHLORID" 
* #100000092338 "COLISTIMETHATE SODIUM"
* #100000092338 ^designation[0].language = #de-AT 
* #100000092338 ^designation[0].value = "COLISTIMETHAT NATRIUM" 
* #100000092339 "DEFIBROTIDE"
* #100000092339 ^designation[0].language = #de-AT 
* #100000092339 ^designation[0].value = "DEFIBROTID" 
* #100000092342 "ESTRAMUSTINE PHOSPHATE"
* #100000092342 ^designation[0].language = #de-AT 
* #100000092342 ^designation[0].value = "ESTRAMUSTIN PHOSPHAT" 
* #100000092343 "FOSINOPRIL SODIUM"
* #100000092343 ^designation[0].language = #de-AT 
* #100000092343 ^designation[0].value = "FOSINOPRIL NATRIUM" 
* #100000092348 "GESTODENE"
* #100000092348 ^designation[0].language = #de-AT 
* #100000092348 ^designation[0].value = "GESTODEN" 
* #100000092355 "AZITHROMYCIN"
* #100000092358 "MAGNESIUM CARBONATE, HEAVY"
* #100000092358 ^designation[0].language = #de-AT 
* #100000092358 ^designation[0].value = "SCHWERES BASISCHES MAGNESIUMCARBONAT" 
* #100000092362 "DIAZEPAM"
* #100000092363 "FAMOTIDINE"
* #100000092363 ^designation[0].language = #de-AT 
* #100000092363 ^designation[0].value = "FAMOTIDIN" 
* #100000092364 "CLARITHROMYCIN"
* #100000092365 "THREONINE"
* #100000092365 ^designation[0].language = #de-AT 
* #100000092365 ^designation[0].value = "THREONIN" 
* #100000092368 "DICLOFENAC POTASSIUM"
* #100000092368 ^designation[0].language = #de-AT 
* #100000092368 ^designation[0].value = "DICLOFENAC KALIUM" 
* #100000092371 "ICHTHAMMOL"
* #100000092371 ^designation[0].language = #de-AT 
* #100000092371 ^designation[0].value = "ICHTHAMMOLUM" 
* #100000092375 "DROSPIRENONE"
* #100000092375 ^designation[0].language = #de-AT 
* #100000092375 ^designation[0].value = "DROSPIRENON" 
* #100000092377 "MUPIROCIN"
* #100000092380 "EPINEPHRINE"
* #100000092380 ^designation[0].language = #de-AT 
* #100000092380 ^designation[0].value = "EPINEPHRIN" 
* #100000092381 "POVIDONE"
* #100000092381 ^designation[0].language = #de-AT 
* #100000092381 ^designation[0].value = "POVIDON" 
* #100000092382 "ERYTHROMYCIN"
* #100000092384 "OPIPRAMOL HYDROCHLORIDE"
* #100000092384 ^designation[0].language = #de-AT 
* #100000092384 ^designation[0].value = "OPIPRAMOL DIHYDROCHLORID" 
* #100000092388 "TRIMETHOPRIM"
* #100000092391 "FLUOCORTOLONE CAPROATE"
* #100000092391 ^designation[0].language = #de-AT 
* #100000092391 ^designation[0].value = "FLUOCORTOLON HEXANOAT" 
* #100000092392 "LEVOCABASTINE HYDROCHLORIDE"
* #100000092392 ^designation[0].language = #de-AT 
* #100000092392 ^designation[0].value = "LEVOCABASTIN HYDROCHLORID" 
* #100000092395 "CHLORHEXIDINE DIHYDROCHLORIDE"
* #100000092395 ^designation[0].language = #de-AT 
* #100000092395 ^designation[0].value = "CHLORHEXIDIN DIHYDROCHLORID" 
* #100000092397 "OXYTOCIN"
* #100000092398 "MAPROTILIN HYDROCHLORID"
* #100000092402 "CEFALEXIN MONOHYDRATE"
* #100000092402 ^designation[0].language = #de-AT 
* #100000092402 ^designation[0].value = "CEFALEXIN MONOHYDRAT" 
* #100000092406 "NITROFURANTOIN"
* #100000092407 "BENZYL NICOTINATE"
* #100000092407 ^designation[0].language = #de-AT 
* #100000092407 ^designation[0].value = "BENZYLNICOTINAT" 
* #100000092408 "SPIRONOLACTONE"
* #100000092408 ^designation[0].language = #de-AT 
* #100000092408 ^designation[0].value = "SPIRONOLACTON" 
* #100000092411 "CYCLOPENTOLATE HYDROCHLORIDE"
* #100000092411 ^designation[0].language = #de-AT 
* #100000092411 ^designation[0].value = "CYCLOPENTOLAT HYDROCHLORID" 
* #100000092414 "METHIONINE"
* #100000092414 ^designation[0].language = #de-AT 
* #100000092414 ^designation[0].value = "METHIONIN" 
* #100000092415 "METYRAPONE"
* #100000092415 ^designation[0].language = #de-AT 
* #100000092415 ^designation[0].value = "METYRAPON" 
* #100000092416 "PHENYLALANINE"
* #100000092416 ^designation[0].language = #de-AT 
* #100000092416 ^designation[0].value = "PHENYLALANIN" 
* #100000092418 "MISOPROSTOL"
* #100000092422 "VALINE"
* #100000092422 ^designation[0].language = #de-AT 
* #100000092422 ^designation[0].value = "VALIN" 
* #100000092424 "AMINOSALICYLIC ACID"
* #100000092424 ^designation[0].language = #de-AT 
* #100000092424 ^designation[0].value = "P-AMINOSALIZYLSÄURE" 
* #100000092425 "SEVOFLURANE"
* #100000092425 ^designation[0].language = #de-AT 
* #100000092425 ^designation[0].value = "SEVOFLURAN" 
* #100000092427 "DICHLOROBENZYL ALCOHOL"
* #100000092427 ^designation[0].language = #de-AT 
* #100000092427 ^designation[0].value = "2,4-DICHLORBENZYLALKOHOL" 
* #100000092429 "SODIUM NITROPRUSSIDE DIHYDRATE"
* #100000092429 ^designation[0].language = #de-AT 
* #100000092429 ^designation[0].value = "NITROPRUSSIDNATRIUM DIHYDRAT" 
* #100000092433 "MEXILETINE HYDROCHLORIDE"
* #100000092433 ^designation[0].language = #de-AT 
* #100000092433 ^designation[0].value = "MEXILETIN HYDROCHLORID" 
* #100000092483 "AMIKACIN"
* #100000092485 "BETAXOLOL HYDROCHLORIDE"
* #100000092485 ^designation[0].language = #de-AT 
* #100000092485 ^designation[0].value = "BETAXOLOL HYDROCHLORID" 
* #100000092486 "LETROZOL"
* #100000092487 "LEVOSIMENDAN"
* #100000092488 "MESNA"
* #100000092489 "MOCLOBEMIDE"
* #100000092489 ^designation[0].language = #de-AT 
* #100000092489 ^designation[0].value = "MOCLOBEMID" 
* #100000092490 "NISOLDIPINE"
* #100000092490 ^designation[0].language = #de-AT 
* #100000092490 ^designation[0].value = "NISOLDIPIN" 
* #100000092491 "PANTOPRAZOL"
* #100000092492 "PIROXICAM"
* #100000092506 "LINEZOLID"
* #100000092509 "OCTREOTIDE"
* #100000092509 ^designation[0].language = #de-AT 
* #100000092509 ^designation[0].value = "OCTREOTID" 
* #100000092510 "PARICALCITOL"
* #100000092518 "TRIAZOLAM"
* #100000092519 "ZOPICLONE"
* #100000092519 ^designation[0].language = #de-AT 
* #100000092519 ^designation[0].value = "ZOPICLON" 
* #100000092530 "FENOFIBRATE"
* #100000092530 ^designation[0].language = #de-AT 
* #100000092530 ^designation[0].value = "FENOFIBRAT" 
* #100000092531 "HEXETIDINE"
* #100000092531 ^designation[0].language = #de-AT 
* #100000092531 ^designation[0].value = "HEXETIDIN" 
* #100000092534 "TIBOLONE"
* #100000092534 ^designation[0].language = #de-AT 
* #100000092534 ^designation[0].value = "TIBOLON" 
* #100000092536 "TOPIRAMATE"
* #100000092536 ^designation[0].language = #de-AT 
* #100000092536 ^designation[0].value = "TOPIRAMAT" 
* #100000092554 "BROMAZEPAM"
* #100000092557 "CAPTOPRIL"
* #100000092558 "MANGANESE GLUCONATE"
* #100000092558 ^designation[0].language = #de-AT 
* #100000092558 ^designation[0].value = "MANGANGLUCONAT" 
* #100000092564 "CARBOPLATIN"
* #100000092570 "TRIAMTEREN"
* #100000092571 "LEVOCETIRIZINE DIHYDROCHLORIDE"
* #100000092571 ^designation[0].language = #de-AT 
* #100000092571 ^designation[0].value = "LEVOCETIRIZIN DIHYDROCHLORID" 
* #100000092577 "CARVEDILOL"
* #100000092578 "CHLORTALIDONE"
* #100000092578 ^designation[0].language = #de-AT 
* #100000092578 ^designation[0].value = "CHLORTALIDON" 
* #100000092588 "TRETINOIN"
* #100000092592 "METHOXSALEN"
* #100000092594 "METHYLERGOMETRINE MALEATE"
* #100000092594 ^designation[0].language = #de-AT 
* #100000092594 ^designation[0].value = "METHYLERGOMETRIN HYDROGENMALEAT" 
* #100000092599 "MAGNESIUMSULFAT"
* #100000092603 "SULPROSTONE"
* #100000092603 ^designation[0].language = #de-AT 
* #100000092603 ^designation[0].value = "SULPROSTON" 
* #100000092605 "DEXAMETHASONE"
* #100000092605 ^designation[0].language = #de-AT 
* #100000092605 ^designation[0].value = "DEXAMETHASON" 
* #100000092609 "ETOFENAMATE"
* #100000092609 ^designation[0].language = #de-AT 
* #100000092609 ^designation[0].value = "ETOFENAMAT" 
* #100000092610 "IPRATROPIUM BROMIDE"
* #100000092610 ^designation[0].language = #de-AT 
* #100000092610 ^designation[0].value = "IPRATROPIUM BROMID" 
* #100000092612 "CITRIC ACID"
* #100000092612 ^designation[0].language = #de-AT 
* #100000092612 ^designation[0].value = "CITRONENSÄURE" 
* #100000092614 "FLUORINE (18F) FLUDEOXYGLUCOSE"
* #100000092614 ^designation[0].language = #de-AT 
* #100000092614 ^designation[0].value = "FLUDEOXYGLUCOSE [*18*F]" 
* #100000092615 "PREDNISOLONE PIVALATE"
* #100000092615 ^designation[0].language = #de-AT 
* #100000092615 ^designation[0].value = "PREDNISOLON PIVALAT" 
* #100000092618 "SULFAMETHOXAZOLE"
* #100000092618 ^designation[0].language = #de-AT 
* #100000092618 ^designation[0].value = "SULFAMETHOXAZOL" 
* #100000092622 "BETAMETHASONE VALERATE"
* #100000092622 ^designation[0].language = #de-AT 
* #100000092622 ^designation[0].value = "BETAMETHASONVALERAT" 
* #100000092626 "CEFOPERAZONE SODIUM"
* #100000092626 ^designation[0].language = #de-AT 
* #100000092626 ^designation[0].value = "CEFOPERAZON NATRIUM" 
* #100000092628 "CHLOROQUINE PHOSPHATE"
* #100000092628 ^designation[0].language = #de-AT 
* #100000092628 ^designation[0].value = "CHLOROQUIN DI(DIHYDROGENPHOSPHAT)" 
* #100000092629 "AMOXICILLIN TRIHYDRATE"
* #100000092629 ^designation[0].language = #de-AT 
* #100000092629 ^designation[0].value = "AMOXICILLIN TRIHYDRAT" 
* #100000092630 "CEFTAZIDIM"
* #100000092631 "SODIUM OXIDRONATE"
* #100000092631 ^designation[0].language = #de-AT 
* #100000092631 ^designation[0].value = "DINATRIUM OXIDRONAT" 
* #100000092632 "CEFIXIME"
* #100000092632 ^designation[0].language = #de-AT 
* #100000092632 ^designation[0].value = "CEFIXIM" 
* #100000092635 "HYDROCORTISONE"
* #100000092635 ^designation[0].language = #de-AT 
* #100000092635 ^designation[0].value = "HYDROCORTISON" 
* #100000092638 "DOXYCYCLINE HYCLATE"
* #100000092638 ^designation[0].language = #de-AT 
* #100000092638 ^designation[0].value = "DOXYCYCLINHYCLAT" 
* #100000092640 "CETRIMIDE"
* #100000092640 ^designation[0].language = #de-AT 
* #100000092640 ^designation[0].value = "CETRIMID" 
* #100000092641 "CITICOLINE SODIUM"
* #100000092641 ^designation[0].language = #de-AT 
* #100000092641 ^designation[0].value = "CITICOLIN NATRIUM" 
* #100000092643 "GEMFIBROZIL"
* #100000092645 "HALOPERIDOL"
* #100000092646 "HISTIDINE"
* #100000092646 ^designation[0].language = #de-AT 
* #100000092646 ^designation[0].value = "HISTIDIN" 
* #100000092647 "IDEBENONE"
* #100000092647 ^designation[0].language = #de-AT 
* #100000092647 ^designation[0].value = "IDEBENON" 
* #100000092651 "INDAPAMIDE"
* #100000092651 ^designation[0].language = #de-AT 
* #100000092651 ^designation[0].value = "INDAPAMID" 
* #100000092652 "ACETAZOLAMIDE"
* #100000092652 ^designation[0].language = #de-AT 
* #100000092652 ^designation[0].value = "ACETAZOLAMID" 
* #100000092656 "FINASTERIDE"
* #100000092656 ^designation[0].language = #de-AT 
* #100000092656 ^designation[0].value = "FINASTERID" 
* #100000092657 "FLUCONAZOL"
* #100000092658 "FLUNITRAZEPAM"
* #100000092659 "FLURBIPROFEN"
* #100000092660 "TERAZOSIN HYDROCHLORIDE"
* #100000092660 ^designation[0].language = #de-AT 
* #100000092660 ^designation[0].value = "TERAZOSIN HYDROCHLORID" 
* #100000092663 "GABAPENTIN"
* #100000092668 "ESTRIOL"
* #100000092669 "UROKINASE"
* #100000092670 "XYLOMETAZOLINE HYDROCHLORIDE"
* #100000092670 ^designation[0].language = #de-AT 
* #100000092670 ^designation[0].value = "XYLOMETAZOLIN HYDROCHLORID" 
* #100000092682 "DRONEDARONE HYDROCHLORIDE"
* #100000092682 ^designation[0].language = #de-AT 
* #100000092682 ^designation[0].value = "DRONEDARON HYDROCHLORID" 
* #100000092717 "DUTASTERIDE"
* #100000092717 ^designation[0].language = #de-AT 
* #100000092717 ^designation[0].value = "DUTASTERID" 
* #100000092718 "FLUTICASONE PROPIONATE"
* #100000092718 ^designation[0].language = #de-AT 
* #100000092718 ^designation[0].value = "FLUTICASONPROPIONAT" 
* #100000092724 "COLECALCIFEROL"
* #100000092727 "INDACATEROL MALEATE"
* #100000092727 ^designation[0].language = #de-AT 
* #100000092727 ^designation[0].value = "INDACATEROLMALEAT" 
* #100000092729 "IVABRADINE HYDROCHLORIDE"
* #100000092729 ^designation[0].language = #de-AT 
* #100000092729 ^designation[0].value = "IVABRADIN HYDROCHLORID" 
* #100000092730 "STREPTOCOCCUS PNEUMONIAE"
* #100000092732 "ISONIAZID"
* #100000092736 "FLUTICASONFUROAT"
* #100000092742 "ALPHA-1-ANTITRYPSIN"
* #100000092742 ^designation[0].language = #de-AT 
* #100000092742 ^designation[0].value = "ALPHA-1-PROTEINASE-INHIBITOR" 
* #100000092747 "ERLOTINIB HYDROCHLORIDE"
* #100000092747 ^designation[0].language = #de-AT 
* #100000092747 ^designation[0].value = "ERLOTINIB HYDROCHLORID" 
* #100000092750 "PEGINTERFERON ALFA-2A"
* #100000092752 "MYCOPHENOLATE MOFETIL"
* #100000092752 ^designation[0].language = #de-AT 
* #100000092752 ^designation[0].value = "MYCOPHENOLAT MOFETIL" 
* #100000092755 "CALCIUMCHLORID"
* #100000092757 "THALLIUM (201TL) CHLORIDE"
* #100000092757 ^designation[0].language = #de-AT 
* #100000092757 ^designation[0].value = "THALLIUM CHLORID [*201*TL]" 
* #100000092758 "INDOMETACIN"
* #100000092763 "BENDROFLUMETHIAZIDE"
* #100000092763 ^designation[0].language = #de-AT 
* #100000092763 ^designation[0].value = "BENDROFLUMETHIAZID" 
* #100000092766 "LEVOMEPROMAZINE HYDROGENMALEATE"
* #100000092766 ^designation[0].language = #de-AT 
* #100000092766 ^designation[0].value = "LEVOMEPROMAZIN HYDROGENMALEAT" 
* #100000092772 "CHLORAMPHENICOL"
* #100000092777 "CHLORPROTHIXENE HYDROCHLORIDE"
* #100000092777 ^designation[0].language = #de-AT 
* #100000092777 ^designation[0].value = "CHLORPROTHIXEN HYDROCHLORID" 
* #100000092778 "DIGITOXIN"
* #100000092780 "GLIQUIDONE"
* #100000092780 ^designation[0].language = #de-AT 
* #100000092780 ^designation[0].value = "GLIQUIDON" 
* #100000092781 "CLOBETASONE BUTYRATE"
* #100000092781 ^designation[0].language = #de-AT 
* #100000092781 ^designation[0].value = "CLOBETASON BUTYRAT" 
* #100000092785 "BAMBUTEROL HYDROCHLORIDE"
* #100000092785 ^designation[0].language = #de-AT 
* #100000092785 ^designation[0].value = "BAMBUTEROL HYDROCHLORID" 
* #100000092789 "BIOTIN"
* #100000092790 "CALCITRIOL"
* #100000092793 "CIMETIDINE"
* #100000092793 ^designation[0].language = #de-AT 
* #100000092793 ^designation[0].value = "CIMETIDIN" 
* #100000092795 "CLOZAPINE"
* #100000092795 ^designation[0].language = #de-AT 
* #100000092795 ^designation[0].value = "CLOZAPIN" 
* #100000092797 "DIFLUCORTOLONE VALERATE"
* #100000092797 ^designation[0].language = #de-AT 
* #100000092797 ^designation[0].value = "DIFLUCORTOLON VALERAT" 
* #100000092798 "DICLOFENAC"
* #100000092800 "DOXYCYCLINE MONOHYDRATE"
* #100000092800 ^designation[0].language = #de-AT 
* #100000092800 ^designation[0].value = "DOXYCYCLIN MONOHYDRAT" 
* #100000092808 "ACICLOVIR"
* #100000092811 "RIVAROXABAN"
* #100000092815 "DIENOGEST"
* #100000092816 "ETHOSUXIMIDE"
* #100000092816 ^designation[0].language = #de-AT 
* #100000092816 ^designation[0].value = "ETHOSUXIMID" 
* #100000092818 "PHENOXYETHANOL"
* #100000092821 "GLYCEROL"
* #100000092822 "HYDROXYPROGESTERONE CAPROATE"
* #100000092822 ^designation[0].language = #de-AT 
* #100000092822 ^designation[0].value = "HYDROXYPROGESTERON CAPROAT" 
* #100000092841 "MYCOBACTERIUM BOVIS"
* #100000092847 "AMIODARONE HYDROCHLORIDE"
* #100000092847 ^designation[0].language = #de-AT 
* #100000092847 ^designation[0].value = "AMIODARON HYDROCHLORID" 
* #100000092872 "ICATIBANT ACETATE"
* #100000092872 ^designation[0].language = #de-AT 
* #100000092872 ^designation[0].value = "ICATIBANT-ACETAT" 
* #100000092913 "GLUTATHION"
* #100000092960 "IPILIMUMAB"
* #100000092971 "PLERIXAFOR"
* #100000092978 "BELATACEPT"
* #100000092986 "ECONAZOLE NITRATE"
* #100000092986 ^designation[0].language = #de-AT 
* #100000092986 ^designation[0].value = "ECONAZOL NITRAT" 
* #100000092990 "EPOETIN THETA"
* #100000092991 "AMIFAMPRIDINE"
* #100000092991 ^designation[0].language = #de-AT 
* #100000092991 ^designation[0].value = "AMIFAMPRIDIN" 
* #100000093008 "DEXKETOPROFEN TROMETAMOL"
* #100000093008 ^designation[0].language = #de-AT 
* #100000093008 ^designation[0].value = "DEXKETOPROFEN-TROMETAMOL" 
* #100000093015 "ADEFOVIR DIPIVOXIL"
* #100000093031 "DIBOTERMIN ALFA"
* #100000093036 "NORETHISTERONE ACETATE"
* #100000093036 ^designation[0].language = #de-AT 
* #100000093036 ^designation[0].value = "NORETHISTERON ACETAT" 
* #100000093039 "CEFUROXIME AXETIL"
* #100000093039 ^designation[0].language = #de-AT 
* #100000093039 ^designation[0].value = "CEFUROXIM AXETIL" 
* #100000093043 "RIVASTIGMINE HYDROGEN TARTRATE"
* #100000093043 ^designation[0].language = #de-AT 
* #100000093043 ^designation[0].value = "RIVASTIGMIN HYDROGENTARTRAT" 
* #100000093052 "INDACATEROL"
* #100000093055 "HYOSCINE BUTYLBROMIDE"
* #100000093055 ^designation[0].language = #de-AT 
* #100000093055 ^designation[0].value = "BUTYLSCOPOLAMINIUMBROMID" 
* #100000093061 "POTASSIUM CLAVULANATE"
* #100000093061 ^designation[0].language = #de-AT 
* #100000093061 ^designation[0].value = "KALIUMCLAVULANAT" 
* #100000093063 "NEBIVOLOL HYDROCHLORIDE"
* #100000093063 ^designation[0].language = #de-AT 
* #100000093063 ^designation[0].value = "NEBIVOLOL HYDROCHLORID" 
* #100000093070 "CANAKINUMAB"
* #100000093073 "ELTROMBOPAG"
* #100000093142 "CITRIC ACID ANHYDROUS"
* #100000093142 ^designation[0].language = #de-AT 
* #100000093142 ^designation[0].value = "CITRONENSÄURE, WASSERFREI" 
* #100000093275 "TRAMADOL HYDROCHLORIDE"
* #100000093275 ^designation[0].language = #de-AT 
* #100000093275 ^designation[0].value = "TRAMADOLHYDROCHLORID" 
* #100000093289 "QUERCUS CORTEX"
* #100000093307 "PRASUGREL"
* #100000093308 "CRATAEGI FOLIUM CUM FLORE"
* #100000093312 "ACHILLEA MILLEFOLIUM L. HERBA"
* #100000093312 ^designation[0].language = #de-AT 
* #100000093312 ^designation[0].value = "MILLEFOLII HERBA" 
* #100000093366 "DULOXETINE HYDROCHLORIDE"
* #100000093366 ^designation[0].language = #de-AT 
* #100000093366 ^designation[0].value = "DULOXETIN HYDROCHLORID" 
* #100000093369 "DAPOXETINE HYDROCHLORIDE"
* #100000093369 ^designation[0].language = #de-AT 
* #100000093369 ^designation[0].value = "DAPOXETINHYDROCHLORID" 
* #100000093396 "ESLICARBAZEPINE ACETATE"
* #100000093396 ^designation[0].language = #de-AT 
* #100000093396 ^designation[0].value = "ESLICARBAZEPINACETAT" 
* #100000093402 "VEDOLIZUMAB"
* #100000093411 "ULIPRISTAL ACETATE"
* #100000093411 ^designation[0].language = #de-AT 
* #100000093411 ^designation[0].value = "ULIPRISTALACETAT" 
* #100000093415 "ZANAMIVIR"
* #100000093427 "MINT OIL, PARTLY DEMENTHOLISED"
* #100000093427 ^designation[0].language = #de-AT 
* #100000093427 ^designation[0].value = "MENTHAE ARVENSIS AETHEROLEUM PARTIM MENTHOLUM DEPLETUM" 
* #100000093436 "PSEUDOEPHEDRINE SULPHATE"
* #100000093436 ^designation[0].language = #de-AT 
* #100000093436 ^designation[0].value = "PSEUDOEPHEDRIN SULFAT" 
* #100000093454 "SUNITINIB MALATE"
* #100000093454 ^designation[0].language = #de-AT 
* #100000093454 ^designation[0].value = "SUNITINIBMALAT" 
* #100000115275 "REGADENOSON"
* #100000115324 "RUTOSIDE TRIHYDRATE"
* #100000115324 ^designation[0].language = #de-AT 
* #100000115324 ^designation[0].value = "RUTOSID TRIHYDRAT" 
* #100000115332 "FERUMOXYTOL"
* #100000115340 "LINAGLIPTIN"
* #100000115346 "ASENAPINE"
* #100000115346 ^designation[0].language = #de-AT 
* #100000115346 ^designation[0].value = "ASENAPIN" 
* #100000115380 "SODIUM PICOSULFATE MONOHYDRATE"
* #100000115380 ^designation[0].language = #de-AT 
* #100000115380 ^designation[0].value = "NATRIUM PICOSULFAT MONOHYDRAT" 
* #100000115388 "FRAGARIAE FOLIUM"
* #100000115415 "BISOPROLOL HEMIFUMARATE"
* #100000115415 ^designation[0].language = #de-AT 
* #100000115415 ^designation[0].value = "BISOPROLOL HEMIFUMARAT" 
* #100000115443 "CLOPIDOGREL BESILATE"
* #100000115443 ^designation[0].language = #de-AT 
* #100000115443 ^designation[0].value = "CLOPIDOGREL BESILAT" 
* #100000115464 "TICAGRELOR"
* #100000115484 "TETRAKIS (2-METHOXYISOBUTYL ISONITRILE) COPPER (I) TETRAFLUOROBORATE"
* #100000115484 ^designation[0].language = #de-AT 
* #100000115484 ^designation[0].value = "TETRAKIS (2-METHOXY-2-METHYLPROPANISOCYANID) KUPFER (I)-TETRAFLUOROBORAT" 
* #100000115485 "ERIBULIN MESYLATE"
* #100000115485 ^designation[0].language = #de-AT 
* #100000115485 ^designation[0].value = "ERIBULINMESILAT" 
* #100000115486 "VOCLOSPORIN"
* #100000115500 "VERNAKALANT HYDROCHLORIDE"
* #100000115500 ^designation[0].language = #de-AT 
* #100000115500 ^designation[0].value = "VERNAKALANT HYDROCHLORID" 
* #100000115532 "AUTOLOGOUS HUMAN CARTILAGE CELLS"
* #100000115532 ^designation[0].language = #de-AT 
* #100000115532 ^designation[0].value = "AUTOLOGE KNORPELZELLEN" 
* #100000115562 "AIR MEDICINAL"
* #100000115562 ^designation[0].language = #de-AT 
* #100000115562 ^designation[0].value = "LUFT ZUR MEDIZINISCHEN ANWENDUNG" 
* #100000115569 "LEVOMETHADONE HYDROCHLORIDE"
* #100000115569 ^designation[0].language = #de-AT 
* #100000115569 ^designation[0].value = "LEVOMETHADONHYDROCHLORID" 
* #100000115606 "DISODIUM CROMOGLYCATE"
* #100000115606 ^designation[0].language = #de-AT 
* #100000115606 ^designation[0].value = "DINATRIUM CROMOGLICAT" 
* #100000115624 "FINGOLIMOD HYDROCHLORIDE"
* #100000115624 ^designation[0].language = #de-AT 
* #100000115624 ^designation[0].value = "FINGOLIMODHYDROCHLORID" 
* #100000115694 "CABAZITAXEL"
* #100000115739 "CYTISINE"
* #100000115739 ^designation[0].language = #de-AT 
* #100000115739 ^designation[0].value = "CYTISIN" 
* #100000115769 "MACROGOL"
* #100000115858 "VELAGLUCERASE ALFA"
* #100000115886 "TAFLUPROST"
* #100000115888 "CLOPIDOGREL HYDROCHLORIDE"
* #100000115888 ^designation[0].language = #de-AT 
* #100000115888 ^designation[0].value = "CLOPIDOGREL HYDROCHLORID" 
* #100000115914 "HELENII RHIZOMA"
* #100000115936 "CONESTAT ALFA"
* #100000124127 "FIDAXOMICIN"
* #100000124128 "TAFAMIDIS MEGLUMINE"
* #100000124128 ^designation[0].language = #de-AT 
* #100000124128 ^designation[0].value = "TAFAMIDIS MEGLUMIN" 
* #100000124138 "BOCEPREVIR"
* #100000124141 "RILPIVIRINE"
* #100000124141 ^designation[0].language = #de-AT 
* #100000124141 ^designation[0].value = "RILPIVIRIN" 
* #100000124142 "RILPIVIRINE HYDROCHLORIDE"
* #100000124142 ^designation[0].language = #de-AT 
* #100000124142 ^designation[0].value = "RILPIVIRIN HYDROCHLORID" 
* #100000124156 "CEFTAROLINE FOSAMIL"
* #100000124156 ^designation[0].language = #de-AT 
* #100000124156 ^designation[0].value = "CEFTAROLIN FOSAMIL" 
* #100000124166 "PALIPERIDONE PALMITATE"
* #100000124166 ^designation[0].language = #de-AT 
* #100000124166 ^designation[0].value = "PALIPERIDON PALMITAT" 
* #100000124173 "TEDUGLUTIDE"
* #100000124173 ^designation[0].language = #de-AT 
* #100000124173 ^designation[0].value = "TEDUGLUTID" 
* #100000124178 "DAPAGLIFLOZIN"
* #100000124179 "TELAPREVIR"
* #100000124192 "ALBUMIN (HUMAN)"
* #100000124192 ^designation[0].language = #de-AT 
* #100000124192 ^designation[0].value = "HUMANALBUMIN" 
* #100000124205 "ACETYL SALICYLIC ACID"
* #100000124205 ^designation[0].language = #de-AT 
* #100000124205 ^designation[0].value = "ACETYLSALICYLSÄURE" 
* #100000124251 "GARDEN SORREL HERB"
* #100000124251 ^designation[0].language = #de-AT 
* #100000124251 ^designation[0].value = "RUMICIS ACETOSAE HERBA" 
* #100000124254 "CALCIUM LEVOMEFOLATE"
* #100000124254 ^designation[0].language = #de-AT 
* #100000124254 ^designation[0].value = "CALCIUM 5-METHYLTETRAHYDROFOLAT" 
* #100000124306 "RUXOLITINIB"
* #100000124322 "PUMPKIN SEED"
* #100000124322 ^designation[0].language = #de-AT 
* #100000124322 ^designation[0].value = "CUCURBITAE SEMEN" 
* #100000124338 "GINGER EXTRACT"
* #100000124338 ^designation[0].language = #de-AT 
* #100000124338 ^designation[0].value = "ZINGIBERIS RHIZOMA (AUSZUG)" 
* #100000124345 "CRIZOTINIB"
* #100000124357 "LIXISENATIDE"
* #100000124357 ^designation[0].language = #de-AT 
* #100000124357 ^designation[0].value = "LIXISENATID" 
* #100000124371 "VISMODEGIB"
* #100000124379 "INGENOL MEBUTATE"
* #100000124379 ^designation[0].language = #de-AT 
* #100000124379 ^designation[0].value = "INGENOLMEBUTAT" 
* #100000124403 "LINACLOTIDE"
* #100000124403 ^designation[0].language = #de-AT 
* #100000124403 ^designation[0].value = "LINACLOTID" 
* #100000124405 "GLUCOSAMINE SULFATE SODIUM CHLORIDE"
* #100000124405 ^designation[0].language = #de-AT 
* #100000124405 ^designation[0].value = "GLUCOSAMIN SULFAT X NATRIUM CHLORID" 
* #100000124419 "SEMAGLUTIDE"
* #100000124419 ^designation[0].language = #de-AT 
* #100000124419 ^designation[0].value = "SEMAGLUTID" 
* #100000124431 "MINOCYCLINE HYDROCHLORIDE DIHYDRATE"
* #100000124431 ^designation[0].language = #de-AT 
* #100000124431 ^designation[0].value = "MINOCYCLIN HYDROCHLORID DIHYDRAT" 
* #100000124466 "OLAPARIB"
* #100000124471 "TAPENTADOL HYDROCHLORIDE"
* #100000124471 ^designation[0].language = #de-AT 
* #100000124471 ^designation[0].value = "TAPENTADOL HYDROCHLORID" 
* #100000124472 "ALOGLIPTIN BENZOATE"
* #100000124472 ^designation[0].language = #de-AT 
* #100000124472 ^designation[0].value = "ALOGLIPTINBENZOAT" 
* #100000124487 "HUMAN COAGULATION FACTOR XIII"
* #100000124487 ^designation[0].language = #de-AT 
* #100000124487 ^designation[0].value = "GERINNUNGSFAKTOR XIII" 
* #100000124500 "LEVOTHYROXINE SODIUM HYDRATE"
* #100000124500 ^designation[0].language = #de-AT 
* #100000124500 ^designation[0].value = "LEVOTHYROXIN NATRIUM HYDRAT" 
* #100000124506 "ICOSAPENT ETHYL"
* #100000124506 ^designation[0].language = #de-AT 
* #100000124506 ^designation[0].value = "ICOSAPENT-ETHYL" 
* #100000124509 "BIFIDOBACTERIUM"
* #100000124520 "LISDEXAMFETAMINE DIMESYLATE"
* #100000124520 ^designation[0].language = #de-AT 
* #100000124520 ^designation[0].value = "LISDEXAMFETAMIN DIMESYLAT" 
* #100000124524 "BRENTUXIMAB VEDOTIN"
* #100000124527 "PERAMPANEL"
* #100000124528 "VEMURAFENIB"
* #100000124530 "SILTUXIMAB"
* #100000124531 "SUGAMMADEX SODIUM"
* #100000124531 ^designation[0].language = #de-AT 
* #100000124531 ^designation[0].value = "SUGAMMADEX-OCTANATRIUM" 
* #100000125892 "RIOCIGUAT"
* #100000125897 "INOTUZUMAB OZOGAMICIN"
* #100000125922 "RAMUCIRUMAB"
* #100000125948 "NECITUMUMAB"
* #100000125968 "OBINUTUZUMAB"
* #100000126026 "DANTROLENE SODIUM, HEMIHEPTAHYDRATE"
* #100000126026 ^designation[0].language = #de-AT 
* #100000126026 ^designation[0].value = "DANTROLEN NATRIUM HEMIHEPTAHYDRAT" 
* #100000126098 "SERENOA SERRULATA FRUIT EXTRACT"
* #100000126098 ^designation[0].language = #de-AT 
* #100000126098 ^designation[0].value = "SABALIS SERRULATAE FRUCTUS (AUSZUG)" 
* #100000126102 "TAFAMIDIS"
* #100000126117 "POMALIDOMIDE"
* #100000126117 ^designation[0].language = #de-AT 
* #100000126117 ^designation[0].value = "POMALIDOMID" 
* #100000126131 "SAUSSUREA COSTUS ROOT"
* #100000126131 ^designation[0].language = #de-AT 
* #100000126131 ^designation[0].value = "AUCKLANDIAE RADIX" 
* #100000126135 "IVACAFTOR"
* #100000126136 "TOFACITINIB CITRATE"
* #100000126136 ^designation[0].language = #de-AT 
* #100000126136 ^designation[0].value = "TOFACITINIBCITRAT" 
* #100000126247 "ALTHAEA OFFICINALIS L. RADIX"
* #100000126247 ^designation[0].language = #de-AT 
* #100000126247 ^designation[0].value = "ALTHAEAE RADIX" 
* #100000126279 "RUXOLITINIB PHOSPHATE"
* #100000126279 ^designation[0].language = #de-AT 
* #100000126279 ^designation[0].value = "RUXOLITINIBPHOSPHAT" 
* #100000126282 "CARFILZOMIB"
* #100000126309 "SECUKINUMAB"
* #100000126336 "MIRABEGRON"
* #100000127068 "INDACATEROL ACETATE"
* #100000127068 ^designation[0].language = #de-AT 
* #100000127068 ^designation[0].value = "INDACATEROLACETAT" 
* #100000127801 "ROLAPITANT HYDROCHLORIDE MONOHYDRATE"
* #100000127801 ^designation[0].language = #de-AT 
* #100000127801 ^designation[0].value = "ROLAPITANT HYDROCHLORID MONOHYDRAT" 
* #100000128434 "TRASTUZUMAB EMTANSINE"
* #100000128434 ^designation[0].language = #de-AT 
* #100000128434 ^designation[0].value = "TRASTUZUMAB EMTANSIN" 
* #100000128601 "THIAMINE MONOPHOSPHATE CHLORIDE DIHYDRATE"
* #100000128601 ^designation[0].language = #de-AT 
* #100000128601 ^designation[0].value = "THIAMINMONOPHOSPHAT CHLORID DIHYDRAT" 
* #100000128757 "SELUMETINIB SULFATE"
* #100000128757 ^designation[0].language = #de-AT 
* #100000128757 ^designation[0].value = "SELUMETINIBSULFAT" 
* #100000129086 "TREMELIMUMAB"
* #100000130999 "BITTER-FENNEL FRUIT OIL"
* #100000130999 ^designation[0].language = #de-AT 
* #100000130999 ^designation[0].value = "FOENICULI AMARI FRUCTUS AETHEROLEUM" 
* #100000134843 "OLARATUMAB"
* #100000138072 "TRIENTINE TETRAHYDROCHLORIDE"
* #100000138072 ^designation[0].language = #de-AT 
* #100000138072 ^designation[0].value = "TRIENTIN TETRAHYDROCHLORID" 
* #100000139237 "MOGAMULIZUMAB"
* #100000140976 "LETERMOVIR"
* #100000141528 "OBETICHOLIC ACID"
* #100000141528 ^designation[0].language = #de-AT 
* #100000141528 ^designation[0].value = "OBETICHOLSÄURE" 
* #100000141557 "LESINURAD"
* #100000141566 "CARVI AETHEROLEUM"
* #100000141661 "GLYCEROL PHENYLBUTYRATE"
* #100000141661 ^designation[0].language = #de-AT 
* #100000141661 ^designation[0].value = "GLYCEROLPHENYLBUTYRAT" 
* #100000141689 "RESLIZUMAB"
* #100000143430 "ATROPINE SULFATE MONOHYDRATE"
* #100000143430 ^designation[0].language = #de-AT 
* #100000143430 ^designation[0].value = "ATROPINSULFAT MONOHYDRAT" 
* #100000143995 "AVATROMBOPAG MALEATE"
* #100000143995 ^designation[0].language = #de-AT 
* #100000143995 ^designation[0].value = "AVATROMBOPAG MALEAT" 
* #100000144872 "ELOTUZUMAB"
* #100000144876 "OCRELIZUMAB"
* #100000145084 "CARIPRAZINE HYDROCHLORIDE"
* #100000145084 ^designation[0].language = #de-AT 
* #100000145084 ^designation[0].value = "CARIPRAZINHYDROCHLORID" 
* #100000151757 "SULTAMICILLIN TOSYLATE DIHYDRATE"
* #100000151757 ^designation[0].language = #de-AT 
* #100000151757 ^designation[0].value = "SULTAMICILLINTOSILAT-DIHYDRAT" 
* #100000153126 "MELPHALAN HYDROCHLORIDE"
* #100000153126 ^designation[0].language = #de-AT 
* #100000153126 ^designation[0].value = "MELPHALANHYDROCHLORID" 
* #100000153736 "BEZLOTOXUMAB"
* #100000155087 "ANIFROLUMAB"
* #100000155674 "PASIREOTIDE PAMOATE"
* #100000155674 ^designation[0].language = #de-AT 
* #100000155674 ^designation[0].value = "PASIREOTIDEMBONAT" 
* #100000156419 "METRELEPTIN"
* #100000156428 "TILDRAKIZUMAB"
* #100000156566 "ORITAVANCIN DIPHOSPHATE"
* #100000156566 ^designation[0].language = #de-AT 
* #100000156566 ^designation[0].value = "ORITAVANCIN DIPHOSPHAT" 
* #100000156702 "SELEXIPAG"
* #100000156709 "OPICAPONE"
* #100000156709 ^designation[0].language = #de-AT 
* #100000156709 ^designation[0].value = "OPICAPON" 
* #100000158307 "SUCROFERRIC OXYHYDROXIDE"
* #100000158307 ^designation[0].language = #de-AT 
* #100000158307 ^designation[0].value = "EISEN(III)OXIDHYDROXID-SACCHAROSE-STÄRKE-KOMPLEX" 
* #100000158327 "COBIMETINIB HEMIFUMARATE"
* #100000158327 ^designation[0].language = #de-AT 
* #100000158327 ^designation[0].value = "COBIMETINIB HEMIFUMARAT" 
* #100000159950 "SACUBITRIL"
* #100000160392 "ABEMACICLIB"
* #100000160902 "ELBASVIR"
* #100000161317 "ELUXADOLINE"
* #100000161317 ^designation[0].language = #de-AT 
* #100000161317 ^designation[0].value = "ELUXADOLIN" 
* #100000162382 "DARATUMUMAB"
* #100000162440 "VENETOCLAX"
* #100000162511 "DURVALUMAB"
* #100000162570 "IXEKIZUMAB"
* #100000162813 "OSIMERTINIB MESYLATE"
* #100000162813 ^designation[0].language = #de-AT 
* #100000162813 ^designation[0].value = "OSIMERTINIB MESYLAT" 
* #100000162855 "ALECTINIB HYDROCHLORIDE"
* #100000162855 ^designation[0].language = #de-AT 
* #100000162855 ^designation[0].value = "ALECTINIBHYDROCHLORID" 
* #100000163085 "ROXADUSTAT"
* #100000163087 "LEBRIKIZUMAB"
* #100000163092 "ENCORAFENIB"
* #100000163237 "IXAZOMIB CITRATE"
* #100000163237 ^designation[0].language = #de-AT 
* #100000163237 ^designation[0].value = "IXAZOMIB CITRAT" 
* #100000163363 "TENOFOVIR DISOPROXIL MALEATE"
* #100000163363 ^designation[0].language = #de-AT 
* #100000163363 ^designation[0].value = "TENOFOVIR DISOPROXIL MALEAT" 
* #100000163521 "POLATUZUMAB VEDOTIN"
* #100000163524 "ENTRECTINIB"
* #100000163528 "DORAVIRINE"
* #100000163528 ^designation[0].language = #de-AT 
* #100000163528 ^designation[0].value = "DORAVIRIN" 
* #100000163576 "SELINEXOR"
* #100000163589 "SARILUMAB"
* #100000163921 "ATEZOLIZUMAB"
* #100000164244 "FOLLITROPIN DELTA"
* #100000164624 "FUSIDIC ACID HEMIHYDRATE"
* #100000164624 ^designation[0].language = #de-AT 
* #100000164624 ^designation[0].value = "FUSIDINSÄURE HEMIHYDRAT" 
* #100000164733 "OMAVELOXOLONE"
* #100000164733 ^designation[0].language = #de-AT 
* #100000164733 ^designation[0].value = "OMAVELOXOLON" 
* #100000164814 "EDOTREOTIDE"
* #100000164814 ^designation[0].language = #de-AT 
* #100000164814 ^designation[0].value = "EDOTREOTID" 
* #100000165751 "CABOTEGRAVIR"
* #100000165774 "TEZEPELUMAB"
* #100000165895 "GUSELKUMAB"
* #100000165993 "BINIMETINIB"
* #100000166051 "AVIBACTAM SODIUM"
* #100000166051 ^designation[0].language = #de-AT 
* #100000166051 ^designation[0].value = "AVIBACTAM NATRIUM" 
* #100000166075 "AVELUMAB"
* #100000166133 "LUTETIUM (177LU) OXODOTREOTIDE"
* #100000166133 ^designation[0].language = #de-AT 
* #100000166133 ^designation[0].value = "LUTETIUM [*177*LU] OXODOTREOTID" 
* #100000166177 "VELPATASVIR"
* #100000166286 "GRAZOPREVIR MONOHYDRATE"
* #100000166286 ^designation[0].language = #de-AT 
* #100000166286 ^designation[0].value = "GRAZOPREVIR MONOHYDRAT" 
* #100000166553 "ALPELISIB"
* #100000166567 "BROLUCIZUMAB"
* #100000166583 "SPHEROIDS OF HUMAN AUTOLOGOUS MATRIX-ASSOCIATED CHONDROCYTES"
* #100000166583 ^designation[0].language = #de-AT 
* #100000166583 ^designation[0].value = "SPHÄROIDE AUS HUMANEN AUTOLOGEN MATRIX-ASSOZIERTEN CHONDROZYTEN" 
* #100000166695 "GLECAPREVIR"
* #100000166696 "PIBRENTASVIR"
* #100000166699 "VELMANASE ALFA"
* #100000166815 "ANDEXANET ALFA"
* #100000166854 "CAPLACIZUMAB"
* #100000166974 "LORLATINIB"
* #100000167177 "13C-METHACETIN"
* #100000167177 ^designation[0].language = #de-AT 
* #100000167177 ^designation[0].value = "METHACETIN-C13" 
* #100000167413 "PATIROMER SORBITEX CALCIUM"
* #100000167413 ^designation[0].language = #de-AT 
* #100000167413 ^designation[0].value = "PATIROMER SORBITEX KALZIUM" 
* #100000167493 "BENRALIZUMAB"
* #100000168524 "CERLIPONASE ALFA"
* #100000168547 "ACALABRUTINIB"
* #100000168683 "VORETIGENE NEPARVOVEC"
* #100000168683 ^designation[0].language = #de-AT 
* #100000168683 ^designation[0].value = "VORETIGEN NEPARVOVEC" 
* #100000168847 "IBALIZUMAB"
* #100000169012 "PONESIMOD"
* #100000169020 "RISANKIZUMAB"
* #100000169021 "BIMEKIZUMAB"
* #100000169091 "EMICIZUMAB"
* #100000169469 "BEMPEDOIC ACID"
* #100000169469 ^designation[0].language = #de-AT 
* #100000169469 ^designation[0].value = "BEMPEDOSÄURE" 
* #100000169821 "ERENUMAB"
* #100000169982 "GALCANEZUMAB"
* #100000169992 "FINERENONE"
* #100000169992 ^designation[0].language = #de-AT 
* #100000169992 ^designation[0].value = "FINERENON" 
* #100000170042 "TALAZOPARIB TOSYLATE"
* #100000170042 ^designation[0].language = #de-AT 
* #100000170042 ^designation[0].value = "TALAZOPARIBTOSILAT" 
* #100000170090 "ERTUGLIFLOZIN L-PYROGLUTAMIC ACID"
* #100000170090 ^designation[0].language = #de-AT 
* #100000170090 ^designation[0].value = "ERTUGLIFLOZIN L-PYROGLUTAMAT" 
* #100000170106 "NIRAPARIB TOSYLATE MONOHYDRATE"
* #100000170106 ^designation[0].language = #de-AT 
* #100000170106 ^designation[0].value = "NIRAPARIB TOSYLAT MONOHYDRAT" 
* #100000170331 "RIBOCICLIB SUCCINATE"
* #100000170331 ^designation[0].language = #de-AT 
* #100000170331 ^designation[0].value = "RIBOCICLIB SUCCINAT" 
* #100000170431 "DARUNAVIR PROPYLENE GLYCOLATE"
* #100000170431 ^designation[0].language = #de-AT 
* #100000170431 ^designation[0].value = "DARUNAVIR PROPYLENGLYCOL" 
* #100000170508 "ELACESTRANT DIHYDROCHLORIDE"
* #100000170508 ^designation[0].language = #de-AT 
* #100000170508 ^designation[0].value = "ELACESTRANTDIHYDROCHLORID" 
* #100000170772 "BRIGATINIB"
* #100000170826 "BUROSUMAB"
* #100000171715 "DAROLUTAMIDE"
* #100000171715 ^designation[0].language = #de-AT 
* #100000171715 ^designation[0].value = "DAROLUTAMID" 
* #100000171724 "PRASUGREL HYDROBROMIDE"
* #100000171724 ^designation[0].language = #de-AT 
* #100000171724 ^designation[0].value = "PRASUGREL HYDROBROMID" 
* #100000171829 "ENFORTUMAB VEDOTIN"
* #100000171856 "VABORBACTAM"
* #100000171994 "PRASUGREL BESILATE"
* #100000171994 ^designation[0].language = #de-AT 
* #100000171994 ^designation[0].value = "PRASUGREL BESILAT" 
* #100000172035 "AGOMELATINE CITRIC ACID"
* #100000172035 ^designation[0].language = #de-AT 
* #100000172035 ^designation[0].value = "AGOMELATIN-CITRONENSÄURE" 
* #100000172265 "SOMAPACITAN"
* #100000172811 "AVACOPAN"
* #100000172849 "FREMANEZUMAB"
* #100000172983 "OLIPUDASE ALFA"
* #100000173468 "ISATUXIMAB"
* #100000173491 "VOLANESORSEN SODIUM"
* #100000173491 ^designation[0].language = #de-AT 
* #100000173491 ^designation[0].value = "VOLANESORSEN NATRIUM" 
* #100000173495 "ROZANOLIXIZUMAB"
* #100000174345 "BICTEGRAVIR SODIUM"
* #100000174345 ^designation[0].language = #de-AT 
* #100000174345 ^designation[0].value = "BICTEGRAVIR NATRIUM" 
* #100000174401 "TEZACAFTOR"
* #100000174462 "TRASTUZUMAB DERUXTECAN"
* #100000174555 "EPTINEZUMAB"
* #100000174563 "PEGUNIGALSIDASE ALFA"
* #100000174576 "SITAGLIPTIN HYDROCHLORIDE MONOHYDRATE"
* #100000174576 ^designation[0].language = #de-AT 
* #100000174576 ^designation[0].value = "SITAGLIPTIN HYDROCHLORID MONOHYDRAT" 
* #100000174626 "DIROXIMEL FUMARATE"
* #100000174626 ^designation[0].language = #de-AT 
* #100000174626 ^designation[0].value = "DIROXIMELFUMARAT" 
* #100000174637 "CRIZANLIZUMAB"
* #100000174660 "VAMOROLONE"
* #100000174660 ^designation[0].language = #de-AT 
* #100000174660 ^designation[0].value = "VAMOROLON" 
* #100000174673 "SOMATROGON"
* #100000174876 "LANADELUMAB"
* #100000174891 "LASMIDITAN SUCCINATE"
* #100000174891 ^designation[0].language = #de-AT 
* #100000174891 ^designation[0].value = "LASMIDITAN HEMISUCCINAT" 
* #100000174917 "CENOBAMATE"
* #100000174917 ^designation[0].language = #de-AT 
* #100000174917 ^designation[0].value = "CENOBAMAT" 
* #100000174943 "INEBILIZUMAB"
* #100000174995 "RELUGOLIX"
* #100000175066 "IVOSIDENIB"
* #100000175148 "PEGVALIASE"
* #100000175189 "LUSPATERCEPT"
* #100000175190 "VERICIGUAT"
* #100000175258 "CEMIPLIMAB"
* #100000175262 "FOSTEMSAVIR TROMETAMOL"
* #100000175358 "TRALOKINUMAB"
* #100000175477 "PATISIRAN SODIUM"
* #100000175477 ^designation[0].language = #de-AT 
* #100000175477 ^designation[0].value = "PATISIRAN NATRIUM" 
* #100000175810 "INOTERSEN SODIUM"
* #100000175810 ^designation[0].language = #de-AT 
* #100000175810 ^designation[0].value = "INOTERSEN NATRIUM" 
* #100000175812 "MACIMORELIN ACETATE"
* #100000175812 ^designation[0].language = #de-AT 
* #100000175812 ^designation[0].value = "MACIMORELINACETAT" 
* #100000175900 "UPADACITINIB HEMIHYDRATE"
* #100000175900 ^designation[0].language = #de-AT 
* #100000175900 ^designation[0].value = "UPADACITINIB HEMIHYDRAT" 
* #100000176019 "VOXELOTOR"
* #100000176055 "BALOXAVIR MARBOXIL"
* #100000176271 "RELATLIMAB"
* #100000176290 "DACOMITINIB MONOHYDRATE"
* #100000176290 ^designation[0].language = #de-AT 
* #100000176290 ^designation[0].value = "DACOMITINIB MONOHYDRAT" 
* #100000176639 "DELAFLOXACIN MEGLUMINE"
* #100000176639 ^designation[0].language = #de-AT 
* #100000176639 ^designation[0].value = "DELAFLOXACIN MEGLUMIN" 
* #100000176729 "LENALIDOMIDE AMMONIUM CHLORIDE"
* #100000176729 ^designation[0].language = #de-AT 
* #100000176729 ^designation[0].value = "LENALIDOMID AMMONIUMCHLORID" 
* #100000177195 "GLASDEGIB MALEATE"
* #100000177195 ^designation[0].language = #de-AT 
* #100000177195 ^designation[0].value = "GLASDEGIBMALEAT" 
* #100000177389 "RIPRETINIB"
* #100000177402 "RAVULIZUMAB"
* #100000177408 "AVAPRITINIB"
* #100000177430 "AVALGLUCOSIDASE ALFA"
* #100000177552 "LAROTRECTINIB SULFATE"
* #100000177552 ^designation[0].language = #de-AT 
* #100000177552 ^designation[0].value = "LAROTRECTINIBSULFAT" 
* #100000177603 "SELPERCATINIB"
* #100000177630 "AMIVANTAMAB"
* #100000177733 "MOSUNETUZUMAB"
* #100000177945 "PEGZILARGINASE"
* #100000178015 "QUIZARTINIB DIHYDROCHLORIDE"
* #100000178015 ^designation[0].language = #de-AT 
* #100000178015 ^designation[0].value = "QUIZARTINIB DIHYDROCHLORID" 
* #100000178153 "RISDIPLAM"
* #100000178158 "OSILODROSTAT PHOSPHATE"
* #100000178158 ^designation[0].language = #de-AT 
* #100000178158 ^designation[0].value = "OSILODROSTATPHOSPHAT" 
* #100000178369 "FARICIMAB"
* #100000178398 "TAGRAXOFUSP"
* #100000179866 "PALOPEGTERIPARATIDE"
* #100000179866 ^designation[0].language = #de-AT 
* #100000179866 ^designation[0].value = "PALOPEGTERIPARATID" 
* #100000180060 "SPESOLIMAB"
* #100000180154 "SOLRIAMFETOL HYDROCHLORIDE"
* #100000180154 ^designation[0].language = #de-AT 
* #100000180154 ^designation[0].value = "SOLRIAMFETOL HYDROCHLORID" 
* #100000180323 "SUTIMLIMAB"
* #100000181098 "CEDAZURIDINE"
* #100000181098 ^designation[0].language = #de-AT 
* #100000181098 ^designation[0].value = "CEDAZURIDIN" 
* #100000181113 "GADOPICLENOL"
* #100000181126 "PEMIGATINIB"
* #100000181406 "SATRALIZUMAB"
* #100000181509 "ZANUBRUTINIB"
* #100000181569 "DIFELIKEFALIN ACETATE"
* #100000181569 ^designation[0].language = #de-AT 
* #100000181569 ^designation[0].value = "DIFELIKEFALINACETAT" 
* #100000181573 "DOSTARLIMAB"
* #100000181691 "VOSORITIDE"
* #100000181691 ^designation[0].language = #de-AT 
* #100000181691 ^designation[0].value = "VOSORITID" 
* #100000181695 "BELANTAMAB MAFODOTIN"
* #100000181711 "PEGCETACOPLAN"
* #100000181759 "TEBENTAFUSP"
* #100000181778 "LONCASTUXIMAB TESIRINE"
* #100000181778 ^designation[0].language = #de-AT 
* #100000181778 ^designation[0].value = "LONCASTUXIMAB TESIRIN" 
* #100000181872 "MARALIXIBAT CHLORIDE"
* #100000181872 ^designation[0].language = #de-AT 
* #100000181872 ^designation[0].value = "MARALIXIBATCHLORID" 
* #100000181875 "GILTERITINIB FUMARATE"
* #100000181875 ^designation[0].language = #de-AT 
* #100000181875 ^designation[0].value = "GILTERITINIB FUMARAT" 
* #100000182481 "PRALSETINIB"
* #100000182766 "OZANIMOD HYDROCHLORIDE"
* #100000182766 ^designation[0].language = #de-AT 
* #100000182766 ^designation[0].value = "OZANIMODHYDROCHLORID" 
* #100000182936 "FILGOTINIB MALEATE"
* #100000182936 ^designation[0].language = #de-AT 
* #100000182936 ^designation[0].value = "FILGOTINIBMALEAT" 
* #100000182956 "GLOFITAMAB"
* #100000183068 "SOTORASIB"
* #100000183311 "TAFASITAMAB"
* #100000183531 "VUTRISIRAN SODIUM"
* #100000183531 ^designation[0].language = #de-AT 
* #100000183531 ^designation[0].value = "VUTRISIRAN NATRIUM" 
* #100000183583 "TIRZEPATIDE"
* #100000183583 ^designation[0].language = #de-AT 
* #100000183583 ^designation[0].value = "TIRZEPATID" 
* #100000183609 "TIRBANIBULIN"
* #100000183873 "CABOTEGRAVIR SODIUM"
* #100000183873 ^designation[0].language = #de-AT 
* #100000183873 ^designation[0].value = "CABOTEGRAVIR NATRIUM" 
* #100000183968 "GIVOSIRAN SODIUM"
* #100000183968 ^designation[0].language = #de-AT 
* #100000183968 ^designation[0].value = "GIVOSIRAN NATRIUM" 
* #100000183984 "EFGARTIGIMOD ALFA"
* #100000184153 "MOMELOTINIB DIHYDROCHLORIDE MONOHYDRATE"
* #100000184153 ^designation[0].language = #de-AT 
* #100000184153 ^designation[0].value = "MOMELOTINIB DIHYDROCHLORID MONOHYDRAT" 
* #100000184244 "CIPAGLUCOSIDASE ALFA"
* #300000002681 "MAVACAMTEN"
* #300000005362 "BEROTRALSTAT DIHYDROCHLORIDE"
* #300000005362 ^designation[0].language = #de-AT 
* #300000005362 ^designation[0].value = "BEROTRALSTATDIHYDROCHLORID" 
* #300000005901 "TECLISTAMAB"
* #300000005928 "FOSLEVODOPA"
* #300000007901 "LENACAPAVIR SODIUM"
* #300000007901 ^designation[0].language = #de-AT 
* #300000007901 ^designation[0].value = "LENACAPAVIR-NATRIUM" 
* #300000010789 "EPCORITAMAB"
* #300000010796 "TALQUETAMAB"
* #300000011169 "ASCIMINIB HYDROCHLORIDE"
* #300000011169 ^designation[0].language = #de-AT 
* #300000011169 ^designation[0].value = "ASCIMINIBHYDROCHLORID" 
* #300000011763 "ELRANATAMAB"
* #300000011961 "ATOGEPANT MONOHYDRATE"
* #300000011961 ^designation[0].language = #de-AT 
* #300000011961 ^designation[0].value = "ATOGEPANT MONOHYDRAT" 
* #300000012022 "CAPMATINIB DIHYDROCHLORIDE MONOHYDRATE"
* #300000012022 ^designation[0].language = #de-AT 
* #300000012022 ^designation[0].value = "CAPMATINIB DIHYDROCHLORID MONOHYDRAT" 
* #300000013700 "RESPIRATORY SYNCYTIAL VIRUS, SUBGROUP A, STABILIZED PREFUSION F PROTEIN 847A"
* #300000013700 ^designation[0].language = #de-AT 
* #300000013700 ^designation[0].value = "RESPIRATORISCHES SYNZYTIALVIRUS, SUBTYP A, STABILISIERTES PRÄFUSION-F-PROTEIN 847A" 
* #300000013701 "RESPIRATORY SYNCYTIAL VIRUS, SUBGROUP B, STABILIZED PREFUSION F PROTEIN 847B"
* #300000013701 ^designation[0].language = #de-AT 
* #300000013701 ^designation[0].value = "RESPIRATORISCHES SYNZYTIALVIRUS, SUBTYP B, STABILISIERTES PRÄFUSION-F-PROTEIN 847B" 
* #300000015561 "ELASOMERAN"
* #300000019018 "TOZINAMERAN"
* #300000021803 "DEUCRAVACITINIB"
* #300000023100 "REGDANVIMAB"
* #300000023191 "PIRTOBRUTINIB"
* #300000023334 "RIMEGEPANT SULFATE TRIHYDRATE"
* #300000023334 ^designation[0].language = #de-AT 
* #300000023334 ^designation[0].value = "RIMEGEPANT HEMISULFAT SESQUIHYDRAT" 
* #300000024416 "DARIDOREXANT HYDROCHLORIDE"
* #300000024416 ^designation[0].language = #de-AT 
* #300000024416 ^designation[0].value = "DARIDOREXANT HYDROCHLORID" 
* #300000024551 "ODEVIXIBAT SESQUIHYDRATE"
* #300000024551 ^designation[0].language = #de-AT 
* #300000024551 ^designation[0].value = "ODEVIXIBAT SESQUIHYDRAT" 
* #300000025307 "MIRIKIZUMAB"
* #300000031355 "(1R,2S,5S)-N-{(1S)-1-CYANO-2-[(3S)-2-OXOPYRROLIDIN-3-YL]ETHYL}-6,6-DIMETHYL-3- [3-METHYL-N-(TRIFLUOROACETYL)-L-VALYL]-3-AZABICYCLO[3.1.0]HEXANE-2-CARBOXAMIDE"
* #300000031355 ^designation[0].language = #de-AT 
* #300000031355 ^designation[0].value = "NIRMATRELVIR" 
* #300000032604 "CABAZITAXEL 2-PROPANOL SOLVATE"
* #300000032604 ^designation[0].language = #de-AT 
* #300000032604 ^designation[0].value = "CABAZITAXEL 2-PROPANOL" 
* #300000042045 "RILTOZINAMERAN"
* #300000043661 "IMELASOMERAN"
* #300000044580 "ETRASIMOD ARGININE"
* #300000044580 ^designation[0].language = #de-AT 
* #300000044580 ^designation[0].value = "ETRASIMOD ARGININ" 
* #300000046469 "ZILUCOPLAN SODIUM"
* #300000046469 ^designation[0].language = #de-AT 
* #300000046469 ^designation[0].value = "ZILUCOPLAN NATRIUM" 
* #300000046472 "DAVESOMERAN"
* #300000051178 "ANDUSOMERAN"
* #300000051270 "RAXTOZINAMERAN"
* #900000000058 "MIXTURE OF ALFA-PINEN AND BETA-PINEN"
* #900000000058 ^designation[0].language = #de-AT 
* #900000000058 ^designation[0].value = "2-PINEN + 2(10)-PINEN" 
* #900000000083 "5-CHLORCARVACROL"
* #900000000092 "ABIRATERONACETAT"
* #900000000094 "ABSINTHII HERBA (EXTRACT)"
* #900000000094 ^designation[0].language = #de-AT 
* #900000000094 ^designation[0].value = "ABSINTHII HERBA (AUSZUG)" 
* #900000000113 "AEGLE FRUCTUS"
* #900000000126 "ALLII SATIVI BULBUS (EXTRACT)"
* #900000000126 ^designation[0].language = #de-AT 
* #900000000126 ^designation[0].value = "ALLII SATIVI BULBUS (AUSZUG)" 
* #900000000128 "ALTHAEAE RADIX (EXTRACT)"
* #900000000128 ^designation[0].language = #de-AT 
* #900000000128 ^designation[0].value = "ALTHAEAE RADIX (AUSZUG)" 
* #900000000154 "ANGELICAE RADIX (EXTRACT)"
* #900000000154 ^designation[0].language = #de-AT 
* #900000000154 ^designation[0].value = "ANGELICAE RADIX (AUSZUG)" 
* #900000000156 "ANISI FRUCTUS (EXTRACT)"
* #900000000156 ^designation[0].language = #de-AT 
* #900000000156 ^designation[0].value = "ANISI FRUCTUS (AUSZUG)" 
* #900000000158 "ANISI STELLATI FRUCTUS (EXTRACT)"
* #900000000158 ^designation[0].language = #de-AT 
* #900000000158 ^designation[0].value = "ANISI STELLATI FRUCTUS (AUSZUG)" 
* #900000000171 "AQUILEGIAE HERBA"
* #900000000201 "AURANTII AMARI EPICARPIUM ET MESOCARPIUM (EXTRACT)"
* #900000000201 ^designation[0].language = #de-AT 
* #900000000201 ^designation[0].value = "AURANTII AMARI EPICARPIUM ET MESOCARPIUM (AUSZUG)" 
* #900000000205 "AURANTII AMARI FLOS (EXTRACT)"
* #900000000205 ^designation[0].language = #de-AT 
* #900000000205 ^designation[0].value = "AURANTII AMARI FLOS (AUSZUG)" 
* #900000000207 "AURANTII DULCIS AETHEROLEUM"
* #900000000212 "AURANTII FOLIUM (EXTRACT)"
* #900000000212 ^designation[0].language = #de-AT 
* #900000000212 ^designation[0].value = "AURANTII FOLIUM (AUSZUG)" 
* #900000000213 "AURANTII FRUCTUS IMMATURUS (EXTRACT)"
* #900000000213 ^designation[0].language = #de-AT 
* #900000000213 ^designation[0].value = "AURANTII FRUCTUS IMMATURUS (AUSZUG)" 
* #900000000214 "AURANTII SINENSIS PERICARPIUM (EXTRACT)"
* #900000000214 ^designation[0].language = #de-AT 
* #900000000214 ^designation[0].value = "AURANTII SINENSIS PERICARPIUM (AUSZUG)" 
* #900000000247 "BILASTIN"
* #900000000266 "CALAMI RHIZOMA (EXTRACT)"
* #900000000266 ^designation[0].language = #de-AT 
* #900000000266 ^designation[0].value = "CALAMI RHIZOMA (AUSZUG)" 
* #900000000277 "CALENDULAE FLOS CUM CALYCE"
* #900000000278 "CALLUNAE HERBA"
* #900000000283 "CANNABIS SATIVA L.,FOLIUM CUM FLORE,CBD TYP (EXTRACT)"
* #900000000283 ^designation[0].language = #de-AT 
* #900000000283 ^designation[0].value = "CANNABIS SATIVA L.,FOLIUM CUM FLORE,CBD TYP (AUSZUG)" 
* #900000000284 "CANNABIS SATIVA L.,FOLIUM CUM FLORE,THC TYP (EXTRACT)"
* #900000000284 ^designation[0].language = #de-AT 
* #900000000284 ^designation[0].value = "CANNABIS SATIVA L.,FOLIUM CUM FLORE,THC TYP (AUSZUG)" 
* #900000000290 "CARDAMOMI FRUCTUS"
* #900000000291 "CARDAMOMI FRUCTUS (EXTRACT)"
* #900000000291 ^designation[0].language = #de-AT 
* #900000000291 ^designation[0].value = "CARDAMOMI FRUCTUS (AUSZUG)" 
* #900000000294 "CARLINAE RADIX (EXTRACT)"
* #900000000294 ^designation[0].language = #de-AT 
* #900000000294 ^designation[0].value = "CARLINAE RADIX (AUSZUG)" 
* #900000000295 "CAROVERINE HYDROCHLORIDE MONOHYDRATE"
* #900000000295 ^designation[0].language = #de-AT 
* #900000000295 ^designation[0].value = "CAROVERINHYDROCHLORID MONOHYDRAT" 
* #900000000297 "CARVI FRUCTUS (EXTRACT)"
* #900000000297 ^designation[0].language = #de-AT 
* #900000000297 ^designation[0].value = "CARVI FRUCTUS (AUSZUG)" 
* #900000000299 "CARYOPHYLLI FLOS (EXTRACT)"
* #900000000299 ^designation[0].language = #de-AT 
* #900000000299 ^designation[0].value = "CARYOPHYLLI FLOS (AUSZUG)" 
* #900000000300 "CASTANEAE FOLIUM"
* #900000000307 "CEFEPRIME DIHYDROCHLORIDE"
* #900000000307 ^designation[0].language = #de-AT 
* #900000000307 ^designation[0].value = "CEFEPIMDIHYDROCHLORID" 
* #900000000312 "CEFTOBIPROL MEDOCARIL SODIUM"
* #900000000312 ^designation[0].language = #de-AT 
* #900000000312 ^designation[0].value = "CEFTOBIPROL MEDOCARIL NATRIUM" 
* #900000000330 "CHAMOMILLAE ROMANAE FLOS (EXTRACT)"
* #900000000330 ^designation[0].language = #de-AT 
* #900000000330 ^designation[0].value = "CHAMOMILLAE ROMANAE FLOS (AUSZUG)" 
* #900000000342 "CINNAMOMI CASSIAE FLOS (EXTRACT)"
* #900000000342 ^designation[0].language = #de-AT 
* #900000000342 ^designation[0].value = "CINNAMOMI CASSIAE FLOS (AUSZUG)" 
* #900000000343 "CINNAMOMI CORTEX (EXTRACT)"
* #900000000343 ^designation[0].language = #de-AT 
* #900000000343 ^designation[0].value = "CINNAMOMI CORTEX (AUSZUG)" 
* #900000000350 "CLARITHROMYCIN CITRATE"
* #900000000350 ^designation[0].language = #de-AT 
* #900000000350 ^designation[0].value = "CLARITHROMYCINCITRAT" 
* #900000000355 "CHLOSTRIDIUM BOTULINUM"
* #900000000355 ^designation[0].language = #de-AT 
* #900000000355 ^designation[0].value = "CLOSTRIDIUM BOTULINUM (AUSZUG, PRODUKTE)" 
* #900000000362 "CNICI BENEDICTI HERBA (EXTRACT)"
* #900000000362 ^designation[0].language = #de-AT 
* #900000000362 ^designation[0].value = "CNICI BENEDICTI HERBA (AUSZUG)" 
* #900000000377 "CORIANDRI FRUCTUS (EXTRACT)"
* #900000000377 ^designation[0].language = #de-AT 
* #900000000377 ^designation[0].value = "CORIANDRI FRUCTUS (AUSZUG)" 
* #900000000378 "CORONAVIRUS"
* #900000000381 "CRATAEGI FOLIUM (EXTRACT)"
* #900000000381 ^designation[0].language = #de-AT 
* #900000000381 ^designation[0].value = "CRATAEGI FOLIUM (AUSZUG)" 
* #900000000382 "CRATAEGI FRUCTUS (EXTRACT)"
* #900000000382 ^designation[0].language = #de-AT 
* #900000000382 ^designation[0].value = "CRATAEGI FRUCTUS (AUSZUG)" 
* #900000000384 "CRATAEGI OXYACANTHAE FLOS (EXTRACT)"
* #900000000384 ^designation[0].language = #de-AT 
* #900000000384 ^designation[0].value = "CRATAEGI OXYACANTHAE FLOS (AUSZUG)" 
* #900000000390 "CUBEBAE FRUCTUS (EXTRACT)"
* #900000000390 ^designation[0].language = #de-AT 
* #900000000390 ^designation[0].value = "CUBEBAE FRUCTUS (AUSZUG)" 
* #900000000391 "CUCURBITAE SEMEN (EXTRACT)"
* #900000000391 ^designation[0].language = #de-AT 
* #900000000391 ^designation[0].value = "CUCURBITAE SEMEN (AUSZUG)" 
* #900000000396 "CYCLIZIN DIHYDROCHLORID"
* #900000000401 "CYNARAE FOLIUM"
* #900000000409 "DICLOFENAC DEANOL"
* #900000000409 ^designation[0].language = #de-AT 
* #900000000409 ^designation[0].value = "DEANOL DICLOFENACAT" 
* #900000000415 "DEPROTEINIZED HEMODERIVATIVE OF CALF BLOOD"
* #900000000415 ^designation[0].language = #de-AT 
* #900000000415 ^designation[0].value = "DEPROTEINISIERTES HAEMODERIVAT AUS KÄLBERBLUT" 
* #900000000432 "DROSERA (HOM)"
* #900000000432 ^designation[0].language = #de-AT 
* #900000000432 ^designation[0].value = "DROSERAE HERBA ET RADIX (AUSZUG)" 
* #900000000436 "ECHINACEAE PALLIDAE RADIX (EXTRACT)"
* #900000000436 ^designation[0].language = #de-AT 
* #900000000436 ^designation[0].value = "ECHINACEAE PALLIDAE RADIX (AUSZUG)" 
* #900000000437 "ECHINACEAE PURPUREAE RADIX (EXTRACT)"
* #900000000437 ^designation[0].language = #de-AT 
* #900000000437 ^designation[0].value = "ECHINACEAE PURPUREAE RADIX (AUSZUG)" 
* #900000000445 "FERRIC CARBOXYMALTOSE"
* #900000000445 ^designation[0].language = #de-AT 
* #900000000445 ^designation[0].value = "EISENCARBOXYMALTOSE" 
* #900000000456 "ESCHERICHIA COLI (EXTRACT)"
* #900000000456 ^designation[0].language = #de-AT 
* #900000000456 ^designation[0].value = "ESCHERICHIA COLI (AUSZUG, PRODUKTE)" 
* #900000000460 "ETHINYLESTRADIOL BETADEX"
* #900000000460 ^designation[0].language = #de-AT 
* #900000000460 ^designation[0].value = "ETHINYLESTRADIOL X BETADEX" 
* #900000000469 "ETHYL ESTER OF IODIZED FATTY ACIDS OF POPPY SEED OIL"
* #900000000469 ^designation[0].language = #de-AT 
* #900000000469 ^designation[0].value = "ETHYLESTER JODIERTER FETTSÄUREN DES MOHNÖLS" 
* #900000000476 "FAEX MEDICINALIS (EXTRACT)"
* #900000000476 ^designation[0].language = #de-AT 
* #900000000476 ^designation[0].value = "HEFE (AUSZUG)" 
* #900000000487 "FESOTERODIN FUMARATE"
* #900000000487 ^designation[0].language = #de-AT 
* #900000000487 ^designation[0].value = "FESOTERODINFUMARAT" 
* #900000000504 "FOMEPIZOLE SULFATE"
* #900000000504 ^designation[0].language = #de-AT 
* #900000000504 ^designation[0].value = "FOMEPIZOL SULFAT" 
* #900000000509 "GADOLINIUM OXIDE"
* #900000000509 ^designation[0].language = #de-AT 
* #900000000509 ^designation[0].value = "GADOLINIUMOXID" 
* #900000000510 "GALANGAE RHIZOMA (EXTRACT)"
* #900000000510 ^designation[0].language = #de-AT 
* #900000000510 ^designation[0].value = "GALANGAE RHIZOMA (AUSZUG)" 
* #900000000532 "GENTIANAE RADIX (EXTRACT)"
* #900000000532 ^designation[0].language = #de-AT 
* #900000000532 ^designation[0].value = "GENTIANAE RADIX (AUSZUG)" 
* #900000000542 "GLYCYL-L-TYROSINE"
* #900000000542 ^designation[0].language = #de-AT 
* #900000000542 ^designation[0].value = "GLYCYLTYROSIN" 
* #900000000549 "GUAIACI LIGNUM (AUSZUG)"
* #900000000552 "HAEMOPHILUS SPEC."
* #900000000555 "HAMAMELIDIS FOLIUM (EXTRACT)"
* #900000000555 ^designation[0].language = #de-AT 
* #900000000555 ^designation[0].value = "HAMAMELIDIS FOLIUM (AUSZUG)" 
* #900000000565 "HERNIARIAE HERBA"
* #900000000581 "HUMIC ACID CONJUGATE WITH SALICYLIC ACID"
* #900000000581 ^designation[0].language = #de-AT 
* #900000000581 ^designation[0].value = "HUMINSÄUREVERBINDUNG MIT SALICYLSÄURE" 
* #900000000589 "HYDROXYZINE DIHYDROCHLORIDE"
* #900000000589 ^designation[0].language = #de-AT 
* #900000000589 ^designation[0].value = "HYDROXYZIN DIHYDROCHLORID" 
* #900000000591 "HYOSCYAMI FOLIUM (EXTRACT)"
* #900000000591 ^designation[0].language = #de-AT 
* #900000000591 ^designation[0].value = "HYOSCYAMI FOLIUM (AUSZUG)" 
* #900000000596 "IBERIDIS HERBA ET RADIX (EXTRACT)"
* #900000000596 ^designation[0].language = #de-AT 
* #900000000596 ^designation[0].value = "IBERIDIS HERBA ET RADIX (AUSZUG)" 
* #900000000599 "IMMUNOCYANIN"
* #900000000610 "IVAE MOSCHATAE HERBA (EXTRACT)"
* #900000000610 ^designation[0].language = #de-AT 
* #900000000610 ^designation[0].value = "IVAE MOSCHATAE HERBA (AUSZUG)" 
* #900000000614 "JUGLANDIS FOLIUM"
* #900000000617 "JUNIPERI LIGNUM (EXTRACT)"
* #900000000617 ^designation[0].language = #de-AT 
* #900000000617 ^designation[0].value = "JUNIPERI LIGNUM (AUSZUG)" 
* #900000000620 "KAEMPFERIAE GALANGAE RHIZOMA"
* #900000000641 "LACTOBACILLUS CASEI"
* #900000000642 "LACTOBACILLUS GASSERI"
* #900000000644 "LACTUCAE SATIVAE FOLIUM"
* #900000000655 "LAVANDULAE FLOS (EXTRACT)"
* #900000000655 ^designation[0].language = #de-AT 
* #900000000655 ^designation[0].value = "LAVANDULAE FLOS (AUSZUG)" 
* #900000000656 "LAVANDULAE LATIFOLIAE AETHEROLEUM"
* #900000000664 "LICHEN ISLANDICUS (EXTRACT)"
* #900000000664 ^designation[0].language = #de-AT 
* #900000000664 ^designation[0].value = "LICHEN ISLANDICUS (AUSZUG)" 
* #900000000666 "LIMONIS AETHEROLEUM"
* #900000000677 "LYSINE L-GLUTAMATE"
* #900000000677 ^designation[0].language = #de-AT 
* #900000000677 ^designation[0].value = "LYSIN GLUTAMAT" 
* #900000000692 "MAGNESIUM DI(HYDROGENGLUTAMATE)"
* #900000000692 ^designation[0].language = #de-AT 
* #900000000692 ^designation[0].value = "MAGNESIUM DI(HYDROGENGLUTAMAT)" 
* #900000000708 "MARRUBII HERBA"
* #900000000709 "MARRUBII HERBA (EXTRACT)"
* #900000000709 ^designation[0].language = #de-AT 
* #900000000709 ^designation[0].value = "MARRUBII HERBA (AUSZUG)" 
* #900000000713 "MATRICARIAE FLOS (EXTRACT)"
* #900000000713 ^designation[0].language = #de-AT 
* #900000000713 ^designation[0].value = "MATRICARIAE FLOS (AUSZUG)" 
* #900000000720 "MELISSAE FOLIUM (EXTRACT)"
* #900000000720 ^designation[0].language = #de-AT 
* #900000000720 ^designation[0].value = "MELISSAE FOLIUM (AUSZUG)" 
* #900000000723 "MENTHAE PIPERITAE FOLIUM (EXTRACT)"
* #900000000723 ^designation[0].language = #de-AT 
* #900000000723 ^designation[0].value = "MENTHAE PIPERITAE FOLIUM (AUSZUG)" 
* #900000000724 "MENTHAE PIPERITAE HERBA"
* #900000000727 "MENYANTHIDIS TRIFOLIATAE FOLIUM (EXTRACT)"
* #900000000727 ^designation[0].language = #de-AT 
* #900000000727 ^designation[0].value = "MENYANTHIDIS TRIFOLIATAE FOLIUM (AUSZUG)" 
* #900000000751 "MORAXELLA (BRANHAMELLA)"
* #900000000763 "MYRISTICAE SEMEN (EXTRACT)"
* #900000000763 ^designation[0].language = #de-AT 
* #900000000763 ^designation[0].value = "MYRISTICAE SEMEN (AUSZUG)" 
* #900000000765 "MYROBALANI FRUCTUS"
* #900000000766 "MYRRHA (EXTRACT)"
* #900000000766 ^designation[0].language = #de-AT 
* #900000000766 ^designation[0].value = "MYRRHA (AUSZUG)" 
* #900000000770 "N2-L-ALANYL-L-GLUTAMIN"
* #900000000770 ^designation[0].language = #de-AT 
* #900000000770 ^designation[0].value = "N(2)-ALANYLGLUTAMIN" 
* #900000000788 "SODIUM MOLYBDATE-MO99"
* #900000000788 ^designation[0].language = #de-AT 
* #900000000788 ^designation[0].value = "NATRIUMMOLYBDAT[*99*MO] ZUR GEWINNUNG VON NATRIUMPERTECHNETAT[*99M*TC]" 
* #900000000805 "O-BETA-HYDROXYETHYL-RUTOSIDE"
* #900000000805 ^designation[0].language = #de-AT 
* #900000000805 ^designation[0].value = "O-(BETA-HYDROXYETHYL)RUTOSIDE" 
* #900000000810 "OLANZAPINE BENZOATE"
* #900000000810 ^designation[0].language = #de-AT 
* #900000000810 ^designation[0].value = "OLANZAPIN BENZOAT" 
* #900000000814 "OLODATEROL HYDROCHLORIDE"
* #900000000814 ^designation[0].language = #de-AT 
* #900000000814 ^designation[0].value = "OLODATEROL HYDROCHLORID" 
* #900000000817 "ONONIDIS RADIX (EXTRACT)"
* #900000000817 ^designation[0].language = #de-AT 
* #900000000817 ^designation[0].value = "ONONIDIS RADIX (AUSZUG)" 
* #900000000819 "ONOPORDI ACANTHII FLOS (EXTRACT)"
* #900000000819 ^designation[0].language = #de-AT 
* #900000000819 ^designation[0].value = "ONOPORDI ACANTHII FLOS (AUSZUG)" 
* #900000000821 "ORIGANI HERBA"
* #900000000851 "PELARGONII RADIX (EXTRACT)"
* #900000000851 ^designation[0].language = #de-AT 
* #900000000851 ^designation[0].value = "PELARGONII RADIX (AUSZUG)" 
* #900000000861 "UREA-PHENOL-FORMALDEHYDERESIN"
* #900000000861 ^designation[0].language = #de-AT 
* #900000000861 ^designation[0].value = "PHENOL-METHANAL-HARNSTOFF-POLYKONDENSAT, SULFONIERT, NATRIUMSALZ" 
* #900000000864 "SOYBEAN PHOSPHOLIPIDS"
* #900000000864 ^designation[0].language = #de-AT 
* #900000000864 ^designation[0].value = "PHOSPHOLIPIDE AUS SOJABOHNEN" 
* #900000000871 "PIMENTAE FRUCTUS"
* #900000000884 "POLYGALAE RADIX (EXTRACT)"
* #900000000884 ^designation[0].language = #de-AT 
* #900000000884 ^designation[0].value = "POLYGALAE RADIX (AUSZUG)" 
* #900000000902 "POTENTILLAE AUREAE HERBA"
* #900000000904 "PRIMULAE FLOS (EXTRACT)"
* #900000000904 ^designation[0].language = #de-AT 
* #900000000904 ^designation[0].value = "PRIMULAE FLOS (AUSZUG)" 
* #900000000905 "PRIMULAE RADIX (EXTRACT)"
* #900000000905 ^designation[0].language = #de-AT 
* #900000000905 ^designation[0].value = "PRIMULAE RADIX (AUSZUG)" 
* #900000000913 "PEPTIDES"
* #900000000913 ^designation[0].language = #de-AT 
* #900000000913 ^designation[0].value = "PEPTIDE" 
* #900000000922 "PULMONARY SURFACTANT PHOSPHOLIPIDS"
* #900000000922 ^designation[0].language = #de-AT 
* #900000000922 ^designation[0].value = "PULMONALE PHOSPHOLIPIDFRAKTION (SURFACTANT)" 
* #900000000937 "RHODIOLA RHIZOMA (EXTRACT)"
* #900000000937 ^designation[0].language = #de-AT 
* #900000000937 ^designation[0].value = "RHODIOLA RHIZOMA (AUSZUG)" 
* #900000000946 "ROSMARINI FOLIUM (EXTRACT)"
* #900000000946 ^designation[0].language = #de-AT 
* #900000000946 ^designation[0].value = "ROSMARINI FOLIUM (AUSZUG)" 
* #900000000949 "RUBI IDAEI FOLIUM (EXTRACT)"
* #900000000949 ^designation[0].language = #de-AT 
* #900000000949 ^designation[0].value = "RUBI IDAEI FOLIUM (AUSZUG)" 
* #900000000952 "RUMICIS ACETOSAE HERBA (EXTRACT)"
* #900000000952 ^designation[0].language = #de-AT 
* #900000000952 ^designation[0].value = "RUMICIS ACETOSAE HERBA (AUSZUG)" 
* #900000000955 "SALMONELLA SPEC. (Extracts, Products)"
* #900000000955 ^designation[0].language = #de-AT 
* #900000000955 ^designation[0].value = "SALMONELLA SPEC. (AUSZUG, PRODUKTE)" 
* #900000000958 "SAMBUCI FLOS (EXTRACT)"
* #900000000958 ^designation[0].language = #de-AT 
* #900000000958 ^designation[0].value = "SAMBUCI FLOS (AUSZUG)" 
* #900000000965 "SANTALI RUBRI LIGNUM"
* #900000000966 "SANTALI RUBRI LIGNUM (EXTRACT)"
* #900000000966 ^designation[0].language = #de-AT 
* #900000000966 ^designation[0].value = "SANTALI RUBRI LIGNUM (AUSZUG)" 
* #900000000988 "SIDAE CORDIFOLIAE HERBA"
* #900000000990 "SILYBIN SODIUM HEMISUCCINATE"
* #900000000990 ^designation[0].language = #de-AT 
* #900000000990 ^designation[0].value = "SILIBININ DINATRIUMDIHEMISUCCINAT" 
* #900000001000 "SILYBI MARIANI FRUCTUS (EXTRACT)"
* #900000001000 ^designation[0].language = #de-AT 
* #900000001000 ^designation[0].value = "SILYBI MARIANI FRUCTUS (AUSZUG)" 
* #900000001010 "SOLIDAGINIS HERBA (EXTRACT)"
* #900000001010 ^designation[0].language = #de-AT 
* #900000001010 ^designation[0].value = "SOLIDAGINIS HERBA (AUSZUG)" 
* #900000001045 "TARAXACI FOLIUM"
* #900000001047 "TARAXACI HERBA"
* #900000001048 "TARAXACI RADIX (EXTRACT)"
* #900000001048 ^designation[0].language = #de-AT 
* #900000001048 ^designation[0].value = "TARAXACI RADIX (AUSZUG)" 
* #900000001051 "TERLIPRESSIN ACETATE"
* #900000001051 ^designation[0].language = #de-AT 
* #900000001051 ^designation[0].value = "TERLIPRESSIN DIACETAT" 
* #900000001063 "THYMI HERBA (EXTRACT)"
* #900000001063 ^designation[0].language = #de-AT 
* #900000001063 ^designation[0].value = "THYMI HERBA (AUSZUG)" 
* #900000001067 "TILIAE FLOS (EXTRACT)"
* #900000001067 ^designation[0].language = #de-AT 
* #900000001067 ^designation[0].value = "TILIAE FLOS (AUSZUG)" 
* #900000001070 "TORMENTILLAE RHIZOMA (EXTRACT)"
* #900000001070 ^designation[0].language = #de-AT 
* #900000001070 ^designation[0].value = "TORMENTILLAE RHIZOMA (AUSZUG)" 
* #900000001081 "TUROCTOCOG ALFA"
* #900000001081 ^designation[0].language = #de-AT 
* #900000001081 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT (TUROCTOCOG ALFA)" 
* #900000001086 "URTICAE RADIX (EXTRACT)"
* #900000001086 ^designation[0].language = #de-AT 
* #900000001086 ^designation[0].value = "URTICAE RADIX (AUSZUG)" 
* #900000001092 "VALERIANAE RADIX (EXTRACT)"
* #900000001092 ^designation[0].language = #de-AT 
* #900000001092 ^designation[0].value = "VALERIANAE RADIX (AUSZUG)" 
* #900000001096 "VARICELLA VIRUS OKA STRAIN (LIVE, ATTENUATED)"
* #900000001096 ^designation[0].language = #de-AT 
* #900000001096 ^designation[0].value = "VARIZELLA VIRUS" 
* #900000001098 "VERBASCI FLOS (EXTRACT)"
* #900000001098 ^designation[0].language = #de-AT 
* #900000001098 ^designation[0].value = "VERBASCI FLOS (AUSZUG)" 
* #900000001099 "VERBASCI FOLIUM"
* #900000001119 "VISCI ALBI HERBA"
* #900000001120 "VISCI ALBI HERBA (EXTRACT)"
* #900000001120 ^designation[0].language = #de-AT 
* #900000001120 ^designation[0].value = "VISCI ALBI HERBA (AUSZUG)" 
* #900000001122 "VITIS VINIFERAE FOLIUM"
* #900000001123 "VITIS VINIFERAE FOLIUM (EXTRACT)"
* #900000001123 ^designation[0].language = #de-AT 
* #900000001123 ^designation[0].value = "VITIS VINIFERAE FOLIUM (AUSZUG)" 
* #900000001129 "ZEDOARIAE RHIZOMA (AUSZUG)"
* #900000001139 "ZUCLOPENTHIXOL DIHYDROCHLORIDE"
* #900000001139 ^designation[0].language = #de-AT 
* #900000001139 ^designation[0].value = "ZUCLOPENTHIXOL DIHYDROCHLORID" 
* #900000001506 "ELECTUARIUM THERIACA (AUSZUG)"
* #900000002047 "PLASMA-PROTEINE"
* #900000002102 "RADIUM BROMATUM (KOMM D)"
* #900000002160 "SCHIEFERÖLE"
* #900000002379 "CAMPHORAE SPIRITUOSA SOLUTIO"
* #900000002379 ^designation[0].language = #de-AT 
* #900000002379 ^designation[0].value = "ALKOHOLISCHE KAMPFERLÖSUNG" 
* #900000004013 "AGNI CASTI FRUCTUS (AUSZUG)"
* #900000004017 "ALLII CEPAE BULBUS (EXTRACT)"
* #900000004017 ^designation[0].language = #de-AT 
* #900000004017 ^designation[0].value = "ALLII CEPAE BULBUS (AUSZUG)" 
* #900000004044 "BAPTISIAE TINCTORIAE RADIX (EXTRACT)"
* #900000004044 ^designation[0].language = #de-AT 
* #900000004044 ^designation[0].value = "BAPTISIAE TINCTORIAE RADIX (AUSZUG)" 
* #900000004050 "BETULAE FOLIUM (EXTRACT)"
* #900000004050 ^designation[0].language = #de-AT 
* #900000004050 ^designation[0].value = "BETULAE FOLIUM (AUSZUG)" 
* #900000004059 "CAMELLIAE SINENSIS NON FERMENTATUM FOLIUM (EXTRACT)"
* #900000004059 ^designation[0].language = #de-AT 
* #900000004059 ^designation[0].value = "CAMELLIAE SINENSIS NON FERMENTATUM FOLIUM (AUSZUG)" 
* #900000004060 "CAPSICI FRUCTUS (EXTRACT)"
* #900000004060 ^designation[0].language = #de-AT 
* #900000004060 ^designation[0].value = "CAPSICI FRUCTUS (AUSZUG)" 
* #900000004070 "CHELIDONII HERBA (EXTRACT)"
* #900000004070 ^designation[0].language = #de-AT 
* #900000004070 ^designation[0].value = "CHELIDONII HERBA (AUSZUG)" 
* #900000004076 "CINCHONAE CORTEX (EXTRACT)"
* #900000004076 ^designation[0].language = #de-AT 
* #900000004076 ^designation[0].value = "CINCHONAE CORTEX (AUSZUG)" 
* #900000004087 "CRATAEGI FOLIUM CUM FLORE (EXTRACT)"
* #900000004087 ^designation[0].language = #de-AT 
* #900000004087 ^designation[0].value = "CRATAEGI FOLIUM CUM FLORE (AUSZUG)" 
* #900000004104 "DULCAMARAE STIPITES (EXTRACT)"
* #900000004104 ^designation[0].language = #de-AT 
* #900000004104 ^designation[0].value = "DULCAMARAE STIPITES (AUSZUG)" 
* #900000004108 "ECHINACEAE ANGUSTIFOLIAE RADIX (EXTRACT)"
* #900000004108 ^designation[0].language = #de-AT 
* #900000004108 ^designation[0].value = "ECHINACEAE ANGUSTIFOLIAE RADIX (AUSZUG)" 
* #900000004109 "ECHINACEAE PURPUREAE HERBA (EXTRACT)"
* #900000004109 ^designation[0].language = #de-AT 
* #900000004109 ^designation[0].value = "ECHINACEAE PURPUREAE HERBA (AUSZUG)" 
* #900000004118 "FRAXINI CORTEX (EXTRACT)"
* #900000004118 ^designation[0].language = #de-AT 
* #900000004118 ^designation[0].value = "FRAXINI CORTEX (AUSZUG)" 
* #900000004137 "HARPAGOPHYTI RADIX (EXTRACT)"
* #900000004137 ^designation[0].language = #de-AT 
* #900000004137 ^designation[0].value = "HARPAGOPHYTI RADIX (AUSZUG)" 
* #900000004140 "HEDERAE FOLIUM (EXTRACT)"
* #900000004140 ^designation[0].language = #de-AT 
* #900000004140 ^designation[0].value = "HEDERAE FOLIUM (AUSZUG)" 
* #900000004141 "HELENII RHIZOMA (EXTRACT)"
* #900000004141 ^designation[0].language = #de-AT 
* #900000004141 ^designation[0].value = "HELENII RHIZOMA (AUSZUG)" 
* #900000004143 "HIPPOCASTANI SEMEN (EXTRACT)"
* #900000004143 ^designation[0].language = #de-AT 
* #900000004143 ^designation[0].value = "HIPPOCASTANI SEMEN (AUSZUG)" 
* #900000004145 "HYOSCYAMI HERBA (EXTRACT)"
* #900000004145 ^designation[0].language = #de-AT 
* #900000004145 ^designation[0].value = "HYOSCYAMI HERBA (AUSZUG)" 
* #900000004150 "IPECACUANHAE RADIX (EXTRACT)"
* #900000004150 ^designation[0].language = #de-AT 
* #900000004150 ^designation[0].value = "IPECACUANHAE RADIX (AUSZUG)" 
* #900000004166 "LUPULI FLOS (EXTRACT)"
* #900000004166 ^designation[0].language = #de-AT 
* #900000004166 ^designation[0].value = "LUPULI FLOS (AUSZUG)" 
* #900000004178 "MILLEFOLII HERBA (EXTRACT)"
* #900000004178 ^designation[0].language = #de-AT 
* #900000004178 ^designation[0].value = "MILLEFOLII HERBA (AUSZUG)" 
* #900000004182 "ADRENAL GLAND EXTRACT"
* #900000004182 ^designation[0].language = #de-AT 
* #900000004182 ^designation[0].value = "NEBENNIERE(EXTRAKT)" 
* #900000004185 "ORTHOSIPHONIS FOLIUM (EXTRACT)"
* #900000004185 ^designation[0].language = #de-AT 
* #900000004185 ^designation[0].value = "ORTHOSIPHONIS FOLIUM (AUSZUG)" 
* #900000004186 "ALPHA-KETOGLUTARIC ACID"
* #900000004186 ^designation[0].language = #de-AT 
* #900000004186 ^designation[0].value = "OXOGLURSÄURE" 
* #900000004191 "PASSIFLORAE HERBA (EXTRACT)"
* #900000004191 ^designation[0].language = #de-AT 
* #900000004191 ^designation[0].value = "PASSIFLORAE HERBA (AUSZUG)" 
* #900000004200 "PIPERIS NIGRI FRUCTUS (AUSZUG)"
* #900000004203 "PLANTAGINIS LANCEOLATAE FOLIUM (EXTRACT)"
* #900000004203 ^designation[0].language = #de-AT 
* #900000004203 ^designation[0].value = "PLANTAGINIS LANCEOLATAE FOLIUM (AUSZUG)" 
* #900000004206 "POPULI TREMULAE CORTEX ET FOLIUM (EXTRACT)"
* #900000004206 ^designation[0].language = #de-AT 
* #900000004206 ^designation[0].value = "POPULI TREMULAE CORTEX ET FOLIUM (AUSZUG)" 
* #900000004218 "RATANHIAE RADIX (EXTRACT)"
* #900000004218 ^designation[0].language = #de-AT 
* #900000004218 ^designation[0].value = "RATANHIAE RADIX (AUSZUG)" 
* #900000004221 "RHEI RADIX (EXTRACT)"
* #900000004221 ^designation[0].language = #de-AT 
* #900000004221 ^designation[0].value = "RHEI RADIX (AUSZUG)" 
* #900000004223 "RHOIS AROMATICAE RADICIS CORTEX (EXTRACT)"
* #900000004223 ^designation[0].language = #de-AT 
* #900000004223 ^designation[0].value = "RHOIS AROMATICAE RADICIS CORTEX (AUSZUG)" 
* #900000004228 "SALMONELLA SPEC."
* #900000004230 "SALVIAE OFFICINALIS FOLIUM (EXTRACT)"
* #900000004230 ^designation[0].language = #de-AT 
* #900000004230 ^designation[0].value = "SALVIAE OFFICINALIS FOLIUM (AUSZUG)" 
* #900000004235 "SENNAE FOLIUM (EXTRACT)"
* #900000004235 ^designation[0].language = #de-AT 
* #900000004235 ^designation[0].value = "SENNAE FOLIUM (AUSZUG)" 
* #900000004238 "SENNAE FRUCTUS ANGUSTIFOLIAE"
* #900000004239 "SENNAE FRUCTUS ANGUSTIFOLIAE (EXTRACT)"
* #900000004239 ^designation[0].language = #de-AT 
* #900000004239 ^designation[0].value = "SENNAE FRUCTUS ANGUSTIFOLIAE (AUSZUG)" 
* #900000004241 "SERPYLLI HERBA (EXTRACT)"
* #900000004241 ^designation[0].language = #de-AT 
* #900000004241 ^designation[0].value = "SERPYLLI HERBA (AUSZUG)" 
* #900000004246 "SOLIDAGINIS VIRGAUREAE HERBA (EXTRACT)"
* #900000004246 ^designation[0].language = #de-AT 
* #900000004246 ^designation[0].value = "SOLIDAGINIS VIRGAUREAE HERBA (AUSZUG)" 
* #900000004248 "STAPHYLOCOCCUS SPEC."
* #900000004251 "STREPTOCOCCUS SSP."
* #900000004253 "SYMPHYTI HERBA (EXTRACT)"
* #900000004253 ^designation[0].language = #de-AT 
* #900000004253 ^designation[0].value = "SYMPHYTI HERBA (AUSZUG)" 
* #900000004254 "SYMPHYTI RADIX (EXTRACT)"
* #900000004254 ^designation[0].language = #de-AT 
* #900000004254 ^designation[0].value = "SYMPHYTI RADIX (AUSZUG)" 
* #900000004257 "TARAXACI HERBA (EXTRACT)"
* #900000004257 ^designation[0].language = #de-AT 
* #900000004257 ^designation[0].value = "TARAXACI HERBA (AUSZUG)" 
* #900000004258 "TARAXACI RADIX ET HERBA (EXTRACT)"
* #900000004258 ^designation[0].language = #de-AT 
* #900000004258 ^designation[0].value = "TARAXACI RADIX ET HERBA (AUSZUG)" 
* #900000004261 "TEREBINTHINA LARICINA"
* #900000004266 "THUJAE HERBA (EXTRACT)"
* #900000004266 ^designation[0].language = #de-AT 
* #900000004266 ^designation[0].value = "THUJAE HERBA (AUSZUG)" 
* #900000004278 "VERBENAE HERBA (EXTRACT)"
* #900000004278 ^designation[0].language = #de-AT 
* #900000004278 ^designation[0].value = "VERBENAE HERBA (AUSZUG)" 
* #900000004284 "AETHEROLEUM MENTHAE"
* #900000004286 "SENNAE FRUCTUS"
* #900000004287 "SENNAE FRUCTUS (AUSZUG)"
* #900000004290 "STREPTOCOCCUS SPEC./ENTEROCOCCUS"
* #900000004291 "STREPTOCOCCUS SPEC. /ENTEROCOCCUS (AUSZUG, PRODUKTE)"
* #900000004292 "HAEMOPHILUS SPEC. (AUSZUG, PRODUKTE)"
* #900000004294 "IMMUNSERUM"
* #900000004317 "DAMIANAE FOLIUM (AUSZUG)"
* #900000004326 "UVAE URSI FOLIUM (AUSZUG)"
* #900000004367 "OPIUM (AUSZUG)"
* #900000004376 "ELEUTHEROCOCCI RADIX (AUSZUG)"
* #900000004380 "IRIDIS RHIZOMA (AUSZUG)"
* #900000004401 "COFFEAE SEMEN"
* #900000004411 "ECHINACEAE PURPUREAE HERBA ET RADIX (AUSZUG)"
* #900000004417 "POLLENEXTRAKT"
* #900000004423 "HERBA PULSATILLAE CUM RADICE (AUSZUG)"
* #900000004454 "MIXED EXTRACT"
* #900000004454 ^designation[0].language = #de-AT 
* #900000004454 ^designation[0].value = "MISCHEXTRAKT" 
* #900000004455 "CYNARAE FOLIUM (EXTRACT)"
* #900000004455 ^designation[0].language = #de-AT 
* #900000004455 ^designation[0].value = "CYNARAE FOLIUM (AUSZUG)" 
* #900000004460 "TOTAL PROTEIN"
* #900000004460 ^designation[0].language = #de-AT 
* #900000004460 ^designation[0].value = "GESAMTPROTEIN" 
* #900000004465 "ZIPRASIDONE HYDROGEN SULFATE DIHYDRATE"
* #900000004465 ^designation[0].language = #de-AT 
* #900000004465 ^designation[0].value = "ZIPRASIDONHYDROGENSULFAT DIHYDRAT" 
* #900000004472 "APROTININ ACETATE"
* #900000004472 ^designation[0].language = #de-AT 
* #900000004472 ^designation[0].value = "APROTININ ACETAT" 
* #900000004474 "ESOMEPRAZOLE MAGNESIUM DIHYDRATE"
* #900000004474 ^designation[0].language = #de-AT 
* #900000004474 ^designation[0].value = "ESOMEPRAZOL MAGNESIUM DIHYDRAT" 
* #900000004475 "D-GLUCOSE 1-PHOSPHATE DISODIUM SALT TETRAHYDRATE"
* #900000004475 ^designation[0].language = #de-AT 
* #900000004475 ^designation[0].value = "D-GLUCOSE 1-PHOSPHAT DINATRIUMSALZ TETRAHYDRAT" 
* #900000004482 "LYSINE L HYDRATE"
* #900000004482 ^designation[0].language = #de-AT 
* #900000004482 ^designation[0].value = "LYSIN L HYDRAT" 
* #900000004484 "WOOD CHARCOAL"
* #900000004484 ^designation[0].language = #de-AT 
* #900000004484 ^designation[0].value = "HOLZKOHLE" 
* #900000004490 "TETRAAZACYCLODODECANE-TETRAACETICACID"
* #900000004490 ^designation[0].language = #de-AT 
* #900000004490 ^designation[0].value = "TETRAAZACYCLODODECANTETRAESSIGSÄURE" 
* #900000004518 "BENDAMUSTIN HYDROCHLORIDE MONOHYDRATE"
* #900000004518 ^designation[0].language = #de-AT 
* #900000004518 ^designation[0].value = "BENDAMUSTIN HYDROCHLORID MONOHYDRAT" 
* #900000004560 "ACLIDINIUM BROMIDE"
* #900000004560 ^designation[0].language = #de-AT 
* #900000004560 ^designation[0].value = "ACLIDINIUM BROMID" 
* #900000004569 "IRINOTECAN HYDROCHLORIDE TRIHYDRATE"
* #900000004569 ^designation[0].language = #de-AT 
* #900000004569 ^designation[0].value = "IRINOTECAN HYDROCHLORID TRIHYDRAT" 
* #900000004570 "FLUCLOXACILLIN SODIUM MONOHYDRATE"
* #900000004570 ^designation[0].language = #de-AT 
* #900000004570 ^designation[0].value = "NATRIUM FLUCLOXACILLINAT MONOHYDRAT" 
* #900000004583 "AESCIN WATER SOLUBLE (ALPHA)"
* #900000004583 ^designation[0].language = #de-AT 
* #900000004583 ^designation[0].value = "AESCIN WASSERLÖSLICH (ALPHA)" 
* #900000004599 "CANGRELOR TETRASODIUM"
* #900000004599 ^designation[0].language = #de-AT 
* #900000004599 ^designation[0].value = "CANGRELOR TETRANATRIUM" 
* #900000004605 "ZOLEDRONIC ACID HEMIPENTAHYDRATE"
* #900000004605 ^designation[0].language = #de-AT 
* #900000004605 ^designation[0].value = "ZOLEDRONSÄURE HEMIPENTAHYDRAT" 
* #900000004609 "ZIPRASIDON MESILATE TRIHYDRATE"
* #900000004609 ^designation[0].language = #de-AT 
* #900000004609 ^designation[0].value = "ZIPRASIDON MESILAT TRIHYDRAT" 
* #900000004611 "ZINK ACETATE DIHYDRATE"
* #900000004611 ^designation[0].language = #de-AT 
* #900000004611 ^designation[0].value = "ZINKDIACETAT DIHYDRAT" 
* #900000004613 "MOMETASONE FUROATE MONOHYDRATE"
* #900000004613 ^designation[0].language = #de-AT 
* #900000004613 ^designation[0].value = "MOMETASON FUROAT MONOHYDRAT" 
* #900000004615 "IBUPROFEN SODIUM DIHYDRATE"
* #900000004615 ^designation[0].language = #de-AT 
* #900000004615 ^designation[0].value = "IBUPROFEN NATRIUM DIHYDRAT" 
* #900000004618 "ONDANSETRON HYDROCHLORIDE DIHYDRATE"
* #900000004618 ^designation[0].language = #de-AT 
* #900000004618 ^designation[0].value = "ONDANSETRON HYDROCHLORID DIHYDRAT" 
* #900000004625 "DONEPEZIL HYDROCHLORIDE MONOHYDRATE"
* #900000004625 ^designation[0].language = #de-AT 
* #900000004625 ^designation[0].value = "DONEPEZIL HYDROCHLORID MONOHYDRAT" 
* #900000004627 "ZINC SULFATE HEPTAHYDRATE"
* #900000004627 ^designation[0].language = #de-AT 
* #900000004627 ^designation[0].value = "ZINKSULFAT-HEPTAHYDRAT" 
* #900000004631 "PIPERACILLIN MONOHYDRATE"
* #900000004631 ^designation[0].language = #de-AT 
* #900000004631 ^designation[0].value = "PIPERACILLIN MONOHYDRAT" 
* #900000004647 "FERROUS SULFATE, DRIED"
* #900000004647 ^designation[0].language = #de-AT 
* #900000004647 ^designation[0].value = "EISEN(II)-SULFAT, GETROCKNETES" 
* #900000004648 "POTASSIUM CITRATE MONOHYDRATE"
* #900000004648 ^designation[0].language = #de-AT 
* #900000004648 ^designation[0].value = "KALIUMCITRAT MONOHYDRAT" 
* #900000004663 "NALOXEGOL OXALATE"
* #900000004663 ^designation[0].language = #de-AT 
* #900000004663 ^designation[0].value = "NALOXEGOL OXALAT" 
* #900000004664 "FLUTEMETAMOL (18F)"
* #900000004665 "PONATINIB"
* #900000004666 "ENZALUTAMIDE"
* #900000004666 ^designation[0].language = #de-AT 
* #900000004666 ^designation[0].value = "ENZALUTAMID" 
* #900000004669 "DISODIUM CLODRONATE TETRAHYDRATE"
* #900000004669 ^designation[0].language = #de-AT 
* #900000004669 ^designation[0].value = "DINATRIUM CLODRONAT TETRAHYDRAT" 
* #900000004671 "AVANAFIL"
* #900000004672 "ZIPRASIDON HYDROCHLORIDE ANHYDROUS"
* #900000004672 ^designation[0].language = #de-AT 
* #900000004672 ^designation[0].value = "ZIPRASIDONHYDROCHLORID WASSERFREI" 
* #900000004674 "CANAGLIFLOZIN"
* #900000004675 "DOLUTEGRAVIR"
* #900000004676 "DOLUTEGRAVIR SODIUM"
* #900000004676 ^designation[0].language = #de-AT 
* #900000004676 ^designation[0].value = "DOLUTEGRAVIR NATRIUM" 
* #900000004677 "BEDAQUILINE FUMARATE"
* #900000004677 ^designation[0].language = #de-AT 
* #900000004677 ^designation[0].value = "BEDAQUILIN FUMARAT" 
* #900000004678 "VILANTEROL TRIFENATATE"
* #900000004678 ^designation[0].language = #de-AT 
* #900000004678 ^designation[0].value = "VILANTEROL TRIFENATAT" 
* #900000004679 "UMECLIDINIUM BROMIDE"
* #900000004679 ^designation[0].language = #de-AT 
* #900000004679 ^designation[0].value = "UMECLIDINIUMBROMID" 
* #900000004680 "SOFOSBUVIR"
* #900000004681 "DAPAGLIFLOZIN PROPANEDIOL MONOHYDRATE"
* #900000004681 ^designation[0].language = #de-AT 
* #900000004681 ^designation[0].value = "DAPAGLIFLOZIN PROPANDIOL MONOHYDRAT" 
* #900000004682 "TENOFOVIR DISOPROXIL PHOSPHATE"
* #900000004682 ^designation[0].language = #de-AT 
* #900000004682 ^designation[0].value = "TENOFOVIR DISOPROXIL PHOSPHAT" 
* #900000004683 "COBICISTAT"
* #900000004684 "REGORAFENIB"
* #900000004685 "ELVITEGRAVIR"
* #900000004689 "COAGULATION FACTOR IX, RECOMBINANT"
* #900000004689 ^designation[0].language = #de-AT 
* #900000004689 ^designation[0].value = "GERINNUNGSFAKTOR IX, REKOMBINANT" 
* #900000004690 "COAGULATION FACTOR VIII, RECOMBINANT"
* #900000004690 ^designation[0].language = #de-AT 
* #900000004690 ^designation[0].value = "GERINNUNGSFAKTOR VIII, REKOMBINANT" 
* #900000004691 "INSULIN DEGLUDEC"
* #900000004693 "CALCIUM ASCORBATE DIHYDRATE"
* #900000004693 ^designation[0].language = #de-AT 
* #900000004693 ^designation[0].value = "CALCIUMASCORBAT DIHYDRAT" 
* #900000004694 "COPPERSULFATE ANHYDROUS"
* #900000004694 ^designation[0].language = #de-AT 
* #900000004694 ^designation[0].value = "KUPFERSULFAT WASSERFREI" 
* #900000004695 "MAGNESIUMHYDROGENPHOSPHATE TRIHYDRATE"
* #900000004695 ^designation[0].language = #de-AT 
* #900000004695 ^designation[0].value = "MAGNESIUMHYDROGENPHOSPHAT TRIHYDRAT" 
* #900000004704 "VORTIOXETINE HYDROBROMID"
* #900000004704 ^designation[0].language = #de-AT 
* #900000004704 ^designation[0].value = "VORTIOXETINHYDROBROMID" 
* #900000004707 "CABOZANTINIB MALATE"
* #900000004707 ^designation[0].language = #de-AT 
* #900000004707 ^designation[0].value = "CABOZANTINIBMALAT" 
* #900000004708 "DELAMANID"
* #900000004709 "AZITHROMYCIN MONOHYDRATE"
* #900000004709 ^designation[0].language = #de-AT 
* #900000004709 ^designation[0].value = "AZITHROMYCIN MONOHYDRAT" 
* #900000004715 "N-ACETYLGALACTOSAMINE-6-SULFATASE, REC."
* #900000004715 ^designation[0].language = #de-AT 
* #900000004715 ^designation[0].value = "N-ACETYLGALACTOSAMIN-6-SULFATASE, REKOMBINANT" 
* #900000004716 "DABRAFENIB MESILATE"
* #900000004716 ^designation[0].language = #de-AT 
* #900000004716 ^designation[0].value = "DABRAFENIBMESILAT" 
* #900000004717 "NALMEFENE HYDROCHLORIDE DIHYDRATE"
* #900000004717 ^designation[0].language = #de-AT 
* #900000004717 ^designation[0].value = "NALMEFENHYDROCHLORID DIHYDRAT" 
* #900000004718 "PASIREOTIDE DIASPARTATE"
* #900000004718 ^designation[0].language = #de-AT 
* #900000004718 ^designation[0].value = "PASIREOTIDDIASPARTAT" 
* #900000004722 "BOSUTINIB MONOHYDRATE"
* #900000004722 ^designation[0].language = #de-AT 
* #900000004722 ^designation[0].value = "BOSUTINIB MONOHYDRAT" 
* #900000004723 "AZILSARTAN MEDOXOMIL POTASSIUM"
* #900000004723 ^designation[0].language = #de-AT 
* #900000004723 ^designation[0].value = "AZILSARTANMEDOXOMIL KALIUM" 
* #900000004724 "LOMITAPIDE MESYLATE"
* #900000004724 ^designation[0].language = #de-AT 
* #900000004724 ^designation[0].value = "LOMITAPIDMESILAT" 
* #900000004725 "DACLATASVIR DIHYDROCHLORIDE"
* #900000004725 ^designation[0].language = #de-AT 
* #900000004725 ^designation[0].value = "DACLATASVIR DIHYDROCHLORID" 
* #900000004726 "AFATINIB DIMALEATE"
* #900000004726 ^designation[0].language = #de-AT 
* #900000004726 ^designation[0].value = "AFATINIBDIMALEAT" 
* #900000004727 "MACITENTAN"
* #900000004728 "SIMEPREVIR SODIUM"
* #900000004728 ^designation[0].language = #de-AT 
* #900000004728 ^designation[0].value = "SIMEPREVIR NATRIUM" 
* #900000004729 "TRAMETINIB DIMETHYL SULFOXIDE"
* #900000004729 ^designation[0].language = #de-AT 
* #900000004729 ^designation[0].value = "TRAMETINIB-DIMETHYLSULFOXID (1:1)" 
* #900000004730 "EMPAGLIFLOZIN"
* #900000004731 "ATALUREN"
* #900000004734 "LIPOPROTEIN LIPASE"
* #900000004734 ^designation[0].language = #de-AT 
* #900000004734 ^designation[0].value = "LIPOPROTEINLIPASE" 
* #900000004747 "PEGINTERFERON BETA-1A"
* #900000004750 "IDELALISIB"
* #900000004752 "PEMETREXED DISODIUM HEMIPENTAHYDRATE"
* #900000004752 ^designation[0].language = #de-AT 
* #900000004752 ^designation[0].value = "PEMETREXED DINATRIUM HEMIPENTAHYDRAT" 
* #900000004754 "POTASSIUM HYDROGEN ASPARTATE HEMIHYDRATE"
* #900000004754 ^designation[0].language = #de-AT 
* #900000004754 ^designation[0].value = "KALIUMHYDROGENASPARTAT HEMIHYDRAT" 
* #900000004758 "NETUPITANT"
* #900000004759 "SODIUM SELENITE ANHYDROUS"
* #900000004759 ^designation[0].language = #de-AT 
* #900000004759 ^designation[0].value = "DINATRIUMSELENIT WASSERFREI" 
* #900000004761 "LUMACAFTOR"
* #900000004764 "ABACAVIR HYDROCHLORIDE"
* #900000004764 ^designation[0].language = #de-AT 
* #900000004764 ^designation[0].value = "ABACAVIRHYDROCHLORID" 
* #900000004765 "LEDIPASVIR"
* #900000004766 "NINTEDANIB ESYLATE"
* #900000004766 ^designation[0].language = #de-AT 
* #900000004766 ^designation[0].value = "NINTEDANIB ESILAT" 
* #900000004767 "TILMANOCEPT"
* #900000004768 "ANAGRELIDE HYDROCHLORIDE MONOHYDRATE"
* #900000004768 ^designation[0].language = #de-AT 
* #900000004768 ^designation[0].value = "ANAGRELIDHYDROCHLORID MONOHYDRAT" 
* #900000004770 "BIRCH BARK (EXTRACT)"
* #900000004770 ^designation[0].language = #de-AT 
* #900000004770 ^designation[0].value = "BETULAE CORTEX (AUSZUG)" 
* #900000004773 "ARGATROBAN MONOHYDRATE"
* #900000004773 ^designation[0].language = #de-AT 
* #900000004773 ^designation[0].value = "ARGATROBAN MONOHYDRAT" 
* #900000004774 "RASAGILINE HEMITARTRATE"
* #900000004774 ^designation[0].language = #de-AT 
* #900000004774 ^designation[0].value = "RASAGILIN HEMITARTRAT" 
* #900000004775 "SODIUM GLYCEROPHOSPHATE, HYDRATED"
* #900000004775 ^designation[0].language = #de-AT 
* #900000004775 ^designation[0].value = "NATRIUMGLYCEROPHOSPHAT, WASSERHALTIG" 
* #900000004783 "DERMATOPHAGOIDES PTERONYSSINUS (ALLERG.)"
* #900000004784 "DERMATOPHAGOIDES FARINAE (ALLERG.)"
* #900000004791 "ARACHIS HYPOGAEA (ALLERG.)"
* #900000004819 "IBRUTINIB"
* #900000004820 "DULAGLUTIDE"
* #900000004820 ^designation[0].language = #de-AT 
* #900000004820 ^designation[0].value = "DULAGLUTID" 
* #900000004821 "SITAGLIPTIN MALATE"
* #900000004821 ^designation[0].language = #de-AT 
* #900000004821 ^designation[0].value = "SITAGLIPTIN MALAT" 
* #900000004822 "TENOFOVIR DISOPROXIL SUCCINATE"
* #900000004822 ^designation[0].language = #de-AT 
* #900000004822 ^designation[0].value = "TENOFOVIR DISOPROXIL SUCCINAT" 
* #900000004823 "PITOLISANT HYDROCHLORIDE"
* #900000004823 ^designation[0].language = #de-AT 
* #900000004823 ^designation[0].value = "PITOLISANT HYDROCHLORID" 
* #900000004829 "ABACAVIR HYDROCHLORIDE MONOHYDRATE"
* #900000004829 ^designation[0].language = #de-AT 
* #900000004829 ^designation[0].value = "ABACAVIRHYDROCHLORID MONOHYDRAT" 
* #900000004834 "TEDIZOLID PHOSPHATE"
* #900000004834 ^designation[0].language = #de-AT 
* #900000004834 ^designation[0].value = "TEDIZOLIDPHOSPHAT" 
* #900000004872 "AFAMELANOTIDE"
* #900000004872 ^designation[0].language = #de-AT 
* #900000004872 ^designation[0].value = "AFAMELANOTID" 
* #900000004882 "DASABUVIR SODIUM MONOHYDRATE"
* #900000004882 ^designation[0].language = #de-AT 
* #900000004882 ^designation[0].value = "DASABUVIR NATRIUM MONOHYDRAT" 
* #900000004883 "PARITAPREVIR"
* #900000004884 "OMBITASVIR"
* #900000004902 "LENVATINIB MESYLATE"
* #900000004902 ^designation[0].language = #de-AT 
* #900000004902 ^designation[0].value = "LENVATINIB MESILAT" 
* #900000004904 "COCARBOXYLASE TETRAHYDRATE"
* #900000004904 ^designation[0].language = #de-AT 
* #900000004904 ^designation[0].value = "COCARBOXYLASE TETRAHYDRAT" 
* #900000004905 "NIVOLUMAB"
* #900000004906 "CERITINIB"
* #900000004908 "GLYCYL-L-GLUTAMINE MONOHYDRATE"
* #900000004908 ^designation[0].language = #de-AT 
* #900000004908 ^designation[0].value = "GLYCYL-L-GLUTAMIN MONOHYDRAT" 
* #900000004909 "GLYCYLTYROSIN DIHYDRATE"
* #900000004909 ^designation[0].language = #de-AT 
* #900000004909 ^designation[0].value = "GLYCYLTYROSIN DIHYDRAT" 
* #900000004910 "APREMILAST"
* #900000004915 "AZADIRACHTAE INDICAE FRUCTUS"
* #900000004917 "CALCIUM SULFATE HEMIHYDRATE"
* #900000004917 ^designation[0].language = #de-AT 
* #900000004917 ^designation[0].value = "CALCIUMSULFAT HEMIHYDRAT" 
* #900000004919 "ASFOTASE ALFA"
* #900000004921 "HUMAN PAPILLOMAVIRUS TYPE 31 L1 PROTEIN"
* #900000004921 ^designation[0].language = #de-AT 
* #900000004921 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 31 L1-PROTEIN" 
* #900000004922 "HUMAN PAPILLOMAVIRUS TYPE 33 L1 PROTEIN"
* #900000004922 ^designation[0].language = #de-AT 
* #900000004922 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 33 L1-PROTEIN" 
* #900000004923 "HUMAN PAPILLOMAVIRUS TYPE 45 L1 PROTEIN"
* #900000004923 ^designation[0].language = #de-AT 
* #900000004923 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 45 L1-PROTEIN" 
* #900000004924 "HUMAN PAPILLOMAVIRUS TYPE 52 L1 PROTEIN"
* #900000004924 ^designation[0].language = #de-AT 
* #900000004924 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 52 L1-PROTEIN" 
* #900000004925 "HUMAN PAPILLOMAVIRUS TYPE 58 L1 PROTEIN"
* #900000004925 ^designation[0].language = #de-AT 
* #900000004925 ^designation[0].value = "HUMANES PAPILLOMVIRUS-TYP 58 L1-PROTEIN" 
* #900000004926 "EVOLOCUMAB"
* #900000004927 "SONIDEGIB DIPHOSPHATE"
* #900000004927 ^designation[0].language = #de-AT 
* #900000004927 ^designation[0].value = "SONIDEGIBDIPHOSPHAT" 
* #900000004928 "SEBELIPASE ALFA"
* #900000004929 "PANOBINOSTAT LACTATE, ANHYDROUS"
* #900000004929 ^designation[0].language = #de-AT 
* #900000004929 ^designation[0].value = "PANOBINOSTATLACTAT, WASSERFREI" 
* #900000004930 "CORNEAL EPITHELIAL CELLS"
* #900000004930 ^designation[0].language = #de-AT 
* #900000004930 ^designation[0].value = "HORNHAUTEPITHELZELLEN" 
* #900000004931 "ELIGLUSTAT TARTRATE"
* #900000004931 ^designation[0].language = #de-AT 
* #900000004931 ^designation[0].value = "ELIGLUSTAT TARTRAT" 
* #900000004932 "EDOXABAN TOSYLATE"
* #900000004932 ^designation[0].language = #de-AT 
* #900000004932 ^designation[0].value = "EDOXABANTOSILAT" 
* #900000004936 "BETULA SPP. (ALLERG.)"
* #900000004937 "ETELCALCETIDE HYDROCHLORIDE"
* #900000004937 ^designation[0].language = #de-AT 
* #900000004937 ^designation[0].value = "ETELCALCETID HYDROCHLORID" 
* #900000004938 "CEFTOLOZANE SULFATE"
* #900000004938 ^designation[0].language = #de-AT 
* #900000004938 ^designation[0].value = "CEFTOLOZANSULFAT" 
* #900000004941 "SAFINAMIDE MESILATE"
* #900000004941 ^designation[0].language = #de-AT 
* #900000004941 ^designation[0].value = "SAFINAMID MESILAT" 
* #900000004942 "CEFTRIAXONE DISODIUM HEMIHEPTAHYDRATE"
* #900000004942 ^designation[0].language = #de-AT 
* #900000004942 ^designation[0].value = "CEFTRIAXON DINATRIUM HEMIHEPTAHYDRAT" 
* #900000004943 "ALIROCUMAB"
* #900000004944 "PEMBROLIZUMAB"
* #900000004949 "ISAVUCONAZONIUM SULFATE"
* #900000004949 ^designation[0].language = #de-AT 
* #900000004949 ^designation[0].value = "ISAVUCONAZONIUMSULFAT" 
* #900000004951 "AMBROSIA ARTEMISIIFOLIA (ALLERG.)"
* #900000004961 "MYRTI AETHEROLEUM"
* #900000004962 "IVABRADINE OXALATE"
* #900000004962 ^designation[0].language = #de-AT 
* #900000004962 ^designation[0].value = "IVABRADIN OXALAT" 
* #900000004966 "IDARUCIZUMAB"
* #900000004967 "BLINATUMOMAB"
* #900000004974 "VALACICLOVIR HYDROCHLORIDE, HYDRATED"
* #900000004974 ^designation[0].language = #de-AT 
* #900000004974 ^designation[0].value = "VALACICLOVIRHYDROCHLORID, WASSERHALTIGES" 
* #900000004975 "HERPES SIMPLEX VIRUS"
* #900000004980 "ALOES ARBORESCENTIS FOLIUM (EXTRACT)"
* #900000004980 ^designation[0].language = #de-AT 
* #900000004980 ^designation[0].value = "ALOES ARBORESCENTIS FOLIUM (AUSZUG)" 
* #900000004982 "FERRIC MALTOL"
* #900000004982 ^designation[0].language = #de-AT 
* #900000004982 ^designation[0].value = "EISEN(III)-MALTOL" 
* #900000004986 "FEBUXOSTAT HEMIHYDRATE"
* #900000004986 ^designation[0].language = #de-AT 
* #900000004986 ^designation[0].value = "FEBUXOSTAT HEMIHYDRAT" 
* #900000004988 "DENGUE VIRUS"
* #900000004988 ^designation[0].language = #de-AT 
* #900000004988 ^designation[0].value = "DENGUE-VIRUS" 
* #900000004991 "MIGALASTAT HYDROCHLORIDE"
* #900000004991 ^designation[0].language = #de-AT 
* #900000004991 ^designation[0].value = "MIGALASTAT HYDROCHLORID" 
* #900000004992 "TIPIRACIL HYDROCHLORIDE"
* #900000004992 ^designation[0].language = #de-AT 
* #900000004992 ^designation[0].value = "TIPIRACIL HYDROCHLORID" 
* #900000005004 "SECALE CERALE L. (EXTRACT)"
* #900000005004 ^designation[0].language = #de-AT 
* #900000005004 ^designation[0].value = "SECALE CERALE L. (AUSZUG)" 
* #900000005005 "PHLEUM PRATENSE L. (EXTRACT)"
* #900000005005 ^designation[0].language = #de-AT 
* #900000005005 ^designation[0].value = "PHLEUM PRATENSE L. (AUSZUG)" 
* #900000005006 "ZEA MAYS L. (EXTRACT)"
* #900000005006 ^designation[0].language = #de-AT 
* #900000005006 ^designation[0].value = "ZEA MAYS L. (AUSZUG)" 
* #900000005014 "PALBOCICLIB"
* #900000005016 "FOSAPREPITANT DIMEGLUMINE"
* #900000005016 ^designation[0].language = #de-AT 
* #900000005016 ^designation[0].value = "FOSAPREPITANT DIMEGLUMIN" 
* #900000005021 "TENOFOVIR ALAFENAMIDE FUMARATE"
* #900000005021 ^designation[0].language = #de-AT 
* #900000005021 ^designation[0].value = "TENOFOVIRALAFENAMID FUMARAT" 
* #900000005028 "BARICITINIB"
* #900000005033 "HYDROXYETHYL STARCH"
* #900000005033 ^designation[0].language = #de-AT 
* #900000005033 ^designation[0].value = "HYDROXYETHYLSTÄRKE" 
* #900000005042 "TIVOZANIB HYDROCHLORIDE MONOHYDRATE"
* #900000005042 ^designation[0].language = #de-AT 
* #900000005042 ^designation[0].value = "TIVOZANIB HYDROCHLORID MONOHYDRAT" 
* #900000005071 "VON WILLEBRAND FACTOR, RECOMBINANT, HUMAN"
* #900000005071 ^designation[0].language = #de-AT 
* #900000005071 ^designation[0].value = "VON WILLEBRAND FAKTOR, REKOMBINANT, HUMAN" 
* #900000005073 "LENALIDOMIDE HYDROCHLORIDE MONOHYDRATE"
* #900000005073 ^designation[0].language = #de-AT 
* #900000005073 ^designation[0].value = "LENALIDOMIDHYDROCHLORID MONOHYDRAT" 
* #900000005080 "VOXILAPREVIR"
* #900000005081 "BRODALUMAB"
* #900000005083 "APIS MELLIFERA (ALLERG.)"
* #900000005084 "VESPULA SSP. (ALLERG.)"
* #900000005092 "TELOTRISTAT ETIPRATE"
* #900000005092 ^designation[0].language = #de-AT 
* #900000005092 ^designation[0].value = "TELOTRISTATETIPRAT" 
* #900000005093 "DUPILUMAB"
* #900000005095 "BURSAE PASTORIS HERBA (EXTRACT)"
* #900000005095 ^designation[0].language = #de-AT 
* #900000005095 ^designation[0].value = "BURSAE PASTORIS HERBA (AUSZUG)" 
* #900000005104 "SODIUM ZIRCONIUM HYDROGEN CYCLOHEXASILICATE HYDRATE (3:2:1:1:x)"
* #900000005104 ^designation[0].language = #de-AT 
* #900000005104 ^designation[0].value = "NATRIUMZIRCONIUMHYDROGENCYCLOHEXASILICAT HYDRAT (3:2:1:1:x)" 
* #900000005105 "VARICELLA ZOSTERVIRUS (EXTRACT, PRODUCT)"
* #900000005105 ^designation[0].language = #de-AT 
* #900000005105 ^designation[0].value = "VARICELLA ZOSTERVIRUS (AUSZUG, PRODUKTE)" 
* #900000005106 "STEM CELLS"
* #900000005106 ^designation[0].language = #de-AT 
* #900000005106 ^designation[0].value = "STAMMZELLEN" 
* #900000005120 "CIPROFLOXACIN HYDROCHLORIDE HYDRATE"
* #900000005120 ^designation[0].language = #de-AT 
* #900000005120 ^designation[0].value = "CIPROFLOXACINHYDROCHLORID HYDRAT" 
* #900000005121 "VACCINIUM MACROCARPON AIT. FRUCTUS (EXTRACT)"
* #900000005121 ^designation[0].language = #de-AT 
* #900000005121 ^designation[0].value = "VACCINIUM MACROCARPON AIT. FRUCTUS (AUSZUG)" 
* #900000005123 "URTICAE FOLIUM (EXTRACT)"
* #900000005123 ^designation[0].language = #de-AT 
* #900000005123 ^designation[0].value = "URTICAE FOLIUM (AUSZUG)" 
* #900000005137 "ROMOSOZUMAB"
* #900000005182 "ERAVACYCLINE DIHYDROCHLORIDE"
* #900000005182 ^designation[0].language = #de-AT 
* #900000005182 ^designation[0].value = "ERAVACYCLIN DIHYDROCHLORID" 
* #900000005183 "NERATINITIB MALEATE"
* #900000005183 ^designation[0].language = #de-AT 
* #900000005183 ^designation[0].value = "NERATINITIBMALEAT" 
* #900000005193 "APALUTAMIDE"
* #900000005193 ^designation[0].language = #de-AT 
* #900000005193 ^designation[0].value = "APALUTAMID" 
* #900000005214 "TROPAEOLI HERBA"
* #900000005215 "ARMORACIAE RADIX"
* #900000005223 "NETARSUDIL DIMESILATE"
* #900000005223 ^designation[0].language = #de-AT 
* #900000005223 ^designation[0].value = "NETARSUDIL DIMESILAT" 
* #900000005224 "FEDRATINIB DIHYDROCHLORIDE MONOHYDRATE"
* #900000005224 ^designation[0].language = #de-AT 
* #900000005224 ^designation[0].value = "FEDRATINIB DIHYDROCHLORID MONOHYDRAT" 
* #900000005226 "SIPONIMOD HEMIFUMARATE"
* #900000005226 ^designation[0].language = #de-AT 
* #900000005226 ^designation[0].value = "SIPONIMOD HEMIFUMARAT" 
* #900000005227 "ESTETROL MONOHYDRATE"
* #900000005227 ^designation[0].language = #de-AT 
* #900000005227 ^designation[0].value = "ESTETROL MONOHYDRAT" 
* #900000005229 "ANGIOTENSIN II ACETATE"
* #900000005229 ^designation[0].language = #de-AT 
* #900000005229 ^designation[0].value = "ANGIOTENSIN-II-ACETAT" 
* #900000005475 "BULEVIRTIDE ACETATE"
* #900000005475 ^designation[0].language = #de-AT 
* #900000005475 ^designation[0].value = "BULEVIRTIDACETAT" 
* #900000005491 "ABROCITINIB"
* #900000005492 "ELEXACAFTOR"
* #900000005509 "TEPOTINIB HYDROCHLORIDE MONOHYDRATE"
* #900000005509 ^designation[0].language = #de-AT 
* #900000005509 ^designation[0].value = "TEPOTINIBHYDROCHLORID MONOHYDRAT" 
* #900000005512 "LUMASIRAN SODIUM"
* #900000005512 ^designation[0].language = #de-AT 
* #900000005512 ^designation[0].value = "LUMASIRAN-NATRIUM" 
* #900000005514 "CORONAVIRUS (EXTRACT, PRODUCTS)"
* #900000005514 ^designation[0].language = #de-AT 
* #900000005514 ^designation[0].value = "CORONAVIRUS (AUSZUG, PRODUKTE)" 
* #900000005527 "DESVENLAFAXINE BENZOATE"
* #900000005527 ^designation[0].language = #de-AT 
* #900000005527 ^designation[0].value = "DESVENLAFAXINBENZOAT" 
* #900000005528 "INCLISIRAN SODIUM"
* #900000005528 ^designation[0].language = #de-AT 
* #900000005528 ^designation[0].value = "INCLISIRAN-NATRIUM" 
* #900000005532 "GEFAPIXANT CITRATE"
* #900000005532 ^designation[0].language = #de-AT 
* #900000005532 ^designation[0].value = "GEFAPIXANTCITRAT" 
* #900000005550 "TUCATINIB HEMIETHANOLATE"
* #900000005550 ^designation[0].language = #de-AT 
* #900000005550 ^designation[0].value = "TUCATINIB HEMIETHANOL" 
* #900000005591 "FOSCARBIDOPA HYDRATE"
* #900000005591 ^designation[0].language = #de-AT 
* #900000005591 ^designation[0].value = "FOSCARBIDOPA HYDRAT" 
* #900000005593 "GADOBUTROL MONOHYDRATE"
* #900000005593 ^designation[0].language = #de-AT 
* #900000005593 ^designation[0].value = "GADOBUTROL MONOHYDRAT" 
* #900000005597 "EVINACUMAB"
* #900000005634 "CRISANTASPASE, RECOMBINANT"
* #900000005634 ^designation[0].language = #de-AT 
* #900000005634 ^designation[0].value = "CRISANTASPASE, REKOMBINANT" 
* #900000005636 "RITLECITINIB TOSYLATE"
* #900000005636 ^designation[0].language = #de-AT 
* #900000005636 ^designation[0].value = "RITLECITINIBTOSILAT" 
* #900000005639 "FAMTOZINAMERAN"
* #900000005640 "RESPIRATORY SYNCYTIAL VIRUS, RECOMBINANT (EXTRACT, PRODUCT)"
* #900000005640 ^designation[0].language = #de-AT 
* #900000005640 ^designation[0].value = "RESPIRATORISCHES SYNCYTIALVIRUS, REKOMBINANT (AUSZUG, PRODUKTE)" 
