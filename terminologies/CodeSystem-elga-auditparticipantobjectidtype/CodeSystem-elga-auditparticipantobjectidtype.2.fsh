Instance: elga-auditparticipantobjectidtype 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-auditparticipantobjectidtype" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-auditparticipantobjectidtype" 
* name = "elga-auditparticipantobjectidtype" 
* title = "ELGA_AuditParticipantObjectIdType" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "Liste der ELGA spezifischen Audit Participant Object ID Type Codes. Der Object ID Type Code beschreibt die Art eines Objekts, welches referenziert wird." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.155" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 6 
* #0 "Transaktions ID"
* #0 ^definition = Transaktions ID (Transaktionsklammer)
* #100 "Policy ID"
* #110 "KBS ID"
* #120 "Assertion ID"
* #130 "Vollmacht Referenz"
* #140 "GDA ID"
