Instance: lkf-ergaenzung 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "lkf-ergaenzung" 
* url = "https://termgit.elga.gv.at/CodeSystem/lkf-ergaenzung" 
* name = "lkf-ergaenzung" 
* title = "LKF_Ergaenzung" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "Ergänzung zu Diagnosesicherheit" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.211" 
* date = "2021-09-27" 
* count = 3 
* concept[0].code = #ART
* concept[0].display = "Diagnose-Art"
* concept[1].code = #STAT
* concept[1].display = "Diagnose-statAuf"
* concept[2].code = #TYP
* concept[2].display = "Diagnose-Typ"
