Instance: elga-medikationtherapieart 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-medikationtherapieart" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-medikationtherapieart" 
* name = "elga-medikationtherapieart" 
* title = "ELGA_MedikationTherapieArt" 
* status = #active 
* version = "1.1.0+20240325" 
* description = "**Description:** ELGA Valueset for type of therapy

**Beschreibung:** ELGA Valueset für Therapieart" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.30" 
* date = "2024-03-25" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:1.2.40.0.10.1.4.3.4.3.6"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/medikationtherapieart"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #EINZEL
* compose.include[0].concept[0].display = "Einzelverordnung"
* compose.include[0].concept[1].code = #NICHTEINZEL
* compose.include[0].concept[1].display = "Nicht-Einzelverordnung"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/medikationtherapieart"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #EINZEL
* expansion.contains[0].display = "Einzelverordnung"
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:1.2.40.0.10.1.4.3.4.3.6"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/medikationtherapieart"
* expansion.contains[1].version = "1.0.0+20230131"
* expansion.contains[1].code = #NICHTEINZEL
* expansion.contains[1].display = "Nicht-Einzelverordnung"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:1.2.40.0.10.1.4.3.4.3.6"
