Instance: ems-ergaenzungmeldepflichtigekrankheiten 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "ems-ergaenzungmeldepflichtigekrankheiten" 
* url = "https://termgit.elga.gv.at/CodeSystem/ems-ergaenzungmeldepflichtigekrankheiten" 
* name = "ems-ergaenzungmeldepflichtigekrankheiten" 
* title = "EMS_ErgaenzungMeldepflichtigeKrankheiten" 
* status = #active 
* content = #complete 
* version = "1.0.0+20230131" 
* description = "Ergänzungsliste der meldepflichtigen Krankheiten für das österreichische EMS" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.187" 
* date = "2020-01-01" 
* count = 5 
* #A81.9a "sonstige transmissible spongiforme Enzephalopathie"
* #J09a "A/H1N1-Virus (Neue Influenza A)"
* #J09b "A/H5N1-Virus (Vogelgrippe)"
* #J09c "A/H7N9-Virus"
* #J09d "MERS-CoV"
