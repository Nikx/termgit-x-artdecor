Instance: elga-proficiencylevelcode 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-proficiencylevelcode" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-proficiencylevelcode" 
* name = "elga-proficiencylevelcode" 
* title = "ELGA_ProficiencyLevelCode" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "**Description:** List of codes representing the level of proficiency in a language.

**Beschreibung:** Codes zur Angabe der Sprachfähigkeit von Patienten (Sprachkenntnisse)" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.174" 
* date = "2024-03-25" 
* publisher = "see" 
* contact[0].name = "http://www.hl7.org/documentcenter/public_temp_C0F0C5A1-1C23-BA17-0C878A83AC28B5A7/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityProficiency.html" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.hl7.org/documentcenter/public_temp_C0F0C5A1-1C23-BA17-0C878A83AC28B5A7/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/LanguageAbilityProficiency.html" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.61"
* compose.include[0].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityProficiency"
* compose.include[0].version = "2.1.0"
* compose.include[0].concept[0].code = #E
* compose.include[0].concept[0].display = "Excellent"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "ausgezeichnet" 
* compose.include[0].concept[1].code = #F
* compose.include[0].concept[1].display = "Fair"
* compose.include[0].concept[1].designation[0].language = #de-AT 
* compose.include[0].concept[1].designation[0].value = "ausreichend" 
* compose.include[0].concept[2].code = #G
* compose.include[0].concept[2].display = "Good"
* compose.include[0].concept[2].designation[0].language = #de-AT 
* compose.include[0].concept[2].designation[0].value = "gut" 
* compose.include[0].concept[3].code = #P
* compose.include[0].concept[3].display = "Poor"
* compose.include[0].concept[3].designation[0].language = #de-AT 
* compose.include[0].concept[3].designation[0].value = "mangelhaft" 

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityProficiency"
* expansion.contains[0].version = "2.1.0"
* expansion.contains[0].code = #E
* expansion.contains[0].display = "Excellent"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "ausgezeichnet" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.61"
* expansion.contains[1].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityProficiency"
* expansion.contains[1].version = "2.1.0"
* expansion.contains[1].code = #F
* expansion.contains[1].display = "Fair"
* expansion.contains[1].designation[0].language = #de-AT 
* expansion.contains[1].designation[0].value = "ausreichend" 
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.61"
* expansion.contains[2].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityProficiency"
* expansion.contains[2].version = "2.1.0"
* expansion.contains[2].code = #G
* expansion.contains[2].display = "Good"
* expansion.contains[2].designation[0].language = #de-AT 
* expansion.contains[2].designation[0].value = "gut" 
* expansion.contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.61"
* expansion.contains[3].system = "http://terminology.hl7.org/CodeSystem/v3-LanguageAbilityProficiency"
* expansion.contains[3].version = "2.1.0"
* expansion.contains[3].code = #P
* expansion.contains[3].display = "Poor"
* expansion.contains[3].designation[0].language = #de-AT 
* expansion.contains[3].designation[0].value = "mangelhaft" 
* expansion.contains[3].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[3].extension[0].valueOid = "urn:oid:2.16.840.1.113883.5.61"
