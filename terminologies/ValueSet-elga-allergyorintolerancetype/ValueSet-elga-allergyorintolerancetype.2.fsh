Instance: elga-allergyorintolerancetype 
InstanceOf: ValueSet 
Usage: #definition 
* id = "elga-allergyorintolerancetype" 
* meta.profile[0] = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-valueset" 
* url = "https://termgit.elga.gv.at/ValueSet/elga-allergyorintolerancetype" 
* name = "elga-allergyorintolerancetype" 
* title = "ELGA_AllergyOrIntoleranceType" 
* status = #active 
* version = "1.2.0+20240325" 
* description = "**Description:** Allergy Or Intolerance. Contains information protected by copyright of SNOMED International. Any use of SNOMED CT in Austria requires a valid affiliate license or sublicense. The corresponding license is free of charge, provided that the use only takes place in Austria and fulfills the conditions of the Affiliate License Agreement. Affiliate licenses can be requested directly from the respective NRC via the [Member Licensing and Distribution Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS).

**Beschreibung:** Art der beobachteten Intoleranz oder Allergie (Allergie oder Intoleranz). Enthält durch SNOMED International urheberrechtlich geschützte Information. Jede Verwendung von SNOMED CT in Österreich erfordert eine aufrechte Affiliate Lizenz oder eine Sublizenz. Die entsprechende Lizenz ist kostenlos, vorausgesetzt die Verwendung findet nur in Österreich statt und erfüllt die Bedingungen des Affiliate License Agreements. Affiliate Lizenzen können über das [Mitglieder-Lizenzierungs- und Distributions-Service](https://mlds.ihtsdotools.org/#/landing/AT?lang=de) (MLDS) direkt beim jeweiligen NRC beantragt werden." 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.10.177" 
* date = "2024-03-25" 
* compose.inactive = "True" 
* compose.include[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* compose.include[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* compose.include[0].version = "1.3.0+20230803"
* compose.include[0].concept[0].code = "418038007"
* compose.include[0].concept[0].display = "Überempfindlichkeit"
* compose.include[0].concept[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[0].designation[0].value = "DEPRECATED" 
* compose.include[0].concept[1].code = "420134006"
* compose.include[0].concept[1].display = "Überempfindlichkeit"
* compose.include[0].concept[2].code = "419199007"
* compose.include[0].concept[2].display = "Allergie"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[0].value = "DEPRECATED" 
* compose.include[0].concept[3].code = "609328004"
* compose.include[0].concept[3].display = "Allergie"
* compose.include[0].concept[4].code = "782197009"
* compose.include[0].concept[4].display = "Intoleranz"

* expansion.timestamp = "2024-03-25T06:36:54.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[0].version = "1.3.0+20230803"
* expansion.contains[0].inactive = true
* expansion.contains[0].code = #418038007
* expansion.contains[0].display = "Überempfindlichkeit"
* expansion.contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].designation[0].value = "DEPRECATED" 
* expansion.contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].version = "1.3.0+20230803"
* expansion.contains[1].code = #420134006
* expansion.contains[1].display = "Überempfindlichkeit"
* expansion.contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].contains[0].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].contains[0].version = "1.3.0+20230803"
* expansion.contains[1].contains[0].inactive = true
* expansion.contains[1].contains[0].code = #419199007
* expansion.contains[1].contains[0].display = "Allergie"
* expansion.contains[1].contains[0].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[1].contains[0].designation[0].value = "DEPRECATED" 
* expansion.contains[1].contains[0].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].contains[0].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].contains[1].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].contains[1].version = "1.3.0+20230803"
* expansion.contains[1].contains[1].code = #609328004
* expansion.contains[1].contains[1].display = "Allergie"
* expansion.contains[1].contains[1].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].contains[1].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
* expansion.contains[1].contains[2].system = "https://termgit.elga.gv.at/CodeSystem/snomed-ct-auszug"
* expansion.contains[1].contains[2].version = "1.3.0+20230803"
* expansion.contains[1].contains[2].code = #782197009
* expansion.contains[1].contains[2].display = "Intoleranz"
* expansion.contains[1].contains[2].extension[0].url = "http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-valueset-systemoid"
* expansion.contains[1].contains[2].extension[0].valueOid = "urn:oid:2.16.840.1.113883.6.96"
