Instance: elga-significantpathogens 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "elga-significantpathogens" 
* url = "https://termgit.elga.gv.at/CodeSystem/elga-significantpathogens" 
* name = "elga-significantpathogens" 
* title = "ELGA_SignificantPathogens" 
* status = #active 
* content = #complete 
* version = "1.0.1+20240129" 
* description = "**Description:** Set of significant pathogens listed in ''Epidemiologisches Meldesystem Benutzerhandbuch Fachlicher Teil'', 1. edition, October 2008, BMGFJ

**Beschreibung:** Erreger meldepflichtiger Infektionskrankheiten laut ''Epidemiologisches Meldesystem Benutzerhandbuch Fachlicher Teil'', 1. Auflage Oktober 2008, des BMGFJ" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.34.5.45" 
* date = "2024-01-29" 
* publisher = "see" 
* contact[0].name = "https://www.elga.gv.at" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "https://www.elga.gv.at" 
* count = 67 
* property[0].code = #hints 
* property[0].type = #string 
* property[1].code = #status 
* property[1].type = #code 
* #SP001 "Adenovirus im Konjunktivalabstrich (*)"
* #SP001 ^property[0].code = #hints 
* #SP001 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP002 "HIV"
* #SP003 "Bacillus anthracis"
* #SP004 "Influenza A/H5"
* #SP005 "Influenza A/H5N1"
* #SP006 "Clostridium botulinum"
* #SP007 "Brucella spp."
* #SP008 "Campylobacter spp."
* #SP009 "Chlamydia trachomatis"
* #SP010 "Vibrio cholerae"
* #SP011 "Denguevirus"
* #SP012 "Corynebacterium diphtheriae"
* #SP013 "Corynebacterium ulcerans"
* #SP014 "Ebolavirus"
* #SP014 ^property[0].code = #hints 
* #SP014 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP015 "Escherichia coli, sonstige darmpathogene Stämme"
* #SP015 ^property[0].code = #hints 
* #SP015 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP016 "Echinococcus spp."
* #SP017 "Rickettsia prowazekii"
* #SP017 ^property[0].code = #hints 
* #SP017 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP018 "FSME-Virus"
* #SP018 ^property[0].code = #hints 
* #SP018 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP019 "Gelbfieber-Virus"
* #SP020 "Giardia lamblia"
* #SP021 "Neisseria gonorrhoeae"
* #SP022 "Haemophilus influenzae"
* #SP023 "Hantavirus"
* #SP023 ^property[0].code = #hints 
* #SP023 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP024 "Hepatitis-A-Virus"
* #SP025 "Hepatitis-B-Virus"
* #SP026 "Hepatitis-C-Virus"
* #SP027 "HDV - Hepatitis-D-Virus akut"
* #SP027 ^property[0].code = #hints 
* #SP027 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP028 "HEV - akute Virushepatitis E"
* #SP028 ^property[0].code = #hints 
* #SP028 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP029 "Influenzavirus"
* #SP030 "Bordetella pertussis"
* #SP031 "Cryptosporidium spp."
* #SP032 "Lassavirus"
* #SP032 ^property[0].code = #hints 
* #SP032 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP033 "Borrelia recurrentis"
* #SP034 "Legionella spp."
* #SP035 "Mycobacterium leprae"
* #SP035 ^property[0].code = #hints 
* #SP035 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP036 "Leptospira interrogans"
* #SP037 "Listeria monocytogenes"
* #SP038 "Plasmodium spp."
* #SP039 "Marburgvirus"
* #SP039 ^property[0].code = #hints 
* #SP039 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP040 "Masernvirus"
* #SP041 "Neisseria meningitidis"
* #SP042 "Mumpsvirus"
* #SP043 "Norovirus"
* #SP043 ^property[0].code = #hints 
* #SP043 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP044 "Chlamydophila psittaci"
* #SP044 ^property[0].code = #hints 
* #SP044 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP045 "Yersinia pestis"
* #SP046 "Streptococcus pneumoniae"
* #SP047 "Variola-Virus"
* #SP048 "Poliovirus"
* #SP049 "Coxiella burnetii"
* #SP050 "Rotavirus"
* #SP050 ^property[0].code = #hints 
* #SP050 ^property[0].valueString = "vgl.Falldefinitionen des Robert-Koch-Institutes" 
* #SP051 "Rubella-Virus"
* #SP052 "DEPRECATED: Rubella-Virus"
* #SP052 ^property[0].code = #status 
* #SP052 ^property[0].valueCode = #retired 
* #SP052 ^property[1].code = #hints 
* #SP052 ^property[1].valueString = "Duplikat, daher auf Ungültig (DEPRECATED) gesetzt" 
* #SP053 "Salmonella spp. außer S. Typhi und S. Paratyphi"
* #SP054 "SARS-Coronavirus, SARS-CoV"
* #SP055 "Shigella spp."
* #SP056 "Treponema pallidum"
* #SP057 "Clostridium tetani"
* #SP058 "Lyssa-Virus"
* #SP059 "Toxoplasma gondii"
* #SP060 "Trichinella spp."
* #SP061 "Mycobacterium-tuberculosis-Komplex"
* #SP062 "Francisella tularensis"
* #SP063 "Salmonella typhi"
* #SP064 "Salmonella paratyphi"
* #SP065 "West-Nil-Virus"
* #SP066 "Yersinia enterocolitica"
* #SP067 "Yersinia pseudotuberculosis"
